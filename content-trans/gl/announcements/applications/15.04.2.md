---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE publica a versión 15.04.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.04.2 das aplicacións de KDE
version: 15.04.2
---
June 2, 2015. Today KDE released the second stability update for <a href='../15.04.0'>KDE Applications 15.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 30 correccións de erros inclúen melloras en, entre outros, Gwenview, Kate, Kdenlive, KDE PIM, Konsole, Marble, KGPG, Kig, KTP (interface de usuario de chamadas) e Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 and the Kontact Suite 4.14.9.
