---
aliases:
- ../announce-applications-15.08.3
changelog: true
date: 2015-11-10
description: KDE publica a versión 15.08.3 das aplicacións de KDE
layout: application
title: KDE publica a versión 15.08.3 das aplicacións de KDE
version: 15.08.3
---
November 10, 2015. Today KDE released the third stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 20 correccións de erros inclúen melloras en, entre outros, Ark, Dolphin, Kdenlive, KDE PIM, Kig, Lokalize e Umbrello.

This release also includes Long Term Support version of KDE Development Platform 4.14.14.
