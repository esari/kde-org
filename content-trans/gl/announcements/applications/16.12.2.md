---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE publica a versión 16.12.2 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.12.2 das aplicacións de KDE
version: 16.12.2
---
February 9, 2017. Today KDE released the second stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis de 20 correccións de erros inclúen melloras en, entre outros, KDE PIM, Dolphin, Kate, Kdenlive, KTouch e Okular.

This release also includes Long Term Support version of KDE Development Platform 4.14.29.
