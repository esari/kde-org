---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Cambios xerais

- Correccións de avisos de AUTOMOC con CMake ≥ 3.10
- Uso máis xeneralizado de rexistro con categoría, para desactivar a saída de depuración de maneira predeterminada (use kdebugsetting para activalo de novo)

### Baloo

- balooctl status: procesar todos os argumentos
- Corrixir varias consultas de etiqueta de palabras
- Simplificar as condicións de cambio de nome
- Corrixir un nome para mostrar incorrecto de UDSEntry

### Iconas de Breeze

- Corrixir o nome de icona, «weather-none» → «weather-none-available» (fallo 379875)
- retirar a icona de Vivaldi porque a icona orixinal da aplicación encaixa perfectamente con Breeze (fallo 383517)
- engadir algúns tipos MIME que faltaban
- Engadir a icona document-send a Breeze (fallo 388048)
- actualizar a icona de intérprete de álbum
- engadir compatibilidade con labplot-editlayout
- retirar duplicados e actualizar o tema escuro
- engadir unha icona de Breeze para Gnumeric

### Módulos adicionais de CMake

- Usar readelf para atopar as dependencias dos proxectos
- Introducir INSTALL_PREFIX_SCRIPT para preparar facilmente os prefixos

### KActivities

- evitar unha quebra en kactivities se non hai dispoñíbel ningunha conexión de D-Bus

### KConfig

- Documentación da API: explicar como usar KWindowConfig desde o construtor dun diálogo
- Marcar KDesktopFile::sortOrder() como obsoleto
- Corrixir o resultado de KDesktopFile::sortOrder()

### KCoreAddons

- Estender CMAKE_AUTOMOC_MACRO_NAMES tamén para a construción propia
- Atopar coincidencias entre claves de licenza e spdx

### KDBusAddons

- Corrixir a detección de ruta absoluta para CMake 3.5 en Linux
- Engadiuse a función de CMake «kdbusaddons_generate_dbus_service_file»

### KDeclarative

- Controis de QML para a creación de kcm

### KDED

- Usar a función de CMake «kdbusaddons_generate_dbus_service_file» desde kdbusaddons para xerar un ficheiro de servizo de D-Bus
- Engadir a propiedade used á definición de ficheiro de servizo

### Compatibilidade coa versión 4 de KDELibs

- Informar ao usuario se o modulo non se pode rexistrar con org.kde.kded5 e saír con erro
- Corrección de compilación de Mingw32

### KDocTools

- engadir unha entidade para Michael Pyne
- engadir entidades para Martin Koller a contributor.entities
- corrixir a entrada de Debian
- engadir a entidade Debian a general.entities
- engadir a entidade kbackup, importárase
- engadir a entidade latex, xa temos 7 entidades en index.docbook distintos en KF5

### KEmoticons

- Engadir o esquema (file://). É necesario cando o usamos en QML e engadímolos todos

### KFileMetaData

- retirar o extractor baseado en QtMultimedia
- Comprobar se se está en Linux en vez de se está TagLib e evitar construír usermetadatawritertest en Windows
- Restaurar # 6c9111a9 ata que se constrúa correctamente e se pode ligar sen TagLib
- Retirar a dependencia de taglib, causada por unha inclusión restante

### KHTML

- Permitir finalmente desactivar a saída de depuración usando rexistro con categoría

### KI18n

- non tratar ts-pmap-compile como executábel
- Corrixir unha fuga de memoria en KuitStaticData
- KI18n: corrixir varias buscas duplas

### KInit

- Retirar código inútil

### KIO

- Properly parse dates in cookies when running in non-English locale (bug 387254)
- [kcoredirlister] Corrixir a creación de ruta subordinada
- Reflectir o estado do lixo en iconNameForUrl
- Enviar os sinais de QComboBox en vez de os sinais de edición de liña de QComboBox
- Corrixiuse que os atallos web mostrasen a súa ruta de ficheiro en vez de o seu nome lexíbel para humanos
- TransferJob: corrixir cando readChannelFinished xa se emitiu antes de chamarse ao inicio (fallo 386246)
- Corrixir unha quebra que debe darse desde Qt 5.10? (fallo 386364)
- KUriFilter: non devolver un erro en caso de que non existan os ficheiros
- Corrixir a creación de rutas
- Crear un diálogo de KFile no que poder engadir un trebello personalizado
- Verificar que qWaitForWindowActive non falla
- KUriFilter: deixar de usar KServiceTypeTrader
- Documentación de API: usar nomes de clase en títulos de capturas de pantalla de exemplo
- Documentación da API: xestionar tamén macros de exportación KIOWIDGETS na creación de QCH
- corrixir a xestión de KCookieAdvice::AcceptForSession (fallo 386325)
- Creouse «GroupHiddenRole» para KPlacesModel
- redirixir cadeas de erros de sócket a KTcpSocket
- Facer cambios internos e retirar código duplicado de kfileplacesview
- Emitir o sinal «groupHiddenChanged»
- Reorganizando o código de uso de agochar e ocultar animacións dentro de KFilePlacesView
- Agora o usuario pode agochar un grupo enteiro de lugares desde KFilePlacesView (fallo 300247)
- Codificación de agochar grupos de lugares en KFilePlacesModel (fallo 300247)
- [KOpenWithDialog] Retirar unha creación redundante de KLineEdit
- Engadir a posibilidade de desfacer en BatchRenameJob
- Engadir BatchRenameJob a KIO
- Corrixir un bloque de código de Doxygen que non remataba con endcode

### Kirigami

- manter o escintileo interactivo
- prefixo axeitado tamén para as iconas
- corrixir o tamaño do formulario
- ler wheelScrollLines de kdeglobals se se está saíndo
- engadir un prefixo nos ficheiros de Kirigami para evitar conflitos
- algunhas correccións de ligazóns estáticas
- mover os estilos de Plasma a plasma-framework
- Usar comiñas simples para caracteres coincidentes + QLatin1Char
- FormLayout

### KJobWidgets

- Ofrecer a API de QWindow para os decoradores KJobWidgets::

### KNewStuff

- Limitar o tamaño da caché das solicitudes
- Requirir a mesma versión interna que se está construíndo
- Evitar que as variábeis globais se usen tras liberalas

### KNotification

- [KStatusNotifierItem] Non «restaurar» a posición do trebello ao mostralo por vez primeira
- Usar as posicións das iconas vellas da área de notificacións para as accións de minimizar e restaurar
- Xestionar a posición dos clics esquerdos en iconas obsoletas da área de notificacións
- non facer que o menú contextual sexa unha xanela
- Engadir un comentario explicativo
- Atrasar a instancia e carga dos complementos de KNotification ata que faga falla

### Infraestrutura KPackage

- invalidar a caché de tempo de execución ao instalar
- funcionalidade experimental de carga de ficheiros RCC en KPackage
- compilar sobre Qt 5.7
- Corrixir a indexación de paquetes e engadir caché en tempo de execución
- novo método KPackage::fileUrl()

### KRunner

- [RunnerManager] Non enredar co número de fíos de ThreadWeaver

### KTextEditor

- Corrixir a coincidencia de caracteres de substitución para liñas de modo
- Corrixir unha regresión causada polo cambio do comportamento da tecla de retroceso
- migrar a unha API non obsoleta como a que xa se usa noutro lugar (fallo 386823)
- Engadir unha inclusión que faltaba para std::array
- MessageInterface: Engadir CenterInView como posición adicional
- Limpeza da lista da preparación de QStringList

### Infraestrutura de KWallet

- Usar a ruta correcta de executábel de servizo para instalar o servizo de D-Bus de kwalletd en Win32

### KWayland

- Corrixir a inconsistencia de nomes
- Crear unha interface para pasar paletas de decoración de servidor
- Incluír explicitamente as funcións std::bind
- [servidor] Engadir o método IdleInterface::simulateUserActivity
- Corrixir unha regresión causada por compatibilidade con versións previas na orixe de datos
- Engadir compatibilidade coa versión 3 da interface do xestor de dispositivos de datos (fallo 386993)
- Restrinxir o ámbito dos obxectos exportados e importados á proba
- Corrixir un erro en WaylandSurface::testDisconnect
- Engadir un protocolo explícito de AppMenu
- Corrixir a funcionalidade de exclusión de ficheiros xerados por automoc

### KWidgetsAddons

- Corrixir unha quebra en setMainWindow en Wayland

### KXMLGUI

- Documentación da API: facer que Doxygen inclúa de novo os macros e funcións relacionadas con sesións
- Desconectar a rañura shortcutedit ao destruír o trebello (fallo 387307)

### NetworkManagerQt

- 802-11-x: compatibilidade co método PWD EAP

### Infraestrutura de Plasma

- [tema Air] Engadir un gráfico de progreso da barra de tarefas (fallo 368215)
- Modelos: retirar un * solto das cabeceiras de licenza
- facer que packageurlinterceptor faga o menos posíbel
- Reverter «Non apagar o renderizador e outros traballos en progreso cando se invoca Svg::setImagePath co mesmo argumento»
- mover aquí os estilos de Plasma de Kirigami
- permitir que as barras de desprazamento desaparezan en móbiles
- aproveitar a instancia de KPackage entre PluginLoader e Applet
- [AppletQuickItem] Só definir o estilo da versión 1 dos controis de QtQuick unha vez por motor
- Non definir unha icona de xanela en Plasma::Dialog
- [RTL] - aliñar correctamente o texto seleccionado en RTL (fallo 387415)
- Inicializar o factor de escala ao último definido en calquera instancia
- Reverter «Inicializar o factor de escala ao último definido en calquera instancia»
- Non actualizar cando o FrameSvg interno é repaint-blocked
- Inicializar o factor de escala ao último definido en calquera instancia
- Mover a comprobación «if» dentro de «#ifdef»
- [FrameSvgItem] Non crear nodos innecesarios
- Non apagar o renderizador e outros traballos en progreso cando se invoca Svg::setImagePath co mesmo argumento

### Prison

- Buscar tamén qrencode co sufixo debug

### QQC2StyleBridge

- simplificar e non intentar bloquear eventos de rato
- se non hai wheel.pixelDelta, usar as liñas de desprazamento de roda globais
- as barras de separadores de escritorio teñen anchuras distintas para cada separador
- asegurar un consello de tamaño que non sexa 0

### Sonnet

- Non exportar os executábeis de asistentes internos
- Sonnet: corrixir o idioma incorrecto para as suxestións nos textos que mesturan idiomas
- Retirar un apaño vello e roto
- Non causar ligazóns circulares en Windows

### Realce da sintaxe

- Indexador de realce: avisar sobre o cambio de contexto fallthroughContext="#stay"
- Indexados de salientado: avisar de atributos baleiros
- Indexados de salientado: activar os erros
- Indexador de realce: informar de elementos itemData que non se usen ou que falten
- Prolog, RelaxNG e RMarkDown: Corrixir problemas de salientado
- Haml: Corrixir casos de itemData incorrectos ou sen usar
- ObjectiveC++: Retirar contextos de comentario duplicados
- Diff, ObjectiveC++: Limpezas e corrixir ficheiros de realce
- XML (depuración): Corrixir unha regra DetectChar incorrecta
- Indexador de realce: permitir comprobacións de contexto entre realces
- Reverter: Engadir GNUMacros de novo a gcc.xml, que isocpp.xml usa
- email.xml: engadir *.email ás extensións
- Indexador de salientado: comprobar se hai ciclos sen fin
- Indexador de realce: comprobar se hai nomes de contexto baleiros e expresións regulares baleiras
- Corrixir a referencia de listas de claves non existentes
- Corrixir casos simples de contextos duplicados
- Corrixir itemDatas duplicado
- Corrixir as regras de DetectChar e Detect2Chars
- Indexador de salientado: comprobar as listas de palabras clave
- Indexados de salientado: avisar de contextos duplicados
- Indexador de salientado: comprobar se hai duplicados de itemData
- Indexador de realce: comprobar DetectChar e Detect2Chars
- Validalo para todos os atributos para os que exista un itemData

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
