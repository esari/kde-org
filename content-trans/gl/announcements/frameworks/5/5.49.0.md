---
aliases:
- ../../kde-frameworks-5.49.0
date: 2018-08-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Non crear unha instancia de QStringRef dentro dunha QString só para buscar nunha QStringList
- Definir os elementos cando se declaran

### Baloo

- [tags_kio] Corrixir varias copias de nome de ficheiro
- Reverter «[tags_kio] Usar UDS_URL en vez de UDS_TARGET_URL.»
- [tags_kio] Usar UDS_URL en vez de UDS_TARGET_URL
- [tags_kio] Consultar as rutas de ficheiro de destino en vez de engadir rutas á entrada UDS do ficheiro
- Permitir URL especiais para atopar ficheiros de tipos concretos
- Evitar a manipulación de listas con complexidade cadrada
- Usar o fastInsert non obsoleto en Baloo

### Iconas de Breeze

- Engadir a icona <code>drive-optical</code> (fallo 396432)

### Módulos adicionais de CMake

- Android: non definir manualmente unha versión aleatoria do SDK de Android
- ECMOptionalAddSubdirectory: Fornecer algúns detalles adicionais
- Corrixir unha comprobación de definición de variábel
- Cambiar a versión de «since»
- Mellorar ECMAddAppIconMacro

### Integración de infraestruturas

- Respectar BUILD_TESTING

### KActivities

- Respectar BUILD_TESTING

### KArchive

- Respectar BUILD_TESTING

### KAuth

- Evitar avisos sobre as cabeceiras de PolkitQt5-1
- Respectar BUILD_TESTING

### KBookmarks

- Respectar BUILD_TESTING

### KCodecs

- Respectar BUILD_TESTING

### KCompletion

- Respectar BUILD_TESTING

### KConfig

- Respectar BUILD_TESTING

### KConfigWidgets

- Respectar BUILD_TESTING

### KCoreAddons

- Corrixir un desbordamento en código de redondeo (fallo 397008)
- Documentación da API: retirar «:» que sobran tras «@note»
- Documentación da API: falar de nullptr, non de 0
- KFormat: substituír un literal de Unicode por un código de Unicode para corrixir a construción con MSVC
- KFormat: corrixir a etiqueta @since do novo KFormat::formatValue
- KFormat: permitir usar cantidades alén dos bytes e os segundos
- Corrixir os exemplos de KFormat::formatBytes
- Respectar BUILD_TESTING

### KCrash

- Respectar BUILD_TESTING

### KDBusAddons

- Non bloquear indefinidamente en ensureKdeinitRunning
- Respectar BUILD_TESTING

### KDeclarative

- asegurarse de que sempre escribimos no contexto raíz do motor
- máis lexíbel
- Mellorar lixeiramente a documentación da API
- Respectar BUILD_TESTING

### Compatibilidade coa versión 4 de KDELibs

- Corrixir qtplugins en KStandardDirs

### KDocTools

- Respectar BUILD_TESTING

### KEmoticons

- Respectar BUILD_TESTING

### KFileMetaData

- Documentación da API: engadir @file ás cabeceiras só de funcións para que Doxygen as inclúa

### KGlobalAccel

- Respectar BUILD_TESTING

### Complementos de interface gráfica de usuario de KDE

- Respectar BUILD_TESTING

### KHolidays

- Instalar a cabeceira de cálculo de amencer e solpor
- Engadiuse o día do ano bisesto como festivo (cultural) de Noruega
- Engadiuse a entrada «name» aos ficheiros de vacacións noruegueses
- Engadíronse descricións aos ficheiros de vacacións noruegueses
- máis actualizacións de vacacións xaponesas de phanect
- holiday_jp_ja, holiday_jp-en_us - updated (bug 365241)

### KI18n

- Aproveitar a función que xa fai o mesmo
- Corrixir a xestión de catálogos e a detección de configuracións rexionais en Android
- Lexibilidade, evitar sentencias sen efecto
- Corrixir KCatalog::translate cando a tradución é igual que o texto orixinal
- cambiouse o nome dun ficheiro
- Que o nome de ficheiro da macro de ki18n siga o estilo doutros ficheiros relacionados con find_package
- Corrixir a comprobación de configuración de _nl_msg_cat_cntr
- Non xerar ficheiros no directorio das fontes
- libintl: determinar se _nl_msg_cat_cntr existe antes de usalo (fallo 365917)
- Corrixir as construcións de fábricas de binarios

### KIconThemes

- Respectar BUILD_TESTING

### KIO

- Instalar o ficheiro de categoría de KDebugSettings relacionado con KIO
- cambiar o nome da cabeceira privada a «_p.h»
- Retirar unha selección personalizada de icona para o lixo (fallo 391200)
- Aliñas as etiquetas arriba no diálogo de propiedades
- Presentar un diálogo de erro cando o usuario intenta crear un directorio chamado «.» ou «..» (fallo 387449)
- Documentación da API: falar de nullptr, non de 0
- proba de rendemento de lstItems de kcoredirlister
- [KSambaShare] Comprobar que o ficheiro cambio antes de cargalo de novo
- [KDirOperator] Usar cores de fondo alternantes para vistas de varias columnas
- evitar unha fuga de memoria nos traballos escravos (fallo 396651)
- SlaveInterface: marcar setConnection e connection como obsoletas, de todos xeitos ninguén pode usalas
- Construtor de UDS lixeiramente máis rápido
- [KFilePlacesModel] Permitir URL bonitos de baloosearch
- Retirar o atallo web de projects.kde.org
- Cambiar KIO::convertSize() por KFormat::formatByteSize()
- Usar fastInsert, que non é obsoleto, en file.cpp (o primeiro de moitos)
- Substituír o atallo web de Gitorious polo de GitLab
- Non mostrar un diálogo de confirmación para a acción do lixo de maneira predeterminada (fallo 385492)
- Non pedir contrasinais en kfilewidgettest

### Kirigami

- permitir engadir e retirar títulos dinamicamente (fallo 396417)
- introducir actionsVisible (fallo 396413)
- adaptar as marxes cando a barra de desprazamento aparece ou desaparece
- mellor xestión do tamaño (fallo 396983)
- Optimizar a preparación da paleta
- AbstractApplciationItem non debería ter tamaño de seu, só implícito
- novos sinais pagePushed e pageRemoved
- corrixir a lóxica
- engadir o elemento ScenePosition (fallo 396877)
- Non fai falla emitir a paleta intermediaria para cada estado
- agochar → mostrar
- Modo de barra lateral pregábel
- kirigami_package_breeze_icons: non tratar as listas como elementos (fallo 396626)
- corrixir a expresión regular de buscar e substituír (fallo 396294)
- animar unha cor produce un efecto desagradábel (fallo 389534)
- elemento enfocado con cor para navegación de teclado
- retirar o atallo de saír
- Retirar Encoding=UTF-8 do ficheiro de formato de escritorio, xa hai tempo que está obsoleto
- corrixir o tamaño da barra de ferramentas (fallo 396521)
- corrixir o tamaño das asas
- Respectar BUILD_TESTING
- Mostrar as iconas de accións que teñen unha fonte de icona en vez de un nome de icona

### KItemViews

- Respectar BUILD_TESTING

### KJobWidgets

- Respectar BUILD_TESTING

### KJS

- Respectar BUILD_TESTING

### KMediaPlayer

- Respectar BUILD_TESTING

### KNewStuff

- Retirar Encoding=UTF-8 dos ficheiros de formato de escritorio, xa hai tempo que está obsoleto
- Cambiar o criterio de orde predeterminado a puntuación no diálogo de descargas
- Corrixir as marxes da xanela DownloadDialog para axustalas ás marxes do tema xeral
- Restaurar un qCDebug retirado sen querer
- Usar a API correcta de QSharedPointer
- Xestionar listas de vista previa baleiras

### KNotification

- Respectar BUILD_TESTING

### Infraestrutura KPackage

- Respectar BUILD_TESTING

### KParts

- Documentación da API: falar de nullptr, non de 0

### KPeople

- Respectar BUILD_TESTING

### KPlotting

- Respectar BUILD_TESTING

### KPty

- Respectar BUILD_TESTING

### KRunner

- Respectar BUILD_TESTING

### KService

- Documentación da API: falar de nullptr, non de 0
- Requirir que as construcións sexan fóra das fontes
- Engadir o operador subseq para atopar coincidencias con secuencias subordinadas

### KTextEditor

- corrección axeitada para o entrecomiñado automático de sangrado de cadea sen formato
- corrixir o sangrador para xestionar ben un novo ficheiro de sintaxe da infraestrutura de realce
- axustar a proba ao novo estado no repositorio de realce de sintaxe
- Mostrar a mensaxe «Repetiuse a busca» no centro da vista para unha mellor visibilidade
- corrixir un aviso, usar simplemente isNull()
- Estender a API de scripting
- corrixir un fallo de segmento en circunstancias especiais nas que se produce un vector baleiro para o número de palabras
- baleirar de maneira forzada a vista previa da barra de desprazamento ao baleirar o documento (fallo 374630)

### KTextWidgets

- Documentación da API: reverter parcialmente unha remisión anterior, non funcionou ben
- KFindDialog: poñer o foco no editor de liña ao mostrar un diálogo aproveitado
- KFind: restabelecer o contador ao cambiar de padrón (p. ex. no diálogo de atopar)
- Respectar BUILD_TESTING

### KUnitConversion

- Respectar BUILD_TESTING

### Infraestrutura de KWallet

- Respectar BUILD_TESTING

### KWayland

- Limpar os búferes de RemoteAccess en aboutToBeUnbound en vez de na destrución do obxecto
- Permitir consellos de cursor en punteiros bloqueados
- Reducir tempos de espera innecesariamente longos cando os espías de sinais fallan
- Corrixir a selección e as probas automáticas de asento
- Substituír as inclusións globais compactas de V5 restantes
- Engadir compatibilidade coa Base de WM de XDG na API de XDGShell
- Permitir compilar XDGShellV5 con XDGWMBase

### KWidgetsAddons

- Corrixir a máscara de entrada de KTimeComboBox para horas A.M. e P.M. (fallo 361764)
- Respectar BUILD_TESTING

### KWindowSystem

- Respectar BUILD_TESTING

### KXMLGUI

- Corrixir que KMainWindow gardase unha configuración incorrecta de trebellos (fallo 395988)
- Respectar BUILD_TESTING

### KXmlRpcClient

- Respectar BUILD_TESTING

### Infraestrutura de Plasma

- se un miniaplicativo é incorrecto ten UiReadyConstraint inmediatamente
- [PluginLoader de Plasma] Gardar os complementos en caché durante o inicio
- Corrixir a desaparición dun nodo cando se aplica atlas a unha textura
- [Contedor] Non cargar as accións dos contedores coa restrición de KIOSK «plasma/containment_actions»
- Respectar BUILD_TESTING

### Prison

- Corrixir o bloqueo do modo mesturado ao superior na xeración de código Aztec

### Purpose

- Asegurarse de que se desactiva a depuración de kf5.kio.core.copyjob durante a proba
- Reverter «proba: Unha forma máis «atómica» de comprobar que se produce un sinal»
- proba: Unha forma máis «atómica» de comprobar que se produce un sinal
- Engadir un complemento de Bluetooth
- [Telegram] Non agardar a que Telegram se peche
- Preparar para usar as cores de estado de Arc na lista despregábel de revisión
- Respectar BUILD_TESTING

### QQC2StyleBridge

- Mellorar os tamaños nos menús (fallo 396841)
- Retirar unha comparación dupla
- Deixar de usar sentencias de connect baseadas en cadeas
- comprobar que a icona sexa válida

### Solid

- Respectar BUILD_TESTING

### Sonnet

- Sonnet: setLanguage debería planificar un novo salientado se o salientado está activado
- Usar a API actual de Hunspell

### Realce da sintaxe

- CoffeeScript: corrixir os modelos en código de JavaScript incrustado e engadir escapes
- Excluír isto en Definition::includedDefinitions()
- Usar inicialización de membros na clase onde sexa posíbel
- engadir funcións para acceder ás palabras clave
- Engadir Definition::::formats()
- Engadir QVector&lt;Definition&gt; Definition::includedDefinitions() const
- Engadir Theme::TextStyle Format::textStyle() const;
- C++: corrixir os literais de coma flotante estándar (fallo 389693)
- CSS: actualizar a sintaxe e corrixir algúns erros
- C++: actualizar para c++20 e corrixir algúns erros de sintaxe
- CoffeeScript e JavaScript: corrixir os obxectos membros. Engadir a extensión .ts en JS (fallo 366797)
- Lua: corrixir unha cadea de varias liñas (fallo 395515)
- Especificación de RPM: engadir un tipo MIME
- Python: corrixir secuencias de escape en comentarios entre comiñas (fallo 386685)
- haskell.xml: non realzar os construtores de datos de Prelude de forma distinta doutros
- haskell.xml: retirar tipos da sección «función prelude»
- haskell.xml: salientar os construtores de datos promovidos
- haskell.xml: engadir as palabras clave family, forall e pattern
- Respectar BUILD_TESTING

### ThreadWeaver

- Respectar BUILD_TESTING

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
