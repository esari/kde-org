---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Marc de treball nou: kirigami, un conjunt de connectors del QtQuick per a construir les interfícies d'usuari basades en les directrius UX del KDE

### Icones Brisa

- S'han actualitzat els colors de «.h» i «.h++» (error 376680)
- S'ha eliminat la petita icona monocromàtica del ktorrent (error 381370)
- «bookmarks» és una icona d'acció, no una icona de carpeta (error 381383)
- S'han actualitzat «utilities-system-monitor» (error 381420)

### Mòduls extres del CMake

- S'ha afegit --gradle a «androiddeployqt»
- S'ha esmenat l'objectiu «apk» d'instal·lació
- S'ha esmenat l'ús de «query_qmake»: diferència entre les crides que espera o no el «qmake»
- S'ha afegit la «dox» de l'API per a KDE_INSTALL_USE_QT_SYS_PATHS de KDEInstallDirs
- S'ha afegit un «metainfo.yaml» per a fer que ECM sigui un marc de treball adequat
- Android: escaneja els fitxers qml al directori d'origen, no al d'instal·lació

### KActivities

- S'emet «runningActivityListChanged» en la creació d'una activitat

### Eines de Doxygen del KDE

- S'escapa l'HTML de les consultes de cerca

### KArchive

- S'han afegit els fitxers de Conan, com a primer experiment per implementar Conan

### KConfig

- Es permet construir KConfig sense Qt5Gui
- Dreceres estàndard: s'usa Ctrl+Re Pàg/Av Pàg per a la pestanya anterior/següent

### KCoreAddons

- S'ha eliminat la declaració init() no utilitzada de K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- API nova per a «spdx» a KAboutLicense per obtenir les expressions de llicència SPDX
- kdirwatch: S'evita una fallada potencial si es destrueix «d-ptr» abans de KDirWatch (error 381583)
- Soluciona la visualització de «formatDuration» amb arrodoniment (error 382069)

### KDeclarative

- S'ha esmenat la «plasmashell» sense establir QSG_RENDER_LOOP

### Compatibilitat amb les KDELibs 4

- S'ha esmenat «Consell obsolet per a KUrl::path() és incorrecte en el Windows» (error 382242)
- S'ha actualitzat kdelibs4support per a usar la implementació basada en la destinació proporcionada pel kdewin
- També s'han marcat els constructors com a obsolets
- S'ha sincronitzat el KDE4Defaults.cmake de les «kdelibs»

### KDesignerPlugin

- S'ha afegit la implementació per al giny nou «kpasswordlineedit»

### KHTML

- S'ha implementat el SVG (error 355872)

### KI18n

- Es permeten carregar els catàlegs i18n des d'ubicacions arbitràries
- Assegura que es genera l'objectiu dels fitxers TS

### KIdleTime

- Només caldrà Qt5X11Extras quan ho necessitem

### KInit

- S'usa una etiqueta de característica adequada per incloure kill(2)

### KIO

- S'ha afegit un mètode nou «urlSelectionRequested» a «KUrlNavigator»
- KUrlNavigator: s'exposa el «KUrlNavigatorButton» que ha rebut un esdeveniment deixat
- S'estanca sense preguntar a l'usuari amb un emergent Copia/Cancel·la
- S'ha assegurat que KDirLister ha actualitzat els elements que han canviat l'URL de destinació (error 382341)
- S'han creat opcions avançades perquè es desplegui/plegui el diàleg «Obre amb» de manera predeterminada (error 359233)

### KNewStuff

- S'ha proporcionat un element pare als menús «KMoreToolsMenuFactory»
- En sol·licitar des de la memòria cau, s'informa de totes les entrades a «bulk»

### Framework del KPackage

- El kpackagetool ara pot generar dades appstream a un fitxer
- S'ha adoptat un KAboutLicense::spdx nou

### KParts

- Es reinicia l'URL a closeUrl()
- S'ha afegit una plantilla per a una aplicació senzilla basada en kpart
- S'ha descartat l'ús de KDE_DEFAULT_WINDOWFLAGS

### KTextEditor

- Es gestiona l'esdeveniment en gra fi quan es gira la roda en fer zoom
- S'ha afegit una plantilla per a un connector del ktexteditor
- Es copien els permisos des del fitxer original en desar una còpia (error 377373)
- Potser s'ha evitat la fallada en construir la cadena (error 339627)
- S'ha esmenat el problema amb afegir «*» a les línies fora dels comentaris (error 360456)
- S'ha esmenat l'acció de desar com a còpia, prohibint sobreescriure el fitxer de destinació (error 368145)
- Ordre «set-highlight»: Uneix els arguments amb espais
- S'ha esmenat una fallada en destruir la vista deguda a la neteja no determinística dels objectes
- S'emeten senyals des de la vora de la icona quan no es fa clic a les marques
- S'ha esmenat la fallada en el mode d'entrada del «vi» (seqüència: «o» «Esc» «O» «Esc» «.») (error 377852)
- Usa mútuament grups exclusius al tipus de marca predeterminada

### KUnitConversion

- S'han marcat MPa i PSI com a unitats comunes

### Framework del KWallet

- S'usa CMAKE_INSTALL_BINDIR per a la generació del servei de D-Bus

### KWayland

- Es destrueixen tots els objectes del KWayland creats pel registre quan es destrueix
- S'emet «connectionDied» si es destrueix el QPA
- [client] Segueix tots els ConnectionThreads creats i s'ha afegit l'API per accedir-hi
- [servidor] Envia l'abandonament d'entrada de text si la superfície amb focus esdevé sense associació
- [servidor] Envia l'abandonament de l'apuntador si la superfície amb focus esdevé sense associació
- [client] Segueix adequadament «enteredSurface» al teclat
- [servidor] Envia l'abandonament del teclat quan el client destrueix la superfície amb el focus (error 382280)
- Es comprova la validesa de la memòria intermèdia (error 381953)

### KWidgetsAddons

- Extracció de la línia d'edició del giny de contrasenya =&gt; la classe KPasswordLineEdit nova
- Esmena una fallada en cercar amb la implementació d'accessibilitat activada (error 374933)
- [KPageListViewDelegate] Passa el giny a «drawPrimitive» en el «drawFocus»

### KWindowSystem

- S'ha suprimit la dependència de la capçalera en el QWidget

### KXMLGUI

- S'ha descartat l'ús de KDE_DEFAULT_WINDOWFLAGS

### NetworkManagerQt

- S'ha afegit la implementació per a ipv*.route-metric
- Soluciona el numerador NM_SETTING_WIRELESS_POWERSAVE_FOO no definit (error 382051)

### Frameworks del Plasma

- [Containment Interface] Sempre emet «contextualActionsAboutToShow» per al contenidor
- Es tracten les etiquetes Button/ToolButton com a text net
- No es realitzen les esmenes específiques «wayland» quan s'està sobre X (error 381130)
- S'afegeix KF5WindowSystem a la interfície d'enllaç
- Declara AppManager.js com a biblioteca «pragma»
- [Components del Plasma] S'ha suprimit el Config.js
- Text net per omissió per a les etiquetes
- Es carreguen les traduccions des de fitxers KPackage si estan agrupats (error 374825)
- [Menú dels components del Plasma] No falla en una acció nul·la
- [Diàleg de plasma] S'han esmenat les condicions de l'etiqueta
- S'ha actualitzat la icona de la safata del sistema de l'Akregator (error 379861)
- [Containment Interface] Manté el contenidor a RequiresAttentionStatus mentre el menú contextual és obert (error 351823)
- Soluciona la gestió de les tecles de disposició a la barra de pestanyes en el RTL (error 379894)

### Sonnet

- Es permet construir el Sonnet sense Qt5Widgets
- CMake: s'ha reescrit el FindHUNSPELL.cmake per usar el pkg-config

### Ressaltat de la sintaxi

- Es permet construir KSyntaxHighlighter sense Qt5Gui
- S'ha afegit la implementació per a compilació creuada per a l'indexador de ressaltat
- Temes: S'han suprimit totes les metadades no usades (llicència, autor, només lectura)
- Tema: S'han suprimit els camps de llicència i autor
- Tema: Es deriva l'etiqueta de només lectura des d'un fitxer al disc
- S'ha afegit un fitxer de ressaltat de sintaxi per al llenguatge de modelatge de dades YANG
- PHP: S'han afegit les paraules clau del PHP 7 (error 356383)
- PHP: S'ha netejat la informació per al PHP 5
- Esmena el «gnuplot», convertint els espais inicials/finals fatals
- Esmena la detecció dels «else if», cal canviar el context, afegeix una regla extra
- L'indexador verifica els espais en blanc inicials/finals en el ressaltat XML
- Doxygen: S'ha afegit el ressaltat del Doxyfile
- Afegeix els tipus estàndards que manquen al ressaltat C i actualitza a C11 (error 367798)
- Q_PI D =&gt; Q_PID
- PHP: Millora el ressaltat de variables en claus a les cometes dobles (error 382527)
- S'ha afegit el ressaltat de PowerShell
- Haskell: Afegeix l'extensió de fitxer .hs-boot (mòdul «bootstrap») (error 354629)
- Soluciona «replaceCaptures()» per funcionar amb més de 9 captures
- Ruby: Usa WordDetect en lloc de StringDetect per a les coincidències de paraula completa
- Soluciona el ressaltat incorrecte per a BEGIN i END en paraules com «EXTENDED» (error 350709)
- PHP: Elimina «mime_content_type()» de la llista de funcions obsoletes (error 371973)
- XML: S'ha afegit l'extensió/tipus MIME de XBEL al ressaltat xml (error 374573)
- Bash: S'ha esmenat el ressaltat incorrecte per a les opcions de les ordres (error 375245)
- Perl: Soluciona el ressaltat de «heredoc» amb espais inicials en el delimitador (error 379298)
- S'ha actualitzat el fitxer de sintaxi SQL (Oracle) (error 368755)
- C++: S'ha esmenat que «-» no forma part de la cadena UDL (error 380408)
- C++: El format del «printf» especifica: afegeix «n» i «p», elimina «P» (error 380409)
- C++: Esmena el valor dels «char» que tenen el color de les cadenes (error 380489)
- VHDL: Soluciona l'error de ressaltat en usar claudàtors i atributs (error 368897)
- Ressaltat del «zsh»: Soluciona les expressions matemàtiques en una expressió de subcadena (error 380229)
- Ressaltat del JavaScript: Afegeix la implementació per l'ampliació E4X de l'XML (error 373713)
- S'ha suprimit la regla de l'extensió «*.conf»
- Sintaxi de Pug/Jade

### ThreadWeaver

- Afegeix una exportació que manca a QueueSignals

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
