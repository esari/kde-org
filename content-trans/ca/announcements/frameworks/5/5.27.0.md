---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Icones noves per tipus MIME.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Usa l'entrada de configuració correcta en la condició «autostart»
- Soluciona una inserció ordenada (també anomenada inserció com «flat_map») (error 367991)
- Afegeix un entorn de tancament que mancava, com ha descobert en Loïc Yhuel (error 353783)
- Transacció no creada =&gt; no s'intenta interrompre
- Soluciona una assignació «m_env = nullptr» que mancava
- Fa segur el fil Baloo::Query, per exemple
- En els sistemes de 64 bits ara el Baloo permet un emmagatzematge de l'índex &gt; 5 GB (error 364475)
- Permet ctime/mtime == 0 (error 355238)
- Gestiona la corrupció de l'índex de la base de dades del «baloo_file», intenta tornar a crear la base de dades o s'interromp si això falla

### BluezQt

- Esmena una fallada quan intenta afegir un dispositiu a un adaptador desconegut (error 364416)

### Icones Brisa

- Icones noves per tipus MIME
- Actualitza diverses icones del KStars (error 364981)
- Estil «actions/24/format-border-set» erroni (error 368980)
- Afegeix una icona d'aplicació del Wayland
- Afegeix una icona d'aplicació de l'Xorg (error 368813)
- Reverteix «distribute-randomize», «view-calendar» + tornar a aplicar l'esmena de transformació (error 367082)
- Canvia els documents de carpeta d'un fitxer a diversos fitxers, ja que s'inclou més d'un fitxer en una carpeta (error 368224)

### Mòduls extres del CMake

- Assegura que no s'afegeix la prova «appstream» dues vegades

### KActivities

- Ordena les activitats alfabèticament per nom a la memòria cau (error 362774)

### Eines de Doxygen del KDE

- Diversos canvis a la disposició general dels documents generats de l'API
- Camí correcte a les etiquetes, depenent de si la biblioteca forma part d'un grup o no
- Cerca: soluciona la «href» de les biblioteques que no són part d'un grup

### KArchive

- Soluciona una fuita de memòria en el KCompressionDevice del KTar
- KArchive: soluciona una fuita de memòria quan ja existeix una entrada amb el mateix nom
- Soluciona una fuita de memòria en el KZip en gestionar directoris buits
- K7Zip: soluciona fuites de memòria en els errors
- Soluciona una fuita de memòria detectada per l'ASAN quan l'«open()» falla en el dispositiu subjacent
- Elimina un «cast» incorrecte a KFilterDev, detectat per l'ASAN

### KCodecs

- Afegeix les macros d'exportació que mancaven a les classes Decoder i Encoder

### KConfig

- Soluciona una fuita de memòria al SignalsTestNoSingletonDpointer, trobat per l'ASAN

### KCoreAddons

- Es registra QPair&lt;QString,QString&gt; com a meta-tipus a KJobTrackerInterface
- No converteix com a URL un URL que tingui un caràcter de cometes dobles
- Esmenes de compilació del Windows
- Soluciona un error molt antic quan s'elimina l'espai en un URL com «foo &lt;&lt;url&gt; &lt;url&gt;&gt;»

### KCrash

- Opció del CMake a KCRASH_CORE_PATTERN_RAISE per redirigir al nucli
- Canvia el nivell de registre predeterminat d'Avís a Info

### Compatibilitat amb les KDELibs 4

- Neteja. No instal·la inclusions que apuntin a inclusions no existents i també elimina aquests fitxers
- Ús més correcte de std::remove_pointer amb C++11 disponible

### KDocTools

- Soluciona «el checkXML5 imprimeix HTML generat a la sortida estàndard per als docbook vàlids» (error 369415)
- Soluciona l'error de no poder executar eines natives en el paquet usant les «kdoctools» amb compilació creuada
- Defineix els objectius de les «kdoctools» executant compilació creuada des d'altres paquets
- Afegeix la implementació de compilació creuada per al «docbookl10nhelper»
- Afegeix la implementació de compilació creuada per al meinproc5
- Converteix el «checkxml5» en un executable de les Qt per implementar la compilació creuada

### KFileMetaData

- Millora l'extractor d'«epub», menys «segfaults» (error 361727)
- Converteix l'indexador d'ODF a prova d'error, comprova si els fitxers hi són (i són realment fitxers) (meta.xml + content.xml)

### KIO

- Esmena els esclaus KIO que usen només TLS1.0
- Soluciona l'ABI trencat al KIO
- KFileItemActions: afegeix «addPluginActionsTo(QMenu *)»
- Només mostra els botons de còpia quan s'ha calculat la suma de verificació
- Afegeix la retroalimentació que manca en calcular una suma de verificació (error 368520)
- Soluciona KFileItem::overlays quan retorna valors de cadenes buides
- Soluciona el llançament de fitxers .desktop de terminal amb el Konsole
- Classifica els muntatges de NFS4 com a «probablySlow», com els nfs/cifs/..
- KNewFileMenu: mostra la drecera d'acció de Carpeta nova (error 366075)

### KItemViews

- En el mode vista de llista usa la implementació predeterminada de «moveCursor»

### KNewStuff

- Afegeix comprovacions del KAuthorized per permetre desactivar el «ghns» a «kdeglobals» (error 368240)

### Paquets dels Frameworks

- No genera fitxers «appstream» per a components que no estan al RDN
- Fa que «kpackage_install_package» funcioni amb KDE_INSTALL_DIRS_NO_DEPRECATED
- Elimina la variable sense ús KPACKAGE_DATA_INSTALL_DIR

### KParts

- Soluciona els URL amb una barra final que s'assumien sempre com a directoris

### KPeople

- Soluciona la compilació de l'ASAN (duplicates.cpp usa KPeople::AbstractContact que és a KF5PeopleBackend)

### KPty

- Usa el camí de l'ECM per cercar binaris «utempter», més fiable que un prefix senzill del CMake
- Crida manualment l'executable del «helper» «utempter» (error 364779)

### KTextEditor

- Fitxers XML: elimina els valors dels colors en el codi font
- XML: elimina els valors dels colors en el codi font
- Definició d'esquema XML: Converteix «version» en un «xs:integer»
- Fitxers de definició de ressaltat: arrodonir amunt la versió a l'enter següent
- Implementa les captures de caràcters múltiples nom en {xxx} per evitar regressions
- Implementa la substitució d'expressions regulars per captures &gt; 9, p. ex. 111 (error 365124)
- Soluciona la representació de caràcters que s'estenen a la línia següent, p. ex. els subratllats ja no es retallen amb diversos tipus de lletra o mides dels tipus de lletra (error 335079)
- Soluciona una fallada: Assegura que el cursor mostrat és vàlid després del plegat de text (error 367466)
- El KateNormalInputMode necessita tornar a executar els mètodes «enter» de SearchBar
- Intenta solucionar la representació dels subratllats i similars (error 335079)
- Només mostra el botó «Visualitza les diferències», si el «diff» està instal·lat
- Usa un giny de missatge no modal per a les notificacions de fitxer modificat externament (error 353712)
- Soluciona una regressió: «testNormal» només funcionava perquè l'execució de la prova era única
- Divideix la prova d'indentació en execucions separades
- Tornar a admetre l'acció «Expandeix els nodes de nivell superior» (error 335590)
- Esmena una fallada en mostrar missatges superiors o inferiors múltiples vegades
- Esmena l'establiment de l'EOL en el mode de línies (error 365705)
- Ressalta els fitxers .nix com a «bash», s'espera que no faci mal (error 365006)

### Framework del KWallet

- Comprova si el «kwallet» està actiu a Wallet::isOpen(name) (error 358260)
- Afegeix la capçalera «boost» que mancava
- Elimina la cerca duplicada de KF5DocTools

### KWayland

- [servidor] No envia l'alliberament de tecla per les tecles no premudes ni en prémer una tecla doble (error 366625)
- [servidor] En substituir la selecció del porta-retalls, cal cancel·lar el DataSource anterior (error 368391)
- S'ha afegit la implementació dels esdeveniments d'entrada/sortida de la superfície
- [client] Segueix totes les «Outputs» creades i afegeix un mètode «get» estàtic

### KXmlRpcClient

- Converteix les categories a org.kde.pim.*

### NetworkManagerQt

- Cal establir l'estat durant la inicialització
- Substitueix totes les crides de bloqueig a la inicialització per només una crida de bloqueig
- Usa la interfície estàndard «o.f.DBus.Properties» per al senyal «PropertiesChanged» al NM 1.4.0+ (error 367938)

### Icones de l'Oxygen

- Elimina un directori no vàlid a index.theme
- Introdueix una prova de duplicats de les icones Brisa
- Converteix totes les icones duplicades a enllaços simbòlics

### Frameworks del Plasma

- Millora la sortida del «timetracker»
- [ToolButtonStyle] Soluciona la fletxa del menú
- i18n: gestiona les cadenes dels fitxers «kdevtemplate»
- i18n: revisió de les cadenes dels fitxers «kdevtemplate»
- Afegeix «removeMenuItem» a PlasmaComponents.ContextMenu
- Actualitza la icona del KTorrent (error 369302)
- [WindowThumbnail] Descarta els mapes de píxels en els esdeveniments de mapa
- No inclou «kdeglobals» en tractar la configuració de la memòria cau
- Esmena Plasma::knownLanguages
- Redimensiona la vista immediatament després d'establir el contenidor
- Evita la creació d'un KPluginInfo des d'una instància del KPluginMetaData
- Les tasques en execució tenen un indicador
- Les línies de la barra de tasques estan d'acord amb RR 128802. En Marco ha donat el vistiplau
- [AppletQuickItem] Surt d'un bucle quan s'ha trobat una disposició

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
