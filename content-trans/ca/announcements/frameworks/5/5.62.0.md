---
aliases:
- ../../kde-frameworks-5.62.0
date: 2019-09-14
layout: framework
libCount: 70
---
### Attica

- Corregeix el fitxer «pkgconfig» de l'«attica»

### Baloo

- Corregeix una fallada al Peruse activada pel Baloo

### Icones Brisa

- Afegeix icones noves d'activitats i escriptoris virtuals
- Fa que les icones petites dels documents recents semblin documents i millora els emblemes de rellotge
- Crea una icona nova de «Carpetes recents» (error 411635)
- Afegeix la icona «preferences-desktop-navigation» (error 402910)
- Afegeix «dialog-scripts» de 22px, i canvia les icones de scripts «actions/places» perquè coincideixin
- Millora la icona «user-trash»
- Usa l'estil buit/omplert per a la paperera buida/omplerta en monocrom
- Fa que les icones de notificacions usin l'estil del contorn
- Fa que les icones «user-trash» semblin com papereres (error 399613)
- Afegeix les icones Brisa per als fitxers ROOT del CERN
- Elimina «applets/22/computer» (error 410854)
- Afegeix les icones «view-barcode-qr»
- El Krita s'ha escindit del Calligra i ara usa el nom Krita en lloc del «calligrakrita» (error 411163)
- Afegeix les icones «batery-ups» (error 411051)
- Fa que «monitor» sigui un enllaç a la icona «computer»
- Afegeix les icones de FictionBook 2
- Afegeix la icona per al «kuiviewer», cal actualització -&gt; error 407527
- Enllaç simbòlic de «port» a «anchor», que mostra una iconografia més adequada
- Canvia la icona de «radio» a «device», afegeix més mides
- Fa que la icona <code>pin</code> apunti a una icona que sembli una xinxeta, no a quelcom sense relació
- Corregeix el dígit que manca i l'alineació ajustada a píxel de les icones d'accions de profunditat (error 406502)
- Fa que les «folder-activities» de 16px s'assemblin més a les de mides més grans
- Afegeix la icona «latte-dock» del repositori del Latte Dock per a kde.org/applications
- Redibuixa la icona «kdesrc-build» usada per kde.org/applications
- Reanomena «media-show-active-track-amarok» a «media-track-show-active»

### Mòduls extres del CMake

- ECMAddQtDesignerPlugin: passa la mostra de codi indirectament a través d'un argument de nom variable
- Manté «lib» com a la LIBDIR predeterminada en els sistemes basats en l'Arch Linux
- Activa de manera predeterminada l'«autorcc»
- Defineix la ubicació d'instal·lació per als fitxers JAR/AAR a l'Android
- Afegeix ECMAddQtDesignerPlugin

### KActivitiesStats

- Afegeix Term::Type::files() i Term::Type::directories() per filtrar només directoris o excloure'ls
- Afegeix @since 5.62 per als «setters» afegits de nou
- Afegeix l'enregistrament adequat usant ECMQtDeclareLoggingCategory
- Afegeix «setter» als camps de consulta Type, Activity, Agent i UrlFilter
- Usa valors constants especials a «terms.cpp»
- Permet el filtratge d'esdeveniments de recursos per interval de dates usant el terme Date

### KActivities

- [kactivities] Usa una icona nova d'activitats

### KArchive

- Corregeix la creació d'arxius als continguts de l'Android: URL

### KCompletion

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KConfig

- Esmena una fuita de memòria al «KConfigWatcher»
- Desactiva «KCONFIG_USE_DBUS» a l'Android

### KConfigWidgets

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)
- [KColorSchemeManager] Optimitza la generació de la vista prèvia

### KCoreAddons

- El KProcessInfo::name() ara només retorna el nom de l'executable. Per a la línia d'ordres completa useu el KProcessInfo::command()

### KCrash

- Evita l'activació del «kcrash» si només s'ha inclòs a través d'un connector (error 401637)
- Desactiva el «kcrash» en executar-ho sota «rr»

### KDBusAddons

- Corregeix un error de concurrència als reinicis automàtics del «kcrash»

### KDeclarative

- Avisa si el KPackage no és vàlid
- [GridDelegate] No selecciona un element desseleccionat en clicar en qualsevol dels seus botons d'accions (error 404536)
- [ColorButton] Reenviament de senyal acceptat des del ColorDialog
- Usa un sistema de coordenades basades en zero al traçador

### KDesignerPlugin

- Fa obsolet el «kgendesignerplugin», elimina l'empaquetament del connector per a tots els ginys KF5

### KDE WebKit

- Usa ECMAddQtDesignerPlugin en lloc d'una còpia privada

### KDocTools

- KF5DocToolsMacros.cmake: usa variables KDEInstallDirs no obsoletes (error 410998)

### KFileMetaData

- Implementa l'escriptura d'imatges

### KHolidays

- Mostra el nom de fitxer a on es retorna un error

### KI18n

- Localitza les cadenes de nombres llargs (error 409077)
- Permet passar el destí a la macro «ki18n_wrap_ui»

### KIconThemes

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KIO

- S'ha corregit l'acció de desfer els fitxers llançats a la paperera a l'escriptori (error 391606)
- kio_trash: s'ha dividit «copyOrMove», per millorar l'error que «mai s'hauria de donar»
- FileUndoManager: una asserció més clara en oblidar un enregistrament
- Corregeix la sortida i una fallada al «kio_file» quan el «put()» falla a «readData»
- [CopyJob] Corregeix una fallada en copiar un directori ja existent i prémer «Omet» (error 408350)
- [KUrlNavigator] Afegeix els tipus MIME acceptats pel «krarc» a «isCompressedPath» (error 386448)
- S'ha afegit un diàleg per establir el permís d'execució per a un fitxer executable en intentar executar-lo
- [KPropertiesDialog] Comprova sempre que el punt de muntatge sigui nul (error 411517)
- [KRun] Primer comprova el tipus MIME per a «isExecutableFile»
- Afegeix una icona per a la paperera arrel i una etiqueta adequada (error 392882)
- Afegeix el suport per gestionar els errors SSL del QNAM a KSslErrorUiData
- Fa un comportament coherent de FileJob
- [KFilePlacesView] Usa un KIO::FileSystemFreeSpaceJob asíncron
- Reanomena l'executable ajudant «kioslave» intern a «kioslave5» (error 386859)
- [KDirOperator] Elideix el centre de les etiquetes que són massa llargues per encaixar (error 404955)
- [KDirOperator] Afegeix les opcions per seguir els directoris nous
- KDirOperator: només activa el menú «Crea nou» si l'element seleccionat és un directori
- KIO FTP: corregeix la congelació de la còpia de fitxer a un fitxer existent (error 409954)
- KIO: adaptació al KWindowSystem::setMainWindow no obsolet
- Fa coherents els noms del fitxer de la llibreta d'adreces
- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)
- [KDirOperator] Usa unes descripcions del sentit d'ordenació més intel·ligibles
- [Permissions editor] Adapta les icones per usar QIcon::fromTheme() (error 407662)

### Kirigami

- Substitueix el botó personalitzat que desborda per PrivateActionToolButton a ActionToolBar
- Si una acció de submenú té establerta una icona, s'assegura que també es mostra
- [Separator] Coincideix amb el color de les vores del Breeze
- Afegeix el component ListSectionHeader del Kirigami
- Corregeix el botó del menú contextual per a les pàgines que no es mostren
- Corregeix el PrivateActionToolButton quan el menú no neteja adequadament l'estat comprovat
- Permet definir una icona personalitzada per a la maneta esquerra del calaix
- Reconstrueix la lògica de «visibleActions» a SwipeListItem
- Permet l'ús d'accions QQC2 als components del Kirigami i ara fa que K.Action es basi en la QQC2.Action
- Kirigami.Icon: corregeix la càrrega de les imatges més grans quan l'origen és un URL (error 400312)
- Afegeix la icona usada per la Kirigami.AboutPage

### KItemModels

- Afegeix la interfície Q_PROPERTIES al KDescendantsProxyModel
- Elimina l'adaptació dels mètodes obsolets a les Qt

### KItemViews

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KNotification

- Evita les notificacions duplicades de la visualització al Windows i elimina els espais en blanc
- Té una icona d'aplicació 1024x1024 com a icona de reserva al Snore
- Afegeix el paràmetre <code>-pid</code> a les crides del dorsal Snore
- Afegeix el dorsal «snoretoast» per a «KNotifications» al Windows

### KPeople

- Fa possible eliminar contactes des dels dorsals
- Fa possible modificar els contactes

### KPlotting

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KRunner

- Assegura que es comprova si la descomposició ha vençut després de finalitzar un treball
- Afegeix un senyal de fet a FindMatchesJob en lloc d'usar QObjectDecorator incorrectament

### KTextEditor

- Permet personalitzar els atributs per als temes de KSyntaxHighligting
- Correcció per a tots els temes: permet desactivar atributs als fitxers XML de ressaltat
- Simplifica «isAcceptableInput» + permet tot per als mètodes d'entrada
- Simplifica «typeChars», no cal un codi de retorn sense filtratge
- Imita QInputControl::isAcceptableInput() en filtrar caràcters teclejats (error 389796)
- Intenta sanejar els finals de línia en enganxar (error 410951)
- Correcció: permet desactivar atributs als fitxers XML de ressaltat
- Millora la compleció de paraules en usar el ressaltat per detectar els límits de les paraules (error 360340)
- Més adaptació de QRegExp a QRegularExpression
- Comprova adequadament si l'ordre «diff» es pot iniciar per fer diferències al fitxer d'intercanvi (error 389639)
- KTextEditor: Corregeix el lliscament de la vora esquerra en canviar entre documents
- Migra més QRegExps a QRegularExpression
- Permet 0 als intervals de línia en mode «vim»
- Usa «find_dependency» del CMake en lloc de «find_package» a la plantilla del fitxer de configuració del CMake

### KTextWidgets

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KUnitConversion

- Afegeix les unitats de potència dels decibels (dBW i múltiples)

### Framework del KWallet

- KWallet: corregeix l'inici del «kwalletmanager», el nom del fitxer «desktop» té un «5»

### KWayland

- [server] Embolcalla «proxyRemoveSurface» en un apuntador intel·ligent
- [server] Usa més el mode actual en la memòria cau i assigna la validesa
- [server] Memòria cau del mode actual
- Implementa «zwp_linux_dmabuf_v1»

### KWidgetsAddons

- [KMessageWidget] Passa el giny a «standardIcon()»
- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### KWindowSystem

- KWindowSystem: afegeix l'opció KWINDOWSYSTEM_NO_WIDGETS del CMake
- Fa obsolet «slideWindow(QWidget *widget)»
- Afegeix la sobrecàrrega de KWindowSystem::setMainWindow(QWindow *)
- KWindowSystem: afegeix la sobrecàrrega de «setNewStartupId(QWindow *...)»

### KXMLGUI

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### NetworkManagerQt

- Reanomena WirelessDevice::lastRequestScanTime a WirelessDevice::lastRequestScan
- Afegeix les propietats «lastScanTime» i «lastRequestTime» a WirelessDevice

### Frameworks del Plasma

- Fa que les icones de notificacions usin l'estil del contorn
- Fa més coherent la mida dels botons d'eina
- Permet que els «applets/containments/wallpapers» ajornin UIReadyConstraint
- Fa que les icones de notificació semblin campanes (error 384015)
- Corregeix la posició inicial incorrecta de les pestanyes de les barres verticals de pestanyes (error 395390)

### Purpose

- Corregeix el connector d'escriptori del Telegram al Fedora

### QQC2StyleBridge

- Evita arrossegar el contingut del quadre combinat del QQC2 fora del menú

### Solid

- Fa constant la propietat sèrie de la bateria
- Exposa adequadament la tecnologia a la interfície de la bateria

### Sonnet

- Afegeix l'opció per construir el connector del Qt Designer (BUILD_DESIGNERPLUGIN, de manera predeterminada en ON)

### Ressaltat de la sintaxi

- C i ISO C++: afegeix els dígrafs (plegat i preprocessador) (error 411508)
- Markdown, TypeScript i Logcat: diverses correccions
- Classe «format»: afegeix funcions per conèixer si els fitxers XML defineixen atributs d'estil
- Combina el material de «test.m» al «highlight.m» existent
- Implementa les cadenes Matlab natives
- Gettext: Afegeix l'estil «Translated String» i l'atribut «spellChecking» (error 392612)
- Estableix el sagnat de l'OpenSCAD a l'estil del C en lloc de cap
- Possibilita canviar les dades de «Definition» després de carregar
- Indexador de ressaltat: comprova la «kateversion»
- Markdown: diverses millores i correccions (error 390309)
- JSP: suport de &lt;script&gt; i &lt;style&gt;; usa IncludeRule ##Java (error 345003)
- LESS: importa les paraules clau del CSS, ressaltat nou i diverses millores
- JavaScript: elimina el context innecessari «Conditional Expression»
- Sintaxi nova: SASS. Diverses correccions per al CSS i el SCSS (error 149313)
- Usa «find_dependency» del CMake al fitxer de configuració del CMake en lloc de «find_package»
- SCSS: corregeix la interpolació (#{...}) i afegeix el color de la interpolació
- Corregeix l'atribut «additionalDeliminator» (error 399348)
- C++: els contractes no són en C++20
- Gettext: corregeix la «cadena no traduïda prèviament» i altres millores/correccions
- Jam: corregeix un «local» amb variable sense inicialitzar i el ressaltat de SubRule
- «Fallthrough» implícit si hi ha «fallthroughContext»
- Afegeix les extensions de fitxer GLSL normals (.vs, .gs, .fs)
- Latex: diverses correccions (mode «math», verbatim imbricat,...) (error 410477)
- Lua: corregeix el color del final amb diversos nivells de condició i imbricació de funcions
- Indexador de ressaltat: tots els avisos són fatals

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
