---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Frameworks nous

- KFileMetadata: Biblioteca de metadades i extracció de fitxers
- Baloo: Framework d'indexació i cerca de fitxers

### Canvis que afecten tots els frameworks

- El requisit de versió de les Qt ha pujat de la 5.2 a la 5.3
- La sortida de depuració s'ha adaptat a una sortida amb categoria, per fer menys soroll per defecte
- La documentació en Docbook s'ha revisat i actualitzat

### Integració del marc de treball

- Esmenar fallada en el diàleg de fitxer de només directoris
- No comptar amb options()-&gt;initialDirectory() per a les Qt &lt; 5.4

### Eines de Doxygen del KDE

- Afegir les pàgines «man» per als scripts del «kapidox» i actualitzar la informació del mantenidor en el setup.py

### KBookmarks

- KBookmarkManager: Usar KDirWatch en lloc de QFileSystemWatcher per detectar quan es crea user-places.xbel.

### KCompletion

- Esmenes a HiDPI per KLineEdit/KComboBox
- KLineEdit: No permetre que l'usuari suprimeixi text quan la línia d'edició és de només lectura

### KConfig

- No recomanar l'ús d'una API obsoleta
- No generar codi obsolet

### KCoreAddons

- Afegir «Kdelibs4Migration::kdeHome()» per als casos no coberts pels recursos
- Esmenar l'avís de tr()
- Esmenar la construcció del KCoreAddons en Clang+ARM

### KDBusAddons

- KDBusService: Documentar com elevar la finestra activa, a «Activate()»

### KDeclarative

- Esmenar una crida obsoleta KRun::run
- El mateix comportament de MouseArea per mapar les coordenades d'esdeveniments fills filtrats
- Detectar quan es crea la icona de cara inicial
- No actualitzar la finestra sencera quan es renderitza el traçador (error 348385)
- Afegir la propietat de context userPaths
- No embussar-se quan el QIconItem és buit

### Compatibilitat amb les KDELibs 4

- S'ha mogut «kconfig_compiler_kf5» a la «libexec», s'usa «kreadconfig5» en lloc de la prova «findExe»
- Documentar les substitucions (subòptimes) per KApplication::disableSessionManagement

### KDocTools

- Canviar la frase quant a informar d'errors, gràcies a dfaure
- Adaptar user.entities alemany a en/user.entities
- Actualitzar el «general.entities»: Canviar el marcatge per als frameworks + plasma, des de les aplicacions fins al nom del producte
- Actualitzar en/user.entities
- Actualització de les plantilles de manual i pàgines man
- Usar CMAKE_MODULE_PATH a cmake_install.cmake
- Error: 350799 (error 350799)
- Actualització general d'entitats
- Cerca dels mòduls requerits en perl.
- Espai de noms per a una macro del «helper» en el fitxer instal·lat de macros.
- Adaptar les traduccions dels noms de tecles a les traduccions estàndards proporcionades pel Termcat

### KEmoticons

- Instal·lar el tema Brisa
- Kemoticons: Fer estàndards les emoticones del Brisa en lloc del Glass
- Paquet d'emoticones del Brisa fet per Uri Herrera

### KHTML

- Permetre que el KHtml sigui usable sense cercar dependències privades

### KIconThemes

- Eliminar temporalment les assignacions de cadenes.
- Eliminar l'entrada de depuració de l'arbre de temes

### KIdleTime

- S'han instal·lat capçaleres privades per connectors de plataforma.

### KIO

- Matar embolcalls QUrl innecessaris

### KItemModels

- Servidor intermediari nou: KExtraColumnsProxyModel, permet afegir columnes a un model existent.

### KNotification

- Esmenar la posició Y d'inici per finestres emergents de reserva
- Reduir dependències i moure al nivell 2
- Recuperar entrades de notificació desconegudes (nullptr deref) (error 348414)
- Eliminar diversos missatges d'avís inútils

### Paquets dels Frameworks

- Fer els subtítols, subtítols ;)
- kpackagetool: esmenar la sortida de text no llatí a la sortida estàndard (stdout)

### KPeople

- Afegir AllPhoneNumbersProperty
- PersonsSortFilterProxyModel ara és disponible per usar en QML

### Kross

- krosscore: Instal·lar la capçalera CamelCase «KrossConfig»
- Esmenar les proves Python2 per a executar amb PyQt5

### KService

- Esmenar kbuildsycoca --global
- KToolInvocation::invokeMailer: Esmenar l'adjunció quan hi ha múltiples adjunts

### KTextEditor

- Mantenir el nivell de registre per defecte per les Qt &lt; 5.4.0, esmenar el nom de categoria del registre
- Afegir hl per Xonotic (error 342265)
- Afegir Groovy HL (error 329320)
- Actualització del ressaltat de J (error 346386)
- Fer que compili amb MSVC2015
- Menys ús de «iconloader», esmenar les icones més pixelades
- Activar/desactivar el botó cerca tot en canvis de patró
- Barra millorada de cerca i substitució
- Eliminar regle inútil del «powermode»
- Barra de cerca més fina
- vi: Esmenar una lectura errònia de l'indicador «markType01»
- Usar la qualificació correcta per cridar el mètode base.
- Eliminar comprovacions, el QMetaObject::invokeMethod ja es protegeix a si mateix.
- Esmenar problemes de HiDPI amb els selectors de color
- Neteja del nucli: QMetaObject::invokeMethod és un «nullptr» segur.
- Més comentaris
- Canviar la manera en què les interfícies són segures amb nul
- Per defecte, només els avisos i superiors a sortida
- Eliminar les tasques pendents del passat
- Usar QVarLengthArray per desar la iteració temporal de QVector.
- Moure el pedaç per sagnar etiquetes de grup al temps de construcció.
- Esmenar diversos problemes seriosos amb el KateCompletionModel en mode d'arbre.
- Esmenar el disseny de model trencat, que confiava en el comportament de les Qt 4.
- Obeir les regles de «umask» en desar fitxers nous (error 343158)
- Afegir meson HL
- Com que el Varnish 4.x presenta diversos canvis de sintaxi en comparació amb el Varnish 3.x, s'han escrit fitxers addicionals de ressaltat de sintaxi separats per a Varnish 4 (varnish4.xml, varnishtest4.xml).
- Esmenar problemes de HiDPI
- vimode: No fallar si l'ordre &lt;c-e&gt; s'executa en el final d'un document. (error 350299)
- Implementar cadenes QML multilínia.
- Esmenar la sintaxi de «oors.xml»
- Afegir CartoCSS hl, per Lukas Sommer (error 340756)
- Esmenar HL de coma flotant, usar el Float incrustat com en C (error 348843)
- Dividir les direccions que s'han revertit (error 348845)
- Error 348317 - [PATCH] El ressaltat de sintaxi del Katepart cal que reconegui els estils d'escapament u0123 per a JavaScript (error 348317)
- Afegir *.cljs (error 349844)
- Actualització del fitxer de ressaltat GLSL.
- Esmenar els colors per defecte per a fer-los més distingibles

### KTextWidgets

- Suprimir el ressaltador antic

### Framework del KWallet

- Esmenar la construcció al Windows
- Imprimir un avís amb codi d'error quan falla l'obertura de la cartera des del PAM
- Retornar el codi d'error del dorsal en lloc de -1 quan falla l'obertura d'una cartera
- Fer que «xifrat desconegut» del dorsal siguin un codi de retorn negatiu
- Controlar el «PAM_KWALLET5_LOGIN» per al KWallet5
- Esmenar la fallada quan la comprovació de MigrationAgent::isEmptyOldWallet() falla
- El KWallet ara es pot desbloquejar des del PAM usant el mòdul «kwallet-pam»

### KWidgetsAddons

- API nova que pren els paràmetres del QIcon per definir les icones en la barra de pestanya
- KCharSelect: Esmenar la categoria Unicode i usar «boundingRect» per al càlcul de l'amplada
- KCharSelect: Esmenar l'amplada de la cel·la per ajustar-la al contingut
- Els marges KMultiTabBar ara són correctes en pantalles amb HiDPI
- KRuler: Fer obsolet KRuler::setFrameStyle() que no està implementat, netejar comentaris
- KEditListWidget: Eliminar marge, per tal d'alinear-se millor amb altres ginys

### KWindowSystem

- Lectura de dades NETWM més sòlida (error 350173)
- Mantenir per versions de les Qt més antigues, com en el kio-http
- S'han instal·lat capçaleres privades per connectors de plataforma.
- Les parts específiques de plataforma es carreguen com a connectors.

### KXMLGUI

- Esmenar el comportament del mètode KShortcutsEditorPrivate::importConfiguration

### Frameworks del Plasma

- Usant un gest de pinça, ara es pot canviar entre els diferents nivells de zoom del calendari
- Comentari quant a la duplicació de codi en «icondialog»
- El color de relleu del control lliscant estava al codi font, s'ha modificat per usar l'esquema de color
- Usar «QBENCHMARK» en lloc d'un requisit fix del rendiment de la màquina
- S'ha millorat significativament la navegació del calendari, proporcionant un resum d'un any i d'una dècada
- Ara el PlasmaCore.Dialog té una propietat «opacity»
- Fer espai per al botó d'opció
- No mostrar el fons circular si hi ha un menú
- Afegir la definició de X-Plasma-NotificationAreaCategory
- Establir que les notificacions i l'OSD es mostrin a tots els escriptoris
- Imprimir un avís útil quan no es pot aconseguir un KPluginInfo vàlid
- Esmenar una repetició infinita potencial a PlatformStatus::findLookAndFeelPackage()
- Reanomenar software-updates.svgz a software.svgz

### Sonnet

- Afegir codi al CMake per activar la construcció del connector Voikko.
- Implementar la factoria «Sonnet::Client» per als verificadors ortogràfics Voikko.
- Implementar el verificador ortogràfic basat en Voikko (Sonnet::SpellerPlugin)

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
