---
aliases:
- ../../kde-frameworks-5.9.0
date: '2015-04-10'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Mòdul nou: ModemManagerQt (embolcall de les Qt per a l'API del ModemManager). Alternativament, actualitzeu al Plasma-NM 5.3 Beta en actualitzar al ModemManagerQt 5.9.0.

### KActivities

- S'ha implementat oblidar un recurs
- Esmenes de construcció
- S'ha afegit un connector per registrar esdeveniments per a les notificacions de KRecentDocument

### KArchive

- Respectar l'opció KZip::extraField també en escriure les entrades de les capçaleres centrals
- S'han eliminat dues declaracions errònies, que ocorrien quan el disc és ple, error 343214

### KBookmarks

- Esmenar la construcció amb les Qt 5.5

### KCMUtils

- Usar un sistema de connectors nous basats en Json. Els KCM se cerquen sota kcms/. Per ara, els fitxers desktop encara cal instal·lar-los sota kservices5/ per compatibilitat
- Carrega i embolcalla la versió només en QML dels KCM si és possible

### KConfig

- Esmenar una declaració en l'ús del «KSharedConfig» en el destructor global d'objectes.
- kconfig_compiler: afegir la implementació per CategoryLoggingName en els fitxers *.kcfgc, per generar crides qCDebug(category).

### KI18n

- Precarregar el catàleg Qt global en usar i18n()

### KIconThemes

- El KIconDialog ara es pot mostrar usant els mètodes del QDialog show() i exec() normals
- Esmenar KIconEngine::paint per gestionar devicePixelRatios diferents

### KIO

- Habilitar el KPropertiesDialog per a mostrar també la informació d'espai lliure de sistemes de fitxers remots (p. ex. smb)
- Esmenar el KUrlNavigator amb mapes de píxels amb PPP alts
- Fer que el KFileItemDelegate gestioni «devicePixelRatios» no per defecte en les animacions

### KItemModels

- KRecursiveFilterProxyModel: S'ha refet per tal d'emetre els senyals correctes en el moment correcte
- KDescendantsProxyModel: Gestiona els moviments informats pel model d'origen.
- KDescendantsProxyModel: Esmenar el comportament quan es fa una selecció quan s'està reiniciant.
- KDescendantsProxyModel: Permetre la construcció i ús del KSelectionProxyModel des del QML.

### KJobWidgets

- Propagar el codi d'error a la interfície de D-Bus del JobView

### KNotifications

- S'ha afegit una versió de l'event() que no té cap icona i n'utilitzarà una per defecte
- S'ha afegit una versió de l'event() que té StandardEvent eventId i QString iconName

### KPeople

- Permetre l'ampliació de l'acció de les metadades usant tipus predefinits
- Esmenar el model que no s'actualitza adequadament després d'eliminar un contacte des de Persona

### KPty

- Exposar al món si el KPty s'ha construït amb la biblioteca utempter

### KTextEditor

- S'ha afegit el fitxer de ressaltat del kdesrc-buildrc
- Sintaxi: s'ha afegit la implementació per literals enters binaris en el fitxer de ressaltat del PHP

### KWidgetsAddons

- Suavitzar l'animació del KMessageWidget amb una relació alta de píxels del dispositiu

### KWindowSystem

- Afegir una implementació fictícia del Wayland per al «KWindowSystemPrivate»
- KWindowSystem::icon amb NETWinInfo no limitat a la plataforma X11.

### KXmlGui

- Mantenir el domini de les traduccions en fusionar fitxers .rc
- Esmenar l'avís en temps d'execució QWidget::setWindowModified: El títol de la finestra no conté cap variable de substitució «[*]»

### KXmlRpcClient

- Instal·lació de traduccions

### Frameworks del Plasma

- S'han esmenat els consells d'eina de la safata del sistema quan temporalment el propietari del consell ha desaparegut o ha esdevingut buit
- Esmenar la TabBar que inicialment no s'ha disposat adequadament, com es pot observar p. ex. en el Kickoff
- Les transicions del PageStack ara usen Animators per a fer més suaus les animacions
- Les transicions del TabGroup ara usen Animators per a fer més suaus les animacions
- Fa que Svg, FrameSvg treballi amb el QT_DEVICE_PIXELRATIO

### Solid

- Actualitzar les propietats de la bateria en reprendre

### Canvis en el sistema de construcció

- Els «Extra CMake Modules» (ECM) ara es versionen com els Frameworks del KDE, i per tant, ara és la 5.9 mentre que anteriorment era la 1.8.
- S'han esmenat molts frameworks perquè es puguin utilitzar sense cercar les seves dependències privades. És a dir, les aplicacions que cerquen un framework només necessiten les seves dependències públiques, no les privades.
- Permetre la configuració de SHARE_INSTALL_DIR, per a gestionar millor les disposicions multiarquitectura

### Integració del marc de treball

- Esmenar una possible fallada en destruir un QSystemTrayIcon (p. ex. activat per Trojita), error 343976
- Esmenar els diàlegs de fitxer modals natius en el QML, error 334963

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
