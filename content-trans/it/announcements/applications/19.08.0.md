---
aliases:
- ../announce-applications-19.08.0
changelog: true
date: 2019-08-15
description: KDE rilascia Applications 19.08.
layout: application
release: applications-19.08.0
title: KDE rilascia KDE Applications 19.08.0
version: '19.08'
version_number: 19.08.0
---
{{< peertube "/6443ce38-0a96-4b49-8fc5-a50832ed93ce" >}}

{{% i18n_date %}}

La comunità KDE è lieta di annunciare il rilascio di KDE Applications 19.08.

Questo rilascio fa parte del continuo sforzo di KDE per fornire agli utenti versioni dei programmi sempre aggiornate. Le nuove versioni di Applications contengono nuove funzioni e un software progettato meglio, che aumenta l'usabilità e la stabilità di applicazioni quali Dolphin, Konsole, Kate, Okular e di tutti i tuoi accessori KDE preferiti. Il nostro obiettivo è rendere il software KDE sempre più facile da usare e assicurarci che tu sia sempre produttivo durante il suo utilizzo.

Speriamo sinceramente che tutte le novità presenti in 19.08 ti piacciano.

## Novità in KDE Applications 19.08

Sono stati risolti più di 170 errori. Tali correzioni re-implementano funzionalità disabilitate, normalizzano scorciatoie e risolvono blocchi, per rendere le tue applicazioni più facili da utilizzare e permetterti di lavorare e giocare in maniera più produttiva.

### Dolphin

Dolphin è il gestore di file e cartelle di KDE che puoi avviare ora da qualunque posizione mediante la nuova scorciatoia globale <keycap>Meta + E</keycap>. È presente anche una nuova funzionalità che riduce il disordine nel tuo desktop. Quando Dolphin è già in esecuzione, se apri cartelle con altre applicazioni, esse verranno aperte in una nuova scheda della finestra esistente, anziché in una nuova finestra di Dolphin. Nota che ora questo comportamento è predefinito, ma può essere comunque disabilitato.

Il pannello delle informazioni (posizionato in modo predefinito alla destra del pannello principale di Dolphin) è stato variamente migliorato. Puoi, per esempio, scegliere di avviare automaticamente file multimediali quando li evidenzi nel panello principale, e ora puoi selezionare e copiare il testo contenuto al suo interno. Se vuoi cambiare quali informazioni esso debba mostrare, lo puoi fare direttamente nel pannello, dato che Dolphin non apre una finestra separata quando decidi di configurarlo.

Abbiamo risolto anche molte asperità e piccoli errori, lavorando per migliorare l'esperienza complessiva di uso del programma.

{{<figure src="/announcements/applications/19.08.0/dolphin_bookmark.png" alt=`Nuova funzionalità di Dolphin per i segnalibri` caption=`Nuova funzionalità di Dolphin per i segnalibri` width="600px" >}}

### Gwenview

Gwenview è il visore di immagini di KDE e in questo rilascio gli sviluppatori hanno migliorato globalmente la funzione di visualizzazione delle miniature. Gwenview ora può utilizzare una «modalità a basso consumo di risorse» che carica le miniature a bassa risoluzione (se disponibili). Questa nuova modalità è molto più veloce e più efficiente per le immagini JPEG e i file RAW. Nel caso in cui Gwenview non sia in grado di generare una miniatura per un'immagine, visualizzerà un'immagine segnaposto piuttosto che riutilizzare la miniatura dell'immagine precedente. Sono stati risolti i problemi che Gwenview aveva con la visualizzazione delle miniature dalle fotocamere Sony e Canon.

Oltre alle modifiche nella sezione delle miniature, Gwenview ha implementato anche un nuovo menu «Condividi» che permette di inviare le immagini a varie posizioni e di caricare e visualizzare correttamente i file nelle posizioni remote a cui si ha accesso tramite KIO. La nuova versione di Gwenview visualizza inoltre più metadati EXIF per le immagini RAW.

{{<figure src="/announcements/applications/19.08.0/gwenview_share.png" alt=`Il nuovo menu «Condividi» di Gwenview` caption=`Il nuovo menu «Condividi» di Gwenview` width="600px" >}}

### Okular

Gli sviluppatori hanno introdotto molti miglioramenti alle annotazioni in Okular, il visore di documenti di KDE. Oltre a un'interfaccia migliorata per le finestre di configurazione delle annotazioni, alle annotazioni con estremità sono state aggiunte varie infiorettature, per esempio permettendo loro di trasformarsi in frecce. Un'altra funzionalità aggiunta è la loro possibilità di espansione e contrazione in un colpo solo.

Il supporto di Okular per i documenti ePub è stato migliorato. Okular non va più in blocco quando tenta di caricare file ePub non validi, e le sue prestazioni con file ePub grandi è molto migliorata. L'elenco delle modifiche in questo rilascio include i bordi delle pagine e lo strumento Evidenziatore nella modalità Presentazione in modalità High DPI.

{{<figure src="/announcements/applications/19.08.0/okular_line_end.png" alt=`Le impostazioni dello strumento di annotazione di Okular con la nuova opzione di estremità` caption=`Le impostazioni dello strumento di annotazione di Okular con la nuova opzione di estremità` width="600px" >}}

### Kate

Grazie ai nostri sviluppatori, sono stati rimossi tre fastidiosi bug in questa versione dell'editor di testo avanzato di KDE. Kate ora porta di nuovo in primo piano la sua finestra esistente quando gli viene richiesto di aprire un nuovo documento da un'altra applicazione. La funzionalità di «apertura rapida» elenca gli elementi in base a quelli usati più di recente e li preseleziona in cima all'elenco. La terza modifica è nella funzionalità «Documenti recenti», che ora funziona quando la configurazione corrente è impostata per non salvare le impostazioni delle finestre singole.

### Konsole

La novità più importante in Konsole, l'emulatore di terminale di KDE, è il rilancio della funzionalità di affiancamento dei pannelli. Ora puoi dividere il pannello principale in quante parti vuoi, sia verticalmente sia orizzontalmente. I pannelli secondari possono essere ulteriormente divisi, in base alle tue esigenze. Questa versione aggiunge anche la capacità di trascinare e rilasciare i pannelli, in modo che tu possa riordinare facilmente la loro disposizione in base alle tue esigenze di lavoro.

Inoltre, la finestra Impostazioni è stata riorganizzata in modo da apparire più chiara e facile da usare.

{{< video src-webm="/announcements/applications/19.08.0/konsole-tabs.webm" >}}

### Spectacle

Spectacle è l'applicazione di KDE per le schermate e a ogni versione riceve funzionalità sempre più interessanti. Questa versione non fa eccezione, e possiede nuove opzioni relative alla funzione Ritardo. Durante la cattura di una schermata, Spectacle visualizzerà il tempo rimanente nella sua finestra del titolo. Questa informazione è visibile pure nel suo elemento all'interno del Gestore dei processi.

Sempre relativamente alla funzione Ritardo, anche il pulsante del Gestore dei processi di Spectacle mostrerà una barra di avanzamento, in modo da poter tenere traccia di quando la schermata viene acquisita. Per finire, se non minimizzi l'applicazione durante l'attesa, noterai che il pulsante «Acquisisci una nuova schermata» cambia in «Annulla». Anche questo pulsante contiene una barra di avanzamento che ti offre l'opportunità di fermare il conto alla rovescia.

Anche il salvataggio delle schermate possiede una nuova opzione. Dopo che hai salvato una schermata, Spectacle mostra un messaggio al suo interno che ti permette di aprire la schermata o la cartella in cui è contenuta.

{{< video src-webm="/announcements/applications/19.08.0/spectacle_progress.webm" >}}

### Kontact

Kontact, la suite di programmi groupware generale e contatti/calendario/posta elettronica di KDE ora offre le faccine (emoji) Unicode e il supporto per il linguaggio Markdown nel compositore dei messaggi. La nuova versione di KMail ti permetterà non solo di creare messaggi dall'aspetto migliore, ma grazie all'integrazione dei correttori grammaticali, quali LanguageTool e Grammalecte, ti aiuterà anche a controllare e correggere il tuo testo.

{{<figure src="/announcements/applications/19.08.0/kontact_emoji.png" alt=`Selettore di faccine` caption=`Selettore di faccine` width="600px" >}}

{{<figure src="/announcements/applications/19.08.0/kmail_grammar.png" alt=`Integrazione del correttore grammaticale in KMail` caption=`Integrazione del correttore grammaticale in KMail` width="600px" >}}

Quando pianifichi eventi, i messaggi di invito in KMail non vengono più eliminati dopo aver inviato una risposta. Ora è possibile spostare un evento da un calendario a un altro nell'editor degli eventi in KOrganizer.

Infine, ma non meno importante: KAddressBook ora può inviare SMS ai contatti mediante KDE Connect, migliorando così l'integrazione tra il tuo desktop e i dispositivi mobili.

### Kdenlive

La nuova versione di Kdenlive, il programma di editing video di KDE, possiede un nuovo set di combinazioni tastiera-mouse che ti aiuterà a essere più produttivo. Puoi, per esempio, cambiare la velocità di una clip nella linea temporale premendo CTRL e poi trascinare sulla clip, oppure attivare l'anteprima delle miniature delle clip video tenendo premuto Maiusc e spostando il mouse sulla miniatura della clip nel contenitore del progetto. Gli sviluppatori hanno anche lavorato molto sull'usabilità creando operazioni di modifica a tre punti coerenti con gli altri editor video, che sicuramente apprezzerai se stai migrando da un altro editor a Kdenlive.

{{<figure src="https://cdn.kde.org/screenshots/kdenlive/19-08.png" alt=`Kdenlive 19.08.0` caption=`Kdenlive 19.08.0` width="600px" >}}
