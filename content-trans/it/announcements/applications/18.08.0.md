---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE rilascia KDE Applications 18.08.0
layout: application
title: KDE rilascia KDE Applications 18.08.0
version: 18.08.0
---
16 agosto 2018. KDE rilascia KDE Applications 18.08.0.

Lavoriamo continuamente per migliorare il software incluso nelle nostre serie di KDE Application, confidiamo che troverai utili tutti i miglioramenti e la risoluzione degli errori.

### Novità in KDE Applications 18.08

#### Sistema

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, il gestore di file completo di KDE, contiene molti miglioramenti:

- La finestra di dialogo «Impostazioni» è stata modernizzata per meglio seguire le nostre linee guida di design ed essere più intuitiva.
- Sono state eliminate diverse perdite di memoria che rallentavano il computer.
- Gli elementi di menu «Crea nuovo» non sono più disponibili nella vista del cestino.
- L'applicazione ora si adatta meglio agli schermi ad alta risoluzione.
- Il menu contestuale ora include altre opzioni utili, consentendoti di ordinare e cambiare direttamente la modalità di visualizzazione.
- L'ordinamento in base all'ora di modifica è ora 12 volte più veloce. Inoltre ora puoi avviare di nuovo Dolphin quando hai eseguito l'accesso con l'account dell'utente root. Il supporto per la modifica dei file di proprietà root, quando utilizzi Dolphin come utente normale, è ancora in fase di sviluppo.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Sono disponibili vari miglioramenti per <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, l'emulatore di terminale di KDE:

- L'oggetto «Trova» ora appare nella parte superiore della finestra, in modo da non disturbare il tuo lavoro.
- È stato aggiunto il supporto per più sequenze escape (DECSCUSR e XTerm Alternate Scroll Mode).
- Ora puoi assegnare qualsiasi carattere come tasto per una scorciatoia.

#### Grafica

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 è un rilascio principale per <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, il visualizzatore e organizzatore di immagini di KDE. Negli ultimi mesi i collaboratori hanno lavorato su una marea di miglioramenti. Le novità includono:

- La barra di stato di Gwenview ora presenta un contatore di immagini e mostra il numero totale delle immagini.
- Ora è possibile ordinare per valutazione e in ordine discendente. L'ordinamento per data ora separa le cartelle e gli archivi ed è stato corretto in alcune situazioni.
- Il supporto del drag-and-drop è stato migliorato e consente sia il trascinamento dei file e delle cartelle in modalità Vista per la loro visualizzazione, sia il trascinamento degli elementi visualizzati in applicazioni esterne.
- L'incollatura delle immagini copiate da Gwenview ora funziona anche per le applicazioni che accettano solo immagini con dati grezzi (raw), ma non percorsi di file. Ora è supportata anche la copia delle immagini modificate.
- La finestra di dialogo di ridimensionamento delle immagini è stata riprogettata per una migliore usabilità e per aggiungere un'opzione per il ridimensionamento delle immagini in base alla percentuale.
- Sono stati corretti degli errori relativi al mirino di puntamento e al cursore per la dimensione dello strumento Riduzione degli occhi rossi.
- La selezione dello sfondo trasparente ora ha un'opzione per «Nessuno»  (sfondo) e può essere configurata anche per il formato SVG.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

L'ingrandimento delle immagini è diventato più comodo:

- L'ingrandimento è abilitato per scorrimento o clic, così come la panoramica, anche quando sono attivi gli strumenti Ritaglio o Riduzione degli occhi rossi.
- Il clic del pulsante centrale commuta di nuovo tra l'ingrandimento adattativo e 100%.
- Sono state aggiunte le scorciatoie di tastiera Maiusc-clic centrale e Maiusc-F per attivare/disattivare l'ingrandimento adattativo.
- Ctrl-clic ora ingrandisce in modo più veloce e affidabile.
- Gwenview ora ingrandisce alla posizione corrente del cursore per le operazioni Ingrandisci, Rimpicciolisci, Adattativo e 100%, quando usi il mouse e le scorciatoie di tastiera.

La modalità di confronto tra le immagini è stata ulteriormente migliorata:

- Corrette la dimensione e l'allineamento dell'evidenziazione della selezione.
- Corretto gli SVG che si sovrappongono all'evidenziazione della selezione.
- Per immagini SVG piccole, l'evidenziazione della selezione corrisponde alla dimensione dell'immagine.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Sono state introdotte diverse piccole migliorie per rendere il tuo lavoro ancora più divertente:

- Sono migliorate le transizione di sfumatura tra le immagini di diverse dimensioni e trasparenze.
- È stata corretta la visibilità delle icone in alcuni pulsanti mobili, durante l'uso di uno schema di colori chiaro.
- Quando salvi un'immagine con nuovo nome, il visore poi non passa a un'immagine non correlata.
- Quando premi il pulsante Condividi e non sono installate le estensioni kipi, Gwenview ti chiede di installarle. Dopo l'installazione esse sono visualizzate subito.
- La barra laterale ora impedisce di essere nascosta accidentalmente durante il ridimensionamento e ricorda la sua larghezza.

#### Ufficio

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, il programma completo di posta di KDE, presenta miglioramenti nel motore di estrazione dei dati di viaggio. Ora supporta i codici a barre dei treni UIC 918.3 e SNCF e la ricerca delle stazioni ferroviarie con Wikidata. È stato aggiunto supporto per gli itinerari per più viaggiatori, e KMail ora si integra con l'app KDE Itinerary.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, l'infrastruttura di gestione delle informazioni personali, è ora più veloce grazie ai carichi sulle notifiche, e ha il supporto XOAUTH per SMTP, consentendo l'autenticazione nativa con Gmail.

#### Istruzione

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, l'interfaccia KDE per il software matematico, ora salva lo stato dei pannelli (\«Variabili\», \«Aiuto\», ecc.) separatamente per ciascuna sessione. Le sessioni di Julia ora si possono creare molto più velocemente.

L'esperienza utente in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, il nostro calcolatore di grafici, è stata molto migliorata per i dispositivi a tocco.

#### Accessori

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

I collaboratori di <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, il versatile strumento di KDE per le schermate, si sono concentrati sul miglioramento della modalità Regione rettangolare:

- Nella modalità Regione rettangolare ora esiste una lente d'ingrandimento che ti aiuta a disegnare una perfetta selezione rettangolare basandosi sui pixel.
- Ora puoi spostare e ridimensionare la selezione rettangolare utilizzando la tastiera.
- L'interfaccia utente segue lo schema di colori dell'utente ed è stata migliorata la presentazione del testo di aiuto.

I collegamenti ora sono copiati automaticamente negli appunti, in modo da condividere più facilmente le tue schermate. Queste ora possono essere salvate automaticamente nelle sotto-cartelle specifiche dell'utente.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, il nostro registratore per webcam, è stato aggiornato in modo da evitare blocchi con le versioni più recenti di GStreamer.

### Ricerca e risoluzione degli errori

Sono stati corretti oltre 120 errori nelle applicazioni, tra cui la suite Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello e altre!

### Elenco completo dei cambiamenti
