---
aliases:
- ../announce-applications-17.12.0
date: 2017-12-14
description: KDE rilascia KDE Applications 17.12.0
layout: application
title: KDE rilascia KDE Applications 17.12.0
version: 17.12.0
---
 14 dicembre 2017. KDE rilascia KDE Applications 17.12.0.

Lavoriamo continuamente per migliorare il software incluso nelle nostre serie di KDE Application, confidiamo che troverai utili tutti i miglioramenti e la risoluzione degli errori.

### Novità in KDE Applications 17.12

#### Sistema

{{<figure src="/announcements/applications/17.12.0/dolphin1712.gif" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, il nostro gestore di file, ora può salvare le ricerche e limitare la ricerca alle sole cartelle. La rinomina di file ora è più semplice; basta fare doppio clic sul nome del file. Ora hai a disposizione più informazioni, in quanto la data di modifica e l'URL di origine dei file scaricati ora sono visualizzati nel pannello delle informazioni. Sono state introdotte, in aggiunta, le nuove colonne Genere, Bitrate e Anno di pubblicazione.

#### Grafica

Al nostro ottimo visore di documenti <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a> è stato aggiunto il supporto per gli schermi HiDPI e il linguaggio Markdown, e la resa di documenti lenti da scaricare ora è mostrata in maniera progressiva. È ora disponibile un'opzione per condividere un documento tramite posta elettronica.

Il visualizzatore di immagini <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a> ora è in grado di aprire ed evidenziare le immagini nel gestore dei file, l'ingrandimento è più morbido, la navigazione tramite tastiera è stata migliorata e ora supporta i formati FITS e Truevision TGA. Le immagini sono ora protette dalla rimozione accidentale col tasto Canc se non sono selezionate.

#### Multimedia

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a> ora usa meno memoria durante la gestione dei progetti video che includono molte immagini, i profili proxy predefiniti sono stati migliorati, ed è stato risolto un fastidioso errore collegato al salto di un secondo in avanti durante la riproduzione all'indietro.

#### Accessori

{{<figure src="/announcements/applications/17.12.0/kate1712.png" width="600px" >}}

È stato migliorato il supporto zip di <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> nel motore libzip. <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a> ha una nuova <a href='https://frinring.wordpress.com/2017/09/25/ktexteditorpreviewplugin-0-1-0/'>estensione per l'anteprima</a> che ti consente di vedere un'anteprima in diretta del documento di testo nel suo formato finale, che applica tutte le estensioni disponibili di KParts (per es. per Markdown, SVG, grafici Dot, UI Qt o patch). Questa estensione funziona anche in <a href='https://www.kde.org/applications/development/kdevelop/'>KDevelop.</a>

#### Sviluppo

{{<figure src="/announcements/applications/17.12.0/kuiviewer1712.png" width="600px" >}}

<a href='https://www.kde.org/applications/development/kompare/'>Kompare</a> ora fornisce un menu di contesto nell'area diff, permettendo un rapido accesso alle azione di navigazione o di modifica. Se sei uno sviluppatore, potrai trovare utile l'anteprima nel nuovo pannello di KUIViewer di oggetti UI descritti da file Qt UI (oggetti, finestre di dialogo, ecc.). Inoltre ora supporta l'API streaming di KParts.

#### Ufficio

La squadra di <a href='https://www.kde.org/applications/office/kontact'>Kontact</a> ha lavorato duramente sulle migliorie e i ritocchi. È stato fatto molto lavoro per modernizzare il codice, e gli utenti noteranno un miglioramento dei messaggi cifrati e l'aggiunta di supporto per text/pgp e <a href='https://phabricator.kde.org/D8395'>Apple® Wallet Pass</a>. Ora esiste un'opzione per selezionare la cartella IMAP durante la configurazione per le assenze, un nuovo avviso in KMail quando un messaggio viene riaperto e l'identità/mailtransport non è la stessa, nuovo <a href='https://micreabog.wordpress.com/2017/10/05/akonadi-ews-resource-now-part-of-kde-pim/'>supporto per Microsoft® Exchange™</a>, supporto per Nylas Mail e importazione Geary migliorata nella procedura guidata di importazione akonadi, insieme con altre varie risoluzioni di errori e miglioramenti generali.

#### Giochi

{{<figure src="/announcements/applications/17.12.0/ktuberling1712.jpg" width="600px" >}}

<a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a> ora può raggiungere un pubblico più vasto, in quanto è stato <a href='https://tsdgeos.blogspot.nl/2017/10/kde-edu-sprint-in-berlin.html'>portato su Android</a>. <a href='https://www.kde.org/applications/games/kolf/'>Kolf</a>, <a href='https://www.kde.org/applications/games/ksirk/'>KsirK</a> e <a href='https://www.kde.org/applications/games/palapeli/'>Palapeli</a> completano la conversione dei giochi KDE a Frameworks 5.

### Più conversioni a KDE Frameworks 5

Altre applicazioni che erano basate su kdelibs4 ora sono state convertite a KDE Frameworks 5. Queste includono il riproduttore musicale <a href='https://www.kde.org/applications/multimedia/juk/'>JuK</a>, il gestore degli scaricamenti <a href='https://www.kde.org/applications/internet/kget/'>KGet</a>, <a href='https://www.kde.org/applications/multimedia/kmix/'>KMix</a>, accessori quali <a href='https://www.kde.org/applications/utilities/sweeper/'>Sweeper</a> e <a href='https://www.kde.org/applications/utilities/kmouth/'>KMouth</a>, <a href='https://www.kde.org/applications/development/kimagemapeditor/'>KImageMapEditor</a> e Zeroconf-ioslave. Un enorme ringraziamento al duro sforzo degli sviluppatori che hanno dedicato parte del loro tempo e lavoro per rendere questo possibile!

### Applicazioni che passano a un proprio programma di rilascio indipendente

<a href='https://www.kde.org/applications/education/kstars/'>KStars</a> ora possiede un proprio programma di rilascio indipendente; consulta questo <a href='https://knro.blogspot.de'>blog di uno sviluppatore</a> per gli annunci. Vale la pena notare che diverse applicazioni come Kopete e Blogilo <a href='https://community.kde.org/Applications/17.12_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>non sono più comprese</a> nelle serie Application, dato che non sono state ancora convertite a KDE Frameworks 5, o non sono al momento attivamente sviluppate.

### Ricerca e risoluzione degli errori

Gli oltre 110 errori corretti in applicazioni tra cui la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello e altre!

### Elenco completo dei cambiamenti

Se vuoi leggere avere altre informazioni sulle modifiche in questo rilascio, <a href='/announcements/changelogs/applications/17.12.0'>consulta l'elenco completo dei cambiamenti</a>. Sebbene non sia facile da affrontare per la sua vastità, l'elenco dei cambiamenti è un metodo eccellente per imparare il funzionamento interno di KDE e scoprire le applicazioni e le funzionalità che possiedi ma non hai mai saputo di avere.
