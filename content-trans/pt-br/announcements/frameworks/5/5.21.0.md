---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Novo framework: KActivitiesStats, uma biblioteca para acessar dados de estatísticas de utilização obtidos pelo gerenciador de atividades do KDE.

### Todos os frameworks

O Qt &gt;= 5.4 é agora obrigatório, ou seja, não há mais suporte para o Qt 5.3.

### Attica

- Adição da variante <i>const</i> ao método <i>getter</i>

### Baloo

- Centralização do tamanho do lote na configuração
- Remoção da indexação de bloqueio do código de arquivos 'text/plain' sem extensão .txt (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358098'>358098</a>)
- Verificação do nome e do conteúdo do arquivo para determinar o tipo MIME (erro <a href='https://bugs.kde.org/show_bug.cgi?id=353512'>353512</a>)

### BluezQt

- ObexManager: Divisão das mensagens de erro para os objetos ausentes

### Ícones Breeze

- Adição de ícones Breeze para o Lokalize
- Sincronização dos ícones dos aplicativos entre o Breeze e o Breeze Escuro
- Atualização dos ícones dos temas e remoção dos grupos do Kicker para correção dos ícones dos aplicativos no sistema
- Adição do suporte ao <i>.xpi</i> para extensões do Firefox (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359913'>359913</a>)
- Utilização do ícone correto do Okular
- Adição do suporte ao ícone do aplicativo Ktnef
- Adição de ícones para o KMenueditor, KMouse e KNotes
- Alteração do ícone de volume silenciado - usar o "mute" (sem som) em vez de apenas ficar em vermelho (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360953'>360953</a>)
- Adição do suporte para os tipos MIME para arquivos <i>djvu</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360136'>360136</a>)
- Adição de link em vez de um item duplo
- Adição do ícone <i>ms-shortcut</i> para o Gnucash (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360776'>360776</a>)
- Altearação do fundo do papel de parede para um genérico
- Atualização dos ícones para usar um papel de parede genérico
- Adição de ícone para o Konqueror (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360304'>360304</a>)
- Adição de ícone de processo-em-andamento para a animação do progresso no KDE (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360304'>360304</a>)
- Adição de ícone de instalação de software e correção da cor do ícone de atualizações
- Adição de ícones para adição e remoção de emblemas para a seleção, adição e montagem no Dolphin
- Remoção da folha de estilo dos ícones dos miniaplicativos de relógio analógico e <i>kickerdash</i>
- Sincronização do Breeze e Breeze Escuro (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360294'>360294</a>)

### Módulos extra do CMake

- Correção do _ecm_update_iconcache para apenas atualizar o local de instalação
- Reversão do "ECMQtDeclareLoggingCategory: Inclusão do &lt;QDebug&gt; com o arquivo gerado"

### Integração do Framework

- Alternativa para a implementação do QCommonStyle do <i>standardIcon</i>
- Definição de um tempo limite padrão para fechamento do menu

### KActivities

- Remoção das validações do compilador, agora que todos os frameworks necessitam do C++11
- Remoção do QML ResourceModel, por ter sido substituído pelo KAStats::ResultModel
- A inserção em um QFlatSet vazio devolveu um iterador inválido

### KCodecs

- Simplificação do código (<b>qCount -&gt; std::count</b>, melhorias com <b>isprint -&gt; QChar::isPrint</b>)
- Detecção da codificação: correção de falhas inesperadas na utilização incorreta do <i>isprint</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357341'>357341</a>)
- Correção de falhas inesperadas por variável não inicializada (erro <a href='https://bugs.kde.org/show_bug.cgi?id=357341'>357341</a>)

### KCompletion

- KCompletionBox: forçar uma janela sem moldura e não atribuir o foco
- O KCompletionBox *não* deve ser uma dica (tooltip)

### KConfig

- Adição do suporte para a obtenção de localizações do QStandardPaths dentro de arquivos <i>.desktop</i>

### KCoreAddons

- Correção do <i>kcoreaddons_desktop_to_json()</i> no Windows
- src/lib/CMakeLists.txt - correção da compilação com uma biblioteca do <i>Threads</i>
- Adição de rotinas substitutas para permitir a compilação no Android

### KDBusAddons

- Evitar a introspecção de uma interface de D-Bus quando não é usada

### KDeclarative

- Uniformização do uso do <i>std::numeric_limits</i>
- [DeclarativeDragArea] Não substituir o "texto" dos dados MIME

### KDELibs 4 Support

- Correção de link obsoleto no Docbook do <b>kdebugdialog5</b>
- Não deixar o Qt5::Network como biblioteca obrigatória no resto das ConfigureChecks

### KDESU

- Definição de macros de funcionalidades para permitir a compilação na <i>libc</i> do <i>musl</i>

### KEmoticons

- KEmoticons: Correção de falha inesperada quando o <i>loadProvider</i> não funciona por alguma razão

### KGlobalAccel

- Possibilidade de finalizar corretamente o <i>kglobalaccel5</i>, corrigindo um desligamento extremamente lento

### KI18n

- O uso dos idiomas da localização regional do sistema Qt como alternativa em sistemas não-UNIX

### KInit

- Limpeza e remodelação da versão para <i>xcb</i> do <i>klauncher</i>

### KIO

- FavIconsCache: Sincronização após a escrita, para que os outros aplicativos possam vê-la e para evitar falhas inesperadas em caso de destruição
- Correcção de muitas questões relacionadas com múltiplas tarefas no KUrlCompletion
- Correção da falha inesperada na janela de mudança de nome (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360488'>360488</a>)
- KOpenWithDialog: Melhoria do texto do título e da descrição da janela (erro <a href='https://bugs.kde.org/show_bug.cgi?id=359233'>359233</a>)
- Possibilidade de melhor instalação multiplataforma dos <i>ioslaves</i>, através do envio da informação do protocolo nos metadados do plugin

### KItemModels

- KSelectionProxyModel: Simplificação do tratamento da remoção de linhas e da lógica de remoção da seleção
- KSelectionProxyModel: Recriação do mapeamento na remoção somente se necessário (erro <a href='https://bugs.kde.org/show_bug.cgi?id=352369'>352369</a>)
- KSelectionProxyModel: Só limpar as associações <i>firstChild</i> no nível superior
- KSelectionProxyModel: Garantir o envio de sinais adequados ao remover a última seleção
- Possibilidade de pesquisar no <i>DynamicTreeModel</i> por papel de visualização

### KNewStuff

- Não falhar inesperadamento se os arquivos <i>.desktop</i> não existirem ou estiverem danificados

### KNotification

- Tratamento do botão esquerdo do mouse nos ícones antigos da área de notificação (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358589'>358589</a>)
- Uso do X11BypassWindowManagerHint apenas na plataforma X11

### Package Framework

- Depois de instalar um pacote, carregá-lo
- Se o pacote existir e estiver atualizado, não falhar
- Adição do Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Definição da URI do contato como URI da pessoa no PersonData, quando não existir nenhuma pessoa
- Definição de um nome para a conexão com o banco de dados

### KRunner

- Importação do modelo de módulos de execução no <i>KAppTemplate</i>

### KService

- Correção do novo aviso do <i>kbuildsycoca</i>, quando um tipo MIME herda de um nome alternativo (alias)
- Correção do tratamento do x-scheme-handler/* no <i>mimeapps.list</i>
- Correção do tratamento do x-scheme-handler/* no processamento do <i>mimeapps.list</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358159'>358159</a>)

### KTextEditor

- Reversão do "Página de configuração do Abrir/salvar: Uso do termo "Pasta" em vez de "Diretório""
- Forçar o uso do UTF-8
- Página de configuração do Abrir/salvar: Uso do termo "Pasta" em vez de "Diretório"
- kateschemaconfig.cpp: Uso dos filtros corretos nas caixas de diálogo de abertura/gravação (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343327'>343327</a>)
- c.xml: uso de estilo padrão para as palavras-chave de controle de fluxo
- isocpp.xml: uso do estilo padrão "dsControlFlow" para as palavras-chave de controle de fluxo
- c/isocpp: adição de mais tipos-padrão em C
- O KateRenderer::lineHeight() devolve um inteiro
- impressão: uso do tamanho da fonte do esquema de impressão selecionado (erro <a href='https://bugs.kde.org/show_bug.cgi?id=356110'>356110</a>)
- Aceleração do cmake.xml: Uso do WordDetect em vez do RegExpr
- Alteração do tamanho das tabulações de 8 para 4
- Correção da mudança da cor do número da linha atual
- Correção da seleção do item de completação com o mouse (erro <a href='https://bugs.kde.org/show_bug.cgi?id=307052'>307052</a>)
- Adição do realce de sintaxe para o <i>gcode</i>
- Correção da pintura do fundo da seleção do MiniMap
- Correção da codificação do <i>gap.xml</i> (uso do UTF-8)
- Correção dos blocos de comentários encadeados (erro <a href='https://bugs.kde.org/show_bug.cgi?id=358692'>358692</a>)

### KWidgetsAddons

- Consideerar as margens do conteúdo ao calcular as sugestões de tamanho

### KXMLGUI

- Correção da edição das barras de ferramentas que perdiam as ações associadas

### NetworkManagerQt

- ConnectionSettings: Inicialização do tempo limite do contato com o <i>gateway</i>
- Novo TunSetting e novo tipo de conexão Tun
- Criação de dispositivos para todos os tipos conhecidos

### Ícones do Oxygen

- Instalação do <i>index.theme</i> na mesma pasta onde sempre esteve
- Instalação no <i>oxygen/base/</i>, para que a mudança de local dos ícones dos aplicativos não conflitem com a versão instalada por esses aplicativos
- Replicação dos links simbólicos dos ícones Breeze
- Adição de novos ícones para adicionar e remover emblemas na sincronização com o Breeze

### Plasma Framework

- [calendário] Correção do miniaplicativo de calendário que não limpava a seleção quando ficava oculto (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360683'>360683</a>)
- Atualização do ícone do áudio para usar as folhas de estilo
- Atualização do ícone do áudio sem som (erro <a href='https://bugs.kde.org/show_bug.cgi?id=360953'>360953</a>)
- Correção da criação forçada dos miniaplicativos quando o Plasma está inalterável
- [Nó que desaparece] Não misturar a opacidade separadamente (erro <a href='https://bugs.kde.org/show_bug.cgi?id=355894'>355894</a>)
- [Svg] Não reprocessar a configuração em resposta ao Theme::applicationPaletteChanged
- Dialog: Definição dos estados do SkipTaskbar/Pager antes de mostrar a janela (erro <a href='https://bugs.kde.org/show_bug.cgi?id=332024'>332024</a>)
- Reintrodução da propriedade <i>busy</i> (ocupado) no miniaplicativo
- Confirmar se o arquivo de exportação do PlasmaQuick é encontrado de forma adequada
- Não importar um layout inexistente
- Possibilidade de um miniaplicativo oferecer um objeto de testes
- Substituição do QMenu::exec por QMenu::popup
- FrameSvg: Correção de ponteiros órfãos no <i>sharedFrames</i> ao alterar o tema
- IconItem: Agendar uma atualização da imagem ao alterar a janela
- IconItem: Animação das mudanças para ativo mesmo com as animações desativadas
- DaysModel: Transformação do <i>update</i> em um <i>slot</i>
- [Item de ícone] Não animar a partir da imagem anterior quando a mesma estava invisível
- [Item de ícone] Não chamar o <i>loadPixmap</i> no <i>setColorGroup</i>
- [Applet] Não substituir a opção "Persistente" na notificação "Desfazer"
- Possibilidade de substituição da definição de mutabilidade do Plasma na criação de um contêiner
- Adição do <i>icon/titleChanged</i>
- Remoção de dependência do QtScript
- O cabeçalho do plasmaquick_export.h está na pasta 'plasmaquick'
- Instalação de alguns cabeçalhos do <i>plasmaquick</i>

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
