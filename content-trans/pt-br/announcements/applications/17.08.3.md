---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: O KDE disponibiliza o KDE Applications 17.08.3
layout: application
title: O KDE disponibiliza o KDE Applications 17.08.3
version: 17.08.3
---
9 de Novembro de 2017. Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../17.08.0'>Aplicações do KDE 17.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

A cerca de uma dúzia de correcções de erros registadas incluem as melhorias no Kontact, no Ark, no Gwenview, no KGpg, no KWave, no Okular, no Spectacle, entre outros.

Esta versão também inclui a última versão da Plataforma de Desenvolvimento do KDE 4.14.38.

As melhorias incluem:

- Solução alternativa para uma regressão do Samba 4.7 relacionada a compartilhamentos SMB protegidos por senha
- O Okular não crasha mais após certas tarefas de rotação
- O Ark preserva datas de modificação de arquivos ao extrair arquivos ZIP
