---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: O KDE Lança as Aplicações do KDE 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: O KDE disponibiliza o KDE Applications 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Hoje o KDE lançou a primeira actualização de estabilidade para as <a href='../19.04.0'>Aplicações do KDE 19.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As cerca de vinte correcções de erros registadas inclui melhorias no Kontact, no Ark, no Cantor, no Dolphin, no Kdenlive, no Spectacle, no Umbrello, entre outros.

As melhorias incluem:

- A marcação de ficheiros no ecrã não corta mais o nome da marca
- Foi corrigido um estoiro no 'plugin' de partilha de texto do KMail
- Foram corrigidas diversas regressões no editor de vídeos Kdenlive
