---
aliases:
- ../announce-applications-14.12.3
changelog: true
date: '2015-03-03'
description: O KDE Lança as Aplicações do KDE 14.12.3.
layout: application
title: O KDE disponibiliza o KDE Applications 14.12.3
version: 14.12.3
---
3 de Março de 2015. Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../14.12.0'>Aplicações do KDE 14.12</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

Com de 19 correções de erros registradas, também inclui melhorias no anagrama do jogo Kanagram, no Modelador de UML Umbrello, no visualizador de documentos Okular e no aplicativo de geometria Kig.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.17, a Plataforma de Desenvolvimento do KDE 4.14.6 e o pacote Kontact 4.14.6.
