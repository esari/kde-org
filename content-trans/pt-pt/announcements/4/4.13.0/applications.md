---
date: '2014-04-16'
description: O KDE Lança a Versão 4.13 das Aplicações e da Plataforma.
hidden: true
title: As Aplicações do KDE 4.13 Beneficiam da Nova Pesquisa Semântica, Introduzem
  Novas Funcionalidades
---
A Comunidade do KDE orgulha-se em anunciar as últimas grandes actualizações das Aplicações do KDE, que trazem novas funcionalidades e correcções. O Kontact (o gestor de informações pessoais) foi o assunto de uma grande actividade, beneficiando das melhorias à tecnologia de Pesquisa Semântica do KDE e trazendo novas funcionalidades. O visualizador de documentos Okular e o editor de texto avançado Kate trouxeram também novas funcionalidades e mudanças na interface. Nas áreas dos jogos e da educação, introduziu-se o novo treinador de pronúncias em línguas estrangeiras Artikulate; o Marble (o globo virtual) trouxe o suporte para o Sol, a Lua, os planetas, para trajectos em bicicleta e para milhas náuticas. O Palapeli (a aplicação de jogos de 'puzzles') atingiu novas dimensões e capacidades.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## O Kontact do KDE Introduz Novas Funcionalidades e Mais Velocidade

O pacote Kontact do KDE introduz uma série de funcionalidades nos seus vários componentes. O KMail introduz o armazenamento na 'cloud' e um suporte melhorado de Sieve para a filtragem do lado do servidor. O KNotes consegue agora gerar alarmes e introduz funcionalidades de pesquisa, tendo também ocorrido algumas melhorias à camada da 'cache' de dados no Kontact, acelerando quase todas as operações.

### Suporte para Armazenamento na 'Cloud'

O KMail introduz o suporte do serviço de armazenamento, de modo que os anexos grandes possam ser guardados em serviços na 'cloud' e incluídos como hiperligações nas mensagens de e-mail. Os suportes de armazenamento incluem o Dropbox, o Box, o KolabServer, o YouSendIt, o UbuntuOne, o Hubic e existe uma opção de WebDAV genérico. Uma ferramenta chamada <em>storageservicemanager</em> ajuda-o com a gestão dos ficheiros nesses serviços.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Suporte para Sieve Muito Melhorado

Os filtros do Sieve, uma tecnologia para deixar que o KMail trate dos filtros no servidor, consegue agora lidar com o suporte para férias em vários servidores. A ferramenta KSieveEditor permite aos utilizadores editarem os filtros em Sieve sem ter de adicionar o servidor ao Kontact.

### Outras Alterações

A barra de filtragem rápida tem uma pequena melhoria na interface e beneficia em grande medida das capacidades de pesquisa melhoradas que foram introduzidas na versão 4.13 da Plataforma de Desenvolvimento do KDE. A pesquisa tornou-se significativamente mais rápida e mais fiável. O compositor introduziu um redutor de URL's, aumentando a tradução existente e as ferramentas de excertos de texto.

As marcas e as anotações dos dados PIM são agora guardadas no Akonadi. Nas versões futuras, serão também guardadas nos servidores (em IMAP ou no Kolab), possibilitando a partilha de marcas entre vários computadores. Akonadi: foi adicionado o suporte para a API do Google Drive. Existe o suporte para pesquisar com 'plugins' de terceiros (o que significa que os resultados podem ser obtidos muito rapidamente) e a pesquisa do lado do servidor (a pesquisa de itens não indexados por nenhum serviço local).

### KNotes, KAddressbook

Foi feito algum investimento significativo no KNotes, corrigindo uma série de erros e pequenos incómodos. A capacidade de definir alarmes para as notas é nova, assim como a pesquisa sobre as notas. Leia mais <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aqui</a>. O KAddressbook ganhou algum suporte para impressão: pode ver mais detalhes <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aqui</a>.

### Melhorias na Performance

A performance do Kontact foi melhorada de forma notória nesta versão. Algumas melhorias devem-se à integração com a nova versão da infra-estrutura de <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>Pesquisa Semântica</a> do KDE, assim como à camada da 'cache' de dados e ao carregamento dos mesmos no próprio KMail. Ocorreu também algum trabalho notório na melhoria do suporte à base de dados PostgreSQL. Poderá encontrar mais algumas informações e detalhes sobre as alterações de performance nestes endereços:

- Optimizações do Armazenamento: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>relatório do 'sprint'</a>
- optimização e redução do tamanho da base de dados: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>lista de correio</a>
- optimização no acesso às pastas: <a href='https://git.reviewboard.kde.org/r/113918/'>quadro de revisões</a>

### KNotes, KAddressbook

Foi feito algum investimento significativo no KNotes, corrigindo uma série de erros e pequenos incómodos. A capacidade de definir alarmes para as notas é nova, assim como a pesquisa sobre as notas. Leia mais <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aqui</a>. O KAddressbook ganhou algum suporte para impressão: pode ver mais detalhes <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aqui</a>.

## O Okular Afina a Interface do Utilizador

Esta versão do leitor de documentos Okular traz um novo conjunto de melhorias. Pode agora abrir vários ficheiros PDF numa instância do Okular, graças ao suporte de páginas separadas. Existe agora um novo modo de ampliação com o rato e são usadas as dimensões em PPP do monitor actual para a representação do PDF, melhorando a aparência dos documentos. Foi incluído um novo botão Reproduzir no modo de apresentação, e existem melhorias nas acções para Procurar e Desfazer/Refazer.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## O Kate introduz uma barra de estado melhorada, a correspondência de parêntesis animada e 'plugins' melhorados

A última versão do editor de texto avançado Kate introduz a <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>correspondência animada dos parêntesis</a>, as mudanças para que <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>os teclados com o AltGr funcionem no modo do Vim</a> e uma série de melhorias nos 'plugins' do Kate, especialmente na área do suporte para o Python e o <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>'plugin' de compilação</a>. Existe uma nova <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>barra de estado melhorada</a> que activa algumas acções directas, como a alteração da indentação, da codificação e do realce, uma nova barra de páginas em cada janela, o suporte de completação de código para a <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>linguagem de programação D</a> e <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>muito mais</a>. A equipa <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>pediu a reacção e algumas sugestões de melhorias para o Kate</a> e está a dividir parte da sua atenção numa versão para as Plataformas 5.

## Diversas funcionalidades por todo o lado

O Konsole traz alguma flexibilidade adicional ao permitir folhas de estilo personalizadas para controlar as barras de páginas. Os perfis podem agora guardar os tamanhos das colunas e linhas desejados. Veja mais <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>aqui</a>.

O Umbrello possibilita a duplicação de diagramas e introduz alguns menus de contexto inteligentes que ajustam os seus conteúdos aos elementos seleccionados. O suporte para desfazer e as propriedades visuais foram também melhorados. O Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introduz o suporte para a antevisão de formatos RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

A mesa de mistura de som KMix introduziu os comandos à distância pelo protocolo de comunicações entre processos DBus (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>detalhes</a>), algumas adições ao menu de som e uma nova janela de configuração (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>detalhes</a>), assim como uma série de correcções de erros e pequenas melhorias.

A interface de pesquisa do Dolphin foi modificada para tirar partido da nova infra-estrutura de pesquisa e recebeu mais algumas melhorias de performance. Para mais detalhes, leia esta <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>visão geral sobre o trabalho de optimização no último ano</a>.

O KHelpcenter adiciona a ordenação alfabética dos módulos e a reorganização das categorias para se tornar mais simples de usar.

## Jogos e aplicações educativas

As aplicações de jogos e educativas do KDE receberam muitas actualizações nesta versão. A aplicação de 'puzzles' do KDE, o Palapeli, ganhou <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>algumas funcionalidades novas</a> que facilitam a resolução de 'puzzles' grandes (até 10 000 peças) para os que se sintam aptos para esse desafio. O KNavalBattle mostra as posições dos barcos inimigos após o fim do jogo, para que possa ver onde errou.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

As aplicações educativas do KDE ganharam novas funcionalidades. O KStars ganhou uma interface de programação através de DBus e pode também usar a API de serviços Web do astrometry.net para optimizar a utilização da memória. O Cantor ganhou o realce de sintaxe no seu editor de programas e as suas infra-estruturas de Scilab e de Python 2 são agora suportadas no editor. A ferramenta de mapas e de navegação Marble agora inclui as posições do <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sol, da Lua</a> e dos <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planetas</a>, permitindo também a <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>captura de filmes das viagens virtuais</a>. Os trajectos em bicicleta foram também melhorados com o suporte para o serviço cyclestreets.net. As milhas náuticas agora também são suportadas e, se carregar num <a href='http://en.wikipedia.org/wiki/Geo_URI'>URI geográfico</a>, o mesmo irá abrir o Marble.

#### Instalar Aplicações do KDE

O KDE, incluindo todas as suas bibliotecas e aplicações, está disponível gratuitamente segundo licenças de código aberto. As aplicações do KDE correm sobre várias configurações de 'hardware' e arquitecturas de CPU, como a ARM e a x86, bem como em vários sistemas operativos e gestores de janelas ou ambientes de trabalho. Para além do Linux e de outros sistemas operativos baseados em UNIX, poderá descobrir versões para o Microsoft Windows da maioria das aplicações do KDE nas <a href='http://windows.kde.org'>aplicações do KDE em Windows</a>, assim como versões para o Mac OS X da Apple nas <a href='http://mac.kde.org/'>aplicações do KDE no Mac</a>. As versões experimentais das aplicações do KDE para várias plataformas móveis, como o MeeGo, o MS Windows Mobile e o Symbian poderão ser encontradas na Web, mas não são suportadas de momento. O <a href='http://plasma-active.org'>Plasma Active</a> é uma experiência de utilizador para um espectro mais amplo de dispositivos, como tabletes ou outros dispositivos móveis.

As aplicações do KDE podem ser obtidas nos formatos de código-fonte e em vários formatos binários a partir de <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a> e também podem ser obtidos via <a href='/download'>CD-ROM</a> ou com qualquer um dos <a href='/distributions'>principais sistemas GNU/Linux e UNIX</a> dos dias de hoje.

##### Pacotes

Alguns distribuidores de SO's Linux/UNIX forneceram simpaticamente alguns pacotes binários do %1 para algumas versões das suas distribuições e, em alguns casos, outros voluntários da comunidade também o fizeram.

##### Localizações dos Pacotes

Para uma lista actualizada dos pacotes binários disponíveis, dos quais a Equipa de Versões do KDE foi informada, visite por favor o <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Wiki da Comunidade</a>.

Poderá <a href='/info/4/4.13.0'>transferir à vontade</a> o código-fonte completo de 4.13.0. As instruções de compilação e instalação da aplicação do KDE 4.13.0 está disponível na <a href='/info/4/4.13.0#binary'>Página de Informações do 4.13.0</a>.

#### Requisitos do Sistema

Para tirar o máximo partido destas versões, recomendamos a utilização de uma versão recente do Qt, como a 4.8.4. Isto é necessário para garantir uma experiência estável e rápida, assim como algumas melhorias feitas no KDE poderão ter sido feitas de facto na plataforma Qt subjacente.

Para tirar um partido completo das capacidades das aplicações do KDE, recomendamos também que use os últimos controladores gráficos para o seu sistema, dado que isso poderá melhorar substancialmente a experiência do utilizador, tanto nas funcionalidades opcionais como numa performance e estabilidade globais.
