---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Correcção de diversos problemas na pesquisa relacionada com a data de modificação
- Iteração da PostingDB: Não estoirar em caso de MDB_NOTFOUND
- Estado do Balooctl: Evitar a apresentação de 'A indexar o conteúdo' nas pastas
- StatusCommand: Mostrar o estado correcto nas pastas
- SearchStore: Tratamento ordeiro de valores de termos vazios (erro 356176)

### Ícones do Brisa

- alterações de ícones e novidades
- os ícones de estado com 22px de tamanho existem agora também em 32px, porque podem ser necessários na bandeja do sistema
- Correcção do valor Fixo para Escalável nas pastas de 32px com o Brisa Escuro

### Módulos Extra do CMake

- Mudança do módulo de CMake do KAppTemplate para global
- Eliminação dos avisos CMP0063 com o KDECompilerSettings
- ECMQtDeclareLoggingCategory: Inclusão do &lt;QDebug&gt; com o ficheiro gerado
- Correcção dos avisos CMP0054

### KActivities

- Arrumação do carregamento de QML para o KCM (erro 356832)
- Alternativa para o erro do Qt SQL que não elimina correctamente as ligações (erro 348194)
- Junção de um 'plugin' que executa aplicações quando é modificado o estado de uma actividade
- Migração do KService para o KPluginLoader
- Migração dos 'plugins' para usarem o kcoreaddons_desktop_to_json()

### KBookmarks

- Inicialização completa do DynMenuInfo no valor devolvido

### KCMUtils

- KPluginSelector::addPlugins: correcção de validação se o parâmetro 'config' é o predefinido (erro 352471)

### KCodecs

- Evitar o esgotamento deliberado de um 'buffer' completo

### KConfig

- Garantia de que o grupo é recomposto de novo no 'kconf_update'

### KCoreAddons

- Adição do KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin)
- Adição do KPluginMetaData::copyrightText(), extraInformation() e otherContributors()
- Adição do KPluginMetaData::translators() e do KAboutPerson::fromJson()
- Correcção de uso-após-libertação no processador de ficheiros '.desktop'
- Possibilidade de construção de um KPluginMetaData a partir de uma localização em JSON
- desktoptojson: mudança do ficheiro de tipo de serviço em falta para um erro do binário
- passagem da chamada do kcoreaddons_add_plugin sem o SOURCES para um erro

### KDBusAddons

- Adaptação para o 'dbus-em-tarefa-secundária' do Qt 5.6

### KDeclarative

- [DragArea] Adição da propriedade 'dragActive'
- [KQuickControlsAddons MimeDatabase] Exposição do comentário do QMimeType

### KDED

- kded: adaptação para o 'dbus' em tarefa secundária do Qt 5.6's: o 'messageFilter' deverá despoletar o carregamento do módulo na tarefa principal

### Suporte para a KDELibs 4

- Dependência para o 'kdelibs4support' do 'kded' (para o 'kdedmodule.desktop')
- Correcção do aviso CMP0064 através da definição da política do CMP0054 como NEW
- Não exportar símbolos que também existem no KWidgetsAddons

### KDESU

- Não perder descritores ao criar um 'socket'

### KHTML

- Windows: remoção da dependência do 'kdewin'

### KI18n

- Documentação da regra do primeiro argumento para os plurais no QML
- Redução de mudanças de tipo indesejadas
- Possibilidade de usar números de precisão dupla como índice nas chamadas i18np*() em QML

### KIO

- Correcção do 'kiod' para o 'dbus' em tarefas separadas do Qt 5.6: o 'messageFilter' tem de esperar pelo carregamento do módulo antes de devolver o resultado
- Mudança do código de erro ao colar/mover para uma sub-pasta
- Correcção do problema de bloqueio do 'emptyTrash'
- Correcção do botão inválido no KUrlNavigator para os URL's remotos
- KUrlComboBox: correcção do retorno de uma localização absoluta no urls()
- kiod: desactivação da gestão de sessões
- Adição da completação automática para a entrada '.', que oferece todos os ficheiros/pastas escondidos (erro 354981)
- ktelnetservice: correcção de erro de correspondência 'menos um' no 'argc', corrigido por Steven Bromley

### KNotification

- [Notificação por Mensagem] Enviar o ID do evento em conjunto
- Definição da razão não-vazia por omissão para a inibição do protector de ecrã (erro 334525)
- Adição de uma sugestão de desactivação do agrupamento de notificações (erro 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Permissão da selecção de um evento específico

### Plataforma de Pacotes

- Possibilidade de fornecimento dos meta-dados em JSON

### KPeople

- Correcção de uma possível remoção dupla no DeclarativePersonData

### KTextEditor

- Realce de sintaxe do 'pli': adição de funções incorporadas, adição de regiões expandidas

### Plataforma da KWallet

- kwalletd: Correcção de fuga de FILE*

### KWindowSystem

- Adição da variante XCB para os métodos estáticos KStartupInfo::sendFoo

### NetworkManagerQt

- Capacidade de trabalhar com versões anteriores do NM

### Plataforma do Plasma

- [ToolButtonStyle] Indicação sempre do 'activeFocus'
- Uso da opção SkipGrouping para a notificação de "remoção de item" (erro 356653)
- Tratamento adequado das ligações simbólicas nos locais dos pacotes
- Adição do HiddenStatus para auto-esconder os plasmóides
- Paragem do redireccionamento de janelas quando o item está desactivado ou escondido. (erro 356938)
- Não emitir o 'statusChanged' se não tiver sido modificado
- Correcção dos ID's dos elementos para a orientação a Este
- Contentor: Não emitir um 'appletCreated' com uma 'applet' nula (erro 356428)
- [Interface de Contentores] Correcção de um deslocamento de alta precisão errático
- Leitura da propriedade X-Plasma-ComponentTypes do KPluginMetadata como uma lista de textos
- [Miniaturas de Janelas] Não estoirar se a composição estiver desactivada
- Possibilidade de os contentores substituírem o CompactApplet.qml

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
