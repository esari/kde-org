---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE stelt KDE Applicaties 18.12.2 beschikbaar.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE stelt KDE Applicaties 18.12.2 beschikbaar
version: 18.12.2
---
{{% i18n_date %}}

Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../18.12.0'>KDE Applicaties 18.12</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan een dozijn aangegeven reparaties van bugs, die verbeteringen aan Kontact, ARK, Konsole, Umbrello, naast andere.

Verbeteringen bevatten:

- Ark verwijdert niet langer bestanden opgeslagen van binnen de ingebedde viewer</li>
- Het adresboek herinnert nu verjaardagen bij samenvoegen van contacten</li>
- Verschillende ontbrekend bijwerken van tonen van diagrammen zijn gerepareerd in Umbrello</li>
