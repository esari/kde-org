---
aliases:
- ../announce-applications-16.04-beta
custom_spread_install: true
date: 2016-03-24
description: KDE stelt KDE Applicaties 16.04 Beta beschikbaar
layout: application
release: applications-16.03.80
title: KDE stelt de beta van KDE Applicaties 16.04 beschikbaar
---
24 maart 2016. Vandaag heeft KDE de beta van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Kijk in de <a href='https://community.kde.org/Applications/16.04_Release_Notes'>uitgavenotities van de gemeenschap</a> voor informatie over nieuwe tarballs, tarballs die nu op KF5 zijn gebaseerd en bekende problemen. Een meer complete aankondiging zal beschikbaar zijn voor de uiteindelijke uitgave

Met de verschillende toepassingen gebaseerd op KDE Frameworks 5, heeft de KDE Applications 16.04 uitgave grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritisch in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de beta te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.

#### Installeren van KDE Applications 16.04 Beta binaire pakketten

<em>Pakketten</em>. Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van KDE Applications 16.04 Beta (intern 16.03.80) voor sommige versies van hun distributie en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. Extra binaire pakketten, evenals updates voor de pakketten zijn nu beschikbaar of zullen beschikbaar komen in de komende weken.

<em>Pakketlocaties</em>. Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Wiki van de gemeenschap</a>.

#### KDE Applications 16.03 Beta compileren

De complete broncode voor KDE Applications 16.04 Beta kan <a href='http://download.kde.org/unstable/applications/16.03.80/src/'>vrij gedownload</a> worden. Instructies over compileren en installeren zijn beschikbaar vanaf de <a href='/info/applications/applications-16.03.80'>Informatiepagina van KDE Applications Beta</a>.
