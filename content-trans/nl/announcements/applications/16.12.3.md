---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDE stelt KDE Applicaties 16.12.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.12.3 beschikbaar
version: 16.12.3
---
9 maart 2017. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../16.12.0'>KDE Applicaties 16.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 20 aangegeven reparaties van bugs, inclusief verbeteringen aan kdepim, ark, filelight, gwenview, kate, kdenlive, okular, naast andere.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.30.
