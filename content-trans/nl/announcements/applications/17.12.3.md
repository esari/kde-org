---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE stelt KDE Applicaties 17.12.3 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 17.12.3 beschikbaar
version: 17.12.3
---
8 maart 2018. Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../17.12.0'>KDE Applicaties 17.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 25 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello, naast andere.

Verbeteringen bevatten:

- Akregator verwijdert niet langer de feed-lijst feeds.opml na een fout
- De modus volledig-scherm van Gwenview werkt nu op de juiste bestandsnaam na hernoemen
- Verschillende zeldzame crashes in Okular zijn geïdentificeerd en gerepareerd
