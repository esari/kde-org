---
aliases:
- ../../kde-frameworks-5.48.0
date: 2018-07-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Zet overblijvend gebruik van qDebug() over naar qcDebug(ATTICA)
- controleren op ongeldige provider-url repareren
- Gebroken url naar API-specificatie repareren

### Baloo

- Ongebruikt item X-KDE-DBus-ModuleName uit de kded-plug-in-metagegevens verwijderen
- [tags_kio] de url zoekopdracht zou een paar sleutel-waarde moeten zijn
- Het vermogensstatussignaal zou alleen uitgestuurd mogem worden wanneer de vermogensstatus wijzigt
- baloodb: wijzigingen van beschrijving in argumenten op de opdrachtregel maken na rename prune -&gt; clean
- Dubbele bestandsnamen in tag-mappen helder tonen

### BluezQt

- D-Bus xml-bestanden bijwerken om "Out*" te gebruiken voor signaaltype Qt annotaties
- Signaal toevoegen voor adreswijziging van apparaten

### Breeze pictogrammen

- Ook het bezemstijl pictogram gebruiken voor edit-clear-all
- Een bezemstijl pictogram gebruiken voor edit-clear-history
- 24px pictogram voor view-media-artist wijzigen

### Extra CMake-modules

- Android: het mogelijk maken om een APK-map van een doel te overschrijven
- Verouderde QT_USE_FAST_OPERATOR_PLUS laten vallen
- -Wlogical-op -Wzero-as-null-pointer-constant toevoegen aan KF5 waarschuwingen
- [ECMGenerateHeaders] optie toevoegen voor andere headerbestandsextensie dan .h
- Geen 64 invoegen bij bouwen van 64bit architecturen op flatpak

### KActivitiesStats

- Fout met één teveel verschil repareren in Cache::clear (bug 396175)
- ResultModel-item verplaatsen repareren (bug 396102)

### KCompletion

- Onnodige moc-include verwijderen
- Zorg ervoor dat KLineEdit::clearButtonClicked wordt uitgestuurd

### KCoreAddons

- QT definities gedupliceerd uit KDEFrameworkCompilerSettings verwijderen
- Verzeker dat het compileert met strikte compileervlaggen
- Ongebruikt sleutelwoord X-KDE-DBus-ModuleName verwijderen uit test servicetype metagegevens

### KCrash

- QT_DISABLE_DEPRECATED_BEFORE reduceren tot minimale dep van Qt
- QT definities gedupliceerd uit KDEFrameworkCompilerSettings verwijderen
- Verzeker dat het bouwt met strikte compileervlaggen

### KDeclarative

- controleer dat de node echt een geldige textuur heeft (bug 395554)

### KDED

- Definitie van KDEDModule servicetype: ongebruikt sleutelwoord X-KDE-DBus-ModuleName verwijderen

### Ondersteuning van KDELibs 4

- Zet zelf QT_USE_FAST_OPERATOR_PLUS
- Geen kf5-config exporteren naar het CMake configuratiebestand
- Ongebruikt item X-KDE-DBus-ModuleName uit de kded-plug-in-metagegevens verwijderen

### KDE WebKit

- KRun::runUrl() &amp; KRun::run() overzetten naar niet-verouderde API
- KIO::Job::ui() -&gt; KIO::Job::uiDelegate() overzetten

### KFileMetaData

- Compileerwaarschuwingen vermijden voor taglib headers
- PopplerExtractor: direct argumenten van QByteArray() gebruiken in plaats van 0 pointers
- taglibextractor: uitpakken van geluidseigenschappen herstellen zonder bestaande tags
- OdfExtractor: met niet-algemene voorvoegselnamen rekening houden
- Geen -ltag toevoegen aan het publieke koppelingsinterface
- de liedteksttag implementeren voor taglibextractor
- automatische tests: geen EmbeddedImageData inbedden die al in de bibliotheek zitten
- mogelijkheid toevoegen om ingebedde coverbestanden te lezen
- lezen van waarderingstag implementeren
- controleren op benodigde versie van libavcode, libavformat en libavutil

### KGlobalAccel

- D-Bus xml-bestand bijwerken om "Out*" te gebruiken voor signaaltype Qt annotaties

### KHolidays

- holidays/plan2/holiday_jp_* - ontbrekende metainfo toevoegen
- bijgewerkte Japanse vakantiedagen (in Japans en Engels) (bug 365241)
- holidays/plan2/holiday_th_en-gb - bijgewerkt Thailand (UK English) voor 2018 (bug 393770)
- Venezolaanse vakantiedagen toevoegen (Spaans) (bug 334305)

### KI18n

- In cmake macrobestand CMAKE_CURRENT_LIST_DIR consequent gebruiken, in plaats van gemengd gebruik met KF5I18n_DIR
- KF5I18NMacros: Geen lege map installeren wanneer er geen po-bestanden bestaan

### KIconThemes

- Kiezen van .ico bestanden ondersteunen in aangepaste pictogrambestandenkiezer (bug 233201)
- Pictogramschaal uit namenspecificatie van pictogrammen 0.13 ondersteunen (bug 365941)

### KIO

- Nieuwe methode fastInsert overal waar van toepassing gebruiken
- Compatibiliteit van UDSEntry::insert herstellen, door een fastInsert methode toe te voegen
- Overzetten naar: KLineEdit::setClearButtonShown -&gt; QLineEdit::setClearButtonEnabled
- D-Bus xml-bestand bijwerken om "Out*" te gebruiken voor signaaltype Qt annotaties
- Gedupliceerde QT-definitie verwijderen uit KDEFrameworkCompilerSettings
- Een juist embleempictogram voor alleen-lezen bestanden en mappen (bug 360980)
- Het mogelijk maken om omhoog naar de hoofdmap te gaan in het bestandenwidget
- [Properties dialog] enige aan rechten gerelateerde tekenreeksen verbeteren (bug 96714)
- [KIO] ondersteuning voor XDG_TEMPLATES_DIR toevoegen in KNewFileMenu (bug 191632)
- trash-docbook bijwerken tot 5.48
- [Properties dialog] alle veldwaarden op het algemene tabblad selecteerbaar maken (bug 105692)
- Ongebruikt item X-KDE-DBus-ModuleName uit metagegevens van kded plug-ins verwijderen
- Vergelijken van KFileItems door url inschakelen
- [KFileItem] meest lokale URL controleren op of het wordt gedeeld
- Regressie repareren bij plakken van binaire gegevens vanaf het klembord

### Kirigami

- meer consistente kleur van muis zwevend over
- submenu voor acties niet openen zonder onderverdeling
- Opnieuw maken van het globale werkbalkconcept (bug 395455)
- Ingeschakelde eigenschap van eenvoudige modellen behandelen
- ActionToolbar introduceren
- pull om te vernieuwen repareren
- Doxygen documenten verwijderen uit interne klasse
- Niet linken tegen Qt5::DBus wanneer DISABLE_DBUS is gezet
- geen extra marge voor overlaysheets in overlay
- het menu voor Qt 5.9 repareren
- Activeer ook de zichtbare eigenschap van de action
- beter uiterlijk/uitlijning in compacte modus
- niet scannen op plug-ins voor elke aanmaak van platformTheme
- de "custom" set verwijderen
- resetters voor alle eigen kleuren toevoegen
- kleuren van werkknop overzetten naar aangepaste colorSet
- aangepaste kleurset introduceren
- beschrijfbare buddyFor naar positioneren van labels met betrekking tot subitems
- Bij gebruik van een andere achtergrondkleur, highlightedText als tekstkleur gebruiken

### KNewStuff

- [KMoreTools] installeren van hulpmiddelen via appstream-url inschakelen
- KNS::Engine d-pointer hack verwijderen

### KService

- Ongebruikt sleutelwoord X-KDE-DBus-ModuleName verwijderen uit test service metagegevens

### KTextEditor

- updateConfig bewaken voor uitgeschakelde statusbalken
- contextmenu toevoegen aan statusbalk om tonen van totaal aantal regels/woorden om te schakelen
- Tonen vaan totaal aantal regels in kate geïmplementeerd (bug 387362)
- Werkbalkknoppen met een menu zo maken dat ze hun menu's tonen na een normale klik in plaats van na een klik en ingedrukt houden (bug 353747)
- CVE-2018-10361: escalatie van rechten
- Breedte van dakje-teken repareren (bug 391518)

### KWayland

- [server] stuur framegebeurtenis in plaats van flush bij relatieve verplaatsing van de aanwijzer (bug 395815)
- XDGV6 test van popup repareren
- Stomme bug met kopiëren/plakken in XDGShellV6 Client repareren
- Oude selectie in klembord niet annuleren als het dezelfde is als de nieuwe (bug 395366)
- Rekening houden met BUILD_TESTING
- Enige problemen met spelling repareren gesuggereerd door nieuw hulpmiddel linter
- Het arclint-bestand in kwayland toevoegen
- @since verbeteren voor overslaan van switcher API

### KWidgetsAddons

- [KMessageWidget] stijlblad bijwerken wanneer pallet wijzigt
- kcharselect-data bijwerken tot Unicode 11.0
- [KCharSelect] generate-datafile.py overzetten naar Python 3
- [KCharSelect] bijwerken van vertalingen voor Unicode 11.0 voorbereiden

### ModemManagerQt

- Ondersteuning voor de Voice en Call interfaces implementeren
- Geen eigen domeinfilterregels instellen

### Oxygen-pictogrammen

- Een pictogram tonen voor verborgen bestanden in Dolphin (bug 395963)

### Plasma Framework

- FrameSvg: maskerframe bijwerken als afbeeldingspad is gewijzigd
- FrameSvg: verknoei gedeelde maskerframes niet
- FrameSvg: updateSizes vereenvoudigen
- Pictogrammen voor toetsenbordindicator T9050
- kleur voor mediapictogram repareren
- FrameSvg: maskFrame opnieuw in de cache zetten als enabledBorders is gewijzigd (bug 391659)
- FrameSvg: hoeken alleen tekenen als beide randen in beide richtingen zijn ingeschakeld
- Aan ContainmentInterface::processMimeData leren hoe te handelen bij wegvallen van takenbeheerder
- FrameSVG: overbodige controles verwijderen
- FrameSVG: include van QObject repareren
- QDateTime gebruiken voor interface met QML (bug 394423)

### Omschrijving

- Delenactie aan Dolphin's contextmenu toevoegen
- Plug-ins juist resetten
- Dubbele plug-ins uitfilteren

### QQC2StyleBridge

- geen pixelwaarden in checkindicator
- RadioIndicator voor iedereen gebruiken
- Niet overlopen rond popups
- op Qt&lt;5.11 worden het besturingspalet volledig genegeerd
- knopachtergrond verankeren

### Solid

- Apparaatlabel met onbekende grootte repareren

### Accentuering van syntaxis

- Reparaties voor Java commentaar
- Gradle-bestanden ook accentueren met Groovy syntaxis
- CMake: accentuering na tekenreeksen met een enkel <code>@</code> symbool repareren
- CoffeeScript: extensie .cson toevoegen
- Rust: trefwoorden &amp; bytes toevoegen, identifiers repareren en andere verbeteringen/reparaties
- Awk: reguliere expressie in een functie repareren en syntaxis bijwerken voor gawk
- Pony: escape voor accent repareren en een mogelijk oneindige lus met #
- CMake syntaxis bijwerken voor de aankomende uitgave 3.12

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
