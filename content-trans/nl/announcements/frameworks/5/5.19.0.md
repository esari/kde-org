---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Opzoeken van attica-plug-in en initialisatie vereenvoudigen

### Breeze pictogrammen

- Veel nieuwe pictogrammen
- Ontbrekende oxygen-pictogrammen voor mime-types toevoegen

### Extra CMake-modules

- ECMAddAppIcon: absoluut pad gebruiken bij werken op pictogrammen
- Ga na dat de prefix is opgezocht op Android
- Een FindPoppler module toevoegen
- PATH_SUFFIXES gebruiken in ecm_find_package_handle_library_components()

### KActivities

- Roep exec() niet aan vanuit QML (bug 357435)
- Bibliotheek KActivitiesStats is nu in een aparte repository

### KAuth

- Voer ook preAuthAction uit voor Backends met AuthorizeFromHelperCapability
- DBus servicenaam van polkit-agent repareren

### KCMUtils

- Probleem met hoge DPI in KCMUtils repareren

### KCompletion

- KLineEdit::setUrlDropsEnabled methode kan niet gemarkeerd worden als verouderd

### KConfigWidgets

- een "Complementair" kleurenschema aan kcolorscheme toevoegen

### KCrash

- Documenten voor KCrash::initialize bijwerken. Ontwikkelaars van toepassingen worden aangemoedigd om het expliciet aan te roepen.

### KDeclarative

- Afhankelijkheden voor KDeclarative/QuickAddons opschonen
- [KWindowSystemProxy] Insteller voor showingDesktop toevoegen
- DropArea: op de juiste manier dragEnter-event negeren met preventStealing repareren
- DragArea: Gedelegeerd item pakken implementeren
- DragDropEvent: functie ignore() toevoegen

### KDED

- Hack voor BlockingQueuedConnection terugnemen, Qt 5.6 zal een betere reparatie bevatten
- Zorg dat kded zich registreert onder aliassen gespecificeerd door de kded modules

### Ondersteuning van KDELibs 4

- kdelibs4support vereist kded (voor kdedmodule.desktop)

### KFileMetaData

- Navragen van de URL van de oorsprong van een bestand toestaan

### KGlobalAccel

- Crash voorkomen wanneer dbus niet beschikbaar is

### KDE GUI-addons

- Lijst met beschikbare paletten in kleurendialoog repareren

### KHTML

- Detectie van koppelingstype naar pictogram repareren (ook "favicon")

### KI18n

- Gebruik van gettext API verminderen

### KImageFormats

- kra en ora imageio-plug-ins toevoegen (alleen-lezen)

### KInit

- Viewport op huidige bureaublad bij init van opstartinformatie negeren
- klauncher naar xcb omzetten
- Een xcb voor interactie met KStartupInfo gebruiken

### KIO

- Nieuwe klasse FavIconRequestJob in nieuwe bibliotheek KIOGui, voor ophalen van favicons
- Crash van KDirListerCache met twee luisteraars naar een lege map in de cache repareren (bug 278431)
- Windows implementatie van KIO::stat voor file:/ protocolfout uitzoeken als het bestand niet bestaat
- Neem niet aan dat bestanden in alleen-lezen map niet verwijderd kunnen worden onder Windows
- .pri bestand voor KIOWidgets repareren: het hangt af van KIOCore, niet van zichzelf
- kcookiejar automatisch laden repareren, de waarden werden verwisseld in 6db255388532a4
- kcookiejar toegankelijk maken onder de dbus servicenaam org.kde.kcookiejar5
- kssld: DBus service bestand installeren voor org.kde.kssld5
- Een DBus servicebestand voor org.kde.kpasswdserver leveren
- [kio_ftp] tonen van bestand/map wijziging tijd/datum repareren (bug 354597)
- [kio_help] rommel verzonden bij bieden van statische bestanden repareren
- [kio_http] NTLMv2 authenticatie proberen als de server NTLMv1 weigert
- [kio_http] bugs bij overzetten die caching braken repareren
- [kio_http] NTLMv2 stage 3 antwoordcreatie repareren
- [kio_http] wachten totdat de opschoner van de cache luistert naar het socket repareren
- kio_http_cache_cleaner: niet bij opstarten verlaten als de map voor de cache nog niet bestaat
- DBus-naam van kio_http_cache_cleaner wijzigen zodat deze niet stopt als die van kde4 actief is

### KItemModels

- KRecursiveFilterProxyModel::match: crash repareren

### KJobWidgets

- Crash in KJob dialogen repareren (bug 346215)

### Pakket Framework

- Zoeken naar hetzelfde pakket meerdere keren vanuit verschillende paden

### KParts

- PartManager: stop volgen van een widget zelfs als het niet langer op topniveau is (bug 355711)

### KTextEditor

- Beter gedrag voor "insert braces around" bij functie automatisch haakjes toevoegen
- Optietoets voor afdwingen van nieuwe standaard wijzigen, nieuwe-regel aan einde-van-bestand = true
- Enkele verdachte aanroepen van setUpdatesEnabled verwijderen (bug 353088)
- Uitzenden van verticalScrollPositionChanged vertragen totdat alle zaken consistent zijn voor invouwen (bug 342512)
- Bijwerken van tagsubstitutie patchen (bug 330634)
- Het palet slechts eenmaal bijwerken voor de wijzigingsgebeurtenis behorende bij qApp (bug 358526)
- Standaard nieuwe-regels achtervoegen bij EOF
- Syntaxisaccentueringsbestand voor NSIS toevoegen

### KWallet Framework

- De bestandsdescriptor dupliceren bij openen van het bestand om de omgeving te lezen

### KWidgetsAddons

- Werking van buddy-widgets repareren met KFontRequester
- KNewPasswordDialog: KMessageWidget gebruiken
- Crash-bij-verlaten in KSelectAction::~KSelectAction voorkomen

### KWindowSystem

- Licentiekop van "Library GPL 2 of later" wijzigen naar "Lesser GPL 2.1 of later"
- Crash repareren als KWindowSystem::mapViewport wordt aangeroepen zonder een QCoreApplication
- Cache QX11Info::appRootWindow in eventFilter (bug 356479)
- Afhankelijkheid van QApplication verwijderen (bug 354811)

### KXMLGUI

- Optie om KGlobalAccel uit te schakelen bij compilatie toevoegen
- Pad naar sneltoetsschema van toepassing repareren
- Lijst met sneltoetsbestanden repareren (verkeerd gebruik van QDir)

### NetworkManagerQt

- Status van verbinding opnieuw controleren en andere eigenschappen om et zeker van te zijn dat ze actueel zijn (versie 2) (bug 352326)

### Oxygen-pictogrammen

- Gebroken gekoppelde bestanden verwijderen
- App-pictogrammen toevoegen uit de kde toepassingen
- Breeze plaatspictogrammen toevoegen aan oxygen
- Oxygen mime-type pictogrammen met breeze mime-type pictogrammen synchroniseren

### Plasma Framework

- Een eigenschap separatorVisible toevoegen
- Meer explicite verwijdering uit m_appletInterfaces (bug 358551)
- complementaryColorScheme uit KColorScheme gebruiken
- AppletQuickItem: probeer niet om de initiële grootte groter in te stellen dan de grootte van de ouder (bug 358200)
- IconItem: usesPlasmaTheme eigenschap toevoegen
- Toolbox niet laden bij typen die geen desktop of paneel zijn
- IconItem: probeer QIcon::fromTheme pictogrammen te laden als svg (bug 353358)
- Controle negeren als slechts één deel van de grootte nul is in compactRepresentationCheck (bug 358039)
- [Units] geeft minstens 1 ms als duur terug (bug 357532)
- clearActions() toevoegen om elke applet-interface-actie te verwijderen
- [plasmaquick/dialog] Geen KWindowEffects gebruiken voor type Notificatievenster
- Applet::loadPlasmoid() is verouderd
- [PlasmaCore DataModel] model niet resetten wanneer een bron is verwijderd
- Marge-hints repareren in achtergrond SVG van paneel dat doorzichtig is
- IconItem: eigenschap geanimeerd toevoegen
- [Unity] Grootte van bureaubladpictogram schalen
- de knop is compose-over-borders
- paintedWidth/paintedheight voor IconItem

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
