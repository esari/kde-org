---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Verschillende Q_ASSERT's door juiste controles vervangen
- Tekenreekslengte controleren om crash te vermijden voor "tags:/" URL
- [tags_kio] Lokale tagging van bestanden door alleen tag: urls te controleren op dubbele slashes
- Het Overblijvende bijwerkintervaltijd wordt hard gecodeerd
- Regressie repareren voor expliciete overeenkomst inclusief mappen
- Ongewijzigde items opschonen in mimetype-tabel met overeenkomsten
- [baloo/KInotify] melden als map is verplaatst vanaf niet bewaakte plaats (bug 342224)
- Mappen overeenkomend met subtekenreeksen van meegenomen/uitgesloten mapppen juist behandelen
- [balooctl] meegenomen/uitgesloten paden normaliseren alvorens ze te gebruiken voor de configuratie
- Baloo::File kopieer-toekenningsoperator optimaliseren, Baloo::File::load(url) repareren
- Inhoud gebruiken om MIME-type te bepalen (bug 403902)
- [Extractor] GPG versleutelde gegevens uitsluiten van indexering (bug 386791)
- [balooctl] een fout gevormd commando exht afbreken in plaats van dit te vertellen
- [balooctl] ontbrekende help voor "config set" toevoegen, tekenreeks normaliseren
- Recursieve isDirHidden vervangen door iteratieve, const-argument toestaan
- Ga na dat alleen mappen toegevoegd worden aan de inotify-bewaking

### Breeze pictogrammen

- Pictogram code-oss toevoegen
- [breeze-icons] video-camera-pictogrammen
- [breeze-icons] nieuw pictogramthema voor onderbreken, slapen en gebruiker omschakelen in Breeze
- 16 px en 22 px versies toevoegen van het pictogram gamepad aan apparaten/
- Tekst van thematekstballon van Breeze consistent maken
- Betterijpictogrammen toevoegen
- Pictogrammen "zichtbaarheid" en "tip"  hernoemen naar "weergave-zichtbaar" en "weergave-verborgen"
- [breeze-icons] monochrome/kleinere SD-kaart en geheugenstick pictogrammen toevoegen (bug 404231)
- Apparaatpictogrammen voor drones toevoegen
- MIME-type pictogrammen van C/C++ header/broncode wijzigen naar cirkel/regelstijl
- Ontbrekende schaduw in MIME-type pictogrammen van C/C++ header/broncode repareren (bug 401793)
- Monochrome pictogram met voorkeur voor lettertype verwijderen
- Pictogram voor selectie van lettertype verbeteren
- Nieuwe bel-stijl pictogram voor alle gebruikers van preferences-desktop-notification gebruiken (bug 404094)
- [breeze-icons] 16px versies van gnumeric-font.svg en link gnumeric-font.svg naar font.svg toevoegen
- Symbolische koppeling naar preferences-system-users die wijzen naar pictogram voor yast-gebruikers toevoegen
- Pictogrammen voor niets-bewerken toevoegen

### Extra CMake-modules

- Releaseme checkout repareren wanneer dit is ingevoegd in a submap
- Nieuw zoekmodule voor Canberra
- Android toolchain bestanden bijwerken naar realiteit
- Compileercontrole toevoegen aan FindEGL

### KActivities

- Natuurlijke sortering gebruiken in ActivityModel (bug 404149)

### KArchive

- KCompressionDevice::open bewaken bij aanroep zonder beschikbare backend (bug 404240)

### KAuth

- Mensen vertellen dat zy meestal KF5::AuthCore zouden moeten gebruiken
- Onze eigen helper compileren tegen AuthCore en niet Auth
- KF5AuthCore introduceren

### KBookmarks

- KIconThemes afhankelijkheden vervangen door equivalent gebruik van QIcon

### KCMUtils

- KCM-naam in KCM-header gebruiken
- Ontbrekende ifndef KCONFIGWIDGETS_NO_KAUTH toevoegen
- Aan wijzigingen in kconfigwidgets aanpassen
- QML module opvulling synchroniseren om systeeminstellingenpagina's te reflecteren

### KCodecs

- Reparatie voor CVE-2013-0779
- QuotedPrintableDecoder::decode: false teruggeven bij fout in plaats van toekennen
- KCodecs::uuencode markeren doet niets
- nsEUCKRProber/nsGB18030Prober::HandleData niet crashen als aLen 0 is
- nsBig5Prober::HandleData: Niet crashen als aLen is 0
- KCodecs::Codec::encode: niet toekennen/crash als makeEncoder nul teruggeeft
- nsEUCJPProbe::HandleData: geen crash als aLen 0 is

### KConfig

- Geldig UTF8 teken schrijven zonder escaping (bug 403557)
- KConfig: symbolische koppelingen naar mappen juist behandelen

### KConfigWidgets

- Benchmark overslaan als geen schemabestanden zijn te vinden
- Een notitie toevoegen voor KF6 om de kernversie van KF5::Auth te gebruiken
- De standaard KColorScheme-configuratie in de cache zetten

### KCoreAddons

- Tel: koppelingen voor telefoonnummers aanmaken

### KDeclarative

- KPackage::fileUrl gebruiken om pakketten van rcc KCM te ondersteunen
- [GridDelegate] lange labels samenvoegen in elkaar repareren (bug 404389)
- [GridViewKCM] contrast en leesbaarheid verhogen voor inline hoverknoppen van delegates (bug 395510)
- De acceptatievlag corrigeren van het eventobject bij DragMove (bug 396011)
- Verschillende itempictogrammen "Geen" gebruiken in rasterweergave van KCM's

### KDESU

- kdesud: KAboutData::setupCommandLine() stelt al help &amp; versie in

### KDocTools

- ondersteuning voor cross-compilatie overzetten naar KF5_HOST_TOOLING
- DocBookXML alleen rapporteren als gevonden als het echt was gevonden
- Spaande entities bijwerken

### KFileMetaData

- [Extractor] Metagegevens toevoegen aan  extractors (bug 404171)
- Extractor voor AppImage bestanden toevoegen
- Extractor van ffmpeg opschonen
- [ExternalExtractor] meer behulpzame uitvoer leveren wanneer extractor mislukt
- EXIF fotoflashgegevens formatteren (bug 343273)
- Zijeffecten vermijden vanwege verouderde waarde van foutnummer
- Kformat gebruiken voor bit- en bemonsteringssnelheid
- Voeg eenheden toe aan framesnelheid en gps-gegevens
- Formatteringsfunctie toevoegen aan informatie over eigenschap
- Een QObject lekken in ExternalExtractor vermijden
- &lt;a&gt; als containerelement behandelen in SVG
- Exiv2::ValueType::typeId controleren voor het converteren naar rationeel getal

### KImageFormats

- ras: crash op gebroken bestanden repareren
- ras: de palet QVector ook beschermen
- ras: maximale bestandscontrole manipuleren
- xcf: niet geïnitialiseerd gebruik van geheugen op gebroken documenten repareren
- const toevoegen, helpt om de functie beter te begrijpen
- ras: maximale grootte manipuleren die "past" in een QVector
- ras: niet toekennen omdat we een enorme vector proberen te reserveren
- ras: tegen delen door nul beschermen
- xcf: niet door 0 delen
- tga: netjes mislukken bij readRawData-fouten
- ras: netjes mislukken bij hoogte*breedte*bpp &gt; lengte

### KIO

- kioexec: KAboutData::setupCommandLine() stelt al help &amp; versie in
- Crash in Dolphin bij loslaten van een bestand in de prullenbak in de prullenbak (bug 378051)
- Middelste deel weglaten bij erg lange bestandsnamen in foutmeldingen (bug 404232)
- Ondersteuning voor portals in KRun toevoegen
- [KPropertiesDialog] keuzelijst van groep repareren (bug 403074)
- Probeer op de juiste manier de kioslave bin in $libexec EN $libexec/kf5 te lokaliseren
- AuthCore in plaats van Auth gebruiken
- Pictogramnaam toevoegen aan serviceproviders in .desktop bestand
- Pictogram van IKWS zoekmachineleverancier lezen uit desktopbestand
- [PreviewJob] ook doorgeven dat we de miniatuurmaker zijn bij stat toepassen op bestand (bug 234754)

### Kirigami

- het gebroken rommelen met contentY in refreshabeScrollView verwijderen
- OverlayDrawer toevoegen aan het materiaal dat gedocumenteerd kan worden door doxygen
- currentItem aanpassen aan de weergave
- juiste kleur aan het pictogram pijl-omlaag
- SwipeListItem: ruimte maken voor de acties bij !supportsMouseEvents (bug 404755)
- ColumnView en gedeeltelijke C++ refactor van PageRow
- we kunnen ten hoogste controls 2.3 als Qt 5.10 gebruiken
- hoogte van horizontale laden repareren
- ToolTip verbeteren in de component ActionTextField
- Een ActionTextField-component  toevoegen
- afstand tussen knoppen repareren (bug 404716)
- Knoppengrootte repareren (bug 404715)
- GlobalDrawerActionItem: pictogram refereren op de juiste manier door groepseigenschap te gebruiken
- scheider tonen als werkbalkkop onzichtbaar is
- een standaard pagina-achtergrond toevoegen
- DelegateRecycler: vertaling repareren na gebruik van het verkeerde domein
- Waarschuwing repareren bij gebruik van QQuickAction
- Enige niet noodzakelijk QString constructies verwijderen
- De tekstballon niet tonen wanneer het afrolmenu wordt getoond (bug 404371)
- schaduwen verbergen indien gesloten
- de noodzakelijke eigenschappen toevoegen voor de afwisselende kleur
- de meeste van de wijzigingen in heuristics van pictogramkleuren
- gegroepeerde eigenschappen op de juiste manier beheren
- [PassiveNotification] timer niet starten totdat venster focus heeft (bug 403809)
- [SwipeListItem] een echte werkknop gebruiken om bruikbaarheid te verbeteren (bug 403641)
- ondersteuning voor optionele afwisselende achtergronden (bug 395607)
- alleen handvatten tonen wanneer er zichtbare acties zijn
- gekleurde pictogrammen ondersteunen in knoppen voor acties
- de knop terug altijd tonen op lagen
- SwipeListItem doc bijwerken naar QQC2
- logica van updateVisiblePAges repareren
- zichtbare pagina's in paginarij blootstellen
- broodkruimel verbergen wanneer de huidige pagina een werkbalk heeft
- de werkbalkstijlpagina overschrijven ondersteunen
- nieuwe eigenschap in pagina: titleDelegate om de titel in werkbalken te overschrijven

### KItemModels

- KRearrangeColumnsProxyModel: de twee kolommen-mapping-methoden publiek maken

### KNewStuff

- Ongeldige inhoud in lijsten uitfilteren
- geheugenlek repareren gevonden door asan

### KNotification

- overbrengen naar findcanberra uit ECM
- Android markeren als officieel ondersteund

### KPackage-framework

- kpackage_install_package als waarschuwing voor verouderd verwijderen

### KParts

- sjablonen: KAboutData::setupCommandLine() stelt al help &amp; versie in

### Kross

- Kross modules installeren in ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: geen noodzaak om werk van KAboutData::setupCommandLine() te herhalen

### KTextEditor

- weergavehoogte van tekstregels proberen te verbeteren - bug 403868 knippen van _ vermijden en andere delen nog steeds gebroken: dubbele hoogte zaken zoals gemengd engels/arabisch, zie bug 404713
- QTextFormat::TextUnderlineStyle gebruiken in plaats van QTextFormat::FontUnderline (bug 399278)
- Het mogelijk maken om alle spaties in het document te tonen (bug 342811)
- Ingesprongen regels niet afdrukken
- KateSearchBar: ook zoeken tonen met afgebroken tip in nextMatchForSelection() aka Ctrl-H
- katetextbuffer: TextBuffer::save() opnieuw maken om beter broncodepaden te scheiden
- AuthCore in plaats van Auth gebruiken
- KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e) opnieuw ontwerpen
- Verbeteringen aan voltooiing
- Het kleurschema naar afdrukken instellen voor afdrukvoorbeeld (bug 391678)

### KWayland

- XdgOutput::done committen alleen indien gewijzigd (bug 400987)
- FakeInput: ondersteuning toevoegen voor aanwijzer verplaatsen met absolute coördinaten
- Ontbrekende XdgShellPopup::ackConfigure toevoegen
- [server] proxymechanisme voor oppervlakgegevens toevoegen
- [server] signaal selectionChanged toevoegen

### KWidgetsAddons

- Juiste pictogram KStandardGuiItem "nee" gebruiken

### Plasma Framework

- [Icon Item] volgende animatie blokkeren ook gebaseerd op zichtbaarheid van venster
- Een waarschuwing tonen als een plug-in een nieuwe versie vereist
- De versies van thema ophogen omdat pictogrammen zijn gewijzigd, om oude caches ongeldig te maken
- [breeze-icons] system.svgz nieuw leven inblazen
- Tekst van thematekstballon van Breeze consistent maken
- glowbar.svgz wijzigen voor soepeler stijl (bug 391343)
- Terugval van achtergrondcontrast doen bij uitvoeren (bug 401142)
- [breeze desktop theme/dialogs] afgeronde hoeken toevoegen aan dialogen

### Omschrijving

- pastebin: geen voortgangsmelding tonen (bug 404253)
- sharetool: gedeelde url bovenaan tonen
- Delen van bestanden met spaties of accenten in namen repareren via Telegram
- ShareFileItemAction een uitvoer of een fout laten leveren als ze geleverd worden (bug 397567)
- Delen van URL's via e-mail inschakelen

### QQC2StyleBridge

- PointingHand gebruiken bij zweven boven koppelingen in Label
- De eigenschappen voor tonen van knoppen honoreren
- klikken op lege gebieden gedraagt zich als PgUp/PgDown (bug 402578)
- Pictogram ondersteunen op ComboBox
- api voor positionering van tekst ondersteunen
- Pictogrammen ondersteunen in knoppen uit lokale bestanden
- De juiste cursor gebruiken bij zweven boven het te bewerken gedeelte van een draaischakelaar

### Solid

- FindUDev.cmake omhoog brengen naar ECM standaarden

### Sonnet

- He geval behandelen als createSpeller een niet beschikbare taal krijgt doorgegeven

### Accentuering van syntaxis

- Waarschuwing over verwijderen van opslagruimte repareren
- MustacheJS: ook sjabloonbestanden voor accentueren, syntaxis repareren en ondersteuning verbeteren voor Handlebars
- ongebruikte contexten fataal maken voor indexeerder
- example.rmd.fold en test.markdown.fold bijwerken met nieuwe nummers
- DefinitionDownloader-header installeren
- octave.xml bijwerken naar Octave 4.2.0
- Accentueren van TypeScript (en React) verbeteren en meer testen voor PHP toevoegen
- Meer accentuering voor geneste talen in markdown toevoegen
- Gesorteerde definities voor bestandsnamen en MIME-typen teruggeven
- ontbrekende ref-update toevoegen
- BrightScript: Unary en hex-nummers, @attribute
- Dubbele *-php.xml bestanden in "data/CMakeLists.txt" vermijden
- Functies toevoegen die alle definities voor een MIME-type of bestandsnaam teruggeven
- MIME-type van literate haskell bijwerken
- toekenning in regex laden voorkomen
- cmake.xml: Bijgewerkt voor versie 3.14
- CubeScript: reparaties voor regeldoorloop-escape in tekenreeksen
- enige minimale howto toevoegen voor toevoegen van testen
- R Markdown: invouwen van blokken verbeteren
- HTML: JSX-, TypeScript- &amp; MustacheJS-code accentueren in de &lt;script&gt; tag (bug 369562)
- AsciiDoc: invouwen voor secties toevoegen
- Schemasyntaxisaccentuering voor FlatBuffers
- Sommige Maxima constanten en functie toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
