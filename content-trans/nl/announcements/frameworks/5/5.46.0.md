---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Oneindige loops vermijden bij ophalen van de URL uit DocumentUrlDB (bug 393181)
- Baloo-DBus-signalen voor verplaatste of verwijderde bestanden
- pri-bestand installeren voor qmake ondersteuning &amp; het documenteren in metainfo.yaml
- baloodb: commando clean toevoegen
- balooshow: alleen kleur toevoegen wanneer aan terminal gehangen
- FSUtils::getDirectoryFileSystem verwijderen
- Hard coderen van bestandssystemen vermijden die CoW ondersteunen
- Uitschakelen van CoW toestaan om te mislukken wanneer dit niet wordt ondersteund door bestandssysteem
- databasesanitizer: vlaggen voor filtering gebruiken
- Samenvoegen van termen in de AdvancedQueryParser repareren
- QStorageInfo gebruiken in plaats van een zelf gemaakte implementatie
- sanitizer: lijst van apparaten maken verbeteren
- Onmiddellijk toepassen van termInConstruction wanneer term gereed is
- Naast elkaar liggende speciale tekens juist behandelen (bug 392620)
- Testgeval voor ontleden van dubbele openingshaakjes '((' toevoegen (bug 392620)
- statbuf consistent gebruiken

### Breeze pictogrammen

- Plasma-browserintegratie van systeemvakpictogram toevoegen
- Virt-manager-pictogram toevoegen met dank aan ndavis
- video-card-inactive toevoegen
- overflow-menu als view-more-symbolic en horizontaal
- Het meer toepasselijke "twee schuifbalken" pictogram voor "configuren" gebruiken

### Extra CMake-modules

- FeatureSummary invoegen alvorens set_package_properties aan te roepen
- Geen plug-ins installeren binnen lib op android
- Maak het mogelijk om verschillende apk uit een project te bouwen
- Controleer of de pakket van toepassing androiddeployqt een main() symbool heeft

### KCompletion

- [KLineEdit] Qt's ingebouwde functionaliteit wisknop gebruiken
- KCompletionBox op wayland repareren

### KCoreAddons

- [KUser] Controleer of .face.icon echt leesbaar is alvorens het terug te geven
- Maak KJob signalen publiek zodat Qt5 connect-syntaxis kan werken

### KDeclarative

- NV graphics-reset laden gebaseerd op config
- [KUserProxy] aanpassen aan accounts-service (bug 384107)
- Optimalisaties van Plasma Mobile
- Ruimte maken voor voet- en koptekst 
- nieuw beleid voor grootte wijzigen (bug 391910)
- zichtbaarheid van acties ondersteunen
- nvidia-reset melding in QtQuickViews ondersteunen

### KDED

- Platform detectie toevoegen en aanpassen aan kded (automatische instelling van $QT_QPA_PLATFORM)

### KFileMetaData

- Beschrijving en doel toevoegen aan Xattr dep
- extractors: waarschuwingen uit systeemkoppen verbergen
- detectie van taglib repareren bij compileren voor Android
- pri-bestand installeren voor qmake ondersteuning &amp; het documenteren in metainfo.yaml
- Samen gevoegde tekenreeksen afbreekbaar maken
- ffmpegextractor: waarschuwingen over veroudering stil maken
- taglibextractor: bug met lege genre repareren
- meer tags in taglibextractor behandelen

### KHolidays

- holidays/plan2/holiday_sk_sk - onderwijzersdag repareren (bug 393245)

### KI18n

- [API dox] nieuwe UI-marker @info:placeholder
- [API dox] nieuwe UI-marker @item:valuesuffix
- Vorige iteratie commando's zijn niet nodig om opnieuw uit te voeren (bug 393141)

### KImageFormats

- [XCF/GIMP lader] maximimaal toegestane afbeeldingsgrootte ophogen naar 32767x32767 op 64 bits platforms (bug 391970)

### KIO

- Soepeler schalen van miniaturen in bestandenkiezer (bug 345578)
- KFileWidget: lijn bestandsnaamwidget perfect uit met pictogramweergave
- KFileWidget: paneelbreedte voor plaatsen opslaan ook na verbergen van paneel
- KFileWidget: voorkomen dat paneelbreedte voor plaatsen iteratief groeit met 1 px
- KFileWidget: zoomknoppen uitschakelen wanneer minimum of maximum is bereikt
- KFileWidget: minimum grootte voor zoomschuifregelaar instellen
- Geen bestandsextensie selecteren
- concatPaths: lege path1 op de juiste verwerken
- Pictogramindelingsraster verbeteren in de dialoog voor bestandskiezer (bug 334099)
- KUrlNavigatorProtocolCombo verbergen als er slechts één protocol wordt ondersteund
- In KUrlNavigatorProtocolCombo alleen ondersteunde schema's tonen
- Bestandskiezer leest miniatuurvoorbeelden uit instellingen van Dolphin (bug 318493)
- Bureaublad en downloads toevoegen aan de standaard lijst met plaatsen
- KRecentDocument slaat nu QGuiApplication::desktopFileName op in plaats van applicationName
- [KUrlNavigatorButton] ook geen stat voor MTP
- getxattr heeft 6 parameters nodig in macOS (bug 393304)
- Een menu-item "Herladen" toevoegen aan het contextmenu van KDirOperator (bug 199994)
- De weergave-instellingen opslaan zelfs bij annuleren (bug 209559)
- [KFileWidget] voorbeeldgebruikersnaam hard coderen
- De bovenste app "Openen met" voor mappen niet tonen; alleen voor bestanden
- Onjuiste parameter detecteren in findProtocol
- Tekst "Andere toepassing..." gebruiken in submenu "Openen met"
- URL van miniaturen op de juiste manier coderen (bug 393015)
- Kolombreedte aanpassen in boomstructuurweergave van dialogen voor openen/opslaan van bestanden (bug 96638)

### Kirigami

- Geen waarschuwing bij gebruik van Page {} buiten een pageStack
- InlineMessages bewerken om een aantal problemen te adresseren
- reparatie op Qt 5.11
- op eenheden baseren voor grootte van werkknop
- kleur van sluitpictogram bij er boven zweven
- een marge onder de voettekst tonen indien nodig
- isMobile repareren
- ook vervagen bij openen/sluiten animatie
- de dbus-zaken alleen op unix-niet android, niet apple
- de tabletMode uit KWin bewaken
- in modus bureaublad acties tonen bij er boven zweven (bug 364383)
- handvat in de bovenste werkbalk
- een grijze knop voor sluiten gebruiken
- minder afhankelijkheid van toepassingsvenster
- minder waarschuwingen zonder toepassingsvenster
- op de juiste manier werken zonder toepassingsvenster
- Heeft geen integrale grootte op scheidingstekens
- De acties niet tonen als ze niet zijn ingeschakeld
- te activeren FormLayout items
- verschillende pictogrammen in het kleurset voorbeeld
- pictogrammen allee meenemen op android
- laat het werken met Qt 5.7

### KNewStuff

- Dubbele marges rondom DownloadDialog
- Hints in UI bestanden repareren over subklassen van aangepaste widgets
- Geen qml-plug-in bieden als een koppelingsdoel

### KPackage-framework

- KDE_INSTALL_DATADIR gebruiken in plaatst van FULL_DATADIR
- Donatie url's toevoegen on gegevens te testen

### KPeople

- PersonSortFilterProxyModel filtering repareren

### Kross

- Maak ook installatie van vertaalde documenten optioneel

### KRunner

- DBus-runner met ondersteuning van jokers in servicenaam

### KTextEditor

- optimaliseren van KTextEditor::DocumentPrivate::views()
- [ktexteditor] veel snellere positionFromCursor
- Enkele klik op regelnummer om regel met tekst te selecteren
- Ontbrekende vet/cursief/... opmaak repareren met moderne Qt versies (&gt;= 5.9)

### Plasma Framework

- Niet getoonde gebeurtenismarkering in agenda met thema's air &amp; oxygen repareren
- "%1 configuren..." gebruiken voor tekst van de actie applet configureren
- [Button Styles] hoogte en verticale uitlijning vullen (bug 393388)
- pictogram video-card-inactive toevoegen voor systeemvak
- uiterlijk voor vlakke knoppen corrigeren
- [Containment Interface] modus bewerken niet ingaan indien onveranderlijk
- ga na dat largespacing is een perfect meervoud van small
- addContainment aanroepen met juiste paramenters
- De achtergrond niet tonen indien Button.flat
- ga na dat de container die we hebben aangemaakt de activiteit heeft waarnaar we hebben gevraagd
- een versie containmentForScreen toevoegen met activiteit
- Geheugenbeheer niet wijzigen om een item te verbergen (bug 391642)

### Omschrijving

- Ga na dat we enige verticale ruimte hebben gegeven om plug-ins te configureren
- KDEConnect plug-in configuratie overbrengen naar QQC2
- AlternativesView overbrengen naar QQC2

### QQC2StyleBridge

- indelingpaddings uit qstyle exporteren, begin vanuit Control
- [ComboBox] behandeling van muiswiel repareren
- laat het muisgebied niet interfereren met besturingen
- acceptableInput repareren

### Solid

- Aankoppelpunt bijwerken na aankoppelbewerking (bug 370975)
- Maak eigenschapcache ongeldig wanneer een interface wordt verwijderd
- Aanmaken van dubbele eigenschapsitems in de cache vermijden
- [UDisks] verschillende controles op eigenschappen optimaliseren
- [UDisks] behandeling van verwijderbare bestandssystemen corrigeren (bug 389479)

### Sonnet

- Verwijderen in-/uitschakelknop repareren
- Toevoegen in-/uitschakelen repareren
- In submappen zoeken naar woordenboeken

### Accentuering van syntaxis

- Project-URL bijwerken
- 'Headline' is een opmerking, dus baseren op dsComment
- Accentuering voor GDB commandolijst en gdbinit-bestanden toevoegen
- Syntaxisaccentuering voor Logcat toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
