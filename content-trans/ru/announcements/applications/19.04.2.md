---
aliases:
- ../announce-applications-19.04.2
changelog: true
date: 2019-06-06
description: KDE Ships Applications 19.04.2.
layout: application
major_version: '19.04'
release: applications-19.04.2
title: KDE выпускает KDE Applications 19.04.2
version: 19.04.2
---
{{% i18n_date %}}

Today KDE released the second stability update for <a href='../19.04.0'>KDE Applications 19.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Зафиксировано более 50 исправлений ошибок в электронном секретаре Kontact, архиваторе Ark, диспетчере файлов Dolphin, музыкальном проигрывателе JuK, видеоредакторе Kdenlive, приложении для построения графиков KmPlot, приложении для просмотра изображений Okular, приложении для создания снимков экрана Spectacle и многих других.

Некоторые из улучшений:

- A crash with viewing certain EPUB documents in Okular has been fixed
- Secret keys can again be exported from the Kleopatra cryptography manager
- The KAlarm event reminder no longer fails to start with newest PIM libraries
