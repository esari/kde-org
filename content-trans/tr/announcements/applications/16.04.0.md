---
aliases:
- ../announce-applications-16.04.0
changelog: true
date: 2016-04-20
description: KDE, KDE Uygulamalar 16.04.0'ı Gönderdi
layout: application
title: KDE, KDE Uygulamalar 16.04.0'ı Gönderdi
version: 16.04.0
---
20 Nisan 2016. KDE, daha kolay erişim, son derece kullanışlı işlevlerin tanıtımı ve KDE Uygulamalarının artık bir adım daha yaklaşmasına neden olan bu küçük sorunlardan kurtulma söz konusu olduğunda etkileyici bir dizi yükseltme ile bugün KDE Uygulamaları 16.04'ü tanıtıyor. Size cihazınız için mükemmel bir kurulum sunar.

<a href='https://www.kde.org/applications/graphics/kcolorchooser/'>KColorChooser</a>, <a href='https://www.kde.org/applications/utilities/kfloppy/'>KFloppy</a>, <a href='https://www.kde.org/applications/games/kmahjongg/'>KMahjongg</a> and <a href='https://www.kde.org/applications/internet/krdc/'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='https://www.kde.org/applications/education/minuet/'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.

### KDE's Newest Addition

{{<figure src="/announcements/applications/16.04.0/minuet1604.png" class="text-center" width="600px">}}

A new application has been added to the KDE Education suite. <a href='https://minuet.kde.org'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.

Minuet, skalalar, akorlar, aralıklar ve ritim hakkında 44 kulak eğitimi alıştırması içerir, müzik içeriğinin piyano klavyesinde görselleştirilmesini sağlar ve kendi egzersizlerinizin kusursuz entegrasyonuna izin verir.

### More Help to you

{{<figure src="/announcements/applications/16.04.0/khelpcenter1604.png" class="text-center" width="600px">}}

Daha önce Plasma altında dağıtılan K Yardım Merkezi, artık KDE uygulamalarının bir parçası olarak dağıtılmaktadır.

K Yardım Merkezi ekibi tarafından yürütülen büyük bir hata önceliklendirme ve temizleme kampanyası, birçoğu daha önce işlevsel olmayan arama işlevini iyileştirmek ve geri yüklemekle ilgili olan 49 çözülmüş hatayla sonuçlandı.

The internal document search which relied on the deprecated software ht::/dig has been replaced with a new Xapian-based indexing and search system which leads to the restoring of functionalities such as searching within man pages, info pages and KDE Software provided Documentation, with the added provision of bookmarks for the documentation pages.

With the removal of KDELibs4 Support and a further cleanup of the code and some other minor bugs, the maintenance of the code has also been thoroughly furnished to give you KHelpCenter in a newly shiny form.

### Aggressive Pest Control

Kontak Takımı'nda 55 büyük hata çözüldü; bunlardan bazıları alarm ayarlama ve Thunderbird postalarının içe aktarılması, Skype &amp; Kişiler Paneli Görünümündeki Google konuşma simgeleri, klasör içe aktarma, vCard içe aktarma, ODF posta eklerini açma, Chromium'dan URL ekleme gibi K Posta ile ilgili geçici çözümler, bağımsız bir kullanımın aksine Kontak'ın bir parçası olarak başlayan uygulamayla araç menüsü farklılıkları, eksik 'Gönder' menü ögesi ve birkaç tane daha. Macarca ve İspanyolca beslemeleri için adreslerin sabitlenmesiyle birlikte Brezilya Portekizcesi RSS beslemeleri için destek eklendi.

The new features include a redesign of the KAddressbook contact editor, a new default KMail Header theme, improvements to the Settings Exporter and a fix of Favicon support in Akregator. The KMail composer interface has been cleaned up along with the introduction of a new Default KMail Header Theme with the Grantlee theme used for the 'About' page in KMail as well as Kontact and Akregator. Akregator now uses QtWebKit - one of the major engines to render webpages and execute javascript code as the renderer web engine and the process of implementing support for QtWebEngine in Akregator and other Kontact Suite applications has already begun. While several features were shifted as plugins in kdepim-addons, the pim libraries were split into numerous packages.

### Derinlikte Arşivleme

{{<figure src="/announcements/applications/16.04.0/ark1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.

Ark artık arşiv türü, sıkıştırılmış ve sıkıştırılmamış boyut, MD5 / SHA-1 / SHA-256 kriptografik hash değerleri gibi şu anda açık olan arşivle ilgili bilgileri görüntüleyen bir özellikler iletişim kutusu da içeriyor.

Ark ayrıca artık ücretsiz olmayan rar izlencelerini kullanmadan RAR arşivlerini açabilir ve çıkarabilir ve lzop/lzip/lrzip biçimleriyle sıkıştırılmış TAR arşivlerini açabilir, çıkarabilir ve oluşturabilir.

Ark Kullanıcı Arayüzü, kullanılabilirliği artırmak ve belirsizlikleri gidermek ve artık varsayılan olarak gizlenmiş durum çubuğu sayesinde dikey alandan tasarruf etmek için menü çubuğunu ve araç çubuğunu yeniden düzenleyerek cilalandı.

### Video Düzenlemelerinde Daha Kesinlik

{{<figure src="/announcements/applications/16.04.0/kdenlive1604.png" class="text-center" width="600px">}}

<a href='https://www.kde.org/applications/multimedia/kdenlive/'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.

Integrated display of audio levels allows easy monitoring of the audio in your project alongwith new video monitor overlays displaying playback framerate, safe zones and audio waveforms and a toolbar to seek to markers and a zoom monitor.

Ayrıca, klibe uygulanan efektleri, bunlar olmadan klibe uygulanan efektleri karşılaştırmak ve görselleştirmek için projeler arasında dizileri kopyalamaya/yapıştırmaya ve zaman çizelgesinde bölünmüş bir görünüme izin veren yeni bir kitaplık özelliği de mevcuttur.

Oluşturma iletişim kutusu, daha hızlı kodlama elde etmek için eklenen bir seçenekle yeniden yazıldı, böylece büyük dosyalar üretildi ve hız efekti sesle de çalışmak üzere yapıldı.

Curves in keyframes have been introduced for a few effects and now your top chosen effects can be accessed quickly via the favourite effects widget. While using the razor tool, now the vertical line in the timeline will show the accurate frame where the cut will be performed and the newly added clip generators will enable you to create color bar clips and counters. Besides this, the clip usage count has been re-introduced in the Project Bin and the audio thumbnails have also been rewritten to make them much faster.

The major bug fixes include the crash while using titles (which requires the latest MLT version), fixing corruptions occurring when the project frames per second rate is anything other than 25, the crashes on track deletion, the problematic overwrite mode in the timeline and the corruptions/lost effects while using a locale with a comma as the separator. Apart from these, the team has been consistently working to make major improvements in stability, workflow and small usability features.

### Ve dahası!

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.

<a href='https://www.kde.org/applications/utilities/kleopatra'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. 
