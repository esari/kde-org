---
aliases:
- ../../kde-frameworks-5.10.0
date: '2015-05-08'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### KActivities

- (no changelog provided)

### KConfig

- Generate QML-proof classes using the kconfigcompiler

### KCoreAddons

- New cmake macro kcoreaddons_add_plugin to create KPluginLoader-based plugins more easily.

### KDeclarative

- Fix crash in texture cache.
- ve diğer düzeltmeler

### KGlobalAccel

- Add new method globalShortcut which retrieves the shortcut as defined in global settings.

### KIdleTime

- Prevent kidletime from crashing on platform wayland

### KIO

- Added KPropertiesDialog::KPropertiesDialog(urls) and KPropertiesDialog::showDialog(urls).
- Asynchronous QIODevice-based data fetch for KIO::storedPut and KIO::AccessManager::put.
- Fix conditions with QFile::rename return value (bug 343329)
- Fixed KIO::suggestName to suggest better names (bug 341773)
- kioexec: Fixed path for writeable location for kurl (bug 343329)
- Store bookmarks only in user-places.xbel (bug 345174)
- Duplicate RecentDocuments entry if two different files have the same name
- Çöp kutusu için tek bir dosya çok büyükse daha iyi hata mesajı (hata 332692)
- Fix KDirLister crash upon redirection when the slot calls openURL

### KNewStuff

- New set of classes, called KMoreTools and related. KMoreTools helps to add hints about external tools which are potentially not yet installed. Furthermore, it makes long menus shorter by providing a main and more section which is also user-configurable.

### KNotifications

- Fix KNotifications when used with Ubuntu's NotifyOSD (bug 345973)
- Don't trigger notification updates when setting the same properties (bug 345973)
- Introduce LoopSound flag allowing notifications to play sound in a loop if they need it (bug 346148)
- Bildirimin bir widget'ı yoksa çökme

### KPackage

- Add a KPackage::findPackages function similar to KPluginLoader::findPlugins

### KPeople

- Use KPluginFactory for instantiating the plugins, instead of KService (kept for compatibility).

### KService

- Giriş yolunun yanlış bölünmesini düzeltin (hata 344614)

### K Cüzdan

- Migration agent now also check old wallet is empty before starting (bug 346498)

### KWidgetsAddons

- KDateTimeEdit: Fix so user input actually gets registered. Fix double margins.
- KFontRequester: fix selecting monospaced fonts only

### KWindowSystem

- Don't depend on QX11Info in KXUtils::createPixmapFromHandle (bug 346496)
- new method NETWinInfo::xcbConnection() -&gt; xcb_connection_t*

### KXmlGui

- İkincil kısayol ayarlandığında kısayolları düzeltin (hata 345411)
- Hata raporlama için bugzilla ürünlerinin/bileşenlerinin listesini güncelleme (hata 346559)
- Global kısayollar: alternatif kısayolu da yapılandırmaya izin ver

### NetworkManagerQt

- Yüklenen başlıklar artık diğer tüm çerçeveler gibi düzenlenmiştir.

### Plasma framework

- PlasmaComponents.Menü artık bölümleri destekliyor
- Use KPluginLoader instead of ksycoca for loading C++ dataengines
- Consider visualParent rotation in popupPosition (bug 345787)

### Sonnet

- Don't try to highlight if there is no spell checker found. This would lead to an infinite loop with rehighlighRequest timer firing constanty.

### Frameworkintegration

- Fix native file dialogs from widgets QFileDialog: ** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed. ** File dialogs opened with open() or show() with parent were not opened at all.

Bu sürüm hakkında fikir ve önerilerinizi <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot makalesinde</a> yorum olarak ekleyebilirsiniz.
