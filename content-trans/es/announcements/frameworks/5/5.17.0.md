---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Se ha corregido el filtro de fechas usado por «timeline://».
- BalooCtl: Volver tras las órdenes.
- Se ha limpiado y blindado «Baloo::Database::open()»; contemplar más condiciones de fallos.
- Se ha añadido una comprobación en «Database::open(OpenDatabase)» para que falle si la base de datos no existe.

### Iconos Brisa

- Muchos iconos añadidos o mejorados
- Usar hojas de estilos en los iconos de Brisa (error 126166).
- Se ha corregido y modificado la pantalla de bloqueo del sistema (error 355902).
- Se ha añadido el icono «dialog-information» de 24 píxeles para las aplicaciones de GTK (error 355204).

### Módulos CMake adicionales

- No mostrar una advertencia cuando los iconos SVG(Z) se proporcionan con múltiples tamaños/niveles de detalle.
- Asegurarse de que se cargan las traducciones en el hilo principal (error 346188).
- Rediseño del sistema de compilación de ECM.
- Hacer que se pueda activar «Clazy» en cualquier proyecto de KDE.
- No buscar la biblioteca XINPUT de XCB de forma predeterminada.
- Limpiar el directorio de exportación antes de volver a generar una APK.
- Usar «quickgit» para la URL del repositorio Git.

### Integración con Frameworks

- Se ha añadido «fallo de instalación de plasmoide» a «plasma_workspace.notifyrc».

### KActivities

- Se ha corregido un bloqueo durante el primer inicio del demonio.
- Se ha movido la creación de «QAction» al hilo principal (error 351485).
- A veces, el formato de «clang» toma una mala decisión (error 355495).
- Se han eliminado potenciales problemas de sincronización.
- Usar «org.qtproject» en lugar de «com.trolltech».
- Se ha eliminado el uso de «libkactivities» de los complementos.
- Se ha eliminado la configuración de «KAStats» de la API.
- Se ha añadido enlace y desenlace a «ResultModel».

### Herramientas KDE Doxygen

- Hacer más robusto a «kgenframeworksapidox».

### KArchive

- Se ha corregido «KCompressionDevice::seek()», llamada cuando se crea un «KTar» sobre un «KCompressionDevice».

### KCoreAddons

- KAboutData: Permitir «https://» y otros esquemas de URL en la página web (error 355508).
- Reparar la propiedad «MimeType» cuando se usa «kcoreaddons_desktop_to_json()».

### KDeclarative

- Adaptar KDeclarative para que use KI18n directamente.
- El «delegateImage» de «DragArea» puede ser ahora una cadena de la que se crea un icono automáticamente.
- Añadir nueva biblioteca CalendarEvents.

### KDED

- Desasignar la variable del sistema SESSION_MANAGER en lugar de definirla como vacía

### Soporte de KDELibs 4

- Corregir algunas llamadas i18n.

### KFileMetaData

- Marcar m4a como legible por taglib

### KIO

- Diálogo de cookies: hacer que funcione como estaba previsto.
- Se ha corregido una sugerencia de nombre de archivo cambiándola a algo aleatorio cuando se cambia el tipo MIME de «guardar como».
- Registrar el nombre «DBus» para «kioexec» (error 353037).
- Actualizar «KProtocolManager» tras un cambio de configuración.

### KItemModels

- Se ha corregido el uso de «KSelectionProxyModel» en «QTableView» (error 352369).
- Se ha corregido el reinicio o cambio del modelo fuente de «KRecursiveFilterProxyModel».

### KNewStuff

- «registerServicesByGroupingNames» puede definir más elementos predeterminados.
- Hacer que «KMoreToolsMenuFactory::createMenuFromGroupingNames» sea más relajado.

### KTextEditor

- Se ha añadido resaltado de sintaxis para «TaskJuggler» y «PL/I».
- Hacer que se pueda desactivar la terminación automática de palabras clave mediante la interfaz de configuración.
- Cambiar el tamaño del árbol cuando se reinicia el modelo de terminación automática.

### Framework KWallet

- Manejar correctamente el caso en el que es usuario nos desactiva.

### KWidgetsAddons

- Se ha corregido un pequeño defecto visual de «KRatingWidget» en pantallas de alta resolución.
- Refactorizar y corregir la funcionalidad introducida en el fallo 171343 (fallo 171343).

### KXMLGUI

- No llamar a «QCoreApplication::setQuitLockEnabled(true)» durante el inicio.

### Framework de Plasma

- Se ha añadido un ejemplo de plasmoide básico a la guía del desarrollador.
- Se han añadido un par de plantillas de plasmoides para «kapptemplate/kdevelop».
- [calendario] Retrasar el reinicio del modelo hasta que la vista esté preparada (error 355943)
- No cambiar la posición cuando se está ocultando (error 354352).
- [IconItem] No fallar cuando el tema de «KIconLoader» es nulo (error 355577).
- Al soltar un archivo de imagen sobre un panel ya no se ofrece la opción de usarlo como fondo para el panel.
- Al soltar un archivo «.plasmoid» sobre un panel o sobre el escritorio se instalará y se añadirá al mismo.
- Se ha eliminado el módulo «kded» de estado de la plataforma, que ya no se usa (error 348840).
- Permitir que se pueda pegar texto en los campos de contraseña.
- Se ha corregido el posicionamiento del menú de edición y se ha añadido un botón para seleccionar.
- [calendario] Usar el idioma de la interfaz de usuario para obtener el nombre del mes (error 353715).
- [calendario] Ordenar los eventos también por su tipo.
- [calendario] Mover la biblioteca de complementos a «KDeclarative».
- [calendario] «qmlRegisterUncreatableType» necesita unos cuantos argumentos más.
- Permitir que se puedan añadir dinámicamente categorías de configuración.
- [calendario] Mover el manejo de complementos a una clase separada.
- Permitir que los complementos puedan suministrar datos de eventos a la miniaplicación del calendario (error 349676).
- Comprobar la existencia del «slot» antes de conectarlo o desconectarlo (error 354751).
- [plasmaquick] No enlazar «OpenGL» explícitamente.
- [plasmaquick] Descartar la dependencia de «XCB::COMPOSITE» y de «DAMAGE».

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
