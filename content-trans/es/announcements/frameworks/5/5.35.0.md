---
aliases:
- ../../kde-frameworks-5.35.0
date: 2017-06-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Mejores notificaciones de errores.

### BluezQt

- Pasar una lista explícita de argumentos. Esto evita que QProcess intente manejar nuestro espacio que contenga una ruta del intérprete de órdenes.
- Se ha corregido la pérdida inmediata de cambios de propiedad tras añadir un objeto (fallo 377405).

### Iconos Brisa

- Se ha actualizado el tipo MIME de «awk», ya que es un lenguaje de script (fallo 376211).

### Módulos CMake adicionales

- Restaurar la prueba de ocultación/visibilidad con Xcode 6.2.
- ecm_qt_declare_logging_category(): más guardianes de inclusión únicos para el archivo de cabecera.
- Se han añadido o mejorado los mensajes «Generado. No editar», y se han hecho coherentes.
- Se ha añadido un nuevo módulo FindGperf.
- Se ha cambiado la ruta de instalación predeterminada de «pkgconfig» para FreeBSD.

### KActivitiesStats

- Se ha corregido «kactivities-stats» en el Nivel 3.

### Herramientas KDE Doxygen

- No se considera la palabra clave Q_REQUIRED_RESULT.

### KAuth

- Verificar que quien nos está llamando es realmente quien dice ser.

### KCodecs

- Generar la salida de «gperf» en tiempo de compilación.

### KCoreAddons

- Se asegura una siembra adecuada por hilos en KRandom.
- No se vigilan las rutas de QRC (fallo 374075).

### KDBusAddons

- No se incluye el PID en la ruta de DBus en Flatpak.

### KDeclarative

- Emitir con consistencia la señal «MouseEventListener::pressed».
- Impedir una fuga de memoria en el objeto MimeData (fallo 380270).

### Soporte de KDELibs 4

- Permitir el uso de espacios en la ruta de CMAKE_SOURCE_DIR.

### KEmoticons

- Corrección: Qt5::DBus es solo para uso privado.

### KFileMetaData

- Usar /usr/bin/env para localizar Python2.

### KHTML

- Generar la salida de «gperf» de «kentities» en tiempo de compilación.
- Generar la salida de «gperf» de «doctypes» en tiempo de compilación.

### KI18n

- Se ha extendido la «Guía del programador» con notas sobre la influencia de «setlocale()».

### KIO

- Se ha solucionado un problema por el que ciertos elementos de las aplicaciones (por ejemplo, la vista de archivos de Dolphin) se convertían en inaccesibles para una configuración multiplantalla con alta densidad de ppp (fallo 363548).
- [RenameDialog] Se fuerza el uso de texto sin formato.
- Identificar los binarios PIE («application/x-sharedlib») como archivos ejecutables (fallo 350018).
- Núcleo: se expone GETMNTINFO_USES_STATVFS en el archivo de cabecera de configuración.
- PreviewJob: Omitir los directorios remotos. Resulta demasiado costoso realizar una vista previa de ellos (fallo 208625).
- PreviewJob: Se limpia el archivo temporal vacío cuando falla «get()» (fallo 208625).
- Se ha acelerado la visualización de la vista de árbol detallada evitando excesivos cambios de tamaño de las columnas.

### KNewStuff

- Se usa un QNAM único (y una caché de disco) para los trabajos HTTP.
- Caché interna para los datos del proveedor durante la inicialización.

### KNotification

- Se ha corregido que los KSNI no puedan registrar servicios en Flatpak.
- Usar el nombre de la aplicación en lugar del PID al crear el servicio DBus SNI.

### KPeople

- No exportar símbolos de bibliotecas privadas.
- Se ha corregido la exportación de símbolos de KF5PeopleWidgets y de KF5PeopleBackend.
- Limitar el uso de «#warning» a GCC.

### Framework KWallet

- Sustituir «kwalletd4» tras finalizar la migración.
- Señalar la terminación del agente de migración.
- Iniciar el temporizador para el agente de migración solo si es necesario.
- Comprobar que no existe más de una instancia de la aplicación tan pronto como sea posible.

### KWayland

- Se ha añadido «requestToggleKeepAbove/below».
- Mantener QIcon::fromTheme en el hilo principal.
- Eliminar el «changedSignal» del PID en «Client::PlasmaWindow».
- Se añade el PID al protocolo de gestión de ventanas de Plasma.

### KWidgetsAddons

- KViewStateSerializer: Se ha corregido un cuelgue que se producía al destruir la vista antes de declarar el serializador (fallo 353380).

### KWindowSystem

- Mejorar la corrección para «NetRootInfoTestWM» en rutas con espacios.

### KXMLGUI

- Definir la ventana principal como padre de los menús emergentes independientes.
- Al construir jerarquías de menús, hacer que los menús tengan como padres a sus contenedores.

### Framework de Plasma

- Se ha añadido el icono de VLC para la bandeja del sistema.
- Plantillas de plasmoides: Usar la imagen que forma parte del paquete (de nuevo).
- Se ha añadido una plantilla para la miniaplicaciones QML de Plasma con extensión QML.
- Volver a crear «plasmashellsurf» cuando se expone, se destruye o se oculta.

### Resaltado de sintaxis

- Haskell: Resaltar «julius quasiquoter» usando las reglas «Normal##Javascript».
- Haskell: Activar también el resaltado de «hamlet» para «shamlet quasiquoter».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
