---
aliases:
- ../../kde-frameworks-5.79.0
date: 2021-02-13
layout: framework
libCount: 83
qtversion: 5.14
---
### Attica

* Cambiar el uso de «QNetworkRequest::FollowRedirectsAttribute» a «QNetworkRequest::RedirectPolicyAttribute».

### Baloo

* [SearchStore] Eliminar las dependencias del sistema de archivos de la propiedad «includeFolder».
* [FileIndexerConfig] Se ha corregido la comprobación oculta de directorios incluidos explícitamente.
* [FilteredDirIterator] Solicitar archivos ocultos de «QDirIterator».

### Iconos Brisa

* Nuevos iconos de «telegram-panel».
* Usar el estilo correcto para los iconos «align-horizontal-left-out» (error 432273).
* Se han añadido iconos del nuevo «kickoff» (error 431883).
* Se ha añadido «rating-half», 100% de opacidad para «rating-unrated» y color del texto para «rating» en el tema oscuro.
* Se han eliminado los iconos de «KeePassXC» (error 431593).
* Corregir los iconos «@3x» (error 431475).
* Se ha añadido el icono «neochat».

### Módulos CMake adicionales

* Activar «GNU_TAR_FOUND» solo cuando está disponible «--sort=name».
* Eliminar la generación de metadatos «fastlane» de una determinada APK.
* KDEFrameworksCompilerSettings: Definir «-DQT_NO_KEYWORDS» y «-DQT_NO_FOREACH» de forma predeterminada.
* [KDEGitCommitHooks] Crear una copia de los guiones en el directorio de origen.
* Permitir también la nueva extensión de archivo «Appstream».
* Extracción de traducciones: Resolver la URL antes de pasarla a «fetchpo.rb».
* Considerar las URL de donación de Appstream para la creación de metadatos de F-Droid.
* Se han corregido los permisos para el «script» (error 431768).
* ECMQtDeclareLoggingCategory: Crear los archivos «.categories» al compilar, no al configurar.
* Se ha añadido una función de CMake para configurar los ganchos previos a envíos de git.

### Integración con Frameworks

* Se han corregido las decoraciones de la ventana que no se podían desinstalar (error 414570).

### Herramientas KDE Doxygen

* Asegurar que no se usa el tipo de letra de doxygen por omisión.
* Se ha mejorado la visualización de «QDoc» y se han corregido algunos errores en el tema oscuro.
* Se ha actualizado la información de mantenimiento.
* Nuevo tema consistente con develop.kde.org/docs.

### KCalendarCore

* Eliminar el registro de «MemoryCalendar» como observador cuando se borra una incidencia.
* Usar «recurrenceId» para borrar la coincidencia correcta.
* Borrar también las asociaciones del bloc de notas al cerrar un «MemoryCalendar».

### KCMUtils

* Asegurar el modo de una sola columna.

### KCodecs

* Se ha eliminado el uso de cadenas de literales no UTF-8.

### KCompletion

* Se ha corregido una regresión causada al portar del operador «+» al operador «|».

### KConfig

* Se ha refactorizado el código de guardar/restaurar la geometría de la ventana para que sea menos frágil.
* Se ha corregido la restauración del tamaño de las ventanas tras cerrarlas cuando estaban maximizadas (error 430521).
* KConfig: Preservar el componente de milisegundos de «QDateTime».

### KCoreAddons

* Se ha añadido «KFuzzyMatcher» para el filtrado difuso de cadenas de texto.
* KJob::infoMessage: Documentar que el argumento «richtext» se eliminará por no usarse.
* KJobUiDelegate::showErrorMessage: Implementar con «qWarning()».
* Marcar como obsoletos los métodos relacionados con «X-KDE-PluginInfo-Depends».
* Dejar de usar las claves «X-KDE-PluginInfo-Depends».

### KDeclarative

* Permitir elementos de una sola columna.
* KeySequenceItem: Asignar una cadena vacía al borrar en lugar de una no definida (error 432106).
* Desambiguar los estados «seleccionado» y «cursor encima» para «GridDelegate» (error 406914).
* Usar el modo sencillo por omisión.

### KFileMetaData

* ffmpegextractor: Usar «av_find_default_stream_index» para encontrar una emisión de vídeo.

### KHolidays

* Se han actualizado las festividades de Mauricio para 2021.
* Se han actualizado las festividades taiwanesas.

### KI18n

* No asignar el códec para el flujo de texto al compilar con Qt6.

### KImageFormats

* Se ha simplificado una parte del código del perfil de color NCLX.
* [imagedump] Se ha añadido la opción «Lista tipos MIME» (-m).
* Se ha corregido un fallo con archivos mal formados.
* ani: Asegurar que «riffSizeData» tiene el tamaño correcto antes del baile de moldeado a «quint32_le».
* Se ha añadido un complemento para los iconos animados de Windows (ANI).

### KIO

* Usar la macro «Q_LOGGING_CATEGORY» en lugar de «QLoggingCategory» explícitamente (error 432406).
* Corregir que se asigne el códec predeterminado a «US-ASCII» en las aplicaciones KIO (error 432406).
* CopyJob: Se ha corregido un fallo con omitir/reintentar (error 431731).
* KCoreDirLister: No sobrecargar las señales «canceled()» y «completed()».
* KFilePreviewGenerator: Modernizar el código base.
* KCoreDirLister: No sobrecargar la señal «clear()».
* MultiGetJob: No sobrecargar las señales.
* FileJob: No sobrecargar la señal «close()».
* SkipDialog: Marcar como obsoleta la señal «result(SkipDialog *_this, int _button)».
* Corregir el bloqueo al cambiar el nombre de un archivo desde el diálogo de propiedades (error 431902).
* Marcar como obsoletas «addServiceActionsTo» y «addPluginActionsTo».
* [KFilePlacesView] Menor opacidad para los elementos ocultos en «mostrar todos».
* No cambiar los directorios al abrir URL que no se pueden listar.
* Se ha modificado la lógica de «KFileWidget::slotOk» en modo archivos+directorio.
* FileUndoManager: Se ha corregido un problema al deshacer la copia de un directorio vacío.
* FileUndoManager: No sobrescribir archivos al deshacer.
* FileUndoManager: Marcar como obsoleto el método «undoAvailable()».
* ExecutableFileOpenDialog: Hacer la etiqueta de texto más genérica.
* KProcessRunner: Emitir la señal «processStarted()» solo una vez.
* Revertir «kio_trash: Corregir la lógica cuando no se ha definido un límite de tamaño».

### Kirigami

* Usar un icono no simbólico para la acción «Salir».
* Hacer que los botones del menú de la barra de herramientas se pulsen correctamente.
* Usar subsección en lugar de sección.
* [controles/BasicListItem]: Añadir la propiedad «reserveSpaceForSubtitle».
* Hacer que la altura implícita del botón de navegación sea explícita.
* Corregir el alineamiento vertical de «BasicListItem».
* [basiclistitem] Asegurar que los iconos son cuadrados.
* [controles/ListItem]: Eliminar el separador con sangría para los elementos principales.
* Volver a añadir el margen derecho del separador de elementos de la lista cuando existe un elemento principal.
* No llamar manualmente a «reverseTwinsChanged» al destruir «FormLayout» (error 428461).
* [org.kde.desktop/Unidades] Hacer que las duraciones coincidan con «controles/Unidades».
* [Unidades] Reducir «veryLongDuration» a 400 ms.
* [Unidades] Reducir 50 ms «shortDuration» y «longDuration».
* No considerar los eventos de ratón sintetizados como eventos de ratón (error 431542).
* Usar «implicitHeight» más agresivamente en lugar de «preferredHeight».
* Usar texturas de atlas para los iconos.
* controles/AbstractApplicationHeader: Centrar los hijos verticalmente.
* [actiontextfield] Se han corregido los márgenes y el tamaño de la acción en línea.
* Actualizar correctamente el tamaño de la cabecera (error 429235).
* [controles/OverlaySheet]: Respetar «Layout.maximumWidth» (error 431089).
* [controles/PageRouter]: Exponer los parámetros definidos al volcar «currentRoutes».
* [controles/PageRouter]: Exponer los parámetros del nivel superior en el mapa de propiedades «params».
* Mover los ejemplos relativos a «pagerouter» a una subcarpeta.
* Posibilidad de arrastrar la ventana desde áreas no interactivas.
* AbstractApplicationWindow: Usar una ventana ancha en los sistemas de escritorio para todos los estilos.
* No ocultar el separador de elementos de lista al situar el cursor encima cuando el fondo es transparente.
* [controles/ListItemDragHandle] Corregir la organización incorrecta para el caso sin desplazamiento (error 431214).
* [controles/applicationWindow]: Tener en cuenta la anchura de los cajones al calcular «wideScreen».

### KNewStuff

* Se han refactorizado la cabecera y el pie de página de «KNSQuick» para el uso de Kirigami.
* Añadir una etiqueta numérica al componente de puntuación para una mejor legibilidad.
* Reducir el tamaño mínimo de la ventana de diálogo GHNS en QML.
* Hacer coincidir la apariencia flotante más clara para los delegados de la cuadrícula KCM.
* Asegurar que el ancho mínimo para el diálogo QML sea el ancho de la pantalla o menor.
* Corregir el diseño del delegado de «BigPreview».
* Volver a validad la entradas en caché antes de mostrar el diálogo.
* Permitir el uso de URL de tipo «kns:/» en la herramienta del diálogo de «knewstuff» (error 430812).
* filecopyworker: Abrir archivos antes de leer/escribir.
* Reiniciar la entrada a «updatetable» cuando no se ha identificado ninguna carga útil para actualizar (error 430812).
* Solucionar fallos ocasionales debido a la retención incorrecta de un puntero.
* Marcar como obsoleta la clase «DownloadManager».
* Marcar como obsoleta la propiedad «AcceptHtmlDownloads».
* Marcar como obsoletas las propiedades «ChecksumPolicy» y «SignaturePolicy».
* Marcar como obsoleta la propiedad «Scope».
* Marcar como obsoleta la propiedad «CustomName».

### KNotification

* Emitir «NewMenu» cuando se define un nuevo menú de contexto (error 383202).
* Marcar como obsoleto «KPassivePopup».
* Asegurarse de que todos los motores hacen referencia de la notificación antes de realizar su trabajo.
* Hacer que la aplicación de ejemplo de notificaciones se pueda compilar y funcione en Android.
* Se ha movido el manejo de la identificación de notificaciones a la clase «KNotification».
* Se ha corregido la eliminación de notificaciones pendientes de la cola (error 423757).

### Framework KPackage

* Documentar la propiedad de «PackageStructure» cuando se usa «PackageLoader».

### KPty

* Se ha corregido la generación de la ruta completa de «kgrantpty» en el código para «! HAVE_OPENPTY».

### KQuickCharts

* Añadir un método «first» a «ChartDataSource» y usarlo en «Legend» (error 432426).

### KRunner

* Comprobar la acción seleccionada en caso de coincidencia de información.
* Corregir la cadena de resultados vacía para la actividad actual.
* Marcar como obsoletas las sobrecargas de ids de «QueryMatch».
* [Lanzador DBus] Prueba «RemoteImage».

### KService

* Marcar como obsoleto «KPluginInfo::dependencies()».
* CMake: Especificar las dependencias de «add_custom_command()».
* Marcar como obsoleta la sobrecarga de «KToolInvocation::invokeTerminal».
* Añadir un método para obtener el «KServicePtr» de la aplicación de terminal predeterminada.
* KService: Se ha añadido un método para definir el directorio de trabajo.

### KTextEditor

* [Vimode] No cambiar de vista al cambiar entre mayúsculas y minúsculas (orden «~») (error 432056).
* Aumentar la anchura máxima de sangrado a 200 (error 432283).
* Asegurar que se actualiza la asignación de intervalos, por ejemplo, en la invalidación de intervalos que ya estén vacíos.
* Mostrar solo el error de caracteres de marcador en «vimode» (error 424172).
* [vimode] Corregir el movimiento para hacer coincidir el elemento uno por uno.
* Retener el texto de sustitución siempre que la barra de búsqueda integrada no esté cerrada (error 338111).
* KateBookMarks: Se ha modernizado el código base.
* No ignorar el canal alfa cuando existe una marca.
* Se ha corregido que el canal alfa se ignorase al leer desde la interfaz de configuración.
* Evitar que la vista previa de la concordancia de paréntesis se extienda demasiado fuera de la vista.
* Impedir la vista previa de la coincidencia de paréntesis se demore en algunos casos tras cambiar a una pestaña diferente.
* Hacer que la vista previa de coincidencia de paréntesis sea más compacta.
* No mostrar la vista previa de coincidencia de paréntesis si cubre el cursor.
* Maximizar la anchura de la vista previa de coincidencia de paréntesis.
* Ocultar la vista previa de la coincidencia de paréntesis cuando hay desplazamiento.
* Evitar los intervalos de resaltado duplicados que eliminan la visualización ARGB.
* Exponer el «KSyntaxHighlighting::Repository» global de solo lectura.
* Se ha corregido un error de sangrado cuando la línea contiene «for» o «else».
* Se ha corregido un error de sangrado.
* Eliminar la «tagLine» del caso especial, ya que producía fallos de actualización aleatorios.
* Se ha corregido la visualización de los marcadores de ajuste de palabras + selección.
* Se ha corregido la sangría cuando se pulsa «Intro» y el parámetro de la función tiene una coma al final.
* Pintar el pequeño hueco en el color de selección si el final de la línea anterior está en la selección.
* Se ha simplificado el código y se han corregido los comentarios.
* Revertir el error de corte, borraba demasiado código para visualizaciones adicionales.
* Evitar la pintura de selección de línea completa, más en línea con otros editores.
* Adaptar el indentador a los archivos «hl» modificados.
* [Modo VI] Se ha adaptado «Command» a «QRegularExpression».
* [Modo VI] Adaptar «findPrevWordEnd» y «findSurroundingBrackets» para que usen «QRegularExpression».
* [Modo VI] Adaptar «QRegExp::lastIndexIn» para usar «QRegularExpression» y «QString::lastIndexOf».
* [Modo VI] Adaptar «ModeBase::addToNumberUnderCursor» para usar «QRegularExpression».
* [Modo VI] Adaptar los usos simples de «QRegExp::indexIn» a «QRegularExpression» y «QString::indexOf».
* Usar rgba(r,g,b,aF) para manejar el canal alfa al exportar en HTML.
* Respetar los colores con alfa al exportar a HTML.
* Introducir un método auxiliar para obtener el nombre del color correctamente.
* Soporte de colores alfa: Capa de configuración.
* Evitar que los marcadores de cambio de línea eliminen el resaltado de la línea actual.
* Pintar la línea actual resaltando también el borde del icono.
* Activar el canal alfa para los colores del editor.
* Corregir que el resaltado de la línea actual tenga un pequeño hueco al principio para líneas ajustadas dinámicamente.
* No realizar cálculos innecesarios, sino comparar los valores directamente.
* Corregir que el resaltado de la línea actual tenga un pequeño hueco al principio.
* [Vimode] No omitir el intervalo de plegado cuando el movimiento cae dentro.
* Usar un bucle «for de intervalo» sobre «m_matchingItems» en lugar de usar «QHash::keys()».
* Se ha adaptado «normalvimode» a «QRegularExpression».
* Exportar correctamente las dependencias correctas.
* Exponer el tema de «KSyntaxHighlighting».
* También se ha añadido «configChanged» a «KTextEditor::Editor» para los cambios de configuración globales.
* Reorganizar un poco cosas relacionadas con la configuración.
* Se ha corregido el manejo de claves.
* Añadir parámetros «doc/view» a las nuevas señales y corregir conexiones antiguas.
* Permitir el acceso y la alteración del tema actual usando la interfaz de configuración.
* Eliminar el calificador constante al pasar «LineRanges».
* Usar «.toString()», ya que «QStringView» carece de «.toInt()» en versiones antiguas de Qt.
* Declarar «toLineRange()» como «constexpr inline» cuando sea posible.
* Reusar la versión de «QStringView» de «QStringRev», eliminar el calificador constante.
* Mover el comentario KF6 del TODO fuera del comentario de doxygen.
* Cursor, Range: Se ha añadido la sobrecarga de «fromString(QStringView)».
* Permitir que se pueda desactivar la «Sangría de alineación de ajuste dinámico de palabras» (error 430987).
* LineRange::toString(): Evitar el signo «-», ya que es confuso para números negativos.
* Usar «KTextEditor::LineRange» en el mecanismo «notifyAboutRangeChange()».
* Se han portado «tagLines()», «checkValidity()» y «fixLookup()» a «LineRanges».
* Se ha añadido «KTextEditor::LineRange».
* Mover la implementación de «KateTextBuffer::rangesForLine()» a «KateTextBlock» y evitar construcciones de contenedores innecesarias.
* [Modo VI] Se ha corregido la búsqueda dentro de intervalos de carpetas (error 376934).
* [Modo VI] Se ha corregido la reproducción de la terminación de macro (error 334032).

### KTextWidgets

* Tener más clases privadas heredadas de las de los padres.

### KUnitConversion

* Definir variable antes de usarla.

### KWidgetsAddons

* Hacer uso de «AUTORCC».
* Tener más clases privadas heredadas de las de los padres.
* Incluir explícitamente «QStringList».

### KWindowSystem

* Se han añadido aplicaciones auxiliares de conveniencia para opacidad fraccionaria.
* Se han corregido realmente los archivos de cabecera.
* Se han corregido archivos de cabecera.
* xcb: Trabajar con la pantalla activa que notifica «QX11Info::appScreen()».

### KXMLGUI

* Se han corregido archivos de cabecera.
* Añadir la señal «KXMLGUIFactory::shortcutsSaved».
* Usar la URL correcta para implicarse en KDE (error 430796).

### Framework de Plasma

* [plasmoidheading] Usar el color planeado en los pies de página.
* [plasmacomponents3/spinbox] Se ha corregido el color del texto seleccionado.
* widgets>lineedit.svg: Se han corregido problemas de alineamiento de píxeles (error 432422).
* Corregir relleno inconsistente izquierdo y derecho en «PlasmoidHeading».
* Se han actualizado los colores de Brisa oscuro y de Brisa claro.
* Se ha revertido «[SpinBox] Se ha corregido un error de lógica en el desplazamiento direccional».
* [calendario] corregir los nombres de importación ausentes.
* [ExpandableListItem] Hacer que la vista de la lista de acciones expandidas respete el tipo de letra.
* DaysCalendar: potar a PC3/QQC2 cuando sea posible.
* Se ha eliminado la animación al situar el cursor sobre botones planos, calendario, elementos de listas y botones.
* Volcar los errores de los plasmoides en la consola.
* Resolver una dependencia cíclica de Units.qml.
* [PlasmaComponents MenuItem] Crear una acción de relleno cuando se destruye la acción.
* No hacer los mapas de bits mayores de lo necesario.
* Añadir los archivos de proyecto del IDE JetBrains para que se ignoren.
* Se han corregido advertencias de conexiones.
* Se ha añadido «RESET» a la propiedad «globalShortcut» (error 431006).

### Purpose

* [nextcloud] Se ha remodelado la interfaz gráfica de configuración.
* Evaluar la configuración inicial.
* Recortar las vistas de lista en la configuración de kdeconnect y de bluetooth.
* Eliminar las propiedades adjuntas no necesarias de «Layout».
* [complementos/nextcloud] Usar el icono de Nextcloud.
* [cmake] Mover «find_package» al «CMakeLists.txt» de nivel superior.

### QQC2StyleBridge

* [caja combinada] Se ha corregido la velocidad de desplazamiento del panel táctil (error 400258).
* «qw» puede ser nulo.
* Permitir el uso de «QQuickWidget» (error 428737).
* Permitir que se pueda arrastrar una ventana desde áreas vacías.

### Solid

* CMake: Usar «configure_file()» para asegurar que se compila el «noop» incremental.
* [Fstab] Ignorar montajes superpuestos del panel (error 422385).

### Sonnet

* No realizar múltiples búsquedas cuando una es suficiente.

### Resaltado de sintaxis

* Se ha añadido el incremento de versión que faltaba de «context.xml».
* Permitir las terminaciones de archivo oficiales. Consultar https://mailman.ntg.nl/pipermail/ntg-context/2020/096906.html
* Actualizar los resultados de referencia.
* Color de operador menos vibrante para temas de Brisa.
* Se han corregido la sintaxis para el nuevo «hugo».
* Se han corregido errores de sintaxis.
* Mejorar la legibilidad del tema Solarizado oscuro.
* Eliminar capturas innecesarias con una regla dinámica.
* Resaltado para el operador de fusión => actualizar referencias.
* Resaltado para el operador de fusión.
* Añadir sobrecargas constantes para algunos accesores.
* Bash, Zsh: corregir cmd;; en un caso (error 430668).
* Atom claro, Brisa oscuro/claro: nuevo color para el operador «;». Brisa claro: nuevo color para «ControlFlow».
* email.xml: Detectar comentarios anidados y caracteres escapados (error 425345).
* Actualizar Atom claro para que use colores alfa.
* Reasignar algunos estilos de símbolos y operadores a «dsOperator».
* Bash: Se ha corregido } en ${!xy*} y más operador de expansión de parámetros (# en ${#xy}; !,*,@,[*],[@] en ${!xy*}) (error 430668).
* Actualización de revisiones de temas.
* Se ha actualizado el resaltado de sintaxis de «kconfig» a Linux 5.9.
* Usar rgba(r,g,b,aF) para manejar el canal alfa de Qt/Navegadores correctamente.
* No usar «rgba» en «themeForPalette».
* Asegurar que se respeta rgba en html y en «Format».
* Correcciones para «atom-one-dark».
* Algunas actualizaciones más para Atom One oscuro.
* No comprobar rgba cuando se comprueba una mejor coincidencia.
* Permitir el uso del canal alfa en los colores del tema.
* Actualizar los colores de Monokai y corregir un azul incorrecto.
* C++: Se ha corregido el sufijo «us».
* Bash: corrección #5: $ al final de una cadena con comillas dobles.
* VHDL: corregir función, procedimiento, rango/unidades del tipo y otras mejoras.
* Se ha actualizado «breeze-dark.theme».
* Raku: #7: Corregir los símbolos que empiezan con «Q».
* Corrección rápida para el contraste ausente de «Extension».
* Añadir el esquema de color «Oblivion» de «GtkSourceView/Pluma/gEdit».

### ThreadWeaver

* Corregir los iteradores de mapeo al compilar con Qt 6.
* No iniciar explícitamente «mutexes» como «NonRecursive».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
