---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE lanza las Aplicaciones 15.08.0.
layout: application
release: applications-15.08.0
title: KDE lanza las Aplicaciones de KDE 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin tiene nuevo aspecto: ahora se basa en KDE Frameworks 5` class="text-center" width="600px" caption=`Dolphin tiene nuevo aspecto: ahora se basa en KDE Frameworks 5`>}}

Hoy, 19 de agosto de 2015, KDE ha lanzado las Aplicaciones de KDE 15.08

Con esta versión se ha pasado un total de 107 aplicaciones a <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>. El equipo se está esforzando en proporcionar a los usuarios un escritorio y unas aplicaciones de la mejor calidad, así que contamos con ustedes para que nos envíen sus comentarios.

Con esta versión se amplía la lista de aplicaciones basadas en KDE Frameworks 5, entre otras, <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>la suite Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, etc.

### Vista previa técnica de la suite Kontact

Durante los pasados meses, el equipo de KDE PIM ha hecho un gran esfuerzo para migrar Kontact a Qt 5 y KDE Frameworks 5. Además, el rendimiento del acceso a los datos ha mejorado considerablemente gracias a la optimización de la capa de comunicaciones. El equipo de KDE PIM está trabajando duramente en pulir aún más la suite Kontact y espera con ilusión los comentarios de los usuarios. Para obtener información más detallada sobre los cambios que se han realizado en KDE PIM, consulte <a href='http://www.aegiap.eu/kdeblog/'>el blog de Laurent Montels</a>.

### Kdenlive y Okular

Esta versión de Kdenlive incluye muchas correcciones en el asistente del DVD, junto con un gran número de soluciones de errores y otras funcionalidades que incluyen la integración de importantes procesos de optimización de código. Se puede obtener más información sobre los cambios realizados en Kdenlive en su <a href='https://kdenlive.org/discover/15.08.0'>exhaustivo registro de cambios</a>. Okular admite ahora la transición de sombreado en el modo de presentación.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku con un rompecabezas de caracteres` class="text-center" width="600px" caption=`KSudoku con un rompecabezas de caracteres`>}}

### Dolphin, Edu y Juegos

Dolphin también se ha migrado a KDE Frameworks 5. En Marble, se ha mejorado la implementación de <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a> y de las capas de anotaciones, edición y KML.

Se han realizado un increíble número de modificaciones de código en Ark que incluyen pequeñas correcciones. Se han hecho numerosas modificaciones en Kstars, entre otras, la mejora del algoritmo ADU y la comprobación de valores fuera de límites, guardar el cambio de meridiano, desviación de la guía, límite del foco automático HFR en el archivo de secuencias, añadir el índice del telescopio e implementar la función de desaparcado. KSudoku se ha vuelto mejor. Las modificaciones incluyen: añadir interfaz gráfica y motor para entrar en los rompecabezas Mathdoku y Killer Sudoku, y añadir una nueva forma de resolver basada en el algoritmo «Dancing Links» (DLX) de Donald Knuth.

### Otras versiones

Junto con esta versión, se lanzará Plasma 4, 4.1122, en su versión LTS por última vez.
