---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE lanza las Aplicaciones de KDE 18.04.1
layout: application
title: KDE lanza las Aplicaciones de KDE 18.04.1
version: 18.04.1
---
Hoy, 10 de mayo de 2018, KDE ha lanzado la primera actualización de estabilización para las <a href='../18.04.0'>Aplicaciones 18.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 20 correcciones de errores registradas, se incluyen mejoras en Kontact, Cantor, Dolphin, Gwenview, JuK, Okular y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- Las entradas duplicadas en el panel de lugares de Dolphin ya no causan cuelgues.
- Se ha corregido un antiguo error en la recarga de archivos SVG en Gwenview.
- La importación C++ de Umbrello ya entiende la palabra clave «explicit».
