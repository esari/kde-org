---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE lanza las Aplicaciones de KDE 15.04.2
layout: application
title: KDE lanza las Aplicaciones de KDE 15.04.2
version: 15.04.2
---
Hoy, 2 de junio de 2015, KDE ha lanzado la segunda actualización de estabilización para las <a href='../15.04.0'>Aplicaciones 15.04</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Entre las más de 30 correcciones de errores registradas, se incluyen mejoras en gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui y umbrello.

También se incluyen versiones de los Espacios de trabajo Plasma 4.11.20, de la Plataforma de desarrollo de KDE 4.14.9 y de la suite Kontact 4.14.9 que contarán con asistencia a largo plazo.
