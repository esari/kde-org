---
date: '2013-12-18'
hidden: true
title: Las aplicaciones de KDE 4.12 experimentan un gran avance en la gestión de la
  información personal y mejoras en todas ellas
---
La Comunidad de KDE se complace en anunciar la última actualización importante de las aplicaciones de KDE, la cual incluye nuevas funcionalidades y soluciones de errores. Esta versión supone enormes mejoras para KDE PIM, las cuales proporcionan un mejor rendimiento y muchas nuevas funcionalidades. Kate mejora la integración con los complementos de Python y añade la implementación inicial de macros para Vim y los juegos y las aplicaciones educativas incorporan varias funcionalidades nuevas.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Kate, el editor de texto más avanzado de Linux, presenta más mejoras en el completado de código, esta vez gracias a la introducción de una función de <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>completado avanzado de código, la cual puede tratar los casos de abreviaciones y coincidencias parciales en las clases</a>. Por ejemplo, si se escribe «QualIdent», una coincidencia para el nuevo código podría ser «QualifiedIdentifier». Ahora, Kate también <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>admite inicialmente macros de Vim</a>. Lo mejor de todo es que estas mejoras se han transferido también a KDevelop y a otras aplicaciones que utilizan la tecnología de Kate.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Ahora, el visor de documentos Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'> tiene en cuenta los márgenes de la impresora</a>, admite audio y vídeo para el formato epub, realiza mejor las búsquedas y puede gestionar más transformaciones, incluidas las de los metadatos de las imágenes Exif. Ahora, en la herramienta de diagramas UML, Umbrello, las asociaciones se pueden <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>dibujar con diferentes diseños</a> y <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>Umbrello añade comentarios si el elemento gráfico está documentado</a>.

El vigilante de la privacidad, KGpg, muestra más información a los usuarios y ahora, KWalletManager, la herramienta para guardar contraseñas, puede <a href=' http://www.rusu.info/wp/?p=248 '>guardarlas en formato GPG</a>. Konsole presenta una nueva funcionalidad: Ctrl-clic para lanzar una URL directamente en la salida de la consola. Ahora también puede <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>mostrar un listado de procesos cuando advierte sobre el cierre de la aplicación</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebkit añade la capacidad de <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>cambiar automáticamente el tamaño del contenido para adaptarse a la resolución del escritorio</a>. El administrador de archivos, Dolphin, presenta numerosas mejoras de rendimiento a la hora de ordenar y mostrar archivos, lo cual reduce el uso de memoria y acelera el funcionamiento. KRDC incluye la función de volver a establecer la conexión de manera automática en VNC y ahora, KDialog proporciona acceso a los cuadros de mensajes «detailedsorry» y «detailederror» para lograr unos scripts más informativos. Kopete ha actualizado su complemento OTR y el protocolo Jabber admite XEP-0264: miniaturas de transferencia de archivos. Además de dichas funcionalidades, el trabajo se ha centrado en limpiar el código y en solucionar advertencias de compilación.

### Juegos y aplicaciones educativas

Los juegos de KDE presentan mejoras en varios aspectos. KReversi presenta un mejor aspecto y una experiencia de juego más fluida gracias a que ahora <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>se basa en QML y en Qt Quick</a>, KNetwalk también <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>se ha portado</a> con los mismos beneficios, así como con la capacidad de definir una anchura y altura personalizadas para la cuadrícula. Ahora, Konquest cuenta con un nuevo jugador de IA llamado «Becai» que representa un gran reto.

En las aplicaciones educativas se han implementado varios cambios importantes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>admite lecciones personalizadas y varios cursos nuevos</a>; KStars cuenta con un nuevo <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>módulo de alineación para telescopios</a> más preciso, tal como se aprecia en <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>este vídeo de youtube</a> con las nuevas funcionalidades. Cantor, que ofrece una potente y sencilla interfaz de usuario para numerosos motores matemáticos, tiene ahora motores <a href='http://blog.filipesaraiva.info/?p=1171'>para Python2 y Scilab</a>. Puede encontrar más información sobre el potente motor de Scilab <a href='http://blog.filipesaraiva.info/?p=1159'>aquí</a>. Marble añade la integración con ownCloud (la configuración está disponible en Preferencias) y ahora implementa la generación en superposición. KAlgebra permite exportar diseños 3D a formato PDF, lo que proporciona una estupenda manera de compartir su trabajo. Por último, pero no menos importante, se han solucionado muchos errores de las aplicaciones educativas de KDE.

### Correo, calendario e información personal 

Se ha trabajado mucho en KDE PIM, el conjunto de aplicaciones de KDE para gestionar el correo, el calendario y otra información personal.

Para empezar, el cliente de correo, KMail, ahora <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>admite AdBlock</a> (cuando está activado el HTML) y ha mejorado la detección de mensajes fraudulentos mediante la ampliación de las URL abreviadas. Un nuevo agente de Akonadi llamado FolderArchiveAgent, permite a los usuarios comprimir los mensajes leídos en carpetas concretas y se ha despejado la interfaz de usuario de la funcionalidad «Enviar después». KMail también se ha beneficiado de las mejoras en la implementación de los filtros Sieve. Sieve permite filtrar los mensajes en el lado del servidor y ahora se pueden <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>crear y modificar los filtros en los servidores</a> y <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convertir los filtros existentes en KMail en filtros de servidor</a>. También <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>se ha mejorado</a> la implementación de mbox en KMail.

En otras aplicaciones, varios de los cambios facilitan el trabajo y lo hacen más agradable. Se ha incluido una nueva herramienta, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>el ContactThemeEditor</a>, el cual permite crear temas Grantlee de KAddressBook para mostrar los contactos. Ahora, la libreta de direcciones también muestra vistas previas antes de imprimir los datos. En KNotes, se ha realizado un <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>gran trabajo a la hora de resolver errores</a>. Ahora, la herramienta de blogging, Blogilo, puede gestionar traducciones y hay una gran variedad de soluciones de errores y de mejoras en todas las aplicaciones de KDE PIM.

La caché de datos subyacente de KDE PIM <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'> ha mejorado mucho su rendimiento, estabilidad y adaptabilidad al mejorar la implementación de PostgreSQL con el reciente Qt 4.8.5</a>, lo cual ha beneficiado a todas las aplicaciones. Y hay una nueva herramienta de línea de órdenes, el calendarjanitor que puede explorar todos los datos del calendario en busca de incidencias y un diálogo de depuración para búsquedas. Desde aquí, queremos hacer una especial mención a Laurent Montel por el trabajo que está realizando en las funcionalidades de PIM.

#### Instalación de las aplicaciones de KDE

El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href='http://windows.kde.org'>KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href='http://mac.kde.org/'>KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no se utilizan. <a href='http://plasma-active.org'>Plasma Active</a> es una experiencia de usuario para una amplia variedad de dispositivos, como tabletas y otro tipo de hardware móvil.

Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a>, y también en <a href='/download'>CD-ROM</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> de la actualidad.

##### Paquetes

Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.12.0 para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible.

##### Ubicación de los paquetes

Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha sido notificado, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Wiki de la Comunidad</a>.

La totalidad del código fuente de %[1] se puede <a href='/info/4/4.12.0'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.12.0 en la <a href='/info/4/4.12.0#binary'>Página de información sobre 4.12.0</a>.

#### Requisitos del sistema

Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, como la 4.8.4. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.

Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
