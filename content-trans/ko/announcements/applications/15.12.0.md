---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: KDE Ships Applications 15.12.
layout: application
release: applications-15.12.0
title: KDE에서 KDE 프로그램 15.12.0 출시
version: 15.12.0
---
2015년 12월 16일. 오늘 KDE에서는 KDE 프로그램 15.12를 출시했습니다.

KDE is excited to announce the release of KDE Applications 15.12, the December 2015 update to KDE Applications. This release brings one new application and feature additions and bug fixes across the board to existing applications. The team strives to always bring the best quality to your desktop and these applications, so we're counting on you to send your feedback.

In this release, we've updated a whole host of applications to use the new <a href='https://dot.kde.org/2013/09/25/frameworks-5'>KDE Frameworks 5</a>, including <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.

### 다양한 새로운 기능 추가

14년간 KDE의 일부였던 KSnapshot이 물러나고 새로운 스크린샷 프로그램인 Spectacle이 등장했습니다.

With new features and a completely new UI, Spectacle makes taking screenshots as easy and unobtrusive as it can ever be. In addition to what you could do with KSnapshot, with Spectacle you can now take composite screenshots of pop-up menus along with their parent windows, or take screenshots of the entire screen (or the currently active window) without even starting Spectacle, simply by using the keyboard shortcuts Shift+PrintScreen and Meta+PrintScreen respectively.

We've also aggressively optimised application start-up time, to absolutely minimise the time lag between when you start the application and when the image is captured.

### 모든 곳 다듬기

많은 프로그램에 안정성과 버그 수정 외에도 여러 다듬기 작업을 진행했습니다.

<a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, the non-linear video editor, has seen major fixes to its User Interface. You can now copy and paste items on your timeline, and easily toggle transparency in a given track. The icon colours automatically adjust to the main UI theme, making them easier to see.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`Ark에서 ZIP 파일 내 주석을 표시할 수 있음`>}}

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, the archive manager, can now display comments embedded in ZIP and RAR archives. We've improved support for Unicode characters in file names in ZIP archives, and you can now detect corrupt archives and recover as much data as possible from them. You can also open archived files in their default application, and we've fixed drag-and-drop to the desktop as well previews for XML files.

### All Play and No Work

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`KSudoku의 새로운 시작 화면(Mac OS X)`>}}

The KDE Games developers have been working hard for the past few months to optimise our games for a smoother and richer experience, and we've got a lot of new stuff in this area for our users to enjoy.

In <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.

In <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.

We've also included stability fixes for games like KNavalBattle, Klickety, <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> and <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.

### 중요한 수정

Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='https://bugs.kde.org'>report them</a>!

In our file manager <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.

The <a href='https://userbase.kde.org/Kontact'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.

### 앞으로 나아가기

더 나은 유지 관리를 위해서 KDE 프로그램 릴리스에서 일부 프로그램을 삭제했으며 더 이상 KDE 프로그램 15.12 릴리스에 포함되지 않습니다

프로그램 4개(Amor, KTux, KSnapshot 및 SuperKaramba)를 삭제했습니다. 위에서 언급한 대로 KSnapshot은 Spectacle로 대체되었으며, SuperKaramba의 위젯 엔진으로서의 기능은 Plasma가 대체합니다. 현대적인 데스크톱에서는 화면 잠금이 매우 다르게 처리되므로 단독 실행형 화면 보호기 지원이 삭제되었습니다.

아트워크 패키지 3개(kde-base-artwork, kde-wallpapers, kdeartwork)를 삭제했습니다. 오랜 기간 동안 업데이트되지 않았습니다.

### 전체 변경 기록
