---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE veröffentlicht die KDE-Anwendungen 15.12.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.12.2
version: 15.12.2
---
16. Februar 2016. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../15.12.0'>KDE-Anwendungen 15.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 30 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen unter anderem für kdelibs, kdepim, Kdenlive, Marble, Konsole, Spectacle, Akonadi, Ark und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
