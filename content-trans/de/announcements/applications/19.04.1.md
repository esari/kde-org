---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE veröffentlicht die Anwendungen 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE veröffentlicht die KDE-Anwendungen 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

Heute veröffentlicht KDE die erste Aktualisierung der <a href='../19.04.0'>KDE-Anwendungen 19.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

About twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello, among others.

Verbesserungen umfassen:

- Tagging files on the desktop no longer truncates the tag name
- A crash in KMail's text sharing plugin has been fixed
- Several regressions in the video editor Kdenlive have been corrected
