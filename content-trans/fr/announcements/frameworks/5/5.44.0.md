---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: Remove checkDb option (bug 380465)
- indexerconfig : description de certaines fonctions
- indexerconfig: Expose canBeSearched function (bug 388656)
- Surveillance de « balooctl » : attente de l'interface « D-Bus »
- fileindexerconfig: Introduce canBeSearched() (bug 388656)

### Icônes « Breeze »

- remove view-media-playlist from preferences icons
- Ajout d'une icône « 24px » pour « media-album-cover »
- Ajout de la prise en charge de « Babe QML » (22px)
- Mise à jour des icônes de poignées pour « kirigami »
- Ajout d'icônes « 64px » multimédia pour Elisa

### Modules additionnels « CMake »

- Définition de « **ANDROID_API** »
- Correction du nom de la commande « readelf » sur « x86 »
- Android toolchain: add ANDROID_COMPILER_PREFIX variable, fix include path for x86 targets, extend search path for NDK dependencies

### Outils KDE avec « DOxygen »

- Exit in error if the output directory is not empty (bug 390904)

### KConfig

- Save some memory allocations by using the right API
- Exportation de « kconf_update » pour les outils

### KConfigWidgets

- Improve KLanguageButton::insertLanguage when no name is passed
- Add icons for KStandardActions Deselect and Replace

### KCoreAddons

- Cleanup m_inotify_wd_to_entry before invalidating Entry pointers (bug 390214)
- kcoreaddons_add_plugin: remove effectless OBJECT_DEPENDS on json file
- Help automoc to find metadata JSON files referenced in the code
- kcoreaddons_desktop_to_json: note the generated file in the build log
- Basculement de « shared-mime-info » vers la version 1.3
- Introduction de « K_PLUGIN_CLASS_WITH_JSON »

### KDeclarative

- Correction de l'échec de compilation pour l'architecture « armhf/aarch64 »
- Arrêt de « QmlObjectIncubationController »
- disconnect render() on window change (bug 343576)

### KHolidays

- Allen Winter is now officially the maintainer of KHolidays

### KI18n

- API dox: add note about calling setApplicationDomain after QApp creation

### KIconThemes

- [KIconLoader] Take into account devicePixelRatio for overlays

### KIO

- Do not assume layout of msghdr and iovec structure (bug 391367)
- Correction de la sélection de protocole dans « KUrlNavigator »
- Remplacer « qSort » par « std::sort »
- [KUrlNavigatorPlacesSelector] Use KFilePlacesModel::convertedUrl
- [Drop Job] Create proper trash file on linking
- Fix unintentional breadcrumb menu item activation (bug 380287)
- [KFileWidget] Hide places frame and header
- [KUrlNavigatorPlacesSelector] Put categories into submenus (bug 389635)
- Favoriser l'utilisation de l'en-tête d'assistant standard de test « KIO »
- Add Ctrl+H to the list of shortcuts for "show/hide hidden files" (bug 390527)
- Add move semantics support to KIO::UDSEntry
- Fix "ambiguous shortcut" issue introduced with D10314
- Stuff the "Couldn't find executable" message box into a queued lambda (bug 385942)
- Improve usability of "Open With" dialog by adding option to filter the application tree
- [KNewFileMenu] KDirNotify::emitFilesAdded after storedPut (bug 388887)
- Fix assert when cancelling the rebuild-ksycoca dialog (bug 389595)
- Fix bug #382437 "Regression in kdialog causes wrong file extension" (bug 382437)
- Démarrage plus rapide de tâches simples
- Repair copying file to VFAT without warnings
- kio_file: skip error handling for initial perms during file copy
- Permettre de générer la sémantique de déplacement pour « KFileItem ». Le constructeur de copie, le destructeur et l'opérateur d'affectation de copie existants sont maintenant également générés par le compilateur.
- Don't stat(/etc/localtime) between read() and write() copying files (bug 384561)
- Distant : ne pas créer des entrées avec des noms vides
- Ajout d'une fonctionnalité « Thèmes pris en charge »
- Use F11 as the shortcut to toggle the aside preview
- [KFilePlacesModel] Group network shares under "Remote" category

### Kirigami

- Show tool button as checked while the menu is shown
- Indicateurs de défilement non interactifs pour plate-forme mobile
- Correction des sous-menus d'actions
- Rendre possible l'utilisation des actions « QQC2 »
- Make it possible to support exclusive action groups (bug 391144)
- Show the text by the page action tool buttons
- Rendre possible pour des actions d'afficher des sous-menus
- Don't have specific component position in its parent
- Don't trigger SwipeListItem's actions unless they are exposed
- Ajout d'une vérification « isNull() » avant de définir si « QIcon » est un masque
- Ajout de « FormLayout.qml » à « kirigami.qrc »
- Correction des couleurs de « swipelistitem »
- Meilleur comportement avec les en-têtes et les pieds de page
- Improve ToolBarApplicationHeader left padding and eliding behavior
- Make sure the navigation buttons don't go under the action
- support for header and footer properties in overlaysheet
- Eliminate unnecessary bottom padding on OverlaySheets (bug 390032)
- Améliorations de l'apparence de l'entête de l'application pour la barre d'outils
- Affiche un bouton « Fermer » sur le bureau (bogue 387815)
- Impossible de fermer la feuille avec la molette de la souris
- Only multiply the icon size if Qt isn't doing it already (bug 390076)
- Prise en compte du pied de page global pour la position de la poignée
- Évènements encadrant la création et la destruction de barre de défilement
- ScrollView: Make the scrollbar policy public and fix it

### KNewStuff

- Add vokoscreen to KMoreTools and add it to the "screenrecorder" grouping

### KNotification

- Utilisation de « QWidget » pour voir si la fenêtre est visible

### Environnement de développement « KPackage »

- Help automoc to find metadata JSON files referenced in the code

### KParts

- Nettoyage du code ancien et mort

### KRunner

- Mise à jour du modèle pour le module externe « krunner »

### KTextEditor

- Add icons for KTextEditor Document-Export, Bookmark-Remove and Formatting Text Upppercase, Lowercase and Capitalize

### KWayland

- Implement releasing of client-freed output
- [server] Properly handle the situation when the DataSource for a drag gets destroyed (bug 389221)
- [server] Don't crash when a subsurface gets committed whose parent surface got destroyed (bug 389231)

### KXMLGUI

- Reset QLocale internals when we have a custom app language
- Do not allow to configure separator actions via context menu
- Don't show context menu if right-clicking outside (bug 373653)
- Improve KSwitchLanguageDialogPrivate::fillApplicationLanguages

### Icônes « Oxygen »

- Ajout d'une icône pour Artikulate (bogue 317527)
- Ajout de l'icône pour le dossier des jeux (bogue 318993)
- fix incorrect 48px icon for calc.template (bug 299504)
- add media-playlist-repeat and shuffle icon (bug 339666)
- Oxygen: add tag icons like in breeze (bug 332210)
- link emblem-mount to media-mount (bug 373654)
- add network icons which are available in breeze-icons (bug 374673)
- sync oxygen with breeze-icons add icons for audio plasmoid
- Add edit-select-none to Oxygen for Krusader (bug 388691)
- Ajout d'une icône pour l'évaluation ou l'absence d'évaluation (bogue 339863)

### Environnement de développement de Plasma

- Utilisation de la nouvelle valeur pour les espacement larges dans Kirigami
- Reduce visibility of PC3 TextField placeholder text
- Don't make Titles 20% transparent either
- [PackageUrlInterceptor] Don't rewrite "inline"
- Don't make Headings 20% transparent, to match Kirigami
- don't put the fullrep in the popup if not collapsed
- Help automoc to find metadata JSON files referenced in the code
- [AppletQuickItem] Preload applet expander only if not already expanded
- Autres micro-optimisations pour le préchargement
- Définition du défaut pour « IconItem » à « smooth=true »
- Ajoute du préchargement de la boîte de dialogue pour l'extension
- [AppletQuickItem] Fix setting default preload policy if no environment variable is set
- fix RTL appearance for ComboBox (bug https://bugreports.qt.io/browse/QTBUG-66446)
- try to preload certain applets in a smart way
- [Icon Item] Set filtering on FadingNode texture
- Initialize m_actualGroup to NormalColorGroup
- Make sure the FrameSvg and Svg instances have the right devicePixelRatio

### Prison

- Update links to dependencies, and mark Android as officially supported
- Passage de la dépendance avec « DMTX » en optionnel
- Ajoute de la prise en charge de « QML » pour Prison
- Définition d'une taille minimale aussi pour les barres codes 1D

### Motif

- Correction d'un niveau pour accommodation à « Kio »

### QQC2StyleBridge

- Fix syntax error in previous commit, detected by launching ruqola
- Show a radiobutton when we are showing an exclusive control (bug 391144)
- Implémentation des éléments de la barre de menus
- Implémentation du bouton de retardement
- Nouveau composant : bouton rond
- Prise en charge de la position de la barre d'outils
- Prise en charge des couleur pour les icônes à l'intérieur des boutons
- Prise en charge de l'option « --reverse »
- Icônes totalement fonctionnelles dans le menu
- consistent shadows with the new breeze style
- Some QStyles seem to not return sensible pixelmetrics here
- Première prise en charge grossières des icônes
- Ne pas revenir au départ avec la molette de souris

### Opaque

- fix a leak and incorrect nullptr check in DADictionary
- [UDisks] Fix auto-mount regression (bug 389479)
- [UDisksDeviceBackend] Avoid multiple lookup
- Mac/IOKit backend: support for drives, discs and volumes

### Sonnet

- Utilisation de « Locale::name() » au lieu de « Locale::bcp47Name() »
- Recherche par « msvc » de la compilation de « libhunspell »

### Coloration syntaxique

- Basic support for PHP and Python fenced code blocks in Markdown
- Prise en charge de « WordDetect » non sensible à la casse
- Scheme highlighting: Remove hard-coded colors
- Ajoute de la coloration syntaxique pour les politiques « CIL » de « SELinux » et des contextes de fichiers
- Ajout de l'extension de fichier « ctp » pour la coloration syntaxique de « PHP ».
- Yacc/Bison: Fix the $ symbol and update syntax for Bison
- awk.xml: add gawk extension keywords (bug 389590)
- Ajout de « APKBUILD » à mettre en surbrillance comme fichier « bash »
- Revert "Add APKBUILD to be highlighted as a Bash file"
- Ajout de « APKBUILD » à mettre en surbrillance comme fichier « bash »

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
