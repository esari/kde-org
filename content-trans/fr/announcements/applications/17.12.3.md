---
aliases:
- ../announce-applications-17.12.3
changelog: true
date: 2018-03-08
description: KDE publie les applications de KDE en version 17.12.3
layout: application
title: KDE publie les applications de KDE en version 17.12.3
version: 17.12.3
---
08 Mars 2018. Aujourd'hui, KDE publie la troisième mise à jour de stabilisation pour les <a href='../17.12.0'>applications 17.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus de 25 corrections de bogues apportent des améliorations à Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Akregator ne supprime plus la liste des fils de discussions « feeds.opml » après une erreur.
- Le mode « Plein écran » de Gwenview fonctionne maintenant avec un nom correct de fichier après son renommage.
- Plusieurs plantages rares dans Okular ont été identifiés et corrigés
