---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE publie les applications de KDE en version 16.04.1
layout: application
title: KDE publie les applications de KDE en version 16.04.1
version: 16.04.1
---
10 mai 2016. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../16.04.0'>applications 16.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 25 corrections de bogues apportent des améliorations à kdepim, ark, kate, dolphin. kdenlive, lokalize, spectacle et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.20 de KDE.
