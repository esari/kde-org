---
aliases:
- ../../kde-frameworks-5.36.0
date: 2017-07-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
All frameworks: Option to build &amp; install QCH file with the public API dox

### Baloo

- FindInotify.cmake'i kasutamine otsustamaks, kas inotify on saadaval

### Breeze'i ikoonid

- Ei sõltuta tarbetult bash'ist ja vaikimisi ei valideerita ikoone

### CMake'i lisamoodulid

- FindQHelpGenerator: välditakse Qt4 versiooni valimist
- ECMAddQch: üllatuste vältimiseks jõuline nurjumine, kui vajalikud tööriistad puuduvad
- perl'i hülgamine for ecm_add_qch'i sõltuvusena, sest seda ei ole tarvis ega ksutata
- Terve paigalduskataloogi läbiuurimine qml-i sõltuvuste leidmiseks
- Uus: ECMAddQch qch &amp; doxygen'i sildifailide genereerimiseks
- KDEInstallDirsTest.relative_or_absolute_usr'i parandus Qt asukohtade kasutamise vältimiseks

### KAuth

- Tõrkeoleku kontroll pärast iga PolKitAuthority kasutust

### KBookmarks

- Tõrgete väljastamine, kui keditbookmarks puudub (veateade 303830)

### KConfig

- Parandus CMake 3.9 jaoks

### KCoreAddons

- FindInotify.cmake'i kasutamine otsustamaks, kas inotify on saadaval

### KDeclarative

- KKeySequenceItem: võimaldamine salvestada Ctrl+Num+1 kiirklahvina
- Lohistamise alustamine vajutamise ja hoidmise puutesündmusega (veateade 368698)
- Ei toetuta QQuickWindow'le QEvent::Ungrab'i edastamisel mouseUngrabEvent'ina (sest see ei tee seda enam Qt 5.8+ puhul) (veateade 380354)

### KDELibs 4 toetus

-  KEmoticons'i otsimine, , mis on CMake config.cmake.in'i järgi sõltuvus (veateade 381839)

### KFileMetaData

- Ekstraktori lisamine qtmultimedia't kasutades

### KI18n

- Tsgamine, et tsfiles'i sihtmärk genereeritakse

### KIconThemes

- Rohkem üksikasju ikooniteemade pakkumise kohta Mac'i ja MSWin'i peal
- Paneeli ikooni vaikimisi suuruseks sai 48

### KIO

- [KNewFileMenu] menüü "Link seadmele" peitmine, kui see jääb tühjaks (veateade 381479)
- KIO::rename kasutaminef KIO::moveAs'i asemel setData's (veateade 380898)
- Kukutamismenüü asukoha parandus Waylandis
- KUrlRequester: signaali NOTIFY saatmine textChanged()-le omaduse text korral
- [KOpenWithDialog] HTML-varjestusega failinimi
- KCoreDirLister::cachedItemForUrl: puhvrit ei looda, kui seda eelnevalt ei ole
- "data" kasutamine failinimena andme-url-i kopeerimisel (veateade 379093)

### KNewStuff

- Vigase veatuvastuse parandus puuduvate knsrc-failide korral
- Mootori lehekülje suuruse muutuja avalikustamine ja kasutamine
- Võimaldamine kasutada QXmlStreamReader'it KNS'i registrifaili lugemiseks

### KPackage raamistik

- kpackage-genericqml.desktop'i lisamine

### KTextEditor

- protsessorikasutuse tippu hüppamise parandus pärast vi käsuriba näitamist (veateade 376504)
- Kerimisriba lohistamisel hüplemise parandus, kui minikaart on lubatud
- Hüppamine kerimisribal klõpsatud asukohta, kui minikaart on lubatud (veateade 368589)

### KWidgetsAddons

- kcharselect'i andmete uuendamine Unicode 10.0 peale

### KXMLGUI

- KKeySequenceWidget: võimaldab salvestada Ctrl+Num+1 kiirklahvina (veateade 183458)
- "Menüühierarhia ehitamisel kuulutatakse menüüde eellaseks nende konteinerid" tagasivõtmine
-  "transientparent'i otsene kasutamine" tagasivõtmine

### NetworkManagerQt

- WiredSetting: wake on lan omadused porditi tagasi NM 1.0.6 peale
- WiredSetting: omadus metered porditi tagasi NM 1.0.6 peale
- Uute omaduste lisamine paljudes seadistusklassides
- Seade: seadme statistika lisamine
- ipTunnel'i seadme lisamine
- Juhtmega seade: NM nõutava versiooni teabe lisamine  omadusele s390SubChannels
- TeamDevice: uue omaduse config lisamine (alates NM 1.4.0)
- Juhtmega seade: omaduse s390SubChannels lisamine
- Introspektsioonide uuendamine (NM 1.8.0)

### Plasma raamistik

- Tagamine, et suurus oleks pärast showEvent'i lõplik
- vlc süsteemisalve ikooni veeriste ja värviskeemi parandus
- Konteineritele vaates fookuse määramine (veateade 381124)
- vana võtme genereerimine enne enabledborders'i uuendamist (veateade 378508)
- nupu Näita parooli näitamine ka siis, kui tekst puudub (veateade 378277)
- usedPrefixChanged'i väljastamine, kui prefiks on tühi

### Solid

- cmake: udisks2 taustaprogrammi ehitamine FreeBSD peal ainult siis, kui on lubatud

### Süntaksi esiletõstmine

- Juliuse failide esiletõstmine JavaScripti alusel
- Haskell: kõigi keele pragmade lisamine võtmesõnadena
- CMake: OR/AND jäid esile tõstmata pärast expr'i ()-s (veateade 360656)
- Makefile: vigaste võtmesõnakirjete eemaldamine makefile.xml's
- indekseerija; tõrgetest teatamise täiustus
- HTML-i süntaksifaili versiooni uuendamine
- Nurksulgmodifikaatorite lisamine HTML-i atribuutidesse
- Testi viiteandmete uuendamine pärast eelmise sissekande muutusi
- Veateade 376979 - nurksulud doxygen'i kommentaarides rikuvad süntaksi esiletõstmise

### ThreadWeaver

- MSVC2017 kompileerimistõrke hädalahendus

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
