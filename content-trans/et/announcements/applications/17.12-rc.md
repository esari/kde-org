---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE Ships Applications 17.12 Release Candidate.
layout: application
release: applications-17.11.90
title: KDE toob välja rakenduste 17.12 väljalaskekandidaadi
---
1. detsember 2017. KDE laskis täna välja rakenduste uute versioonide väljalaskekandidaadi. Kuna sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Check the <a href='https://community.kde.org/Applications/17.12_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

KDE rakendused 17.12 vajavad põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga väljalaskekandidaati paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.

#### KDE rakenduste 17.12 väljalaskekandidaadi binaarpakettide paigaldamine

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 Release Candidate (internally 17.11.90) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.

#### KDE rakenduste 17.12 väljalaskekandidaadi kompileerimine

The complete source code for KDE Applications 17.12 Release Candidate may be <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-17.11.90'>KDE Applications 17.12 Release Candidate Info Page</a>.
