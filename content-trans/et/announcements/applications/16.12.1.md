---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE toob välja KDE rakendused 16.12.1
layout: application
title: KDE toob välja KDE rakendused 16.12.1
version: 16.12.1
---
January 12, 2017. Today KDE released the first stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Väljalaskes on ära parandatud ANDMEKADU kaasa toonud viga iCali ressursis, mis ei suutnud luua faili std.ics, kui seda eelnevalt juba olemas ei olnud.

Rohkem kui 40 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Arki, Gwenview, Kajonggi, Okulari, Kate, Kdenlive'i ja teiste rakenduste täiustused,

This release also includes Long Term Support version of KDE Development Platform 4.14.28.
