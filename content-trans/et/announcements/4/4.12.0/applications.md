---
date: '2013-12-18'
hidden: true
title: KDE rakendused 4.12 astuvad suure sammu edasi isikliku teabe haldamisel ja
  pakuvad rohkelt kõikvõimalikke parandusi ja täiustusi
---
KDE kogukond annab uhkusega teada KDE rakenduste uusimast väljalaskest, mis pakub uusi omadusi ja parandusi. Selles väljalaskes on massiliselt täiustusi saanud KDE PIM-i komplekt, mis tagab uusi võimalusi ja tunduvalt parema jõudluse. Kate on viimistlenud lõimimist Pythoni pluginatega ja lisanud esialgse Vim-i makrode toetuse. Mängud ja õpirakendused pakuvad rohkelt uusi võimalusi.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/kate.png" width="600px">}}

Linuxi kõige võimsama graafilise tekstiredaktori Kate koodilõpetus sai taas täiustusi, seekord <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>täiustatud vastete leidmise süsteemi, lühendite käitlemise ja osaliste vastete leidmise klassides</a>. Nii näiteks leiab otsing "Qualident" vastena ka "Qualifiedidentifier". Samuti sai Kate <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>esialgse VIM-i makrode toetuse</a>. Ja mis kõige toredam: need parandused on tulnud kasutusele ka KDevelopis ja teistes rakendustes, mis pruugivad Kate tehnoloogiat.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/okular.png" width="600px">}}

Dokumendinäitaja Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>võtab nüüd arvesse printerite riistvaraliselt määratud veeriseid</a>, pakub EPubi vormingule audio- ja videotoetust, on saanud täiustusi otsingul ja suudab käidelda senisest rohkem teisendusi, kaasa arvatud Exifi piltide metaandmete puhul. UML-skeemide tööriistas Umbrello võivad seosed olla nüüd <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>esitatud erineva paigutusega</a> ning <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>juhul, kui vidin on dokumenteeritud, näitab Umbrello visuaalset tagasisidet</a>.

Privaatsuse kaitsja KGpg näitab nüüd kasutajale rohkem teavet ning paroolide salvestamise tööriist KWalletManager (KDE turvalaegas) suudab neid <a href='http://www.rusu.info/wp/?p=248'>salvestada GPG vormis</a>. Konsool pakub uut võimalust: Ctrl+klõps avab URL-i otse Konsooli väljundist. Samuti näitab Konsool nüüd <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>ühes väljumishoiatusega protsesside loetelu.</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.12.0/screenshots/dolphin.png" width="600px">}}

KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.

### Mängud ja õpitarkvara

KDE mängudes on mitmel pool vaeva nähtud. KReversi aluseks on nüüd <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>QML ja Qt Quick</a>, mis muudab mängu kenamaks ja sujuvamaks. KNetWalk on samuti <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>Qt Quicki peale porditud</a>, mis on taganud samad tulemused, ning lisaks saanud võimaluse määrata alusvõrgule meelepärane laius ja kõrgus. Konquestil on aga välja pakkuda uus võimekas tehisaju Becai.

In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.

### Kirjad, kalender ja isiklik teave

Palju vaeva on nähtud KDE PIM-i ehk KDE kirjade, kalendri ja muu isikliku teabe käitlemise rakenduste kogumi kallal.

Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.

In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.

Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!

#### KDE rakenduste paigaldamine

KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href='http://windows.kde.org'>KDE software on Windows</a> site and Apple Mac OS X versions on the <a href='http://mac.kde.org/'>KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href='http://plasma-active.org'>Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.12.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Tarkvarapaketid

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.12.0 for some versions of their distribution, and in other cases community volunteers have done so.

##### Pakettide asukohad

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.12.0'>Community Wiki</a>.

The complete source code for 4.12.0 may be <a href='/info/4/4.12.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.12.0 are available from the <a href='/info/4/4.12.0#binary'>4.12.0 Info Page</a>.

#### Nõuded süsteemile

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.

In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
