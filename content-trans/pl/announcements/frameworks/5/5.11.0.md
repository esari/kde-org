---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Dodatkowe moduły CMake

- Nowe argumenty dla ecm_add_tests(). (błąd 345797)

### Integracja Szkieletów

- Użyj poprawnego initialDirectory dla KDirSelectDialog
- Upewnij się, że podano schemat przy próbie zastąpienia początkowej wartości adresu url
- Przyjmuj tylko istniejące katalogi w trybie FileMode::Directory

### KAktywności

(brak dziennika zmian)

### KAuth

- Uczyń KAUTH_HELPER_INSTALL_ABSOLUTE_DIR dostępnym dla wszystkich użytkowników KAuth

### KKodeki

- KEmailAddress: Dodano przeładowanie do extractEmailAddress oraz firstEmailAddress , które zwracają błąd.

### KCompletion

- Naprawiono niechciane zaznaczenie przy edytowaniu nazwy pliku w oknie dialogowym pliku (błąd 344525)

### KConfig

- Usunięto usterkę, gdy QWindow::screen() jest null
- Dodano KConfigGui::setSessionConfig() (błąd 346768)

### KCoreAddons

- Nowe wygodne API KPluginLoader::findPluginById()

### KDeclarative

- tworzenie wsparcia ConfigModule z KPluginMetdata
- naprawiono zdarzenia pressAndhold

### Obsługa KDELibs 4

- Użyto QTemporaryFile zamiast kodowania na twardo pliku tymczasowego.

### KDocTools

- Uaktualnienie tłumaczeń
- Uaktualniono dostosowywanie/ru
- Naprawiono obiekty ze złymi odsyłaczami

### KEmotikony

- Wystrój jest przechowywany we wtyczce integracji

### KGlobalAccel

- [biblioteki uruchomieniowe] Przeniesiono kod platformy do wtyczek

### KIconThemes

- Zoptymalizowano KIconEngine::availableSizes()

### KIO

- Nie próbuj uzupełniać użytkowników i sprawdzać ich gdy przedrostek nie jest pusty. (błąd 346920)
- Użyto KPluginLoader::factory() przy wczytywaniu KIO::DndPopupMenuPlugin
- Naprawiono martwą blokadę przy używaniu pośredników sieciowych (błąd 346214)
- Naprawiono KIO::suggestName tak aby zachowywało rozszerzenia plików
- Uruchom kbuildsycoca4 przy uaktualnianiu sycoca5.
- KFileWidget: Nie przyjmuj plików w trybie tylko-katalogi
- KIO::AccessManager: Umożliwiono traktowanie sekwencyjnego QIODevice asynchronicznie

### KNewStuff

- Dodano nową metodę fillMenuFromGroupingNames
- KMoreTools: dodano wiele nowych grup
- KMoreToolsMenuFactory: obsługa dla "git-clients-and-actions"
- createMenuFromGroupingNames: uczyniono parametr url opcjonalnym

### KNotification

- Usunięto usterkę w NotifyByExecute przy braku ustawionego elementu interfejsu (błąd 348510)
- Poprawiono obsługę zamykanych powiadomień (błąd 342752)
- Zastąpiono QDesktopWidget przez QScreen
- KNotification upewnia się, że może być używane w wątku bez graficznego interfejsu użytkownika

### Pakiety Szkieletów

- Chroń dostępu do struktury qpointer (błąd 347231)

### KLudzie

- Użyto QTemporaryFile zamiast zakodowanego na twardo /tmp.

### KPty

- Używaj tcgetattr &amp; tcsetattr, jeśli dostępny

### Kross

- Naprawiono wczytywanie modułów Kross "forms" oraz "kdetranslation"

### KService

- Przy uruchomieniu jako administrator zachowaj właściciela pliku dla istniejących plików pamięci podręcznej (błąd 342438)
- Chroń przed odmową otwarcia strumienia (błąd 342438)
- Naprawiono sprawdzanie nieprawidłowych uprawnień przy zapisywaniu pliku (błąd 342438)
- Naprawiono odpytywanie ksycoca o x-scheme-handler/* pseudo-mimetypes. (błąd 347353)

### KTextEditor

- Zezwól aplikacjom/wtyczkom trzecim na wgrywanie własnych plików XML do podświetleń w katepart5/składni tak jak to było za czasów KDE 4.x
- Dodano KTextEditor::Document::searchText()
- Przywrócono użycie KEncodingFileDialog (błąd 343255)

### KTextWidgets

- Dodano metodę do czyszczenia dekoratora
- Zezwolono na używanie własnego dekoratora sonnet
- Zaimplementowano "znajdź poprzedni" w KTextEdit.
- Ponownie dodano obsługę tekstu-na-mowę

### KWidgetsAddons

- KAssistantDialog: Ponownie dodano przycisk pomocy, który był obecny w wersji KDELibs4

### KXMLGUI

- Dodano zarządzanie sesją dla KMainWindow (błąd 346768)

### ZarządzanieSieciąQt

- Wyrzucono wsparcie WiMAX dla NM 1.2.0+

### Szkielety Plazmy

- Składnik kalendarza może teraz wyświetlać numery tygodni (błąd 338195)
- Użyto QtRendering do czcionek w polach haseł
- Naprawiono tablicę wyszukiwania AssociatedApplicationManager gdy ma ona typ mime (błąd 340326)
- Naprawiono kolorowanie tła panelu (błąd 347143)
- Pozbyto się wiadomości "Nie można wczytać apletu"
- Możliwość wczytywania QML kcms w oknach ustawień plazmoidu
- Nie używaj DataEngineStructure dla apletów
- Przenieś libplasma tak daleko od sycoca jak to tylko możliwe
- [plasmacomponents] Spraw, aby SectionScroller podążał za ListView.section.criteria
- Paski przewijania nie chowają się same przy obecnym ekranie dotykowym (błąd 347254)

### Sonnet

- Użyto jednej centralnej pamięci podręcznej dla SpellerPlugins.
- Zmniejszono tymczasowe przydziały.
- Optymalizacja: nie wymazuj pamięci podręcznej słownika przy kopiowaniu obiektów sprawdzania pisowni.
- Zoptymalizowano wywołania save() poprzez wywoływanie ich jednokrotnie na końcu, gdy jest to potrzebne.

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
