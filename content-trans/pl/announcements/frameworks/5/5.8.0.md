---
aliases:
- ../../kde-frameworks-5.8.0
date: '2015-03-13'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nowe szkielety:

- KPeople, zapewnia dostęp do wszystkich kontaktów i osób
- KXmlRpcClient, współdziałanie z usługami XMLRPC

### Ogólne

- Kilka poprawek do kompilowania z nadchodzącym Qt 5.5

### KAktywności

- Ukończono prace nad usługą punktowania zasobów

### KArchiwum

- Pliki ZIP z nadmiarowym opisem danych od teraz są obsługiwane poprawnie

### KCMUtils

- Przywrócono KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: dodano obsługę dla ukrytego klucza

### KDeclarative

- Listy QML są wystawiane raczej przy użyciu QJsonArray
- Obsługa niedomyślnych devicePixelRatios w obrazach
- Wystawianie hasUrls w DeclarativeMimeData
- Umożliwiono użytkownikowi ustawienie liczby rysowanych poziomych linii

### KDocTools

- Naprawiono budowanie na MacOSX przy używaniu Homebrew
- Lepsze stylizowanie obiektów multimedialnych (obrazów, ...) w dokumentacji
- Kodowanie nieprawidłowych znaków w ścieżkach używanych w XML DTDs, pominięto błędy

### KGlobalAccel

- Aktywowanie zestawu znaczników czasowych jako dynamiczna własność na wyzwalanych QAction.

### KIconThemes

- Naprawiono QIcon::fromTheme(xxx, someFallback), które nie wracało do zestawu awaryjnego

### KImageFormats

- Czytnik obrazów PSD od teraz używa endian.

### KIO

- Przedawniono metodę UDSEntry::listFields oraz dodano UDSEntry::fields, która zwraca QVector bez czasochłonnych przekształceń.
- Synchronizacja zakładek tylko gdy zmiana została wprowadzona przez proces bookmarkmanager (błąd 343735)
- Naprawiono uruchamianie usługi kssld5 dbus
- Zaimplementowanie quota-used-bytes oraz quota-available-bytes z RFC 4331, aby przekazywać informacje o wolnej przestrzeni do http ioslave.

### KNotifications

- Opóźniono inicjację dźwięku do chwili kiedy rzeczywiście jest potrzebny
- Naprawiono ustawienia powiadomień, które nie były stosowane natychmiast
- Naprawiono powiadomienia dźwiękowe, które były zatrzymywane po odtworzeniu pierwszego pliku 

### KNotifyConfig

- Dodano opcjonalną zależność na QtSpeech, aby ponownie włączyć mówione powiadomienia.

### KService

- KPluginInfo: obsługa stringlists jako właściwości

### KTextEditor

- Dodano licznik słów na pasku stanu
- vimode: naprawiono awarię przy usuwaniu ostatniego wiersza w trybie Visual Line

### KWidgetsAddons

- KRatingWidget od teraz współgra z devicePixelRatio

### KWindowSystem

- KSelectionWatcher oraz KSelectionOwner można używać niezależnie od QX11Info.
- KXMessages można używać niezależnie od QX11Info

### ZarządzanieSieciąQt

- Dodano obsługę dla ZarządzaniaSiecią 1.0.0

#### Szkielety Plazmy

- Naprawiono plasmapkg2 dla przetłumaczonych systemów
- Usprawniono układ podpowiedzi
- Make it possible to let plasmoids to load scripts outside the plasma package ...

### Zmiany w systemie budowania (extra-cmake-modules)

- Rozszerz makro ecm_generate_headers tak, aby obsługiwało także nagłówki CamelCase.h

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
