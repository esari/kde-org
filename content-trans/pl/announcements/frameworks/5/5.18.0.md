---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Naprawiono kilka błędów związanych z mtime
- PostingDB Iter: Nie zakładaj warunku MDB_NOTFOUND
- Stan Balooctl: Unikaj wyświetlania 'Indeksowanie treści' dotyczącego katalogów
- Polecenie stanu: Wyświetlaj właściwy stan dla katalogów
- Magazyn wyszukiwania: Poprawnie obsługuj puste wartości wyrażeń (błąd 356176)

### Ikony Bryzy

- dodano ikony i inne dodatki
- Ikony stanu o rozmiarze 32 pikseli zostały zastąpione tymi o rozmiarze 22 piksele jako iż potrzebujesz ich na tacce systemowej
- Zmieniono wartość stałe na skalowalne dla katalogów 32 piksele w Ciemnej Bryzie

### Dodatkowe moduły CMake

- Uczyniono moduł CMake KAppTemplate globalnym
- Wyciszono ostrzeżenia CMP0063 w KDECompilerSettings
- ECMQtDeclareLoggingCategory: Dołączaj &lt;QDebug&gt; z wygenerowanym plikiem
- Usunięto ostrzeżenia CMP0054

### KAktywności

- Usprawniono wczytywanie QML dla KCM (błąd 356832)
- Work-around for the Qt SQL bug that does not clean up connections properly (bug 348194)
- Merged a plugin that executes applications on activity state change
- Port from KService to KPluginLoader
- Przestawiono wtyczki na używanie kcoreaddons_desktop_to_json()

### KBookmarks

- Fully initialize DynMenuInfo in return value

### KCMUtils

- KPluginSelector::addPlugins: naprawiono warunek konieczny taki, że parametr 'config' jest domyślnym (błąd 352471)

### KKodeki

- Avoid deliberately overflowing a full buffer

### KConfig

- Ensure group is unescaped properly in kconf_update

### KCoreAddons

- Dodano KAboutData::fromPluginMetaData(wtyczka const KPluginMetaData)
- Dodano KPluginMetaData::copyrightText(), extraInformation() oraz otherContributors()
- Dodano KPluginMetaData::translators() oraz KAboutPerson::fromJson()
- Fix use-after-free in desktop file parser
- Make KPluginMetaData constructible from a json path
- desktoptojson: make missing service type file an error for the binary
- uczyniono wywoływanie kcoreaddons_add_plugin bez SOURCES błędem

### KDBusAddons

- Adapt to Qt 5.6's dbus-in-secondary-thread

### KDeclarative

- [DragArea] Dodano właściwość dragActive
- [KQuickControlsAddons MimeDatabase] Wystawiono komentarz QMimeType

### KDED

- kded: adapt to Qt 5.6's threaded dbus: messageFilter must trigger module loading in the main thread

### Obsługa KDELibs 4

- kdelibs4support requires kded (for kdedmodule.desktop)
- Fix CMP0064 warning by setting policy CMP0054 to NEW
- Nie eksportuj symboli, które istnieją także w KWidgetsAddons

### KDESU

- Don't leak fd when creating socket

### KHTML

- Windows: remove kdewin dependency

### KI18n

- Document the first argument rule for plurals in QML
- Zmniejszono niechciane zmiany rodzaju
- Uczyniono możliwym wykorzystanie l. podwójnej precyzji jako indeksy dla wywołań i18np*() w QML

### KIO

- Naprawiono kiod dla szyny dbus Qt 5.6 z wątkami: messageFilter musi czekać na wczytanie modułu zanim coś zwróci
- Zmiana kodu błędu przy wklejaniu/przenoszeniu do podkatalogu
- Naprawiono zablokowane opróżnianie kosza
- Naprawiono zły przycisk w KUrlNavigator dla zdalnych adresów URL
- KUrlComboBox: naprawiono zwracanie bezwzględnej ścieżki z urls()
- kiod: wyłączono zarządzanie sesją
- Dodano samoczynne uzupełnianie przy wpisaniu '.' co będzie przedstawiać wszystkie ukryte pliki/katalogi* (błąd 354981)
- ktelnetservice: naprawiono wyłączanie poprzez jeden w sprawdzaniu argumentów argc, łatka Stevena Bromleya

### KNotification

- [Powiadomienie oknem wysuwnym] Wysyłaj także ID zdarzenia
- Domyślnie ustawiono niepusty powód powstrzymujący wygaszacz ekranu; (błąd 334525)
- Dodano wskazówkę umożliwiającą pominięcie grupowania powiadomień (błąd 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Umożliwiono wybranie danego zdarzenia

### Pakiety Szkieletów

- Umożliwiono podanie metadanych w json

### KLudzie

- Naprawiono możliwe podwójne usunięcie w DeclarativePersonData

### KTextEditor

- Składnia h/l dla pli: dodano wbudowane funkcje, dodano rozwijane obszary

### Szkielet Portfela

- kwalletd: Naprawiono wyciek FILE*

### KWindowSystem

- Dodano wariant xcb dla statycznych metod KStartupInfo::sendFoo

### ZarządzanieSieciąQt

- dostosowano do pracy ze starszymi wersjami NM

### Szkielety Plazmy

- [ToolButtonStyle] Zawsze wskazuj activeFocus
- Używaj flagi SkipGrouping dla powiadomienia "usunięto element interfejsu" (błąd 356653)
- Poprawnie obsługuj dowiązania symboliczne w ścieżkach do pakietów
- Dodano ukryty stan dla samoukrywających się plazmoidów
- Przestań przekierowywać okna, gdy element jest wyłączony lub ukryty. (błąd 356938)
- Nie emituj statusChanged jeśli stan nie został zmieniony
- Naprawiono identyfikatory elementu dla kierunku wschodniego
- Pojemniki: Nie emituj appletCreated przy zerowym aplecie (błąd 356428)
- [Interfejs pojemników] Zmniejszono zbyt dużą precyzję przy przewijaniu
- Odczytuj własność KPluginMetada o nazwie X-Plasma-ComponentTypes jako listę ciągów znaków
- [Miniatury okien] Nie ulegaj usterce, gdy kompozytor jest wyłączony
- Pozwól pojemnikom zastępować CompactApplet.qml

Możesz przedyskutować i podzielić się pomysłami dotyczącymi tego wydania w dziale komentarzy <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artykułu dot</a>.
