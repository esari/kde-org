---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE levererar KDE-program 16.08.2
layout: application
title: KDE levererar KDE-program 16.08.2
version: 16.08.2
---
13:e oktober, 2016. Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../16.08.0'>KDE-program 16.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 30 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, dolphin, kgpg, kolourpaint och okular.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.25.
