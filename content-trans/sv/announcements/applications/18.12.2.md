---
aliases:
- ../announce-applications-18.12.2
changelog: true
date: 2019-02-07
description: KDE levererar Program 18.12.2.
layout: application
major_version: '18.12'
release: applications-18.12.2
title: KDE levererar KDE-program 18.12.2
version: 18.12.2
---
{{% i18n_date %}}

Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../18.12.0'>KDE-program 18.12</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Fler än ett dussin registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Konsole, Lokalize och Umbrello.

Förbättringar omfattar:

- Ark tar inte längre bort filer som sparas inne i den inbäddade visningen</li>
- Adressboken kommer nu ihåg födelsedagar när kontakter sammanfogas</li>
- Flera saknade uppdateringar av diagramvisning rättades i Umbrello</li>
