---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE levererar KDE-program 16.08.0
layout: application
title: KDE levererar KDE-program 16.08.0
version: 16.08.0
---
18:e augusti, 2016. KDE introducerar idag KDE Program 16.08 med en imponerande samling uppgraderingar när det gäller bättre åtkomst, tillägg av mycket användbara funktioner, och att bli av med några småproblem, som för KDE Program ett steg närmare att erbjuda dig den perfekta lösningen för ditt system.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> och KDiskFree har nu konverterats till KDE Ramverk 5, och vi ser fram emot dina kommentarer och din insikt i de nyaste funktionerna som introduceras i den här utgåvan.

I den fortsatta arbetet att dela Kontact-svitens bibliotek för att göra dem enklare att använda för tredje part, har det komprimerade arkivet kdepimlibs delats upp i akonadi-contacts, akonadi-mime och akonadi-notes.

Följande paket har avvecklats: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu och mplayerthumbs. Det hjälper oss att fokusera på den återstående koden.

### Hålla Kontact

<a href='https://userbase.kde.org/Kontact'>Kontact-sviten</a> har fått den vanliga omgången uppstädningar, felrättningar och optimeringar i den här utgåvan. Märkbart är användning av QtWebEngine i diverse komponenter som tillåter att en modernare HTML-återgivning används. Vi har också förbättrat stöd för VCard4, samt lagt till nya insticksprogram som kan varna om vissa villkor uppfylls när e-post skickas, t.ex. verifiera att du tillåter att e-post skickas för en given identitet, eller kontrollera om e-post skickas som vanlig text, etc.

### Ny version av Marble

<a href='https://marble.kde.org/'>Marble</a> 2.0 ingår i KDE Program 16.08 och omfattar mer än 450 kodändringar, som omfattar förbättringar av navigering och återgivning samt en experimentell vektoråtergivning av data från OpenStreetMap.

### Mer arkivering

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> kan nu extrahera AppImage- och .xar-filer, samt testa korrekthet för zip-, 7z- och rar-arkiv. Det kan också lägg till eller redigera kommentarer i rar-arkiv.

### Terminalförbättringar

I <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> har förbättringar gjorts rörande återgivningsalternativ för teckensnitt och handikappstöd.

### Med mera

<a href='https://kate-editor.org'>Kate</a> har fått flyttbara flikar. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Mer information...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>Kgeografi</a> har lagt till provins- och regionkartor för Burkina Faso.

### Offensiv skadedjursbekämpning

Mer än 120 fel har rättats i program, inklusive Kontact-sviten, Ark, Cantor, Dolphin, KCalc, Kdenlive med flera.

### Fullständig ändringslogg
