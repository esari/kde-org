---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
libCount: 70
---
### Breeze-ikoner

+ Centrera 16 bildpunkters help-about och help-whatsthis
+ Lägg till ikon för kirigami-gallery
+ Lägg till ikon för Kontrast
+ Lägg till applications/pkcs12 Mime-typikon
+ Gör så att breeze-dark ljudvolymikoner matchar breeze
+ Gör mikrofonstilen mer konsekvent med andra ljudikoner och bildpunktsjusterad
+ Använd 35 % ogenomskinlighet för borttonade vågformer i audio-volume ikoner och gör tystade vågformer borttonade
+ Gör så att audio-off och audio-volume-muted är likadana
+ Lägg till ikonen snap-angle
+ Rätta att 22 bildpunkters application-x-ms-shortcut länkar till en 16 bildpunkters ikon
+ Rätta inkonsekvens i Mime-typ ikoner mellan ljusa och mörka versioner
+ Rätta osynliga skillnader mellan ljus och mörk i vscode- och wayland-ikoner
+ Lägg till ikonen document-replace (som för åtgärden Overwrite)
+ Lägg till mall för att skapa Python-skript som redigerar SVG:er
+ Lägg till SMART statusikon (fel 423997)
+ Lägg till ikonerna "task-recurring" och "appointment-recurring" (fel 397996)

### Extra CMake-moduler

+ Lägg till ecm_generate_dbus_service_file
+ Lägg till ecm_install_configured_file
+ Exportera Wayland_DATADIR

### KActivitiesStats

+ Ignorera BoostConfig.cmake om närvarande (fel 424799)

### KCMUtils

+ Stöd färgläggningsindikering för QWidget- och QtQuick-baserade moduler
+ Lägg till metod för att rensa insticksväljare (fel 382136)

### KConfig

+ Uppdatera sGlobalFileName när TestMode i QStandardPath ändras
+ Dokumentation av programmeringsgränssnitt: ange explicit förväntad kodning för KConfig nyckel och gruppnamn

### KConfigWidgets

+ KCModule: Indikera när en inställning har ändrats från förvalt värde
+ Vi återställning till förvalda systemvärden använd inte stilens standardpalett

### KCoreAddons

+ Introducera KRandom::shuffle(container)

### KDeclarative

+ SettingStateBinding : exponera om förvald färgläggning är aktiverad
+ Säkerställ att KF5CoreAddons installeras innan KF5Declarative används
+ Lägg till KF5::CoreAddons i öppet gränssnitt för KF5::QuickAddons
+ Introducera SettingState* element för att göra det enklare att skriva inställningsmoduler
+ stöd underrättelser om inställning i configpropertymap

### KDE GUI Addons

+ Flytta KCursorSaver från libkdepim, förbättrad

### KImageFormats

+ Anpassa licens till LGPL-2.0-or-later

### KIO

+ KFilterCombo: application/octet-stream är också hasAllFilesFilter
+ Introducera OpenOrExecuteFileInterface för att kunna öppna körbara filer
+ RenameDialog: Visa om filer är identiska (fel 412662)
+ [rename dialog] Konvertera överskrivningsknappen till KStandardGuiItem::Overwrite (fel 424414)
+ Visa upp till tre filobjektåtgärder på plats, inte bara en (fel 424281)
+ KFileFilterCombo: Visa filändelser om det finns upprepad Mime-kommentar
+ [KUrlCompletion] Lägg inte till / sist i kompletterade kataloger
+ [Egenskaper] Lägg till SHA512-algoritm i komponenten för checksummering (fel 423739)
+ [WebDav] Rätta kopior som inkluderar överskrivning för webdav-slaven (fel 422238)

### Kirigami

+ Stöd synlighet av åtgärder i GlobalDrawer
+ Introducera komponenten FlexColumn
+ Layoutoptimeringar för mobil
+ Ett lager måste också täcka flikraden
+ PageRouter: använd faktiskt förinlästa sidor vid sändning
+ Förbättra dokumentation för (Abstract)ApplicationItem
+ Rätta segmenteringsfel för rivning av PageRouter
+ Gör ScrollablePage rullningsbar med tangentbordet
+ Förbättra åtkomstmöjligheter för Kirigami inmatningsfält
+ Rätta statiskt bygge
+ PageRouter: Lägg till programmeringsgränssnitt av bekvämlighetsskäl för annars manuella uppgifter
+ Introducera programmeringsgränssnitt för PageRouter livscykel
+ Tvinga en låg z-ordning för reservmetoden med programvara i ShadowedRectangle

### KItemModels

+ KSelectionProxyModel: tillåt att modellen används för innehåll med ny stil
+ KRearrangeColumnsProxyModel: rätta hasChildren() när inga kolumner ännu är inställda

### KNewStuff

+ [QtQuick dialogruta] använd uppdateringsikon som finns (fel 424892)
+ [QtQuick GHNS dialog] Förbättra kombinationsrutans beteckningar
+ Visa inte väljaren downloaditem när det bara finns ett nerladdningsbart objekt
+ Fixa om layouten lite grand, för ett mer balanserat utseende
+ Rätta layout för statuskortet EntryDetails
+ Länka bara till kärna, inte snabbinsticksprogrammet (eftersom det inte fungerar)
+ Viss uppsnyggning av välkomstskärmen (och använd inte qml-dialogrutor)
+ Använd dialogrutan när en knsrc-fil skickas in, inte annars
+ Ta bort knappen från huvudfönstret
+ Lägg till en dialogruta (för att direkt visa dialogrutan när en fil skickas in)
+ Lägg till den nya bitarna i huvudprogrammet
+ Lägg till en enkel (åtminstone för närvarande) modell för att visa knsrc-filer
+ Flytta qml-huvudfilen till en qrc
+ Omvandla testverktyget för KNewStuff dialogruta till ett riktigt verktyg
+ Tillåt borttagning av uppdateringsbara poster (fel 260836)
+ Fokusera inte automatiskt på första elementet (fel 417843)
+ Rätta visning av knappar för detaljinformation och avinstallation i rutvisningsläge (fel 418034)
+ Rätta att knappar flyttas när söktexten infogas (fel 406993)
+ Rätta saknad parameter för översatt sträng
+ Rätta detaljerad kombinationsruta för installation i QWidgets dialogruta (fel 369561)
+ Lägg till verktygstips för olika visningslägen i QML-dialogruta
+ Ställ in post till oinstallerad om installation misslyckas (fel 422864)

### KQuickCharts

+ Ta också hänsyn till modellegenskaper när ModelHistorySource används

### Kör program

+ Implementera KConfig övervakning för aktiverade insticksprogram och inställningsmoduler för körprogram (fel 421426)
+ Ta inte bort virtuell metod från bygge (fel 423003)
+ Avråd från AbstractRunner::dataEngine(...)
+ Rätta inaktiverade körprogram och körinställningar för Plasmoid
+ Fördröj att skicka ut metadata konverteringsvarningar till KF 5.75

### KService

+ Lägg till överlagring för att starta terminal med ENV-variabler (fel 409107)

### KTextEditor

+ Lägg till ikoner för alla knappar i meddelandet om ändrad fil (fel 423061)
+ Använd kanoniska docs.kde.org webbadresser

### Ramverket KWallet

+ använd bättre namn och följ HIG
+ Markera programmeringsgränssnittet som avrått från även i D-Bus gränssnittsbeskrivning
+ Lägg till kopia av org.kde.KWallet.xml utan programmeringsgränssnitt som avråds från

### Kwayland

+ plasma-window-management: Anpassa till ändringar i protokoll
+ PlasmaWindowManagement: anta protokolländringar

### KWidgetsAddons

+ KMultiTabBar: måla flikikoner som aktiva när musen hålls över dem
+ Rätta KMultiTabBar så att ikonen målas nedskiftad/kontrollerad av stildesign
+ Använd ny överskrivningsikon för Skriv över grafiskt användargränssnittsobjekt (fel 406563)

### KXMLGUI

+ Gör KXmlGuiVersionHandler::findVersionNumber öppen i KXMLGUIClient

### NetworkManagerQt

+ Ta bort loggning av hemligheter

### Plasma ramverk

+ Ställ in cpp ägarskap för enheternas instans
+ Lägg till några saknade PlasmaCore importer
+ Ta bort användning av sammanhangsegenskaper för tema och enheter
+ PC3: Förbättra menyns utseende
+ Justera ljudikoner igen för att motsvara breeze ikoner
+ Gör så att showTooltip() går att anropa från QML
+ [PlasmaComponents3] Ta hänsyn till egenskaperna icon.[name|source]
+ Filtrera enligt formfactors om angivna
+ Uppdatera tysta ljud ikon att motsvara breeze ikoner
+ Ta bort felaktig registrering av metatyp för typer som saknar "*" i namnet
+ Använd 35 % ogenomskinlighet för tonade element i nätverksikoner
+ Visa inte Plasma dialogrutor i aktivitetsbytare (fel 419239)
+ Rätta Qt-felwebbadress för fix av teckensnittsåtergivning
+ Använd inte handmarkör eftersom den inte är konsekvent
+ [ExpandableListItem] använd standardstorlek för knappar
+ [PlasmaComponents3] Visa fokuseringseffekt för verktygsknapp och undvik skugga när den är platt
+ Förena höjd av PC3-knappar, textfält och kombinationsrutor
+ [PlasmaComponents3] Lägg till saknade funktioner i TextField
+ [ExpandableListItem] Rätta dumt fel
+ Skriv om button.svg för att göra den lättare att förstå
+ Kopiera DataEngine relays innan iteration (fel 423081)
+ Gör signalstyrkan i nätverksikoner synligare (fel 423843)

### QQC2StyleBridge

+ Använd "höjd" stil för verktygsknappar som inte är platta
+ Reservera bara utrymme för  ikonen i verktygsknapp om vi verkligen har en ikon
+ Stöd att visa en menypil på verktygsknappar
+ Rätta verkligen höjd för menyavskiljare på hög upplösning
+ Uppdatera Mainpage.dox
+ Ställ in höjd för MenuSeparator riktigt (fel 423653)

### Solid

+ Rensa m_deviceCache innan introspektion igen (fel 416495)

### Syntaxfärgläggning

+ Konvertera DetectChar till riktigt Detect2Chars
+ Mathematica: några förbättringar
+ Doxygen: rätta några fel ; DoxygenLua: börjar bara med --- eller --!
+ AutoHotkey: fullständig omskrivning
+ Nim: rätta kommentarer
+ Lägg till syntaxfärgläggning för Nim
+ Scheme: rätta identifierare
+ Scheme: lägg till datumkommentar, nästlad kommentar och andra förbättringar
+ Ersätt några reguljära uttryck med AnyChar, DetectChar, Detect2Chars eller StringDetect
+ language.xsd: ta bort HlCFloat och introducera char-typ
+ KConfig: rätta $(...) och operatorer + några förbättringar
+ ISO C++: rätta prestanda för markering av tal
+ Lua: egenskap med Lua54 och några andra förbättringar
+ ersätt RegExpr=[.]{1,1} med DetectChar
+ Lägg till PureScript färgläggning baserat på Haskell-regler. Det ger en ganska bra approximation och startpunkt för PureScript
+ README.md: använd kanoniska docs.kde.org webbadresser

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
