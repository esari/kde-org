---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Allmänt

Många moduler har nu Python-bindningar.

### Attica

- lägg till stöd för display_name i kategorier

### Breeze-ikoner

- för många ändringar av ikoner för att lista här

### Extra CMake-moduler

- Aktivera -Wsuggest-override för g++ &gt;= 5.0.0
- Skicka -fno-operator-names när det stöds
- ecm_add_app_icon : ignorera SVG-filer tyst när de inte stöds
- Bindningar: Många rättningar och förbättringar

### Integrering med ramverk

- Stöd några av frågorna i KNSCore med underrättelser

### KArchive

- Rätta KCompressionDevice (sökning) för att fungera med Qt &gt;= 5.7

### KAuth

- Uppdatera de flesta exempel, ta bort föråldrade

### KConfig

- Rätta linkning på Windows: länka inte kentrymaptest till KConfigCore

### KConfigWidgets

- Gör ingenting i ShowMenubarActionFilter::updateAction om det inte finns några menyrader

### KCoreAddons

- Rätta fel 363427 - osäkra tecken tolkas felaktigt som en del av webbadressen (fel 363427)
- kformat: Gör det möjligt att översätta relativa datum riktigt (fel 335106)
- KAboutData: Dokumentera att e-postadress för fel också kan vara en webbadress

### KDeclarative

- [IconDialog] Ställ in riktiga ikongrupper
- [QuickViewSharedEngine] Använd setSize istället för setWidth/setHeight

### Stöd för KDELibs 4

- Synkronisera KDE4Defaults.cmake från kdelibs
- Rätta cmake-kontroll av HAVE_TRUNC

### KEmoticons

- KEmoticons: använd D-Bus för att underrätta processer som kör om ändringar gjorda i inställningsmodulen
- KEmoticons: större förbättringar av prestanda

### KIconThemes

- KIconEngine: Centrera ikon i begärd rektangel

### KIO

- Lägg till KUrlRequester::setMimeTypeFilters
- Rätta tolkning av kataloger som listar en specifik FTP-server (fel 375610)
- bevara grupp/ägare vid filkopiering (fel 103331)
- KRun: avråd från användning av runUrl() till förmån för runUrl() med RunFlags
- kssl: Säkerställ att katalog för användarcertifikat har skapats innan användning (fel 342958)

### KItemViews

- Verkställ filter för proxy omedelbart

### KNewStuff

- Gör det möjligt att införa resurser, i huvudsak för systemövergripande inställningar
- Misslyckas inte vid flyttning till tillfällig katalog under installation
- Avråd från användning av klassen security
- Blockera inte vid körning av kommandot post-install (fel 375287)
- [KNS] Ta hänsyn till typ av distribution
- Fråga inte om vi får filen i /tmp

### KNotification

- Lägg till loggningsunderrättelse i filer igen (fel 363138)
- Markera icke-bestående underrättelser som flyktiga
- Stöd "standardåtgärder"

### Ramverket KPackage

- Skapa inte programdata om den är markerad med NoDisplay
- Rätta listning när begärd sökväg är absolut
- rätta hantering av arkiv med en katalog inuti (fel 374782)

### KTextEditor

- rätta återgivning av miniavbildning för omgivningar med stort antal punkter/tum

### KWidgetsAddons

- Lägg till metoder för att dölja åtgärden att avslöja lösenord
- KToolTipWidget: Bli inte ägare till den grafiska innehållskomponenten
- KToolTipWidget: Dölj omedelbart om innehållet förstörs
- Rätta överskridande av fokus i KCollapsibleGroupBox
- Rätta varning när en KPixmapSequenceWidget förstörs
- Installera också deklarationsfiler med blandat skiftläge för vidarebefordring för klasser från deklarationsfiler med flera klasser
- KFontRequester: Sök efter närmaste träff för ett saknat teckensnitt (fel 286260)

### KWindowSystem

- Tillåt tabulator som blir modifierad av skift (fel 368581)

### KXMLGUI

- Felrapportering: Tillåt en webbadress (inte bara en e-postadress) för anpassad rapportering
- Hoppa över tomma genvägar vid tvetydighetskontroll

### Plasma ramverk

- [ContainmentInterface] Inget behov av values() eftersom contains() slår upp nycklar
- Dialog: Dölj när fokus ändras till ConfigView med hideOnWindowDeactivate
- [PlasmaComponents Menu] Lägg till egenskapen maximumWidth
- Sakar ikon vid anslutning till openvpn via Blåtandsnätverk (fel 366165)
- Säkerställ att vi visar aktiverat LIstItem när musen hålls över det
- gör alla höjder i kalenderrubrik jämna (fel 375318)
- rätta färgstilar i Plasma nätverksikon (fel 373172)
- Ställ in wrapMode till Text.WrapAnywhere (fel 375141)
- Uppdatera kalarm-ikon (fel 362631)
- vidarebefordra status från miniprogram till omgivningar på ett riktigt sätt (fel 372062)
- Använd KPlugin för att läsa in kalenderinsticksprogram
- använd färgläggningsfärgen för markerad text (fel 374140)
- [Icon Item] Avrunda storleken som vi vill läsa in en bildpunktsavbildning i
- egenskapen portrait är inte relevant när det inte finns någon text (fel 374815)
- Rätta egenskapen renderType för diverse komponenter

### Solid

- Provisorisk lösning för fel vid hämtning av DBus-egenskap (fel 345871)
- Behandla ingen lösenordsfras som felet Solid::UserCanceled

### Sonnet

- Lägg till fil för grekiska trigram
- Rätta segmenteringsfel i generering av trigram och exponera konstanten MAXGRAMS i deklarationen
- Sök efter libhunspell.so utan version, bör vara mer framtidssäkert

### Syntaxfärgläggning

- C++ färgläggning: uppdatera till Qt 5.8

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
