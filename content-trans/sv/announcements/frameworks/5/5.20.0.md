---
aliases:
- ../../kde-frameworks-5.20.0
date: 2016-03-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Breeze-ikoner

- Många nya ikoner
- Tillägg av virtualbox Mime-typikoner och vissa andra saknade Mime-typer
- Tillägg av stöd för synaptic- och octopi-ikoner
- Rätta klipp ut-ikon (fel 354061)
- Rätta namn av audio-headphones.svg (+=d)
- Betygsikoner med mindre marginal (1 bildpunkt)

### Integrering med ramverk

- Ta bort möjligt filnamn i KDEPlatformFileDialog::setDirectory()
- Filtrera inte enligt namn om det finns Mime-typer

### KActivities

- Ta bort beroende av Qt5::Widgets
- Ta bort beroende av KDBusAddons
- Ta bort beroende av Kl18n
- Ta bort oanvända inkluderingar
- Utmatning från skalskript förbättrad
- Tillägg av datamodell (ActrivitiesModel) som visar aktiviteter i biblioteket
- Bygg bara biblioteket normalt
- Ta bort tjänst- och arbetsrymdskomponenter från bygget
- Flytta biblioteket in i src/lib från src/lib/core
- Rätta CMake-varningar
- Rätta krasch i sammanhangsberoende meny för aktiviteter (fel 351485)

### KAuth

- Rätta låsning av kded5 när ett program som använder kauth avslutas

### KConfig

- KConfigIniBackend: Rätta dyrbar bortkoppling i uppslagning

### KCoreAddons

- Rätta överföring av Kdelibs4-inställning för Windows
- Lägg till programmeringsgränssnitt för att hämta information om Ramverks körningsversion
- KRandom: Använd inte upp till 16K av /dev/urandom som frö för rand() (fel 359485)

### KDeclarative

- Anropa inte nollpekare för objekt (fel 347962)

### KDED

- Gör det möjligt att kompilera med -DQT_NO_CAST_FROM_ASCII

### Stöd för KDELibs 4

- Rätta sessionshantering för program baserade på KApplication (fel 354724)

### KDocTools

- Använd Unicode-tecken för anrop

### KFileMetaData

- KFileMetadata kan nu fråga efter och lagra information om det ursprungliga brevet som en sparad fil kan ha varit en bilaga till

### KHTML

- Rätta uppdatering av markör i vy
- Begränsa användning av minne för strängar
- KHTML-visning av Java-miniprogram: Reparera felaktigt D-Bus anrop till kpasswdserver

### KI18n

- Använd portabelt importmakro för nl_msg_cat_cntr
- Hoppa över uppslagning av . och .. för att hitta översättningar för ett program
- Begränsa användning av _nl_msg_cat_cntr till GNU gettext-implementeringar
- Tillägg av KLocalizedString::languages()
- Utför bara anrop till Gettext om katalogen har hittats

### KIconThemes

- Säkerställ att variabel initieras

### KInit

- kdeinit: Föredra att ladda bibliotek från RUNPATH
- Implementera "Qt5 TODO: use QUrl::fromStringList"

### KIO

- Rätta att anslutning av KIO programslav inte fungerar om appName innehåller '/' (fel 357499)
- Försök flera metoder för behörighetskontroll i händelse av misslyckanden
- help: rätta mimeType() för get()
- KOpenWithDialog: Visa Mime-typens namn och kommentar i texten för kryssrutan "Kom ihåg" (fel 110146)
- En serie ändringar för att undvika att kataloger listas om efter namnbyte av en fil i flera fall (fel 359596)
- http: byt namn på m_iError till m_kioError
- kio_http: Läs och kasta text efter en 404 med errorPage=false
- kio_http: Rätta hur Mime-typ bestäms när webbadress slutar med '/'
- FavIconRequestJob: Lägg till åtkomstens hostUrl() så att Konqueror kan ta reda på vad jobbet gällde i sin slot
- FavIconRequestJob: Rätta jobb som hänger sig vid avbrott på grund av för stor favoritikon
- FavIconRequestJob: Rätta errorString(), den hade bara webbadressen
- KIO::RenameDialog: Återställ stöd för förhandsgranskning, lägg till data- och storleksetiketter (fel 356278)
- KIO::RenameDialog: Omstrukturera duplicerad kod
- Rätta felaktiga konverteringar mellan sökväg och QUrl
- Använd kf5.kio i kategorinamnet för att matcha andra kategorier

### KItemModels

- KLinkItemSelectionModel: Lägg till ny standardkonstruktor
- KLinkItemSelectionModel: Gör den länkade markeringsmodellen tilldelningsbar
- KLinkItemSelectionModel: Hantera ändringar av modellen selectionModel
- KLinkItemSelectionModel: Lagra inte modellen lokalt
- KSelectionProxyModel: Rätta iterationsfel
- Nollställ tillstånd hos KSelectionProxyModel vid behov
- Tillägg av en egenskap som indikerar när modellerna utgör en länkad kedja
- KModelIndexProxyMapper: Förenkla logik för kopplingskontroll

### KJS

- Begränsa användning av minne för strängar

### KNewStuff

- Visa en varning om ett fel uppstår i gränssnittet

### Paketet Framework

- Låt KDocTools vara valfria för KPackage

### KPeople

- Rätta användning av programmeringsgränssnitt vars användning avråds från
- Tillägg av actionType i det deklarativa insticksprogrammet
- Vänd på filtreringslogiken i PersonsSortFilterProxyModel
- Gör QML-exemplet något mer användbart
- Lägg till actionType i PersonActionsModel

### KService

- Förenkla kod, reducera avreferering av pekare, förbättringar relaterade till omgivningar
- Lägg till testprogrammet kmimeassociations_dumper, inspirerat av fel 359850
- Rätta att chromium- och wine-program inte laddas i vissa distributioner (fel 213972)

### KTextEditor

- Rätta markering av alla förekomster i ReadOnlyPart
- Iterera inte över en QString som om den var en QStringList
- Initiera statiska QMaps på ett riktigt sätt
- Föredra toDisplayString(QUrl::PreferLocalFile)
- Stöd att skicka surrogattecken från inmatningsmetod
- Krascha inte vid avslutning när textanimering fortfarande pågår

### Ramverket KWallet

- Säkerställ att KDocTools slås upp
- Skicka inte ett negativt tal till D-Bus, det ger en assert i libdbus
- Städa cmake-filer
- KWallet::openWallet(Synchronous): låt inte tidsgräns gå ut efter 25 sekunder

### KWindowSystem

- Stöd _NET_WM_BYPASS_COMPOSITOR (fel 349910)

### KXMLGUI

- Använd annat språknamn än modersmål som reserv
- Rätta sessionshantering felaktig sedan KF5 och Qt5 (fel 354724)
- Genvägssystem: Stöd globalt installerade system
- Använd qHash(QKeySequence) från Qt vid bygge med Qt 5.6+
- Genvägssystem: Rätta fel där två KXMLGUIClients med samma namn skriver över varandras systemfiler
- kxmlguiwindowtest: Lägg till genvägsdialogrutor, för att prova editorn av genvägssystem
- Genvägssystem: Förbättra användbarhet genom att ändra texter i det grafiska användargränssnittet
- Genvägssystem: Förbättra kombinationsrutan med systemlista (automatisk storlek, rensa inte vid okänt system)
- Genvägssystem: Lägg inte till namnet på den grafiska användargränssnittet först i filnamnet
- Genvägssystem: Skapa katalog innan försök att spara nytt genvägssystem
- Genvägssystem: Återställ layoutmarginal, annars ser det mycket trångt ut
- Rätta minnesläcka för starthake i KXmlGui

### Plasma ramverk

- IconItem: Skriv inte över källa när QIcon::name() används
- ContainmentInterface: Rätta användning av QRect right() och bottom()
- Ta bort i stort sett duplicerad kod för att hantera QPixmaps
- Lägg till dokumentation av programmeringsgränssnitt för IconItem
- Rätta stilmall (fel 359345)
- Töm inte fönstermask vid varje ändring av geometri när sammansättning är aktiv och ingen mask har ställts in
- Miniprogram: Krascha inte när panelen tas bort (fel 345723)
- Tema: Töm cache med bildpunktsavbildningar när temat ändras (fel 359924)
- IconItemTest: Hoppa över när grabToImage misslyckas
- IconItem: Rätta ändrad färg av SVG-ikoner som läses in från ikontema
- Rätta upplösning av svg iconPath i IconItem
- Om sökväg skickas, välj slutet (fel 359902)
- Lägg till egenskaperna configurationRequired och reason
- Flytta contextualActionsAboutToShow till miniprogram
- ScrollViewStyle: Använd inte marginaler för bläddringsbara objekt
- DataContainer: Rätta slot-kontroll innan upp- eller nerkoppling
- ToolTip: Förhindra multipla geometriändringar medan innehållet ändras
- SvgItem: Använd inte Plasma::Theme från återgivningstråden
- AppletQuickItem: Rätta hur egen ansluten layout hittas (fel 358849)
- Mindre expansion för verktygsraden
- ToolTip: Visa timer om Tooltip anropas (fel 358894)
- Inaktivera animering av ikoner i Plasma verktygstips
- Ta bort animering från verktygstips
- Standardtema följer färgschema
- Rätta att IconItem inte läser in ikoner med namn som inte ingår i tema (fel 359388)
- Föredra andra omgivningar än skrivbord i containmentAt()
- WindowThumbnail: Kasta glx pixmap i stopRedirecting() (fel 357895)
- Ta bort föråldrat filter för miniprogram
- ToolButtonStyle: Lita inte på yttre id
- Anta inte att en corona hittas (fel 359026)
- Kalender: Lägg till riktiga bakåt/framåt knappar och en "Idag" knapp (fel 336124, 348362, 358536)

### Sonnet

- Inaktivera inte språkdetektering bara på grund av att ett språk är inställt
- Inaktivera automatisk inaktivering av automatisk stavningskontroll som förval
- Rätta textbrytningar
- Rätta att sökvägar för Hunspell ordlistor saknar '/' (fel 359866)
- Lägg till &lt;programkatalog&gt;/../share/hunspell till sökvägen för ordlistor

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
