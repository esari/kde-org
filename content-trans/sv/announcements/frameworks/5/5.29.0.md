---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nytt ramverk

Utgåvan inkluderar Prison, ett nytt ramverk för att generera streckkod (inklusive QR-koder).

### Allmänt

FreeBSD har lagts till metainfo.yaml i alla ramverk testade att fungera på FreeBSD.

### Baloo

- Prestandaförbättringar vid skrivning (fyra gånger snabbare vid utskrift av data)

### Breeze-ikoner

- Gör att BINARY_ICONS_RESOURCE normalt är på
- Lägg till vnd.rar mime för shared-mime-info 1.7 (fel 372461)
- Lägg till ikon för claws (fel 371914)
- Lägg till gdrive-ikon istället för generell molnikon (fel 372111)
- Rätta fel "list-remove-symbolic använder fel bild" (fel 372119)
- Andra tillägg och förbättringar

### Extra CMake-moduler

- Hoppa över test av Python-bindningar om PyQt inte är installerat
- Lägg bara till testen om Python hittas
- Reducera minimal CMake som krävs
- Lägg till modulen ecm_win_resolve_symlinks

### Integrering med ramverk

- Hitta QDBus, som behövs av appstream kpackage-hanteraren
- Låt KPackage ha beroenden från packagekit och appstream

### KActivitiesStats

- Skicka resursen länkad händelse på korrekt sätt

### KDE Doxygen-verktyg

- Anpassa till ändring från quickgit till cgit
- Rätta fel om gruppnamn inte definierat. Kan fortfarande sluta fungera vid dåliga villkor.

### KArchive

- Lägg till metoden errorString() för att ge felinformation

### KAuth

- Lägg till tidsgränsegenskap (fel 363200)

### KConfig

- kconfig_compiler - Skapa kod med överskridningar
- Tolka funktionsnyckelord på korrekt sätt (fel 371562)

### KConfigWidgets

- Säkerställ att menyalternativ får avsedd menyroll

### KCoreAddons

- KTextToHtml: Rätta fel "[1] tillagt i slutet på en hyperlänk" (fel 343275)
- KUser: Sök bara efter en avatar om inloggningsnamnet inte är tomt

### KCrash

- Använd inte DISPLAY på Mac i linje med KInit
- Stäng inte alla fildeskriptorer på OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - Lägg till överskridningar i genererad kod

### KDESU

- Deaktivera XDG_RUNTIME_DIR i processer som körs med kdesu

### KFileMetaData

- Hitta verkligen FFMpegs libpostproc

### KHTML

- Java: Använd namn på rätt knappar
- Java: Sätt namn i rättighetsdialogruta

### KI18n

- Kontrollera pekarolikhet från dngettext på korrekt sätt (fel 372681)

### KIconThemes

- Tillåt att ikoner från alla kategorier visas (fel 216653)

### KInit

- Sätt miljövariabler från KLaunchRequest när en ny process startas

### KIO

- Konverterad till kategoriserad loggning
- Rätta kompilering för WinXP SDK
- Tillåt matchning av checksummor med stora bokstäver under checksummefliken (fel 372518)
- Sträck aldrig ut den sista kolumnen (datum) i fildialogrutan (fel 312747)
- Importera och uppdatera kcontrol docbooks för kod i kio från kde-runtime master
- [OS X] Gör så att KDE:s papperskorg använder OS X papperskorg
- SlaveBase: Lägg till dokumentation om händelsesnurror, underrättelser och kded-moduler

### KNewStuff

- Lägg till nya alternativ för arkivhantering (underkatalog) i knsrc
- Konsumera de nya felsignalerna (ange jobbfel)
- Hantera underlighet rörande filer som försvinner när de just skapats
- Installera kärnans deklarationsfiler, med blandat skiftläge

### KNotification

- [KStatusNotifierItem] Spara/återställ position för grafisk komponent när dess fönster döljes/återställs (fel 356523)
- [KNotification] Tillåt att underrättelser förses med webbadresser

### Ramverket KPackage

- Fortsätt installera metadata.desktop (fel 372594)
- Läs in metadata manuellt om en absolut sökväg skickas
- Rätta potentiellt fel om paket inte är kompatibelt med appstream
- Låt KPackage få reda på X-Plasma-RootPath
- Rätta generering av filen metadata.json

### KPty

- Flytta sökning efter utempter sökväg (inklusive /usr/lib/utempter/)
- Lägg till bibliotekssökväg så att binärprogrammet utempter hittas på Ubuntu 16.10

### KTextEditor

- Förhindra Qt-varningar om ett klippbordsläge som inte stöds på Mac
- Använd syntaxdefinitioner från KF5::SyntaxHighlighting

### KTextWidgets

- Ersätt inte fönsterikoner med resultatet av en misslyckad uppslagning

### Kwayland

- [klient] Rätta avreferering av nollpekare i ConfinedPointer och LockedPointer
- [klient] Installera pointerconstraints.h
- [server] Rätta regression i SeatInterface::end/cancelPointerPinchGesture
- Implementering av protokollet PointerConstraints
- [server] Reducera kostnad av pointersForSurface
- Returnera SurfaceInterface::size i global sammansättningsrymd
- [tools/generator] Generera uppräkning FooInterfaceVersion på serversidan
- [tools/generator] Omge argument för wl_fixed-begäran med wl_fixed_from_double
- [tools/generator] Generera implementering av begäran på klientsidan
- [tools/generator] Generera resurstillverkning på klientsidan
- [tools/generator] Generera återanrop och lyssnare på klientsidan
- [tools/generator] Skicka det som q-pekare till Client::Resource::Private
- [tools/generator] Generera Private::setup(wl_foo *arg) på klientsidan
- Implementering av protokollet PointerGestures

### KWidgetsAddons

- Förhindra krasch på Mac
- Ersätt inte ikoner med resultatet av en misslyckad uppslagning
- KMessageWidget: Rätta layout när radbrytning är aktiverad utan åtgärder
- KCollapsibleGroupBox: Dölj inte grafiska komponenter, överskrid istället fokusprincip

### KWindowSystem

- [KWindowInfo] Lägg till pid() och desktopFileName()

### Oxygen-ikoner

- Lägg till ikon application-vnd.rar (fel 372461)

### Plasma ramverk

- Kontrollera att metadata är giltig i settingsFileChanged (fel 372651)
- Vänd inte layout av flikrad om den är vertikal
- Ta bort radialGradient4857 (fel 372383)
- [AppletInterface] Dra aldrig bort fokus från fullRepresentation (fel 372476)
- Rätta SVG-ikon id-prefix (fel 369622)

### Solid

- winutils_p.h: Återställ kompatibilitet med WinXP SDK

### Sonnet

- Sök också efter hunspell-1.5

### Syntaxfärgläggning

- Normalisera XML-licensegenskapsvärden
- Synkronisera syntaxdefinitioner från ktexteditor
- Rätta sammanfogning av vikområden

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
