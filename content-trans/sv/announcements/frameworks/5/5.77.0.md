---
aliases:
- ../../kde-frameworks-5.77.0
date: 2020-12-12
layout: framework
libCount: 83
qtversion: 5.13
---
### Attica

* Rätta krasch vid laddning av tillhandahållare genom att kontrollera svarspekaren innan avreferering (fel 427974)

### Baloo

* [DocumentUrlDB] Ta bort underliggande listpost från databas om tom
* Lägg till dokumenttypen Presentation för Office OpenXML bildspel och mall
* [MetaDataMover] Rätta uppslagning av överliggande dokumentidentifierare
* [DocumentUrlDB] Lägg till metod för triviala namnbyten och förflyttningar
* [MetaDataMover] Gör namnbyte till enbart en databasoperation
* [Dokument] Lägg till överliggande dokumentidentifierare och befolka den
* Ersätt direkt syslog-anrop med kategoriserat loggningsmeddelande

### Breeze-ikoner

* Lägg till textfältsvarianter: -frameless (-> text-field), -framed
* Lägg till symboliskt namn symbolisk länk för input-* ikoner
* Lägg till ikon för True Type XML-teckensnitt
* Lägg till add-subtitle
* Ändra MathML-ikon för att använda en formel och använda specifik Mime-typ
* Lägg till ikon för QEMU diskavbild och SquashFS-avbild
* Lägg till åtgärdsikon för edit-move
* Lägg till ikon för kärndump
* Lägg till en hel mängd Mime-typer för textning
* Ta bort oanvändbar suddighet från kontrastikonen

### Extra CMake-moduler

* Rätta kategoriextrahering från skrivbordsfiler
* Definiera installationkatalogvariabel för filmallar
* Lägg till generering av snabbfilsmetadata för Android-byggen
* (Qt)WaylandScanner: Markera filer riktigt som SKIP_AUTOMOC

### KActivitiesStats

* ResultModel: exponera resurs Mime-typ
* Lägg till händelsefiltrering för filer och kataloger (fel 428085)

### KCalendarCore

* Rätta underhållsansvarig, det är meningen att det ska vara Allen, inte jag :)
* Lägg till egenskapen CONFERENCE
* Lägg till bekvämlighetsmetod alarmsTo till Calendar
* Kontrollera att dagliga upprepningar inte föregår dtStart

### KCMUtils

* Ta bort fix som tog sönder flernivåers inställningsmoduler i ikonläge

### KConfig

* Rätta KConfigGroup::copyTo med KConfigBase::Notify (fel 428771)

### KCoreAddons

* Undvik krasch när fabriken är tom (när vi returnerar ett fel)
* KFormat: Lägg till flera relativa datum- och tidfall
* Aktivera KPluginFactory för att valfritt skicka KPluginMetaData till insticksprogram

### KDAV

* Ta bort det eftersom det orsakar för många fel

### KDeclarative

* Synkronisera marginaler från AbstractKCM till SimpleKCM
* Ta bort föråldrad licenstext
* Licensiera om fil till LGPL-2.0-or-later
* Licensiera om fil till LGPL-2.0-or-later
* Licensiera om fil till LGPL-2.0-or-later
* Licensiera om fil till LGPL-2.0-or-later
* Skriv om KeySequenceItem (och hjälpfunktioner) att använda KeySequenceRecorder (fel 427730)

### KDESU

* Tolka undantagna dubbla citationstecken riktigt
* Lägg till stöd för OpenBSD's doas(1)

### KFileMetaData

* Rätta några läckor i extrahering av OpenDocument och Office OpenXML
* Lägg till olika undertyper för OpenDocument- och OpenXML-dokument

### KGlobalAccel

* Läs in statiskt länkade kglobalacceld gränssnittsinsticksprogram

### KDE GUI Addons

* Gör så att inhibering av genvägar fungerar från början (fel 407395)
* Rätta potentiell krasch i Wayland inhibitor rivning (fel 429267)
* CMake: Hitta Qt5::GuiPrivate när Wayland-stöd är aktiverat
* Lägg till KeySequenceRecorder som bas för KKeySequenceWidget och KeySequenceItem (fel 407395)

### KHolidays

* Rätta avrundning av solpositionshändelser mindre än 30 s innan nästa timma
* Undvik tolkning av varje helgfil två gånger i defaultRegionCode()
* Beräkna astronomiska årstider bara en gång per förekomst
* Rätta uppslagning av helgområde för ISO 3166-2 koder
* Gör HolidayRegion kopieringsbar/flyttbar
* Lägg till stöd för att räkna ut civila skymningstider

### KIdleTime

* Läs in statiskt länkade insticksprogram för systempollning

### KImageFormats

* Öka inte längre färgdjup till 8 för 16-bitars okomprimerad PSD-fil

### KIO

* Ny fildialogruta: tillåt att acceptera att katalog skapas innan stat har kört (fel 429838)
* Läck inte DeleteJob tråd
* KUrlNavBtn: gör så att öppna underkataloger från kombinationsmeny med tangentbordet fungerar (fel 428226)
* DropMenu: Använd översatta genvägar
* [ExecutableFileOpenDialog] ge knappen Avbryt fokus
* Platsvisning: färglägg bara plats när den visas (fel 156678)
* Lägg till egenskap för att visa insticksåtgärder i undermenyn "Åtgärder"
* Ta bort nyintroducerad metod
* KIO::iconNameForUrl: lös upp ikon för fjärrfiler baserat på namn (fel 429530)
* [kfilewidget] Använd ny standardgenväg för "Skapa katalog"
* Omstrukturera inläsning av sammanhangsberoende meny och gör den mer skalbar
* RenameDialog: tillåt överskrivning när filer är äldre (fel 236884)
* DropJob: använd ny ikon edit-move för 'Flytta hit'
* Rätta moc_predefs.h gen när ccache är aktiverad via -DCMAKE_CXX_COMPILER=ccache CMAKE_CXX_COMPILER_ARG1=g++
* Vi kräver Qt 5.13 nu, så ta bort ifdef
* Konvertera KComboBox till QComboBox
* Dokumenträttning begärd av Méven Car @meven
* Omstrukturera några snurror med användning av modern C++
* Rensa död kod
* Ta bort redundant kontroll om nyckel finns
* Förenkla kod i RequiredNumberOfUrls
* Ta bort några tomma rader
* Förenkla kod och gör den konsekventare
* ioslaves: rätta administratörsbehörighet för remote:/
* KFileItem: isWritable använd KProtocolManager för fjärrfiler
* Lägg till överlagring för att lägga till alternativ först i menyn "Åtgärder"
* Använd modern kodningsstil
* Lägg inte till onödiga avskiljare (fel 427830)
* Använd en initieringslista med klammerparenteser istället för operatorn <<
* MkPathJob: skriv om villkorligt kompilerad kod för att förbättra läsbarhet

### Kirigami

* Gör så att GlobalDrawer sidhuvud ställer in position av verktygsrader/flikrader/dialogknapprutor
* inViewport riktigt bilagd
* rätta huvudreglage på pekskärm
* Omstrukturera AbstractapplicationHeader med nytt "ScrollIntention" koncept
* på skrivbord fyll alltid förankringar till överliggande
* [controls/BasicListItem]: Förankra inte vid icke existerade objekt
* Visa inte avatartext vid liten storlek
* Ta bort # och @ från extraheringsprocessen för initial avatar
* Initiera egenskap i sizeGroup
* använd interaktion med musen vid isMobile för enklare testning
* Snabbfixa inledande/avslutande genom att använd avslutande värden för inledande avskiljares marginal
* [controls/BasicListItem]: Lägg till inledande/avslutande egenskaper
* rätta positionering av blad när innehållets storlek ändras
* Använd gammalt beteende i brett läge och lägg inte till topMargin i FormLayout
* Rätta formulärlayout på liten skärm
* Man kan inte använda AbstractListItem i en SwipeListItem
* Rätta "Kan inte tilldela [undefined] till int" i OverlaySheet
* [overlaysheet]: Gör inte en ryckig övergång när innehållets höjd ändras
* [overlaysheet]: Animera höjdändringar
* Rätta positionering av överlagrat blad
* ange alltid index vid klick på en sida
* rätta dragning av FAB:er i höger-till-vänster läge
* Förbättra listavgränsares utseende (fel 428739)
* rätta lådgrepp i höger-till-vänster läge
* Rätta återgivning av kanter med riktig storlek för reserv med programvara (fel 427556)
* Placera inte reservalternativ med programvara utanför den skuggade rektangelns objektgränser
* Använd fwidth() för utjämning i lågeffektläge (fel 427553)
* Återge också en bakgrundsfärg i lågeffektläge
* Aktivera genomskinlig återgivning för Shadowed(Border)Texture i lågeffekt
* Avbryt inte alfakomponenter för skuggrektanglar i lågeffektläge
* Använd inte ett längre utjämningsvärde när strukturen för ShadowedBorderTexture återges
* Ta bort "utklippta" steg från skuggrektangel och relaterade skuggningar
* Använd icon.name istället för iconName i dokumentation
* [Avatar] Gör så att ikonen använder en ikonstorlek nära till textstorleken
* [Avatar] Gör så att initialerna använder mer utrymme och förbättra vertikal justering
* [Avatar] Exponera egenskaperna sourceSize och smooth för vem som helst som vill animera storleken
* [Avatar] Ange källstorleken för att förhindra att bilder är suddiga
* [Avatar] Ange förgrundsfärg en gång
* [Avatar] Ändra bakgrundstoning
* [Avatar] Ändra kantbredd till en bildpunkt för att motsvara andra besvärsbredder
* [Avatar] Gör så att padding, verticalPadding och horizontalPadding alltid fungerar
* [avatar]: Lägg till toning för färger

### KItemModels

* KRearrangeColumnsProxyModel: bar kolumn 0 har underliggande objekt

### KMediaPlayer

* Installera definitionsfiler av tjänsttyp för spelare och gränssnitt enligt filnamnets matchande typ

### KNewStuff

* Rätta avinstallation när posten inte finns i cache
* När vi anropar för att kontrollera uppdateringar, förväntar vi oss uppdateringar (fel 418082)
* Återanvänd QWidgets dialogruta (fel 429302)
* Omge kompatibilitetsblock i KNEWSTUFFCORE_BUILD_DEPRECATED_SINCE
* Skriv inte cache för mellanliggande tillstånd
* Använd enum för uppackning istället för strängvärden
* Rätta att post försvinner för tidigt från uppdateringsbar sida (fel 427801)
* Lägg till DetailsLoadedEvent enum till ny signal
* Arbeta om programmeringsgränssnitt för adoptions (fel 417983)
* Rätta ett antal eftersläntrare med gamla leverantörswebbadresser
* Ta bort post från cache innan ny post infogas (fel 424919)

### KNotification

* Skicka inte transient tips (fel 422042)
* Rätta skiftlägeskänsligt fel för AppKit-sidhuvud på macOS
* Anropa inte ogiltiga underrättelseåtgärder
* Rätta minneshantering av notifybysnore

### Ramverket KPackage

* Kasta X-KDE-PluginInfo-Depends

### KParts

* Avråd från metoden embed(), på grund av avsaknad av användning
* Gör så att KParts använder KPluginMetaData istället för KAboutData

### KQuickCharts

* Arbeta om algoritmen för linjeutjämning
* Flytta verkställ interpolering till steget polish
* Centrera punktdelegater i linjediagram och ändra storlek på dem med linjebredd
* Lägg till en "jämn" kryssruta i exempel över linjediagram
* Säkerställ att punktdelegater i linjediagram städas bort riktigt
* Visa också namn i verktygstips på sidan med exempel på linjediagram
* Dokumentera LineChartAttached och rätta ett stavfel i dokumentation av LineChart
* Lägg till egenskaperna name och shortName i LineChartAttached
* Dokumentera egenskapen pointDelegate noggrannare
* Ta bort medlemmen previousValues och rätta staplade linjediagram
* Använd pointDelegate i exemplet med linjediagram för att visa värden när muspekaren hålls över
* Lägg till stöd för "point delegate" i linjediagram
* LineChart: Flytta punktberäkningar från updatePaintNode till polish

### Kör program

* Avråd från KDE4 paketrester
* Använd det nya stödet för konstruktor för KPluginMetaData i KPluginLoader

### KService

* [kapplicationtrader] Rätta programmeringsgränssnittets dokumentation
* KSycoca: återskapa databas när version < förväntad version
* KSycoca: Håll reda på resursfiler för KMimeAssociation

### KTextEditor

* Konvertera KComboBox till QComboBox
* använd KSyntaxHighlighting themeForPalette
* rätta i18n-anrop, saknar argument (fel 429096)
* Förbättra det automatiska temavalet

### KWidgetsAddons

* Skicka inte ut signalen passwordChanged två gånger
* Lägg till KMessageDialog, en asynkroncenterad variant av KMessageBox
* Återställ det gamla standardläget för sammanhangsberoende menyer i KActionMenu
* Konvertera KActionMenu till QToolButton::ToolButtonPopupMode

### KWindowSystem

* Läs in statiskt länkade integreringsinsticksprogram
* Konvertera från pid() till processId()

### KXMLGUI

* Introducera HideLibraries och avråd från HideKdeVersion
* Skriv om KKeySequenceWidget för att KeySequenceRecorder (fel 407395)

### Plasma ramverk

* [Representation] Ta bara bort övre/undre vaddering när sidhuvud/sidfot är synliga
* [PlasmoidHeading] Använd teknik från Representation för inset och marginaler.
* Lägg till en representationskomponent
* [Desktop theme] Byt namn på hint-inset-side-margin till hint-side-inset
* [FrameSvg] Byt namn på insetMargin till inset
* [PC3] Använd PC3 Scrollbar i ScrollView
* [Breeze] Rapportera inset tips
* [FrameSvg*] Byt namn på shadowMargins till inset
* [FrameSvg] Lagra skuggmarginaler och ta hänsyn till prefix
* Avsluta animeringen innan längden på förloppsradens färgläggning ändras (fel 428955)
* [textfield] Rätta överlappande text på rensningsknapp (fel 429187)
* Visa kombinationsmeny vid korrekt globala position
* Använd gzip -n för att förhindra inbäddade byggtider
* Använd KPluginMetaData för att lista containmentActions
* Konvertera packageStructure inläsning från KPluginTrader
* Använd KPluginMetaData för att lista DataEngines
* [TabBar] Lägg till färgläggning vid tangentbordsfokus
* [FrameSvg*] Exponera skuggmarginaler
* Gör värdet MarginAreasSeparator klarare
* [TabButton] Justera ikon och text till mitten när texten är vid sidan om ikonen
* [SpinBox] Rätta logikfel i rullningens riktning
* rätta rullningslist i höger-till-vänster läge på mobil
* [PC3 ToolBar] Inaktivera inte kanter
* [PC3 ToolBar] Använd rätta svg-marginalegenskaper för vaddering
* [pc3/scrollview] Ta bort pixelAligned
* Lägg till marginalområden

### Syfte

* [blåtand] Rätta delning av flera filer (fel 429620)
* Läs översatt insticksprogrammens åtgärdsbeteckning (fel 429510)

### QQC2StyleBridge

* knapp: lita på down, inte pressed för stilen
* Reducera runda knappars storlek på mobil
* rätta rullningslist i höger-till-vänster läge på mobil
* rätta förloppsrad med höger-till-vänster läge
* rätta visning av höger-till-vänster för RangeSlider

### Solid

* Inkludera errno.h för EBUSY/EPERM
* FstabBackend: returnera DeviceBusy när umount misslyckades för EBUSY (fel 411772)
* Rätta detektering av senaste libplist och libimobiledevice

### Syntaxfärgläggning

* rätta de genererade filernas beroenden
* indexer: rätta några problem och inaktivera två kontroller (fångstgrupp och keyword with delimiter)
* indexer: läs in alla xml-filer i minne för enkel kontroll
* C++ färgläggning: uppdatera till Qt 5.15
* starta om syntaxgeneratorer när källkodsfilen ändras
* systemd enhet: uppdatera till systemd v247
* ILERPG: förenkla och testa
* Zsh, Bash, Fish, Tcsh: lägg till truncate och tsort i unixcommand nyckelord
* Latex: några matematikomgivningar kan vara nästlade (fel 428947)
* Bash: Många rättningar och förbättringar
* lägg till --syntax-trace=stackSize
* php.xml: Rätta matchande endforeach
* Flytta bestThemeForApplicationPalette från KTextEditor hit
* debchangelog: lägg till Trixie
* alert.xml: Lägg till `NOQA`, ytterligare en populär varning i källkod
* cmake.xml: Bestäm uppströms att `cmake_path` ska fördröjas för nästa utgåva

### Säkerhetsinformation

Den utgivna koden har signerats med GPG genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Fingeravtryck för primär nyckel: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
