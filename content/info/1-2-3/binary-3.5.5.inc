<ul>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>Packages: <a href="ftp://ftp.archlinuxppc.org/extra/os/ppc/">ftp://ftp.archlinuxppc.org/extra/os/ppc/</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- Debian -->
<li><a href="http://www.debian.org/">Debian</a>
    <ul type="disc">
      <li>
         Debian Unstable: <a href="http://packages.debian.org/unstable/kde/">http://packages.debian.org/unstable/kde/</a>
      </li>
    </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Kubuntu 6.06 LTS (dapper): <a href="http://kubuntu.org/announcements/kde-355.php">Intel i386, AMD64 and 
PowerPC</a>
      </li>
    </ul>
  <p />
</li>

<!-- Pardus -->
<li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         1.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.5/Pardus/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>

<!-- SUSE LINUX -->
<li>
  <a href="http://www.opensuse.org">SUSE Linux</a>
  The SUSE KDE packages will be publicly developed in the openSUSE project from 
now on. This means that the packages are no longer available via the 
supplementary tree from ftp.suse.com, but via the <a 
href="http://en.opensuse.org/Build_Service">Build Service</a> repositories 
from openSUSE. Please read <a 
href="http://en.opensuse.org/Build_Service/User">this page</a> for 
installation instructions.<br>
  <ul type="disc">
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE 
Linux 10.1 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.0/">SUSE 
Linux 10.0 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_9.3/">SUSE 
Linux 9.3 YUM repository</a> (64bit and 32bit)
    </li>
  </ul>
  <p /> 
</li>


</ul>
