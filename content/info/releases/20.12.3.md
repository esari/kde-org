---
aliases:
- /info/releases-20.12.3
date: 2021-03-04
title: "20.12.3 Releases Source Info Page"
layout: releases
signer: Heiko Becker
signing_fingerprint: D81C0CB38EB725EF6691C385BB463350D6EF31EF
---
