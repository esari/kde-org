KDE Project Security Advisory
=============================

Title:           KDE Partition Manager: kpmcore_externalcommand helper can be exploited in local privilege escalation
Risk Rating:     Important
CVE:             CVE-2020-27187
Versions:        kpmcore == 4.1.0
Author:          Andrius Štikonas <andrius@stikonas.eu>
Date:            17 October 2020

Overview
========

kpmcore_externalcommand helper contains a logic flaw in which the service invoking dbus
is not properly checked. An attacker on your local machine can replace /etc/fstab,
execute mount and other partitioning related commands while KDE Partition Manager is running.
mount command can then be used to gain full root privileges.

Impact
======

KDE Partition Manager 4.1.0 should not be used on systems with untrusted users or running untrusted software.

Solution
========

KDE Partition Manager 4.2.0 fixes this issue.

You can apply the following patches on top of KPMcore 4.1.0:
https://invent.kde.org/system/kpmcore/-/commit/c466c5db11b5cee546d1ec0594c2f1105a354fed (fix)
https://invent.kde.org/system/kpmcore/-/commit/7ec4b611dcf822439b081613cca4184689266454 (removes KF5 5.73 dependency)


Credits
=======

Thanks to David Edmundson who co-authored polkit port.
