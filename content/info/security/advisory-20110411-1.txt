KDE Security Advisory: Konqueror Partially Universal XSS in Error Pages
Original Release Date: 2011-04-11
URL: http://www.kde.org/info/security/advisory-20110411-1.txt

0. References:
    CVE-2011-1168
    http://www.nth-dimension.org.uk/pub/NDSA20110321.txt.asc

1. Systems affected:

    Konqueror as shipped with KDE SC 4.4.0 up to and including KDE SC 4.6.1. Earlier
    versions of KDE SC may also be affected.

2. Overview:

    When Konqueror cannot fetch a requested URL, it renders an error page with
    the given URL. If the URL contains JavaScript or HTML code, this code is 
    also rendered, allowing for the user to be tricked into visiting a 
    malicious site or providing credentials to an untrusted party.

    A basic example of this vulnerability is as follows:

    http://thisdomainwillnotresolveandrekonqerrorpagewillbeshownwithfullurlembedded.twitter.com/"><h1>Test</h1>

    When the invalid domain fails to resolve, the error page returned by
    Konqueror will include "Test".

    JavaScript makes this problem worse:

    https://secure.twitter.com/"><style>#box {display: 
    none}</style></div>Welcome to Twitter</h1><form><p>Username: <input 
    name="username" type="text"></p><p>Password: <input name="password" 
    type="Password"></p><p><a 
    href="javascript:alert('Pwned')">Login</a></p></form><div id="box">

    Even with JavaScript turned off, an iFrame can still be used to trick the
    user:

    https://secure.twitter.com/"><style>#box {display: 
    none}</style></div><iframe src='data:text/html,<a><form><p>Username: <input 
    type=text></p><p>Password: <input 
    type=password></p></form><script>alert(1)</script>'><div id="box">

    The vulnerability and technical information about the exploit were
    provided by Tim Brown of Nth Dimension. We thank them for their 
    responsible disclosure and cooperative handling of the matter.

3. Impact:

    Exploitation may trick the user into providing credentials to untrusted
    third parties, or could take advantage of an unknown vulnerability in
    KHTML to act as a catalyst for a further exploit.

4. Solution:

    Source code patches have been made available which fix these
    vulnerabilities. At the time of this writing most OS vendor / binary
    package providers should have updated binary packages. Contact your OS
    vendor / binary package provider for information about how to obtain
    updated binary packages.

5. Patch:

    Patches for this issue were provided by Maksim Orlovich (maksim@kde.org).

    Patches have been committed to the kdelibs Git repository in the
    following revision numbers:

    4.4 branch: afaaf24
    4.5 branch: da03cc0
    4.6 branch: 8b06e2c
    master: aaa8c42

    These patches can also be obtained directly from the following URLs:

    4.4 branch:
    http://quickgit.kde.org/?p=kdelibs.git&a=blobdiff_plain&h=52a3a464960be6c9b05f593e3d424a5b80560d03&hp=77dc792cb2e2c79e3872060d23c1913304ff8427&f=khtml/khtml_part.cpp

    4.5 branch:
    http://quickgit.kde.org/?p=kdelibs.git&a=blobdiff_plain&h=5d4b9b5a197f191b641712782479ff45b95c8b49&hp=6af7d4a0f525cfb7c70c0c613794afff86b81ba9&f=khtml/khtml_part.cpp

    4.6 branch:
    http://quickgit.kde.org/?p=kdelibs.git&a=blobdiff_plain&h=fda41ceaa6e5ce7cbb50312cbe12be7a6f056c79&hp=d4098c3eadb0e3238643be749073dd54c22a5bbc&f=khtml/khtml_part.cpp

    master:
    http://quickgit.kde.org/?p=kdelibs.git&a=blobdiff_plain&h=ec89b0c8083989afb52ebde714e1fe757ab2e387&hp=35c1d30a781646138b5d74a00508390e1df707e7&f=khtml/khtml_part.cpp

