KDE Project Security Advisory
=============================

Title:          plasma-workspace, kscreenlocker: Lock screen bypass
Risk Rating:    Low
CVE:            CVE-2016-2312
Platforms:      X11
Versions:       plasma-workspace < 5.5.0, kscreenlocker < 5.5.5
Author:         Martin Gräßlin mgraesslin@kde.org
Date:           09 February 2016

Overview
========

Turning all screens off while the lock screen is shown can result in the screen being unlocked when turning a screen on again.

Impact
======

An unauthorized user might gain access to a locked system. Physical access to the hardware is required.

Workaround
==========

None

Solution
========

For plasma-workspace apply the following patches:
 5.0 branch: http://commits.kde.org/plasma-workspace/5651785ad6663e2ef4d12a94b0b5f1cb7d40a9a1
 5.1 branch: http://commits.kde.org/plasma-workspace/1fe565e5dae31e57d81556b07e7459be14c5d834
 5.2 branch: http://commits.kde.org/plasma-workspace/e1036973552a8964dffcbca0743eb1accc14bc56
 5.3 branch: http://commits.kde.org/plasma-workspace/de6e19fd8c30166bdbc1333dcd5ef2278f570fa2
 5.4 branch: http://commits.kde.org/plasma-workspace/23a9ed7ba9995570227dbcd69c23f009de7dde49

For kscreenlocker upgrade to Plasma 5.5.5 (after 1 March 2016) or apply the following patch:
http://commits.kde.org/kscreenlocker/fae65f1cdd6446042b31ccd0eafd7a4c0b6623e3

References
==========

https://bugs.kde.org/show_bug.cgi?id=358125
https://bugzilla.opensuse.org/show_bug.cgi?id=964548

Credits
=======

Thanks to Dirk Weber for finding the issue, the openSUSE community for helping investigating and Martin Gräßlin for fixing the issue.

