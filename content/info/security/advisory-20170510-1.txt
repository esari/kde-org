KDE Project Security Advisory
=============================

Title:          kauth: Local privilege escalation
Risk Rating:    High
CVE:            CVE-2017-8422
Versions:       kauth < 5.34, kdelibs < 4.14.32
Date:           10 May 2017


Overview
========
KAuth contains a logic flaw in which the service invoking dbus
is not properly checked.

This allows spoofing the identity of the caller and with some
carefully crafted calls can lead to gaining root from an
unprivileged account.

Solution
========
Update to kauth >= 5.34 and kdelibs >= 4.14.32 (when released)

Or apply the following patches:
  kauth: https://commits.kde.org/kauth/df875f725293af53399f5146362eb158b4f9216a
kdelibs: https://commits.kde.org/kdelibs/264e97625abe2e0334f97de17f6ffb52582888ab

Credits
=======
Thanks to Sebastian Krahmer from SUSE for the report and
to Albert Astals Cid from KDE for the fix.
