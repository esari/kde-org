-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1



KDE Security Advisory: kpdf temp file writing DoS vulnerability
Original Release Date: 2005-08-09
URL: http://www.kde.org/info/security/advisory-20050809-1.txt

0. References
        CAN-2005-2097


1. Systems affected:

        KDE 3.3.1 up to including KDE 3.4.1.


2. Overview:

        kpdf, the KDE pdf viewer, shares code with xpdf. xpdf contains
        a vulnerability that causes it to write a file in $TMPDIR with
        almost infinite size, which can severly impact system performance.


3. Impact:

        Remotely supplied pdf files can be used to fill up all available
        disk space when opened with kpdf.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.3.1 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        fc6fc7fa6886d6ff19037e7547846990  post-3.3.1-kdegraphics-4.diff

        Patch for KDE 3.4.1 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        fc6fc7fa6886d6ff19037e7547846990  post-3.4.1-kdegraphics-4.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFC+MoTvsXr+iuy1UoRAu6FAJ9onzGluXpLsI5xyqD9tm8ltMUtJgCgraKX
O5HGw1sUuRBBsSgzR7fxqds=
=xp98
-----END PGP SIGNATURE-----
