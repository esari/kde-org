-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: kcheckpass local root vulnerability
Original Release Date: 2005-09-05
URL: http://www.kde.org/info/security/advisory-20050905-1.txt

0. References

        CAN-2005-2494

1. Systems affected:

        All KDE releases starting from KDE 3.2.0 up to including
        KDE 3.4.2.


2. Overview:

        Ilja van Sprundel from suresec.org notified the KDE
        security team about a serious lock file handling error
        in kcheckpass that can, in some configurations, be used
        to gain root access.

        In order for an exploit to succeed, the directory /var/lock
        has to be writeable for a user that is allowed to invoke
        kcheckpass. This is the case on some Linux distributions.


3. Impact:

        A local user can escalate its privileges to the root user.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patch for KDE 3.4.2 is available from 
        ftp://ftp.kde.org/pub/kde/security_patches :

        2065be8baea09c89416385ac5dd892a9 post-3.4.2-kdebase-kcheckpass.diff



-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFDHJwhvsXr+iuy1UoRAhrmAKCgq+TD9I1lzE1H3vR1f/X2VUoSugCdH3wm
GWK+f6AGiG2Eh7rC5JC7fdA=
=SCi3
-----END PGP SIGNATURE-----
