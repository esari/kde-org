---
version: "5.64.0"
title: "KDE Frameworks 5.64.0 Source Info and Download"
type: info/frameworks
date: 2019-11-10
patch_level:
- 5.64.1
---
