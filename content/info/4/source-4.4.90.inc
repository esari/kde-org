<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeaccessibility-4.4.90.tar.bz2">kdeaccessibility-4.4.90</a></td><td align="right">5.2MB</td><td><tt>f489cda63d83ed23ad4cd68ed85104370875940f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeadmin-4.4.90.tar.bz2">kdeadmin-4.4.90</a></td><td align="right">1.4MB</td><td><tt>4fe6409ce26978cf9eb46b37acb60646194f1a73</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeartwork-4.4.90.tar.bz2">kdeartwork-4.4.90</a></td><td align="right">99MB</td><td><tt>6132ca305689be26310b90a439113a399231ec31</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdebase-4.4.90.tar.bz2">kdebase-4.4.90</a></td><td align="right">2.5MB</td><td><tt>275491ea710f011fd40c0f246dc54b1283b3ac0e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdebase-runtime-4.4.90.tar.bz2">kdebase-runtime-4.4.90</a></td><td align="right">5.5MB</td><td><tt>34cf63d9541bdeb8c64c08357ad9811ecc69fbf1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdebase-workspace-4.4.90.tar.bz2">kdebase-workspace-4.4.90</a></td><td align="right">63MB</td><td><tt>04aff37ae1be92c64e130e211dba84f8fa40eb95</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdebindings-4.4.90.tar.bz2">kdebindings-4.4.90</a></td><td align="right">6.1MB</td><td><tt>0cf8adcf881bbeecdeba37ac40527fd478a6debe</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeedu-4.4.90.tar.bz2">kdeedu-4.4.90</a></td><td align="right">62MB</td><td><tt>c1a9777e24fae64db2c373c0bd3e63d5fe0f0f7d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdegames-4.4.90.tar.bz2">kdegames-4.4.90</a></td><td align="right">56MB</td><td><tt>3c01220d3954b03cae7a7a3b443f28bcdf2bd788</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdegraphics-4.4.90.tar.bz2">kdegraphics-4.4.90</a></td><td align="right">3.7MB</td><td><tt>265d3dd4986aefd9554640522434e79b92d083d2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdelibs-4.4.90.tar.bz2">kdelibs-4.4.90</a></td><td align="right">14MB</td><td><tt>39b0f7e8fa27fa1727efd1ed9b859e32e194f562</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdemultimedia-4.4.90.tar.bz2">kdemultimedia-4.4.90</a></td><td align="right">1.6MB</td><td><tt>9622c0a7aeef17f411afb5b8470c165fb5350bab</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdenetwork-4.4.90.tar.bz2">kdenetwork-4.4.90</a></td><td align="right">7.8MB</td><td><tt>7e9e5c65ede992405af8c0e950211e66724759db</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdepimlibs-4.4.90.tar.bz2">kdepimlibs-4.4.90</a></td><td align="right">2.6MB</td><td><tt>20abb62a89e6861f4c888afbc60990fd4e960265</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeplasma-addons-4.4.90.tar.bz2">kdeplasma-addons-4.4.90</a></td><td align="right">1.7MB</td><td><tt>296c7054914af632ffabc3403a0a5ad4b1902d64</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdesdk-4.4.90.tar.bz2">kdesdk-4.4.90</a></td><td align="right">5.4MB</td><td><tt>60ecfeefb0c544e92de00fb9b2caf343d46bb32a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdetoys-4.4.90.tar.bz2">kdetoys-4.4.90</a></td><td align="right">396KB</td><td><tt>5a530a3499c7552abd761c8119f4737c3fd92e85</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdeutils-4.4.90.tar.bz2">kdeutils-4.4.90</a></td><td align="right">3.5MB</td><td><tt>de3c481cd1006e849740f18df3ad0de2a1b243e2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/kdewebdev-4.4.90.tar.bz2">kdewebdev-4.4.90</a></td><td align="right">2.1MB</td><td><tt>5cf081fe9aa706d31b263fbae44fba8b11c6de08</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.90/src/oxygen-icons-4.4.90.tar.bz2">oxygen-icons-4.4.90</a></td><td align="right">130MB</td><td><tt>8f70753f7b8bb1461067f656c275f2d566e0c95c</tt></td></tr>
</table>
