<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeaccessibility-4.6.0.tar.bz2">kdeaccessibility-4.6.0</a></td><td align="right">5,0MB</td><td><tt>d15e366b3ba9d6cd9be38bb7b5e8d1edae858a9d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeadmin-4.6.0.tar.bz2">kdeadmin-4.6.0</a></td><td align="right">776KB</td><td><tt>a52d35e8479744d19e04c46c1844ebd27bfb5fb0</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeartwork-4.6.0.tar.bz2">kdeartwork-4.6.0</a></td><td align="right">112MB</td><td><tt>9af6e510b851841981e8e822b6bf4d60b751d1c6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdebase-4.6.0.tar.bz2">kdebase-4.6.0</a></td><td align="right">2,6MB</td><td><tt>887ddea760aaabb277082f9e8c0b2f8905e318b8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdebase-runtime-4.6.0.tar.bz2">kdebase-runtime-4.6.0</a></td><td align="right">5,6MB</td><td><tt>024b7035a410f7c08e501ea1250553e746a469d2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdebase-workspace-4.6.0.tar.bz2">kdebase-workspace-4.6.0</a></td><td align="right">67MB</td><td><tt>c192f712d16617e625acc84fcd9e0546df887a70</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdebindings-4.6.0.tar.bz2">kdebindings-4.6.0</a></td><td align="right">6,8MB</td><td><tt>a2d641bbee7fddfbd76652fb41cd784d8c92288c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeedu-4.6.0.tar.bz2">kdeedu-4.6.0</a></td><td align="right">69MB</td><td><tt>790acbf29ad76d25c1156ce31c28b4fbd8baaa7e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdegames-4.6.0.tar.bz2">kdegames-4.6.0</a></td><td align="right">57MB</td><td><tt>19450f38c66674944966f221660a5aeda80a1eb1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdegraphics-4.6.0.tar.bz2">kdegraphics-4.6.0</a></td><td align="right">4,9MB</td><td><tt>bef8ebfa33f66405975e4e1b5c327fa144c3e40e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdelibs-4.6.0.tar.bz2">kdelibs-4.6.0</a></td><td align="right">13MB</td><td><tt>6ea3fc69f98fa91c5159ccd743d4d548e801c7bc</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdemultimedia-4.6.0.tar.bz2">kdemultimedia-4.6.0</a></td><td align="right">1,6MB</td><td><tt>704b5bc12c5d3c64809824b433dabeb101008825</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdenetwork-4.6.0.tar.bz2">kdenetwork-4.6.0</a></td><td align="right">8,3MB</td><td><tt>c69dcc48c8fc4064e183136b6d85e6379978925f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdepimlibs-4.6.0.tar.bz2">kdepimlibs-4.6.0</a></td><td align="right">3,1MB</td><td><tt>14624960410d270077df530d70d34e270260b3c4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeplasma-addons-4.6.0.tar.bz2">kdeplasma-addons-4.6.0</a></td><td align="right">1,9MB</td><td><tt>d266ca34c293743a64362b063ab58f671e7c1777</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdesdk-4.6.0.tar.bz2">kdesdk-4.6.0</a></td><td align="right">5,8MB</td><td><tt>af8d67b1823f72b759ac604a1be15d8ec91b2b2c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdetoys-4.6.0.tar.bz2">kdetoys-4.6.0</a></td><td align="right">396KB</td><td><tt>758f73eeb46f54ce21c6b50303b427fd2264e124</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdeutils-4.6.0.tar.bz2">kdeutils-4.6.0</a></td><td align="right">3,6MB</td><td><tt>d63e7abd1e91d4d33b7d03525c90abb3e8a56383</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/kdewebdev-4.6.0.tar.bz2">kdewebdev-4.6.0</a></td><td align="right">2,2MB</td><td><tt>cfcba6738e34d0860d9d8bc0dcd2fa8fdb0b1707</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.6.0/src/oxygen-icons-4.6.0.tar.bz2">oxygen-icons-4.6.0</a></td><td align="right">288MB</td><td><tt>a5c9cdfd2ead610b3b7d763006802ee90afdb580</tt></td></tr>
</table>
