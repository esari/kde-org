<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeaccessibility-4.1.0.tar.bz2">kdeaccessibility-4.1.0</a></td><td align="right">6,1MB</td><td><tt>7d53001b2db8d7d8bd82a63e8be882ae</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeadmin-4.1.0.tar.bz2">kdeadmin-4.1.0</a></td><td align="right">1,8MB</td><td><tt>c814d39956c605a8cc60016a26a9401f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeartwork-4.1.0.tar.bz2">kdeartwork-4.1.0</a></td><td align="right">41MB</td><td><tt>3de07b7d7bc5219d135c68dce4266861</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdebase-4.1.0.tar.bz2">kdebase-4.1.0</a></td><td align="right">4,3MB</td><td><tt>6b58b056d27e3103f087f12abe899a49</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdebase-runtime-4.1.0.tar.bz2">kdebase-runtime-4.1.0</a></td><td align="right">50MB</td><td><tt>a19999207ad37131a2888b6e32160008</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdebase-workspace-4.1.0.tar.bz2">kdebase-workspace-4.1.0</a></td><td align="right">46MB</td><td><tt>6d08adbb9944dea896ac65fd60d72377</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdebindings-4.1.0.tar.bz2">kdebindings-4.1.0</a></td><td align="right">4,5MB</td><td><tt>9c7ee50816ac6e0d5d2ea2f2968ac94d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeedu-4.1.0.tar.bz2">kdeedu-4.1.0</a></td><td align="right">56MB</td><td><tt>0debdf843969152cb14b7186919b8c2b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdegames-4.1.0.tar.bz2">kdegames-4.1.0</a></td><td align="right">31MB</td><td><tt>2c0a4c089bf31ff9bd3133c3f58c4dc7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdegraphics-4.1.0.tar.bz2">kdegraphics-4.1.0</a></td><td align="right">3,3MB</td><td><tt>2488d850b66eba1b542bded3def3e87e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdelibs-4.1.0.tar.bz2">kdelibs-4.1.0</a></td><td align="right">8,8MB</td><td><tt>86496aed25d4dce440418b3064a27913</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdemultimedia-4.1.0.tar.bz2">kdemultimedia-4.1.0</a></td><td align="right">1,4MB</td><td><tt>7965e42c3de193bde7f1e5437c9bedec</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdenetwork-4.1.0.tar.bz2">kdenetwork-4.1.0</a></td><td align="right">7,1MB</td><td><tt>bd0193f2ea4f2d055c7bae0233d5e10e</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdepim-4.1.0.tar.bz2">kdepim-4.1.0</a></td><td align="right">13MB</td><td><tt>5ccd9ca2bf92c0f94ac3b0bf5a5a1344</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdepimlibs-4.1.0.tar.bz2">kdepimlibs-4.1.0</a></td><td align="right">1,9MB</td><td><tt>b1eddf3b85a70a31f6a4385948b2a6cd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeplasma-addons-4.1.0.tar.bz2">kdeplasma-addons-4.1.0</a></td><td align="right">3,9MB</td><td><tt>c67db067de416209fd63ff0deea44510</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdesdk-4.1.0.tar.bz2">kdesdk-4.1.0</a></td><td align="right">4,7MB</td><td><tt>eb4e7bc753c80f617c113f31ab501168</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdetoys-4.1.0.tar.bz2">kdetoys-4.1.0</a></td><td align="right">1,3MB</td><td><tt>c8c9c2f66f65fc7acfa8060d08667406</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdeutils-4.1.0.tar.bz2">kdeutils-4.1.0</a></td><td align="right">2,2MB</td><td><tt>456d811618e5417e224476089df9a3b3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/stable/4.1.0/src/kdewebdev-4.1.0.tar.bz2">kdewebdev-4.1.0</a></td><td align="right">2,5MB</td><td><tt>58a1b35897cf0194476c9aac8a1d61e0</tt></td></tr>
</table>
