---
title: "KDE Applications 17.12.2 Info Page"
announcement: /announcements/announce-applications-17.12.2
layout: applications
signer: Albert Astals Cid
signing_fingerprint: 8692A42FB1A8B666C51053919D17D97FD8224750
---
