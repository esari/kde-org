---
qtversion: 5.15.2
date: 2022-03-12
layout: framework
libCount: 83
---

### Changes to multiple frameworks

* Remove broken Python bindings generation
* Make the BUILD_DESIGNERPLUGIN option dependent on not cross-compiling

### Breeze Icons

* Add app icon for TeXstudio (bug 391459)

### Extra CMake Modules

* [ECMQmlModule] Fix doc syntax
* ECMGeneratePriFile: support multiple include install dirs
* KDEInstallDirs6: replace ECMQueryQMake usage with qtpaths
* Adapt Android toolchain file and FindGradle to Qt6

### KActivitiesStats

* Move Boost dependency to BUILD_TESTING block

### KActivities

* Check executables exist in PATH before passing them to QProcess

### KAuth

* Normalize header names and include path layout to KF standards
* Prepare KF6 KAuthWidgets library, with an interface lib for KF5

### KBookmarks

* Check executables exist in PATH before passing them to QProcess

### KCalendarCore

* Compare timeSpecs when comparing incidences for equality
* Preserve timeSpec of all-day event's dtEnd

### KConfig

* Add KWindowStateSaver
* Remove warning from kauthorized.h
* KConfigCompiler: support ItemAccessors=true with signalling items
* QMake pri files: fix missing new path to version header
* Support build without Qt session manager
* Add KConfig GUI logging category

### KConfigWidgets

* KHamburgerMenu: Avoid null receiver warning upon showing menubar
* Add a KColorScheme::operator==
* KHambugerMenu: Fix crash on windows when showing the window menubar from the KHamburgerMenu

### KContacts

* Split internal and external API into different files
* Also install the address format API header
* Improve output for incomplete addresses and formats with 3+ fields per line
* Rewrite the address formatter

### KCoreAddons

* Deprecate KPluginMetaData::fromDesktopFile
* KPluginMetaDataTest: Explicitly call dedicated constructors
* KPluginMetaData: Emit runtime deprecation warnings for deprecated code path
* KPluginMetaData: Add note for deprecated code paths to constructor
* KPluginMetaData: Use QFileInfo::completeBaseName for deriving plugin id
* Downgrade mismatched host tool version to WARNING
* Add a way to indicate to not show a notification on finished
* Allow to create valid KPluginMetaData for plugins without embedded JSON metadata
* Add an OUTPUT_FILE argument to kcoreaddons_desktop_to_json()
* Add KSignalHandler

### KDAV

* Add iCloud info to README
* Replace absolute URIs with relative ones in calendar-multiget request

### KDeclarative

* Fix double deleting of NetworkAccessManagerFactory (bug 450507)
* Delete networkAccessManagerFactory when we delete the engine
* Improve Open[app]() functions (bug 443984)

### KDESU

* Drop obsolete KF5Service and add KF5Config dependency

### KDocTools

* [ca@valencia] Add Catalan (Valencian) Language

### KFileMetaData

* Port to KPluginMetaData::findPlugins for plugin querying
* enable mobipocket extractor by default

### KDE GUI Addons

* [KOverlayIconEngine] Implement ScaledPixmapHook for high-dpi support

### KHolidays #

* holiday_pl_pl - add Fat Thursday and State Holidays w/o day-off (bug 447201)

### KIconThemes

* Fix pixelated icons in item views (bug 450336)
* [KIconEngine] Create high-dpi pixmap in paint
* Read the reference icon already scaled

### KImageFormats

* jxl: encoding improvements
* avif: adjust dimension and memory limits

### KInit

* Check executables exist in PATH before passing them to QProcess

### KIO

* KFilePlacesView: Fix potential crash in `previousVisibleIndex` (bug 450966)
* KFilePlacesView: Fix crash when dragging over topmost section header (bug 450966)
* Add the keywords kdeapi and api for KDE
* Remove Qt4 runner
* Update KDE and Qt runners
* Update KDE API runner to current website
* Have a runner for Krita Manual
* Make KReddit more specific (KDE Reddit)
* Add Invent Merge Requests
* Remove icon in rust.desktop
* DesktopExecParser: parse environment variables too (bug 448859)
* Hide mount point labels if mount point can't be found
* [kemailclientlauncherjob] Fix emitting result
* Fix trash KCM not having any icon on wayland (bug 449859)
* Report error messages instead of error names
* QMake pri files: fix missing new path to version header
* [KFilePlacesView] Consider section header for drop highlight
* [KFileWidget] Hide KMessageWidget initially
* Fix potential crash in pastejob (bug 439948)
* Check executables exist in PATH before passing them to QProcess
* KBuildSycocaProgressDialog: drop kbuildsycoca4-related code
* Make KFilePlacesModel somewhat usable from QML
* Make KDirModel somewhat usable from QML
* Disable broken DBusActivation with urls arguments for now
* [KFilePlacesView] Mount place when dropping onto it (bug 206629)
* [KFilePlacesView] Unset active/selected state for drag pixmap
* Introduce API to unwatch directories
* OpenUrlJob: fallback to QDesktopServices::openUrl() on Windows and MacOS
* [KFileWidget] Display KFilePlacesView errors
* [KFileWidget] Use KMessageWidget for error reporting
* [KDirOperator] Consider IO finished in case of error
* [KFilePlacesModel] Check query and fragment for closest parent
* [KFilePlacesModel] Check for convertedUrl in closestItem
* Fix memory leak after SimpleJob::putOnHold()
* Fix memory leak when killing a job
* [KFilePlacesView] Add drag auto-activation
* [knewfilemenu] Bind stat job connection lifetime to dialog, not the whole menu (bug 433347)
* [KFilePlacesView] Workaround place being added despite urlsDropped
* [KFilePlacesView] Always set pen explicitly
* [KFilePlacesView] Allow for an empty "Places" section
* [KFilePlacesView] Use DisplayRole when editing a place
* [KPropertiesDialog] Avoid layout shift when interrupting size calculation

### Kirigami

* Dialog: Accommodate vertical scrollbars widths properly
* Dialog: Fix implicitWidth binding loop when absolute maximum size used
* consider visibility when calculating page content positions
* update commandlink url to a working one
* FormLayout: make section headings slightly smaller but bolder
* NavigationTabBar: check checkedButton state before updating currentIndex
* Drawer should not eat mouse events at edges (bug 438017)
* Add a CI job to build and test the static build
* Use extra spacing to distinguish narrow-mode form labels and UI controls (bug 450105)
* Revert "Bold labels for narrow FormLayout" (bug 450105)
* Units: add mediumSpacing property
* Ensure OverlayDrawer handles is keyboard navigable
* CheckableListItem: Reset list item's checked property when checking box (bug 449766)
* CheckableListItem: Only conditionally trigger action when checked
* PageRow: properly handle back button
* WheelHandler: Round contentX/contentY positions (bug 449884)
* Allow compiling against Qt configured with -no-opengl
* Fix install path of settings and swipe navigator components

### KJSEmbed

* Check executables exist in PATH before passing them to QProcess

### KNewStuff

* kmoretools: Fix OBS appstream id (bug 414533)
* Add conditional cache preference to http requests
* Engine: Ensure we are not using the wrong ProvidersUrl
* Add http2 to KNewStuff requests
* KNSCore::Engine: Expose our Attica::Provider

### KNotification

* Allow to build the Java parts with Gradle from both Qt5 and Qt6

### KPty

* Fix FindUTEMPTER when cross-compiling

### KRunner

* Bundle plasma-runner.desktop into a qrc, for CI and Windows

### KService

* Use :/kservicetypes5 instead of :/kf/kservicetypes5

### KTextEditor

* Remove the KateTextLine string() accessor
* TextLine: Remove unused operator[]
* textline: Remove empty destructor and inline ctors
* vi_input_mode_manager member might be invalid if previous test caused a call
* fix highlighting on next/previous match (n/N)
* fix highlighting when opening/closing search bar
* prevent normal search mode highlight when using hls mode
* disable hls mode when switching to normal mode
* handle empty match result
* ignore :noh[lsearch] when hlsearch is disabled
* remove highlights when switching away from vimode
* react to color scheme changes
* vimode: implement hlsearch (bug 449641 449643)
* Mark some internal classes as final
* consistent right aligned status bar content
* Fix default font weight never gets honored
* remove helper that removed style of font
* minimal fix to keep font styles
* Remove menu-indicator from buttons
* katebookmarks: always populate the menu with some actions
* remove modified icon from the statusbar
* view: fix buggy scrolling on macOS (bug 442060)
* katestatusbar: fix margin in status bar buttons on certain styles
* vimode: explicitly set KateViewConfig::WordCompletionRemoveTail during tests
* vimode: find+replace more occurrences of the Control/Meta modifiers
* vimode: move #ifdefs to a common header
* vimode: fix swapped modifiers and arrow keys in insert/replace modes on macOS
* more statusbar out of stack
* hide the dictionary button when there are no dictionaries
* statusbar: Dont create actions with no text
* Lazy load ModeMenuList
* Make the statusbar smaller
* Remove superfluous QTextStream::setCodec call
* Guard against null widgets (bug 450094)
* use the url of the latest used document for save (bug 448618)
* proper parent for hl menu submenus
* fix menu parents for wayland
* Remove some left over dead expand code
* Differentiate docs with identical filenames (bug 381532)
* Remove partial expand stuff
* Remove ExpandingDelegate, replace with simpler QStyledItemDelegate
* Remove more dead completion code
* Remove ExpandingTree, merge with ArgumentHintTree
* Make sure we always only reselect old item if its still the first
* Allow to configure whether the doc tip is always visible or on demand
* Make sure expansion stuff still works in argument hint tree
* Improve wayland support for doc tip
* Use QTextBrowser, we don't want editing in here
* Fix keyboard navigation
* Handle expanding widgets
* completion: show doc in a tooltip instead of as a tree node
* Don't copy QKeyEvents, use a simple data class instead

### KUnitConversion

* Add teaspoon (tsp) and tablespoon (tbsp) units (bug 450908)

### KWayland

* Check executables exist in PATH before passing them to QProcess

### KWidgetsAddons

* KCharSelect: connect to the appropriate QFontComboBox signal (bug 445477)
* Check executables exist in PATH before passing them to QProcess
* Make KSqueezedTextLabel more robust wrt QFontMetrics

### KWindowSystem

* Ensure that xdgActivationTokenArrived is always emitted asynchronously (bug 450342)

### KXMLGUI

* Support build without Qt session manager
* Add an action in help menu for command bar

### ModemManagerQt

* Normalise where headers are installed with the rest of KF

### Plasma Framework

* Remove or upgrade QQC1 imports from tests where possible
* show the header if both this and dialog background fallback
* Units: add mediumSpacing
* Remove the use of the QtQml namespace
* Qt6 porting

### Prison

* Fix include dir for .pri file
* Normalise where headers are installed with the rest of KF

### Purpose

* Remove call to ecm_find_qmlmodule(org.kde.kdeconnect)

### QQC2StyleBridge

* Thicken menus on transient touch as well as being in Tablet Mode
* PlasmaDesktopTheme: ensure we initialise the colours
* PlasmaDesktopTheme: Do not compute the colors for invisible items
* PlasmaDesktopTheme: Do not call syncColors after syncWindow
* Menu: use hmargin and vmargin from QStyle
* Support displaying icons in ItemDelegate (bug 425867)
* fix checkbox position for CheckDelegate
* Drop lib prefix when building for Windows
* KQuickStyleItem: Simplify the updatePolish
* Use correct DBus signal for listening for font changes
* Use raw pointer instead of QPointer to track watchers

### Solid

* Use enum in switch instead of raw int
* udisks backend: Add UD2_DBUS_PATH_BLOCKDEVICES and use more constants
* udev: Do not ignore joysticks
* Check executables exist in PATH before passing them to QProcess

### Sonnet

* settings: Emit `autodetectLanguageChanged`
* QMake pri files: fix missing new path to version header

### Syntax Highlighting

* Bash: fix parameter expansion replacement with path without extended glob
* Lua: fix function highlighting with local function (#17)
* Propagate Qt major version to external project build of the host tools
* Bash/Zsh: fix keyword ! in ! cmd

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
