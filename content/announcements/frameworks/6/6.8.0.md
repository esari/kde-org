---
qtversion: 6.5.0
date: 2024-11-08
layout: framework
libCount: 72
draft: false
---
{{< details title="Baloo" href="https://commits.kde.org/baloo" >}}
+ [excludeMimeTypes] Exclude model/obj and text/rust from content indexing. [Commit.](http://commits.kde.org/baloo/3e74d49259ffdd519d14dd56888bb1d1c6a1be94) See bug [#488533](https://bugs.kde.org/488533)
{{< /details >}}

{{< details title="Bluez Qt" href="https://commits.kde.org/bluez-qt" >}}
+ Simplify PendingCallPrivate. [Commit.](http://commits.kde.org/bluez-qt/7af3f10b510352b7981d391a60fea6dc508e98c6) 
{{< /details >}}

{{< details title="Breeze Icons" href="https://commits.kde.org/breeze-icons" >}}
+ Add mimetype icons for text/x-typst. [Commit.](http://commits.kde.org/breeze-icons/36d83f89326efffa147b3cd08a50d9a500307a16) 
+ Monochromize not-explicitly-colorized symbolic folder icons. [Commit.](http://commits.kde.org/breeze-icons/19cd41e649f664268f55874bafeffdabffcaad0d) Fixes bug [#494721](https://bugs.kde.org/494721)
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/breeze-icons/012ced8bdd715f4f1c3e8a3bcc21d368d60d6447) 
+ Unify common parts of index.theme for breeze and breeze-dark. [Commit.](http://commits.kde.org/breeze-icons/c39caf44b2e20a76b7d6f70735b203b17d1b85a0) 
+ Sync index.theme changes from breeze to breeze-dark. [Commit.](http://commits.kde.org/breeze-icons/f062ac004d406c6a927035d05364850c613107f1) Fixes bug [#494399](https://bugs.kde.org/494399)
+ Rename spinbox-* icons to value-*. [Commit.](http://commits.kde.org/breeze-icons/ff1ccc38f1b2d1708935566d3d5e55d30ce57b21) 
{{< /details >}}

{{< details title="Extra CMake Modules" href="https://commits.kde.org/extra-cmake-modules" >}}
+ FindKF6: Print custom message when required components are not found. [Commit.](http://commits.kde.org/extra-cmake-modules/02e39e9538d3699e470194c989f7047efe44d2cc) 
+ Add a directory check when appending a module dir to qmlimportscanner. [Commit.](http://commits.kde.org/extra-cmake-modules/6b36ce1d6f846c2ecd65ea2d7b78c2f97e8a868c) 
+ Add Python bindings. [Commit.](http://commits.kde.org/extra-cmake-modules/5117ebef040d04ccbc21e826214ceac173c6e820) 
+ Break enums onto multiple lines. [Commit.](http://commits.kde.org/extra-cmake-modules/f138af23cfee106002a880685cc7fe24affa5a2f) 
+ Set import paths for QML modules to all CMake search paths. [Commit.](http://commits.kde.org/extra-cmake-modules/fe0f606bf7f222e36f7560fd7a2c33ef993e23bb) 
+ Remove the old/unused SIP-based Python binding generation infrastructure. [Commit.](http://commits.kde.org/extra-cmake-modules/21158fbc37e9b723b47a0fa74e968d553d0d6aba) 
+ ECMGeneratePkgConfigFile: try to deduce additional include dirs. [Commit.](http://commits.kde.org/extra-cmake-modules/85aa50ba8193f986a8548ed144f81cc2b3f42c85) 
+ Fix custom definitions for generated pkgconfig files. [Commit.](http://commits.kde.org/extra-cmake-modules/3e7dd8a3248ee9b7d2776ba949a125e23b33035b) 
+ Fix QM loader unit tests with a static Qt. [Commit.](http://commits.kde.org/extra-cmake-modules/ebc7d1fd20db2f075f0dac16041f87103294b51a) 
+ Don't fall back to qmlplugin dump on static Qt builds. [Commit.](http://commits.kde.org/extra-cmake-modules/d60dcf29a8b8f450b739e0aeba95088adc1a204d) 
+ Retire Qt5 Android CI. [Commit.](http://commits.kde.org/extra-cmake-modules/b86907d5919985112abafea1aeda868fb9dbefd8) 
+ Automatically install dependent targets of QML modules in static builds. [Commit.](http://commits.kde.org/extra-cmake-modules/5abfa28d452aee5420ac09b0b30b2283329fd234) 
+ Allow to specify an export set for targets installed by finalize_qml_module. [Commit.](http://commits.kde.org/extra-cmake-modules/d76623182d940660fae9730e100d830f77d03535) 
+ Don't check websites in Appstream tests. [Commit.](http://commits.kde.org/extra-cmake-modules/03b3feb6c13677f482f5bbe7dc41abca0a2cd2e4) 
{{< /details >}}

{{< details title="KArchive" href="https://commits.kde.org/karchive" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/karchive/80ea38a000e1d2a3c3d4070e797c219427457dcf) 
+ [k7zip] Correctly handle pack sizes > INT_MAX. [Commit.](http://commits.kde.org/karchive/3d8f1bb256aef1a19ab8a4563ae42cbf77e76c84) 
{{< /details >}}

{{< details title="KBookmarks" href="https://commits.kde.org/kbookmarks" >}}
+ Port to KStandardActions. [Commit.](http://commits.kde.org/kbookmarks/b4a9b1d5aeb62a1d1a11198045e99f3b30e1f4ba) 
+ Add missing include. [Commit.](http://commits.kde.org/kbookmarks/e5ae47b797d2b68a088193708e9bad99a6479fb1) 
{{< /details >}}

{{< details title="KCalendarCore" href="https://commits.kde.org/kcalendarcore" >}}
+ Fix Duration's operator- accidentally adding instead of subtracting. [Commit.](http://commits.kde.org/kcalendarcore/9ea0a61825312282023408c4b5c5ad0084911dbf) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kcalendarcore/7fc4b5ad6e99f0ca4c2c83763923bbb6267eadd6) 
{{< /details >}}

{{< details title="KCMUtils" href="https://commits.kde.org/kcmutils" >}}
+ Fix compilation with Qt 6.9 (dev). [Commit.](http://commits.kde.org/kcmutils/38c18ec7ab48cf425e3fa0a247493030f6c28511) 
{{< /details >}}

{{< details title="KCodecs" href="https://commits.kde.org/kcodecs" >}}
+ Add test for passing unknown codec to codecForName. [Commit.](http://commits.kde.org/kcodecs/1e8fb7bbfb15c86c8f56a858f6c1fef11ba22c66) 
+ Fix buffer overflow in Codec::codecForName. [Commit.](http://commits.kde.org/kcodecs/21942a570f9c4b95d26866af5bcf7ea03a65f1ad) 
{{< /details >}}

{{< details title="KColorScheme" href="https://commits.kde.org/kcolorscheme" >}}
+ Reset palette to default-constructed one when scheme is unset. [Commit.](http://commits.kde.org/kcolorscheme/c3a353138868a60298c688276fc57eed221499d4) 
+ Don't call activateSchemeInternal in init unless really needed. [Commit.](http://commits.kde.org/kcolorscheme/30614b6afe3d955e6786419e70772f4d05e9dd8b) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kcolorscheme/e9156e1b96a53c4035c15330609ee5abd78e117a) 
{{< /details >}}

{{< details title="KCompletion" href="https://commits.kde.org/kcompletion" >}}
+ Add linux-qt6-static CI. [Commit.](http://commits.kde.org/kcompletion/0f19954f0d97dbd02083dfc9b06c4754559f91ff) 
{{< /details >}}

{{< details title="KConfig" href="https://commits.kde.org/kconfig" >}}
+ Kwindowconfig: If sizes are same as default, revert them to default when saving. [Commit.](http://commits.kde.org/kconfig/65db9ccc4bc8e3fbb10f1694391141287bd253ef) See bug [#494377](https://bugs.kde.org/494377)
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kconfig/d0b2ea613849715a88fc1437a52cf19a4a43d02c) 
+ Correctly install QML module in a static build. [Commit.](http://commits.kde.org/kconfig/3a2e23c1080d0edf81cada401420f00b00d5baf1) 
{{< /details >}}

{{< details title="KConfigWidgets" href="https://commits.kde.org/kconfigwidgets" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kconfigwidgets/a464f0d2ddd9924fa3b707bc33881ba100bf3a7c) 
{{< /details >}}

{{< details title="KContacts" href="https://commits.kde.org/kcontacts" >}}
+ Fix IM protocol resource data initialization in static builds. [Commit.](http://commits.kde.org/kcontacts/c17339b30c1e31899724174a8ed4369ff0c1307d) 
{{< /details >}}

{{< details title="KCoreAddons" href="https://commits.kde.org/kcoreaddons" >}}
+ Make KJob::elapsedTime const. [Commit.](http://commits.kde.org/kcoreaddons/4e5d852493246ab7a82ce1dc8dcb4847aab75592) 
+ Fix absolute path generation into (not installed) header. [Commit.](http://commits.kde.org/kcoreaddons/adfb509b88a2c085592a31d2d10b66b4dadcd1b8) 
+ KPluginMetaData: reduce string allocation. [Commit.](http://commits.kde.org/kcoreaddons/12010a23b15df1d1e8432133b818636eacd397dc) 
+ Update git blame ignore file. [Commit.](http://commits.kde.org/kcoreaddons/dd4aa2b3369fc366635303c17addfadf4c1c5336) 
+ Reformat code with clang-format. [Commit.](http://commits.kde.org/kcoreaddons/91e2403ac694152a35867bfa002f6e0c778d1b04) 
+ Kjob: add elapsedTime() returns the ms the job ran. [Commit.](http://commits.kde.org/kcoreaddons/e048b5e8237db23562ec38a86021fef51783bdd2) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kcoreaddons/2baf3e6504febba03de48e1c941641d985d7097a) 
+ Install QML module correctly when building statically. [Commit.](http://commits.kde.org/kcoreaddons/b0c4e3ba54ca43bea3a158482a3f5567e48df989) 
+ ExportUrlsToPortal: use QScopeGuard::dismiss for the success code path. [Commit.](http://commits.kde.org/kcoreaddons/ed6348c29b8c714b0ad36cb432f98e843966f322) 
{{< /details >}}

{{< details title="KDocTools" href="https://commits.kde.org/kdoctools" >}}
+ Upload new file sq.xml. [Commit.](http://commits.kde.org/kdoctools/775c17fb87a8e9446fe9d5ef3d7d294520130620) 
{{< /details >}}

{{< details title="KFileMetaData" href="https://commits.kde.org/kfilemetadata" >}}
+ UserMetadata: complete Windows implementation. [Commit.](http://commits.kde.org/kfilemetadata/879533be9e5956a94da578631a6c30cbd919dea8) 
{{< /details >}}

{{< details title="KGlobalAccel" href="https://commits.kde.org/kglobalaccel" >}}
+ Add WITH_X11 option to re-enable X11 code after runtime cleanup. [Commit.](http://commits.kde.org/kglobalaccel/2747cd4c451c8b8badf0a4b109cb98b8e71341ec) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kglobalaccel/b7e661b762a4c70a0b06242f20432263ffaf28b0) 
{{< /details >}}

{{< details title="KGuiAddons" href="https://commits.kde.org/kguiaddons" >}}
+ Add namespace for Android as required by newer gradle versions. [Commit.](http://commits.kde.org/kguiaddons/86737180d715a8d4630d31b9b0d1be242420591c) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kguiaddons/3527c3483eb0fe1b69a59386caeedc6c0d70d48d) 
+ Correctly install static QML modules. [Commit.](http://commits.kde.org/kguiaddons/5c1fcfab15e1c97f370be4c2d720640328fb4cd2) 
{{< /details >}}

{{< details title="KHolidays" href="https://commits.kde.org/kholidays" >}}
+ Fix misunderstanding of All Saints Day in Swedish calendar. [Commit.](http://commits.kde.org/kholidays/4de0a0ab99057dd282a263ed052069548893b4c1) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kholidays/faefc68d99ff43f4b5d7e3c982198f848cdffd02) 
{{< /details >}}

{{< details title="KI18n" href="https://commits.kde.org/ki18n" >}}
+ Allow explicit setting of Python3 fallback executible path. [Commit.](http://commits.kde.org/ki18n/deefa53bbc0739cbcbb04caa1e872b5972f1595e) 
+ Use raw pointer for some pimpl'd public classes. [Commit.](http://commits.kde.org/ki18n/56aed6eba154f5338c882fefee1213a9fc9fab5d) 
+ Add missing include. [Commit.](http://commits.kde.org/ki18n/e1804d3f9cc897df9dc7163987a1c65b926bfb80) 
+ Trigger binding reevaluation on language change. [Commit.](http://commits.kde.org/ki18n/bf0f4ab86e50fd22e14189f8feda057efb621182) 
+ Propagate QML dependency for a static build with KTranscript enabled. [Commit.](http://commits.kde.org/ki18n/5017ea3745e3544ee2e6b549733e07215a9597a9) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/ki18n/927dea27ccce96b08900d06f258a58010a806d8d) 
+ Reduce temporary allocations. [Commit.](http://commits.kde.org/ki18n/f35c6ccea18b0690c5c43ebe663dfb38e8b50a1f) 
+ Modernize member initialization. [Commit.](http://commits.kde.org/ki18n/1d1f4039237caaf359935a222e3563ec20414aa6) 
+ Fix container size type narrowing warnings. [Commit.](http://commits.kde.org/ki18n/f519f309dc3ee68fea1bbf6141b07c9321890b3f) 
+ Remove commented-out KLocale leftovers. [Commit.](http://commits.kde.org/ki18n/a4c092b0276794383d5d8eeebd96ab4e8fa20f38) 
+ Align argument names between definition and declaration. [Commit.](http://commits.kde.org/ki18n/13947f2e6930fd123ea49212be6b60086ebbceec) 
+ Re-evaluate the languages we translate to on QEvent::LanguageChange. [Commit.](http://commits.kde.org/ki18n/9c687f8afc7a29dbd40d9f48cc3a000d11236cd3) 
+ Cleanup KLocalizedContext d-ptr handling. [Commit.](http://commits.kde.org/ki18n/c0ceceff982550e3bb88d825ab6732989f41a99a) 
+ Special-case the language fallback for country-less English. [Commit.](http://commits.kde.org/ki18n/8129d21f90fd181c1d6b7acbb927ec294d7c980b) 
+ Use QStringView for locale splitting. [Commit.](http://commits.kde.org/ki18n/56c9bb09a534db768ec1fcec563ce9f689e223f8) 
{{< /details >}}

{{< details title="KIconThemes" href="https://commits.kde.org/kiconthemes" >}}
+ Port to KStandardActions. [Commit.](http://commits.kde.org/kiconthemes/346aa668c766e9219102a44f65e46aea9cf42b95) 
+ Postpone spawning KColorSchemeManager instance. [Commit.](http://commits.kde.org/kiconthemes/df1e75fc8a248040235ac1412e0bf6c55bff2a9f) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kiconthemes/80cf679c0b95e8f43415e1f15dcf2e367f4c6eb3) 
+ Init mimeType icons on demand. [Commit.](http://commits.kde.org/kiconthemes/a5a400cf8992e423f2bbc62d596a1a28cce78ffe) 
+ Set up KColorSchemeManager on Android as well. [Commit.](http://commits.kde.org/kiconthemes/9a7bd2a719e1be10697c247b6a875fea622a1184) 
+ Reduce temporary allocations. [Commit.](http://commits.kde.org/kiconthemes/f257acd63f4810d3e59ca06b8bfb80ce257e2141) 
{{< /details >}}

{{< details title="KImageformats" href="https://commits.kde.org/kimageformats" >}}
+ TGA: Fixed GrayA image loading error. [Commit.](http://commits.kde.org/kimageformats/0378bd67e12c9e7602c70f7d64a1216a76bd2e38) 
+ Exr: Fix read/write with openexr 3.3. [Commit.](http://commits.kde.org/kimageformats/7d696a81d2bda42e118bdd464757541a4464bd47) Fixes bug [#494571](https://bugs.kde.org/494571)
+ JXL improvements. [Commit.](http://commits.kde.org/kimageformats/b5d5abe0ea179c7877040d4def381ce0569ee514) 
+ JXR: Fixed image reading on sequential devices. [Commit.](http://commits.kde.org/kimageformats/ac1006cc669a5a6a871f13cbab50cd8d0ab6634c) 
+ Simplified read/verify header process. [Commit.](http://commits.kde.org/kimageformats/97120b2537a56f8856df7cdf2142f6c94b9fee29) 
{{< /details >}}

{{< details title="KIO" href="https://commits.kde.org/kio" >}}
+ Minor: use existing variables which contain these strings. [Commit.](http://commits.kde.org/kio/63aa5ca00b4b1c24324383ccfb11f86b789dd431) 
+ Fix crash from HTTPProtocol::del() which calls with inputData=nullptr. [Commit.](http://commits.kde.org/kio/379c59579bd178d97e235b90c0cb4e254d3eea5b) 
+ Port away from Qt::Core5Compat when using Qt 6.7 or newer. [Commit.](http://commits.kde.org/kio/4a9f2af601ddd7892ff1dfb12a13c2622cfe393c) 
+ Remove unused KConfigWidgets dependency. [Commit.](http://commits.kde.org/kio/6030bab89dd33d1b574f2429655f9d3e0e90ab03) 
+ Port to KStandardActions. [Commit.](http://commits.kde.org/kio/5c3cba4a500be44419ca34375da687de0909b130) 
+ Add missing KColorScheme link. [Commit.](http://commits.kde.org/kio/65f31c694a820292ef3c898baeb0ff449f920f37) 
+ Add missing include. [Commit.](http://commits.kde.org/kio/55069e210b787b21e9df708e24dfc7dec839c237) 
+ Include DBus error in log when communication with kpasswdserver fails. [Commit.](http://commits.kde.org/kio/d82755c3ed3dba6270785ae1b6776c03abc65eb1) 
+ Update git blame ignore file. [Commit.](http://commits.kde.org/kio/b4119b9f1586e3fe7524be78e3baa565c3173af1) 
+ Reformat code with clang-format. [Commit.](http://commits.kde.org/kio/7154aa45ea27959253d9dc5d9dbbea14bdaac015) 
+ Copyjob/transferjob: use KJob::startElapsedTimer. [Commit.](http://commits.kde.org/kio/7f7fd57d4309ce8f85d916bc4420bb5000ab4562) 
+ Http worker: handle dav[s] protocol. [Commit.](http://commits.kde.org/kio/6930d03c1ff141b17e6f09d88d6356a14d021ab4) Fixes bug [#365356](https://bugs.kde.org/365356)
+ [KFileFilterCombo] Fix setting 'All' filter as default. [Commit.](http://commits.kde.org/kio/e7f7d1b84dd335bcc8ef978550d07e7f45729cba) 
+ KNewFileMenu: Prevent using home directory as template directory. [Commit.](http://commits.kde.org/kio/c48a170cf87034ebf5aa3afce5982ce8b8de7a85) Fixes bug [#494679](https://bugs.kde.org/494679)
+ [KFileFilter] Ignore label when comparing filters. [Commit.](http://commits.kde.org/kio/a7c9d59f273fecd68892602f68c108a5966d98fc) 
+ [KFileFilter] Remove excess spaces in logging. [Commit.](http://commits.kde.org/kio/9e0da224eb30c903cf8b105de85d8a40c6a2678a) 
+ [KFileFilterCombo] More verbose logging when not finding a filter. [Commit.](http://commits.kde.org/kio/380b1cebb37094b52eba239cd802d1f54250f78e) 
+ [http] Inline handleRedirection into the metaDataChanged slot. [Commit.](http://commits.kde.org/kio/51c6003751eaa17f3d738a2ee05ed7a9406864a6) 
+ [webdav] Handle redirections which add trailing slashes. [Commit.](http://commits.kde.org/kio/b382e97754a9cdb7c235ca11ef3959b63607ff87) See bug [#484580](https://bugs.kde.org/484580)
+ Copyjob: prefer custom struct over std::pair. [Commit.](http://commits.kde.org/kio/e0ec5353a9cc4d6157ecde81bea9e17e3463a55f) 
+ PreviewJob: use standard thumbnailer caching is disabled. [Commit.](http://commits.kde.org/kio/d103f25d0dbafd3720c199cb9153ee05c01e2f2a) 
+ KDirListerTest: improve test stability. [Commit.](http://commits.kde.org/kio/e94d76bba34b5d64b7df458b814fe1d2ed2d6dbe) 
+ Tests: Make sure KIO::UDSEntryList can be compared. [Commit.](http://commits.kde.org/kio/f14906026a29528704ce8f7fca68de8e9eb2bee0) 
+ Expose UDSEntry equal operator to KIO namespace. [Commit.](http://commits.kde.org/kio/df1bd222d42718dcad3d765766087063514268ef) 
+ Core/copyjob: report speed when copying multiple files. [Commit.](http://commits.kde.org/kio/0d701bcf51bd603763304235b4a37d063decdb75) See bug [#391199](https://bugs.kde.org/391199)
+ KPreview: store standard thumbnails in /tmp subfolder. [Commit.](http://commits.kde.org/kio/f18e87891659ff9a75c0882c71711170552f211c) 
+ Preview: better clean after standard thumbnailer. [Commit.](http://commits.kde.org/kio/1ffb86c9a3f6ccec917efe8fff7b486ac25bc7e1) Fixes bug [#493274](https://bugs.kde.org/493274)
+ Openurljob.cpp: Avoid opening files in endless loop if mimetype is set to open with xdg-open. [Commit.](http://commits.kde.org/kio/4fbda144ab4b77a2d99eb0dca1ff3cf75957cf55) Fixes bug [#494335](https://bugs.kde.org/494335)
+ [KFilePlacesView] Improve automatic resize heuristic. [Commit.](http://commits.kde.org/kio/c70e10edfc6ac20fc5d7a092fd87894b09dd91ab) Fixes bug [#449544](https://bugs.kde.org/449544)
+ Workerinterface: remove unused #include. [Commit.](http://commits.kde.org/kio/7da57342291035c7d48ae7a14b3c6e824d615323) 
+ Add translation context to admin security warning. [Commit.](http://commits.kde.org/kio/4fba175fa83c6c021f679a77ac934f4521f41fe6) 
+ Kfileitem: linkDest prevent readlink error when file is not a symlink. [Commit.](http://commits.kde.org/kio/f35ce298174afee8413927a04631b035ea66030f) 
+ Check that admin worker was installed by root. [Commit.](http://commits.kde.org/kio/01fc16733f8a1ff0447c5aaa1e78f22faaceef1a) 
+ Clean up Properties dialog to follow HIG, improve UX, remove frames and fix padding regression. [Commit.](http://commits.kde.org/kio/b425985c9c5845d304a4461929656bbd937c3a01) Fixes bug [#484789](https://bugs.kde.org/484789)
+ TrashSizeCache: Use correct flags for QDirIterator. [Commit.](http://commits.kde.org/kio/94e6143e32e2f0c07a8cac8bdcb4bab220712def) See bug [#479283](https://bugs.kde.org/479283)
{{< /details >}}

{{< details title="Kirigami" href="https://commits.kde.org/kirigami" >}}
+ TitleSubtitle: Don't explicit set renderType. [Commit.](http://commits.kde.org/kirigami/0f2e8ad8558bc9427e123ca6bfe6864351aa870c) 
+ Upper mound for overlaysheet width. [Commit.](http://commits.kde.org/kirigami/da96a379e36ee24a67a7b0c531221a37b31311e3) 
+ SelectableLabel: fix a11y properties. [Commit.](http://commits.kde.org/kirigami/a0bee99bb05ebfb3d0ec9a365a55fe0fdc83b9ab) 
+ Fix Kirigami Application (Qt6) template. [Commit.](http://commits.kde.org/kirigami/a3393de5b1b60ff34e2d5eff19871265121f26f6) Fixes bug [#494478](https://bugs.kde.org/494478)
+ SelectableLabel: Use onPressedChanged. [Commit.](http://commits.kde.org/kirigami/94b0513ec9ebb32c0df6edbef53b19c8ef11064f) See bug [#481293](https://bugs.kde.org/481293)
+ Reformat code with clang-format. [Commit.](http://commits.kde.org/kirigami/cc9469a7adedce6011e1a843979fcd432b218d87) 
+ Icon: Always respect the animated property. [Commit.](http://commits.kde.org/kirigami/9926955b7061b911fce67fcdb1bcbd3aae690932) Fixes bug [#466357](https://bugs.kde.org/466357)
+ Adjust tst_qicon for desktop theme. [Commit.](http://commits.kde.org/kirigami/2aaba2bcf7ea02ed3825cd54f55439bb41217144) 
+ Fix loading desktop theme. [Commit.](http://commits.kde.org/kirigami/140dd808077dcf86ce3e54f3b574c2f0842e7c36) Fixes bug [#491294](https://bugs.kde.org/491294)
+ Fix presumable typos confusing background and foreground colors. [Commit.](http://commits.kde.org/kirigami/a1362165380b8e7892266f3cdb332191b3f3f24f) See bug [#491294](https://bugs.kde.org/491294)
+ Always print Theme file loading errors. [Commit.](http://commits.kde.org/kirigami/06f7b5b91102f310ef164569d6739c4333720055) See bug [#491294](https://bugs.kde.org/491294)
+ SelectableLabel: override default padding values more completely. [Commit.](http://commits.kde.org/kirigami/9738ae7ab7d823630f7ea475673f9c8cc8c65dce) Fixes bug [#495256](https://bugs.kde.org/495256)
+ Fix icon for positive state of InlineMessage. [Commit.](http://commits.kde.org/kirigami/74e4ab023b045050a0a8c2733bcba4bfb5360f14) 
+ SelectableLabel: fix binding loop warning on cursorShape. [Commit.](http://commits.kde.org/kirigami/ab9a305e41bbee2a95fdd5ba21836d85be2fa16c) 
+ ScrollablePage: Add properties to set if the scrollbars are interactive. [Commit.](http://commits.kde.org/kirigami/8e996d05b06fb39636b5726dd5ee82d725922652) 
+ SelectableLabel: use property alias instead of direct binding, expose more through aliases. [Commit.](http://commits.kde.org/kirigami/6042752ad600ba832b597b79309056895928bee7) 
+ Dialog: fix multiple binding loops (again). [Commit.](http://commits.kde.org/kirigami/07ef78b5aaed379e533c00a72e05b7830e825344) 
+ Make the close button actually close. [Commit.](http://commits.kde.org/kirigami/488ea3d3e1557c83e0a9d1df5451fb261791a7d2) 
+ Layout: Reverse the stacking order of items inserted into ToolBarLayout. [Commit.](http://commits.kde.org/kirigami/08d7cb277872c0a013a596b0189d4c7228c632fb) See bug [#490929](https://bugs.kde.org/490929)
+ Modify SelectableLabel to use TextEdit instead. [Commit.](http://commits.kde.org/kirigami/1c54d7d246dbb6c2bf083429f40aee07bcbdcf4a) See bug [#493581](https://bugs.kde.org/493581)
+ Cleanup and fix static QML module installation. [Commit.](http://commits.kde.org/kirigami/2e57ce0f1c578bba769ded74cccf5909477b9e20) 
+ Disable PageRow gesture on android. [Commit.](http://commits.kde.org/kirigami/1166d23d7b3f5063f1271e2ac7234a0b565073ff) 
+ Make OverlaySheet look exactly like Dialog. [Commit.](http://commits.kde.org/kirigami/f99c00fc421b44336b51063db50d15e403db4974) Fixes bug [#489357](https://bugs.kde.org/489357)
+ Top align icon in multiline InlineMessage. [Commit.](http://commits.kde.org/kirigami/e6836324e129318c3fac12a40cb4c1e8c60ef0c2) 
{{< /details >}}

{{< details title="KItemModels" href="https://commits.kde.org/kitemmodels" >}}
+ Install QML module correctly when building statically. [Commit.](http://commits.kde.org/kitemmodels/9efd9273888b55cf844e9a76ce38cdb72f87e6c4) 
+ Fix QML unit tests when building against a static Qt. [Commit.](http://commits.kde.org/kitemmodels/94d8663a535f39f22c59754dc8f739f7f57bd93a) 
+ Make unit tests independent of QtWidgets. [Commit.](http://commits.kde.org/kitemmodels/06acc272ecb489dbf468c20a6807a2c70cad33d0) 
+ Don't hardcode library type. [Commit.](http://commits.kde.org/kitemmodels/b3e44d10db27bc25c54bcf6d440111341ee1542b) 
+ Kbihash: adapt to source incompatible change in Qt. [Commit.](http://commits.kde.org/kitemmodels/706ebed8628f97819a025eed6d9892ea52219d83) 
{{< /details >}}

{{< details title="KJobWidgets" href="https://commits.kde.org/kjobwidgets" >}}
+ Hide arrowButton in KWidgetJobTracker on startup. [Commit.](http://commits.kde.org/kjobwidgets/2a37b22c2524d176d5269b872f9f8dcf3c8f5d06) 
+ Add dedicated WITH_X11 option to avoid automagic. [Commit.](http://commits.kde.org/kjobwidgets/7a297c5469247c8391673b1d12d1279ace26ae28) 
{{< /details >}}

{{< details title="KNewStuff" href="https://commits.kde.org/knewstuff" >}}
+ Make sure the action's dialog closes. [Commit.](http://commits.kde.org/knewstuff/d31ed6cccc30091754501476e18fa3b2efe62903) Fixes bug [#492998](https://bugs.kde.org/492998)
+ Put qnetworkreplys in a self-aborting unique_ptr. [Commit.](http://commits.kde.org/knewstuff/c7a457ea5ffe2c13b853a0230295ab36cefa609a) See bug [#492998](https://bugs.kde.org/492998)
+ Parent the xml loader's httpjob. [Commit.](http://commits.kde.org/knewstuff/93667cb0fe8cfca5b1b85e957d3cd5324ca48fc0) See bug [#492998](https://bugs.kde.org/492998)
+ Filecopyworker: try to gracefully quit the thread. then terminate it. [Commit.](http://commits.kde.org/knewstuff/271c92e978b1a9637d043faff2fd3b15121864b1) See bug [#492998](https://bugs.kde.org/492998)
{{< /details >}}

{{< details title="KNotifications" href="https://commits.kde.org/knotifications" >}}
+ Typo--. [Commit.](http://commits.kde.org/knotifications/4f73061109b8acc4a11866f18b9fa128a1cc5476) 
+ Add namespace for Android as required by newer gradle. [Commit.](http://commits.kde.org/knotifications/166d1aab0af09f08981deff77334edc25077ac9e) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/knotifications/f469d60e24f8b6d9eb300ee7cf017237710c3019) 
{{< /details >}}

{{< details title="KParts" href="https://commits.kde.org/kparts" >}}
+ Define undeprecated Capabilities key in JSON metadata, define JSON schema, remove obsolete key. [Commit.](http://commits.kde.org/kparts/0475e56ea95a40b04b88c52263896ded952a31fe) 
{{< /details >}}

{{< details title="KQuickCharts" href="https://commits.kde.org/kquickcharts" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kquickcharts/41a965c06a9333829fcae0b9ed0073fde8989055) 
+ Correctly install static QML modules. [Commit.](http://commits.kde.org/kquickcharts/baf8cf9cae5270e44943fcdfd9198b1e555eff50) 
{{< /details >}}

{{< details title="KStatusNotifieritem" href="https://commits.kde.org/kstatusnotifieritem" >}}
+ Fix absolute path generation in (not installed) header. [Commit.](http://commits.kde.org/kstatusnotifieritem/60e6b2a1b458cbea454b43372c83d5947410d8d1) 
+ Typo--. [Commit.](http://commits.kde.org/kstatusnotifieritem/d7e51ea073772080e94646f8bf3c29bfde1f8c73) 
{{< /details >}}

{{< details title="KTextEditor" href="https://commits.kde.org/ktexteditor" >}}
+ Vi mode: Don't infinite loop in searcher. [Commit.](http://commits.kde.org/ktexteditor/1017a93e178480e5b9c714eaa338c013211c2138) 
+ Remove unused var. [Commit.](http://commits.kde.org/ktexteditor/1c43c728f7fe596b7c7d07799a54053d2e51c9dc) 
+ Fix ignores. [Commit.](http://commits.kde.org/ktexteditor/a20a9ac8dc5f0fa1942897d973e811ff01e251e6) 
+ Less deprecated stuff used. [Commit.](http://commits.kde.org/ktexteditor/7175db68d021fc268b60e01408c5f35349d6c2ff) 
+ Don't temporarily clear document URL during openUrl(). [Commit.](http://commits.kde.org/ktexteditor/e1a5e8e5a0da983051867e81bb358f418b3c88e9) 
+ Only discard completion if the cursor was at the end of line. [Commit.](http://commits.kde.org/ktexteditor/42bc6f7d40f8848648893b0dbc24795a611ff797) 
+ Update git blame ignore file. [Commit.](http://commits.kde.org/ktexteditor/a4fd86e719fc04eb6049e4af47726895837dc44b) 
+ Reformat code with clang-format. [Commit.](http://commits.kde.org/ktexteditor/353beb5d2a42ae5d3584881ce7073fd1c21884d0) 
+ Fix implicit conversion of Qt::Key in Qt 6.9. [Commit.](http://commits.kde.org/ktexteditor/350a793b53e2db0d5ae8df0c75a4673f7b4ffa43) 
+ Try to avoid unwanted completions. [Commit.](http://commits.kde.org/ktexteditor/f61fb1ecf2e59c66a1173ab7f31049696514740b) 
+ Fix session restore of file type. [Commit.](http://commits.kde.org/ktexteditor/00c588e4671fb796aeac7b0bbfcf45507af62426) Fixes bug [#492201](https://bugs.kde.org/492201)
+ Make ViewPrivate::displayRangeChanged public. [Commit.](http://commits.kde.org/ktexteditor/848a468f14f5f747326623da60ba2b7dbef7c2db) 
+ Set DocumentPrivate::m_reloading to false only if loading. [Commit.](http://commits.kde.org/ktexteditor/df0cfcb161ac28c2b5f1f800c1788cdab3b6e8e6) 
+ Give a more proper name to the test. [Commit.](http://commits.kde.org/ktexteditor/73d0ef99b00317197905d26cb90990f94b722839) 
+ Fix multiblock range handling when unwrapping line. [Commit.](http://commits.kde.org/ktexteditor/2c7e0711efd65e68687d530240bb46a1cf8de122) Fixes bug [#494826](https://bugs.kde.org/494826)
+ Fix line removal not handled properly in KateTemplateHandler. [Commit.](http://commits.kde.org/ktexteditor/12bedd2d2b11cfc673d159c5c477208fc11fa071) Fixes bug [#434093](https://bugs.kde.org/434093)
+ Inline blocksize into buffer. [Commit.](http://commits.kde.org/ktexteditor/27a5ed8149fc1371e44d31db334e3408546d0e56) 
+ Improve MovingRangeTest::benchCheckValidity. [Commit.](http://commits.kde.org/ktexteditor/c17f2ac52e7f85ce47ddae25d7c0fa2ea135de31) 
+ Improve TextRange::checkValidity performance. [Commit.](http://commits.kde.org/ktexteditor/ecc8f8460e14c5ac260b28643238617bbab77718) 
+ Do all testing in clean temp dirs. [Commit.](http://commits.kde.org/ktexteditor/2bca7560cc1f53149386d789b4a0d879c19c1a56) 
+ Add a swap file test. [Commit.](http://commits.kde.org/ktexteditor/dc459a78f81364327ab56c596701ace958501335) 
+ Add benchmarks for moving stuff. [Commit.](http://commits.kde.org/ktexteditor/0368a9909f5367cd79e967989be46e34706ad971) 
+ Use std::vector for cursor storage. [Commit.](http://commits.kde.org/ktexteditor/98a243be2e96cd3689796ae3d8f228106ade5447) 
+ Allow disabling editorconfig. [Commit.](http://commits.kde.org/ktexteditor/7e97a15452714d6de96e4e95c5bd5356955aa281) Fixes bug [#471008](https://bugs.kde.org/471008)
{{< /details >}}

{{< details title="KTextTemplate" href="https://commits.kde.org/ktexttemplate" >}}
+ Import i18n scripts from grantlee. [Commit.](http://commits.kde.org/ktexttemplate/ff0fd8f45b5551c9171710e88754c62884a9842c) Fixes bug [#492237](https://bugs.kde.org/492237)
+ Fix "now" tag to allow single quoted strings. [Commit.](http://commits.kde.org/ktexttemplate/12a7ee9d0799f5c3afbbcc1d05def12aa4d0bab6) 
{{< /details >}}

{{< details title="KTextWidgets" href="https://commits.kde.org/ktextwidgets" >}}
+ Port to KStandardActions. [Commit.](http://commits.kde.org/ktextwidgets/d913f8f1392cb132a86ec9b52d89f968d7ab629d) 
+ Make implicit deps explicit. [Commit.](http://commits.kde.org/ktextwidgets/75d33d0e7a6eb647c549697ff08c17db63ff9d01) 
{{< /details >}}

{{< details title="KUnitConversion" href="https://commits.kde.org/kunitconversion" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kunitconversion/6dfd0de31674baa652378704a52704acc8fc726e) 
{{< /details >}}

{{< details title="KWallet" href="https://commits.kde.org/kwallet" >}}
+ Don't exclude deprecated functions from build. [Commit.](http://commits.kde.org/kwallet/38f293ce0b025ad30d7bc1fd5c0f8dd1d3b07c6c) Fixes bug [#493356](https://bugs.kde.org/493356)
{{< /details >}}

{{< details title="KWidgetsAddons" href="https://commits.kde.org/kwidgetsaddons" >}}
+ Fix time entry in locales with mixed-case AM/PM suffixes. [Commit.](http://commits.kde.org/kwidgetsaddons/790e953481555c9383d64a5440f7575933aeb23f) 
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/kwidgetsaddons/891765adda0142e2cd6040c7c8c6367c2c57c536) 
+ Don't use Oxygen style in KSeparator. [Commit.](http://commits.kde.org/kwidgetsaddons/7ae80b9249ff4b9f48d39c0e2ea658acfd4a3d23) 
+ KMessageWidget: Improve accessibility. [Commit.](http://commits.kde.org/kwidgetsaddons/27c5095ddf77ef74d998cfff8b8a5c7cc1d4a50e) 
{{< /details >}}

{{< details title="KXMLGUI" href="https://commits.kde.org/kxmlgui" >}}
+ Simplify code: use erase remove. [Commit.](http://commits.kde.org/kxmlgui/2ba5347531606e370066f0898c8226960e1058ea) 
+ Fix window position not being restored. [Commit.](http://commits.kde.org/kxmlgui/99cd03cec0bc2738bd176836cc0794f14a7d5a60) Fixes bug [#493401](https://bugs.kde.org/493401)
{{< /details >}}

{{< details title="Prison" href="https://commits.kde.org/prison" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/prison/4cbf60370560eff93b34d5497786cfb9e1946c9a) 
{{< /details >}}

{{< details title="QQC2 Desktop Style" href="https://commits.kde.org/qqc2-desktop-style" >}}
+ TextArea: Make placeholder wrap. [Commit.](http://commits.kde.org/qqc2-desktop-style/5537d04c6cb96e70df2ccd357ea84878135fc8b2) 
{{< /details >}}

{{< details title="Solid" href="https://commits.kde.org/solid" >}}
+ Restore MediaChanged handling for Audio CDs. [Commit.](http://commits.kde.org/solid/df5843ed76065f0e56d1189d010e10497c17f936) 
+ Support reproducible builds by omitting host paths in bison/yacc outputs. [Commit.](http://commits.kde.org/solid/f7a9accf646cc885b9f547ebbecad9772e8ef190) 
+ [udisks] Don't add/remove devices in slotMediaChanged. [Commit.](http://commits.kde.org/solid/99510948944ecda04f9cec6b3bd94b140d191a1c) See bug [#464149](https://bugs.kde.org/464149)
+ Port implicit QByteArray, QChar and QString conversions in iokit. [Commit.](http://commits.kde.org/solid/8cbd8ec73facb20f6692c24c79adb8cf6d01b4fa) 
+ Drop unfinished Power API. [Commit.](http://commits.kde.org/solid/1050d2be61e05848d340a04708fff35327ea2da9) 
+ Fstabwatcher: use libmount monitor on Linux. [Commit.](http://commits.kde.org/solid/9a437bb192580ad4a925f40a569c48f73acf2272) 
+ Fstabhandling: use libmount in Linux. [Commit.](http://commits.kde.org/solid/576f097c3d8dfe8eaf8d90b24198d2beed8dae49) 
{{< /details >}}

{{< details title="Sonnet" href="https://commits.kde.org/sonnet" >}}
+ Add linux-qt6-static CI. [Commit.](http://commits.kde.org/sonnet/6e31602058171f7d7a430c2b9053910f9082e251) 
+ Remove ASPELL runtime dependency from plugin building check. [Commit.](http://commits.kde.org/sonnet/7c31ad29b007c89598b19b34fda963eb8ed7d96d) 
+ Provide SONNET_NO_BACKENDS option to deactivate build failures with no backends. [Commit.](http://commits.kde.org/sonnet/1764decd7b7426320f24f3dbde44fc13993d1ed2) 
{{< /details >}}

{{< details title="Syndication" href="https://commits.kde.org/syndication" >}}
+ Add CI for static builds on Linux. [Commit.](http://commits.kde.org/syndication/0ae483cd0a39a5869c2d8e0e6e91f19158660f24) 
{{< /details >}}

{{< details title="Syntax Highlighting" href="https://commits.kde.org/syntax-highlighting" >}}
+ State: Fix inconsistent linkage warning on Windows. [Commit.](http://commits.kde.org/syntax-highlighting/81bb5f3280b8243c0fa6a8a465958ee37decfc0f) 
+ Better contrast for search highlighting. [Commit.](http://commits.kde.org/syntax-highlighting/fbc3eacc344b201cc584cd9b976163841ddf67a3) Fixes bug [#494599](https://bugs.kde.org/494599)
+ Odin: Add escape character to strings. [Commit.](http://commits.kde.org/syntax-highlighting/ca67790a83adc3687d84da94a5ac7c691bfba8d6) 
+ Update. [Commit.](http://commits.kde.org/syntax-highlighting/b03c9384f7ec466feea94450430d91880e70cd3f) 
+ Add comments. [Commit.](http://commits.kde.org/syntax-highlighting/311dfb065cb3ba6bd3e52ec0abad3164a53a06d9) 
+ Add TLA+ syntax highlighting. [Commit.](http://commits.kde.org/syntax-highlighting/3ef763640510dbecc2c31be7f8fb4f027499bfc3) 
+ Init. [Commit.](http://commits.kde.org/syntax-highlighting/729fda76daf7d3388ff917ed361fd551f0755825) 
+ Move try to cflow_begin. [Commit.](http://commits.kde.org/syntax-highlighting/f7b200e1b0bfb4560150043241f14f8007c65655) 
{{< /details >}}

