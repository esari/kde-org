---
qtversion: 6.5.0
date: 2024-10-11
layout: framework
libCount: 72
draft: false
---
{{< details title="Breeze Icons" href="https://commits.kde.org/breeze-icons" >}}
+ Delete 32px colorful folder action icon symlink. [Commit.](http://commits.kde.org/breeze-icons/ffa1c091bba0fb72afc107824970512a5c3bdfc0) Fixes bug [#478493](https://bugs.kde.org/478493)
+ Add new knotes-symbolic.svg icon (22/32/48). [Commit.](http://commits.kde.org/breeze-icons/df07ec7f1c27c1e4dac773a57c2736f230e2aca4) Fixes bug [#398901](https://bugs.kde.org/398901)
+ Format system-suspend-inhibited, system-suspend-uninhibited. [Commit.](http://commits.kde.org/breeze-icons/f164fb8d3ccc255131dd1e50d117799b182dbb76) 
+ Redesign system-suspend-inhibited, system-suspend-uninhibited. [Commit.](http://commits.kde.org/breeze-icons/fe056a5d3c9363199fd157baa39051f9b89c41e7) 
+ Fix recoloring in applications-multimedia-symbolic and applications-engineering-symbolic. [Commit.](http://commits.kde.org/breeze-icons/2824794758beb7da7a9bdb9b1a170b038ec13543) Fixes bug [#492879](https://bugs.kde.org/492879)
+ Add show-background icon. [Commit.](http://commits.kde.org/breeze-icons/34867b228eba605ce1b9706fb5973600426cb91d) See bug [#472863](https://bugs.kde.org/472863)
{{< /details >}}

{{< details title="Extra CMake Modules" href="https://commits.kde.org/extra-cmake-modules" >}}
+ Merge output targets from multiple qt6_target_qml_sources() calls. [Commit.](http://commits.kde.org/extra-cmake-modules/8f6acc46a49c626c5276b7d1965ffe3c407ae1ad) 
+ Don't install QML files on Android. [Commit.](http://commits.kde.org/extra-cmake-modules/b47332e6d1f4df06b62957fb017a0d58491d9a65) 
+ Propagate OUTPUT_TARGETS of qt6_target_qml_sources to the caller. [Commit.](http://commits.kde.org/extra-cmake-modules/6d6aa2a6cf0f78dae699d83c1227152e4d6482bf) 
+ Port Qt doc generation to qhelpgenerator. [Commit.](http://commits.kde.org/extra-cmake-modules/346594cb2cff0e084ce96dd7c079620834106994) 
+ Correctly forward the OUTPUT_TARGETS argument of qt6_add_qml_module(). [Commit.](http://commits.kde.org/extra-cmake-modules/2417c912a0d7b881bacc7f07c94fcac6e5159bbf) 
+ Set install destination for object files. [Commit.](http://commits.kde.org/extra-cmake-modules/7723bb971110ac6e341bb2750f58141925830e16) 
+ Upstream FindGLIB2.cmake changes from Qt. [Commit.](http://commits.kde.org/extra-cmake-modules/22ac0bbc791176bf39b1e4f120a62952854620fa) 
+ KDEClangFormat: Ignore source files being in _install folder. [Commit.](http://commits.kde.org/extra-cmake-modules/f76b04eccf545b43bae22367fe0be9751516f404) 
+ Reenable individual targets for clang-format to enable better status reporting an parallelization. [Commit.](http://commits.kde.org/extra-cmake-modules/fb1d3add90da1d048571ff3390ff960a1284793e) 
{{< /details >}}

{{< details title="KArchive" href="https://commits.kde.org/karchive" >}}
+ K7zip: prevent crash when archive has no modification times for files. [Commit.](http://commits.kde.org/karchive/0f3f45078c297f53cb36842fd63020b299ee1196) 
{{< /details >}}

{{< details title="KAuth" href="https://commits.kde.org/kauth" >}}
+ Silence false positive clazy checks. [Commit.](http://commits.kde.org/kauth/92271bbc8c36a6802e09c877646628f3880f8764) 
{{< /details >}}

{{< details title="KCMUtils" href="https://commits.kde.org/kcmutils" >}}
+ Drop obsolete includes and HAVE_X11. [Commit.](http://commits.kde.org/kcmutils/f651c8b0a962ab64958783a8a5b3c31fcb447fbd) 
+ Always show title and caption in tooltip if truncated. [Commit.](http://commits.kde.org/kcmutils/6a1143382dd65cecaeabba278d2455406c29173e) 
{{< /details >}}

{{< details title="KColorScheme" href="https://commits.kde.org/kcolorscheme" >}}
+ Follow system color scheme on Linux. [Commit.](http://commits.kde.org/kcolorscheme/69453f2835051b879da8b6e3e756ffec38cd7e19) 
+ Fix build against a static Qt. [Commit.](http://commits.kde.org/kcolorscheme/9d1588adacbdc05eacd524eeb4d1ad0535b39121) 
{{< /details >}}

{{< details title="KConfig" href="https://commits.kde.org/kconfig" >}}
+ Make unit tests work with a static Qt build. [Commit.](http://commits.kde.org/kconfig/76973da7a5e9d52ff8765190383cfb1d1064497c) 
{{< /details >}}

{{< details title="KConfigWidgets" href="https://commits.kde.org/kconfigwidgets" >}}
+ General/widgetStyle -> KDE/widgetStyle. [Commit.](http://commits.kde.org/kconfigwidgets/257c10d026dd5f3a90734b60281dfc2b7723c05c) 
{{< /details >}}

{{< details title="KCoreAddons" href="https://commits.kde.org/kcoreaddons" >}}
+ Make unit tests work with a static Qt build. [Commit.](http://commits.kde.org/kcoreaddons/cf88947aaa7de169a3e9909e33ad17f00d757b1f) 
+ Kcoreaddonsplugin: Link against Qt6::Network. [Commit.](http://commits.kde.org/kcoreaddons/eefe01f27ee0c1974fdfc3b4e9272a101036d4df) 
+ Restore KProcess on Android. [Commit.](http://commits.kde.org/kcoreaddons/9d4b796c0778d12b8ce4e3d0d0fe1abc9325f20d) 
+ Don't build kprocesstest for Android/iOS. [Commit.](http://commits.kde.org/kcoreaddons/be7572d0b70619d9292d1de5ef1e47c28c0de633) 
+ Fix build on ios. [Commit.](http://commits.kde.org/kcoreaddons/d79ec0c633f1a88618b909f4e5ae461beb779f28) 
{{< /details >}}

{{< details title="KDBusAddons" href="https://commits.kde.org/kdbusaddons" >}}
+ Kdbusservice: Don't unregister service name slightly before exit. [Commit.](http://commits.kde.org/kdbusaddons/7431ea1f8a5f08d412e447a3ddd3d668a7c1ca28) Fixes bug [#492300](https://bugs.kde.org/492300)
{{< /details >}}

{{< details title="KDeclarative" href="https://commits.kde.org/kdeclarative" >}}
+ Qpixmapitem: prevent a crash when there are no window yet. [Commit.](http://commits.kde.org/kdeclarative/0bac11e5cf99118019bb27615100f6fae6413ee7) 
{{< /details >}}

{{< details title="KDocTools" href="https://commits.kde.org/kdoctools" >}}
+ Provide option to enable relocatable docbook files on non WIN32 platforms. [Commit.](http://commits.kde.org/kdoctools/c17f3aa3f91a31cd1467bedde4cae5cc5b763d71) 
{{< /details >}}

{{< details title="KGuiAddons" href="https://commits.kde.org/kguiaddons" >}}
+ KCountryFlagEmoji: Improve fitting to the icon's bounding box. [Commit.](http://commits.kde.org/kguiaddons/898aa54d51142042e93e889a70cd50328696bc01) 
+ KCountryFlagEmoji: Fix emoji representation for non-country codes. [Commit.](http://commits.kde.org/kguiaddons/455dfe7bb38e4ca6edbaf2ef31bf1d7b75bf25c4) 
+ KCountryFlagEmoji: Add test and demo cases for non-coutry codes. [Commit.](http://commits.kde.org/kguiaddons/90d51b6cda03ae01e509b8f57e8f858580971d5e) 
+ Fix window insets foreground coloring on older Android versions. [Commit.](http://commits.kde.org/kguiaddons/1d4de186b1b39f96153feb976dd28b9ce77e0675) 
+ Fix build with Qt < 6.7. [Commit.](http://commits.kde.org/kguiaddons/c25496f122be110bd08d4ac55aa8d498b5b46bdb) 
+ Fix color luma computation for Android window insets. [Commit.](http://commits.kde.org/kguiaddons/7563c8e22a0f0abf98538bede9833c023f9306c9) 
+ WaylandClipboard: fix QMimeData::urls() not working. [Commit.](http://commits.kde.org/kguiaddons/b7eeb723dfff9d42b164dadbdcc3572f87f6934d) 
+ Update version for window insets API to match reality. [Commit.](http://commits.kde.org/kguiaddons/dacc0e3e1c150467204f4d44042fe37619b9b0e8) 
+ Add QML bindings for KWindowInsetsControllert. [Commit.](http://commits.kde.org/kguiaddons/8246a5415884400125a8b8991709611f8fe6e75d) 
+ Add KWindowInsetsController. [Commit.](http://commits.kde.org/kguiaddons/7d4a624b71ee752c2cad26cb83a1db3d7726a47f) 
+ WaylandClipboard: make sure format list doesn't have duplicate items. [Commit.](http://commits.kde.org/kguiaddons/fec5ea2866bbf322fb754e6b3298a71ee5e9273b) 
{{< /details >}}

{{< details title="KHolidays" href="https://commits.kde.org/kholidays" >}}
+ Holiday_si_sl: added missing Slovenian commemoration day. [Commit.](http://commits.kde.org/kholidays/8207aeaac25610eed8c36dc8805beaf196670ad5) 
{{< /details >}}

{{< details title="KI18n" href="https://commits.kde.org/ki18n" >}}
+ Make KTranscript work in static builds. [Commit.](http://commits.kde.org/ki18n/b884954ec46f0cbbc439c207a4900ecdac2b0c40) 
+ Make QML API unit test work with static Qt builds. [Commit.](http://commits.kde.org/ki18n/18c4bb778b68519a2c9b593b079da30f81fbb8c8) 
{{< /details >}}

{{< details title="KImageformats" href="https://commits.kde.org/kimageformats" >}}
+ Fix endianness bug in PCX reader on big endian architectures. [Commit.](http://commits.kde.org/kimageformats/ae641f7e948516bfbae04be4f156bc1618540389) 
+ Fixed read of BGR32 and RGB555 formats. [Commit.](http://commits.kde.org/kimageformats/46f7b90ce6cd5b3f03c965ce8a208a0aeefcdb08) 
+ FIxed comparison of unsigned expression. [Commit.](http://commits.kde.org/kimageformats/f7c8eaa140e1000f57a501d6dfd1d9d1f58a8e1c) 
+ Raw: Getting the image size does not need unpacking. [Commit.](http://commits.kde.org/kimageformats/36bfee8ae32e702e47b5f64dc52241bd62aafd65) 
{{< /details >}}

{{< details title="KIO" href="https://commits.kde.org/kio" >}}
+ [OpenFileManagerWindowJob] Fix crash when falling back to KRunStrategy. [Commit.](http://commits.kde.org/kio/d64f526593d395f3efb07c2949c65167ed9c35fd) Fixes bug [#486494](https://bugs.kde.org/486494)
+ Previewjob: Use .cache as temp folder, delete temp file after use. [Commit.](http://commits.kde.org/kio/9f3d680248ab863faa97decff289babd6b34252b) Fixes bug [#494096](https://bugs.kde.org/494096). See bug [#494061](https://bugs.kde.org/494061)
+ KFileItemActions: Try reading X-KDE-Show-In-Submenu as bool instead of string. [Commit.](http://commits.kde.org/kio/2684836232e21c4bcba44f94117fe6da1c76178f) 
+ KFilePlacesView: have setUrl() handle trailing slashes in place URLs. [Commit.](http://commits.kde.org/kio/9140e58ea4ec9034d75fc7a3572cf0789dd4bb32) 
+ KFilePlacesItem: Use Solid to find home mount point. [Commit.](http://commits.kde.org/kio/b5d45a857e6fdb3fb7b08e44753eef203ea605c6) 
+ Fileitem,file_unix: simplify types for stat. [Commit.](http://commits.kde.org/kio/427b7061b0b66b3fd15047f9cec6a049c0433466) 
+ Remove one level of three nesting in kdevtemplate. [Commit.](http://commits.kde.org/kio/8663ae1661bf51a94656054fce1777da099c1c49) 
+ Knewfilemenutest: cleanup. [Commit.](http://commits.kde.org/kio/228a82a89459203edb19aff6cd022af2f563d83a) 
+ Knewfilemenutest: test files and folders in ~/Templates. [Commit.](http://commits.kde.org/kio/b6df70a645b9022ec9b3c6578ae02c76dc702caf) 
+ ConnectionBackenp: fix passing errorString. [Commit.](http://commits.kde.org/kio/7e1b203b5ec847a906ac2a2c9bc2e673644322ce) 
+ Knewfilemenu: Make ~/Templates work by simply placing files and folders there. [Commit.](http://commits.kde.org/kio/a72c50527f6d38d49050b97e026e4c44bb550c9e) Fixes bug [#191632](https://bugs.kde.org/191632)
+ KFilePlacesItem: Cache groupType. [Commit.](http://commits.kde.org/kio/e395cbbb78bc4367acf9d58b75dfca3bd4dfbf0d) 
+ Previewjob: avoid calling mkdir for path with two slashs. [Commit.](http://commits.kde.org/kio/8b312a668ae6c622abd0dc2f1d3bef47c602d1f9) 
{{< /details >}}

{{< details title="Kirigami" href="https://commits.kde.org/kirigami" >}}
+ Add since info for new API. [Commit.](http://commits.kde.org/kirigami/e8f4fc0416f472f2c3cae0dfe90d5c3e27adb7ff) 
+ Re-enable cachegen on Android. [Commit.](http://commits.kde.org/kirigami/c8629adbe8d1397cbdfd023a234ce83c41b3159b) 
+ Create CMake config file only once all build parameters are known. [Commit.](http://commits.kde.org/kirigami/c1b721fc33f47dd00fc6e5223eb7bb86d83ceab7) 
+ Fix unit tests when using a static build. [Commit.](http://commits.kde.org/kirigami/d1072b0baf44be2d0e66d0b3138011a28e0dfa8c) 
+ PlatformTheme: fix crash when item is being destroyed. [Commit.](http://commits.kde.org/kirigami/e818435e6807745b71e5a2335b4645301ea7d471) 
+ OverlaySheet: make default title vertically center aligned. [Commit.](http://commits.kde.org/kirigami/3cd858b861fe6a5a98152f97adf2ab120e5ba3eb) Fixes bug [#489357](https://bugs.kde.org/489357)
+ Fix clang-format version imcompatibilities and avoid turning formatting of. [Commit.](http://commits.kde.org/kirigami/b0b8e3bc78f926eef3ac538c3c39dbd39917875e) 
+ Dialog: Take header width into account, small fix for footerToolbar width. [Commit.](http://commits.kde.org/kirigami/93458917c46935e39c775f57c910a6897009bed5) 
+ Workaround on incorrect palette update. [Commit.](http://commits.kde.org/kirigami/d3a32c9734c2c283952f398b418f45a9f955336d) Fixes bug [#493654](https://bugs.kde.org/493654)
+ Platform: Check if weak pointer is expired in PlatformThemeChangeTracker ctor. [Commit.](http://commits.kde.org/kirigami/3fc4542b237346cbc213bbd411be03224da408d5) Fixes bug [#493652](https://bugs.kde.org/493652)
+ Dialog: Use footer buttons width if its wider than content. [Commit.](http://commits.kde.org/kirigami/4fb4d656dfa5161ac4676d278bcd7cb08327a753) 
+ Fix sidebar text color in systemsettings. [Commit.](http://commits.kde.org/kirigami/f4fbe297b67fdd77675b4d0b66c855dd998c60f8) 
+ Use disabled text colors also for inherit-ing Theme instances. [Commit.](http://commits.kde.org/kirigami/bf3b2d41708e6331ed26aa280609b8f39f3f7772) Fixes bug [#433256](https://bugs.kde.org/433256)
+ Autotests/tst_theme: Add a test that verifies only one signal emission happens. [Commit.](http://commits.kde.org/kirigami/d721bbb9664ffc5438805f17d699576bc7db67e5) 
+ Autotests/tst_theme: Remove waiting for events. [Commit.](http://commits.kde.org/kirigami/5d62fd35d62b85d08f23276a9d6ffd09f03d1b23) 
+ Autotests/tst_theme: Explicitly mark root test objects as not inheriting. [Commit.](http://commits.kde.org/kirigami/a4e1ac31c2b6dc1e441fa53abb617c26b7986692) 
+ Add PlatformThemeChangeTracker to BasicTheme::sync(). [Commit.](http://commits.kde.org/kirigami/90f7cde0a922daf0a550ac27991cfd739db7e200) 
+ Platform: Replace PlatformTheme::queueChildUpdate with ChangeTracker::Data flag. [Commit.](http://commits.kde.org/kirigami/d6662b561e22110435669890840fceee7179b315) 
+ Platform: Don't use queued signals for batching change signals in PlatformTheme. [Commit.](http://commits.kde.org/kirigami/729362f5c5f4ba0ffc0b117a9909d10f395d6c8d) 
+ Fix NavigationTabBar sizing on mobile. [Commit.](http://commits.kde.org/kirigami/bdda04f3088baa33f2dbcd658982b381ebcc90f0) 
+ ContextualHelpButton: Clip to avoid text overflow. [Commit.](http://commits.kde.org/kirigami/63782366296b5f1d08f5dae9eb7812d27dc98311) 
+ ContextualHelpButton: Fix flickering when the popup covers the button. [Commit.](http://commits.kde.org/kirigami/7c011959dbe2b67de71b8f06a24b4b3a31fc312b) Fixes bug [#489688](https://bugs.kde.org/489688)
+ ColumnView currently allows having a QObject item inside its children list. [Commit.](http://commits.kde.org/kirigami/39641d463cd0c76a58b5bfd7fd633efff1bc8369) 
+ ToolBarLayout: Add support for actions that are separators. [Commit.](http://commits.kde.org/kirigami/2e53a1c5bea3a72cc492fecc20f7cc486fce2ea2) 
{{< /details >}}

{{< details title="KNewStuff" href="https://commits.kde.org/knewstuff" >}}
+ Don't emit twice slotEntryChanged. [Commit.](http://commits.kde.org/knewstuff/1210fa8c0cea78737467c40e8439b1d51f2778b5) See bug [#492557](https://bugs.kde.org/492557)
+ Qtquick: make sort/filter buttons like Discover/kcms. [Commit.](http://commits.kde.org/knewstuff/cb2eb3fdd38a9701061ab053e0990ae36a21a51a) 
{{< /details >}}

{{< details title="KNotifications" href="https://commits.kde.org/knotifications" >}}
+ Fix Android notification permission check. [Commit.](http://commits.kde.org/knotifications/92c7d31a04d987f49312e0f66b0fabc59e5dfc02) 
+ Ensure notification permission request callback is run on the right thread. [Commit.](http://commits.kde.org/knotifications/73ae34a61ac68c7dbc1d6f6e3c92d9f0ca017719) 
{{< /details >}}

{{< details title="KNotifyConfig" href="https://commits.kde.org/knotifyconfig" >}}
+ Port to Qt Multimedia. [Commit.](http://commits.kde.org/knotifyconfig/f410b4808fa34fecf75969dadd4cbc1bad7cf690) 
+ Knotifyeventlist: Set the config paths in the right order. [Commit.](http://commits.kde.org/knotifyconfig/f1e0cf88e7059d7b620c84c2ce24d9ccd750f46f) 
{{< /details >}}

{{< details title="KPackage" href="https://commits.kde.org/kpackage" >}}
+ Don't double emit emitResult(). [Commit.](http://commits.kde.org/kpackage/cdf14b11ef914e3304b0171b91a61c2388d0b7ee) Fixes bug [#492557](https://bugs.kde.org/492557)
{{< /details >}}

{{< details title="KRunner" href="https://commits.kde.org/krunner" >}}
+ Add querying property to RunnerManager. [Commit.](http://commits.kde.org/krunner/d2d238df62a813ae8975b3a24050d9dd3a759d96) 
+ Convert to newer REUSE.toml format. [Commit.](http://commits.kde.org/krunner/358853e9725d7f7159a84c50f182641c74c40855) 
{{< /details >}}

{{< details title="KStatusNotifieritem" href="https://commits.kde.org/kstatusnotifieritem" >}}
+ Document flatpak manifest requirements. [Commit.](http://commits.kde.org/kstatusnotifieritem/3e17e35d5e5b692aea278b8e0c0a99300039fd0c) 
{{< /details >}}

{{< details title="KSVG" href="https://commits.kde.org/ksvg" >}}
+ Fix typo in docs. [Commit.](http://commits.kde.org/ksvg/21d90279233a270135d6b3307969be618a3f76ca) 
{{< /details >}}

{{< details title="KTextEditor" href="https://commits.kde.org/ktexteditor" >}}
+ Port away from deprecated KPluralHandlingSpinBox. [Commit.](http://commits.kde.org/ktexteditor/14cc259405f6b3aba5c47fd4eb1cbe07920d627d) 
+ Fix unexpected space indentation in Go var group. [Commit.](http://commits.kde.org/ktexteditor/9113274ad58fb588d5b6e30d98446495fec591b2) Fixes bug [#487054](https://bugs.kde.org/487054)
+ Read dir kateconfig on view creation. [Commit.](http://commits.kde.org/ktexteditor/58a165c5e067b780a260bb0e10a0b50e2b7f3715) Fixes bug [#489600](https://bugs.kde.org/489600)
+ Fix pressing <ENTER> on } inserts two lines instead of one. [Commit.](http://commits.kde.org/ktexteditor/07b303eecb9fd6b1323f1d81ad843db0b9eb0581) Fixes bug [#479717](https://bugs.kde.org/479717)
+ Dont remove trailing spaces in markdown by default. [Commit.](http://commits.kde.org/ktexteditor/51b218106a097baf8d170d4fcee1720d1ee37f8c) Fixes bug [#451648](https://bugs.kde.org/451648)
+ Multicursors: Avoid indenting the sameline twice. [Commit.](http://commits.kde.org/ktexteditor/f6e5ebdb5cef5e143742277a9c56d70a28fc8370) 
+ Blockmode: repair indent when the cursor is in the first column. [Commit.](http://commits.kde.org/ktexteditor/28d1eec9cb387448b45534830242f8104246c773) 
+ Multicursor: Fix indent with multiple cursors. [Commit.](http://commits.kde.org/ktexteditor/aed2a1271bdd26caa8f1395197157b2eddad4674) 
+ With latest syntax definition, more tests pass for ruby. [Commit.](http://commits.kde.org/ktexteditor/63ee6c1d47be5dd406f6a40f4b9687a29d78f121) 
+ Use more views. [Commit.](http://commits.kde.org/ktexteditor/8d95e4300281470cab9e23a925874404644a63f6) 
+ Avoid double signal emission. [Commit.](http://commits.kde.org/ktexteditor/5cd23b8aaa59e7255c6d8cfe66e664e30331a461) 
+ Less deprecated calls, works locally. [Commit.](http://commits.kde.org/ktexteditor/f43cec80bc64e6e845b0e169b66675efcd02f9d8) 
+ Store multiline ranges spanning multiple blocks in TextBuffer. [Commit.](http://commits.kde.org/ktexteditor/a65e18369bc6043577131dd43d4b3092400d5d5e) 
+ Remove MovingRange caching in TextBlock. [Commit.](http://commits.kde.org/ktexteditor/f696c4cd929b5ab66f9a1a4fe1e589ea78201def) 
+ Add hint the file might got moved. [Commit.](http://commits.kde.org/ktexteditor/b913a3e01ed5fa33f05e869177da1476bac0b9b1) Fixes bug [#476071](https://bugs.kde.org/476071)
+ Dont create selection highlights with multiple selections. [Commit.](http://commits.kde.org/ktexteditor/1c0ad3cf524bc32ce88057e801eddda6fbd2c4b7) 
+ Optimize killLine for multiple cursors. [Commit.](http://commits.kde.org/ktexteditor/22c752ba993d8c194856d5adb43401754dc2304d) 
+ Completion: Allow async population of documentation. [Commit.](http://commits.kde.org/ktexteditor/a5be52450748f6d17f47b6f185c3049429dc91aa) 
+ More const to avoid wrong use of these members. [Commit.](http://commits.kde.org/ktexteditor/da3f3f78ecc679fb91327bc77657365533d63d44) 
+ Ensure modify the renderer that is used for printing. [Commit.](http://commits.kde.org/ktexteditor/786fc1c139e56d9e143b1a9a707e28527f1eafd1) Fixes bug [#465526](https://bugs.kde.org/465526). Fixes bug [#488605](https://bugs.kde.org/488605). Fixes bug [#487081](https://bugs.kde.org/487081). Fixes bug [#483550](https://bugs.kde.org/483550)
+ Fix text insertion with multiple cursors at same position. [Commit.](http://commits.kde.org/ktexteditor/49e3eb49ca632cecc099f434e96182375fdf0279) Fixes bug [#492869](https://bugs.kde.org/492869)
+ Add command names for "Remove Spaces" and "Keep Extra Spaces". [Commit.](http://commits.kde.org/ktexteditor/1c51727ad2f0b61e1f8a7f7ea9f9c20762a8a45e) 
+ Minimap now follows the theme also for search matches. [Commit.](http://commits.kde.org/ktexteditor/d700015aa80a4d45fe0c6e4097d4d3db4431f547) 
+ Run clang-format. [Commit.](http://commits.kde.org/ktexteditor/5757e5071e4adcce139df2eaf9de3b00537c6865) 
+ Fix merging of selections in opposite directions. [Commit.](http://commits.kde.org/ktexteditor/5be86bab29d56def43ed34e7a9ef3fa0ef72cab3) See bug [#492869](https://bugs.kde.org/492869)
+ Fix secondary cursor at boundary of selection doesn't get removed. [Commit.](http://commits.kde.org/ktexteditor/c5d89b91dc2da4ed0d9b8e7c27c84b748da5ab72) See bug [#492869](https://bugs.kde.org/492869)
+ Fix warnings. [Commit.](http://commits.kde.org/ktexteditor/5587406d091ab07921bdac183b665b0e177a1c74) 
{{< /details >}}

{{< details title="KTextWidgets" href="https://commits.kde.org/ktextwidgets" >}}
+ Use static regex for reusable objects. [Commit.](http://commits.kde.org/ktextwidgets/38c513bd21d1d845b3399674354193674a47c840) 
+ Remove unused Q_D macro. [Commit.](http://commits.kde.org/ktextwidgets/2c9a4103443bed4971a72de0dd7bf1ca56a1a3d8) 
{{< /details >}}

{{< details title="KUserFeedback" href="https://commits.kde.org/kuserfeedback" >}}
+ Build master ECM as part of the Flatpak build. [Commit.](http://commits.kde.org/kuserfeedback/194ca31ebfa6fef6f7420f1997dd17e2d13623a8) 
{{< /details >}}

{{< details title="KWidgetsAddons" href="https://commits.kde.org/kwidgetsaddons" >}}
+ Fix crash with older Qt. [Commit.](http://commits.kde.org/kwidgetsaddons/4e2f8b785bc751a45756a705ddbd5d3795698b89) Fixes bug [#493060](https://bugs.kde.org/493060)
+ Fix build against a static Qt. [Commit.](http://commits.kde.org/kwidgetsaddons/5be6ffee97f918f6de28a360a48e6f05d8d58228) 
+ KDateComboBox: emit dateEntered() on FocusOut. [Commit.](http://commits.kde.org/kwidgetsaddons/9cb528072a11b71f8d408390083bd60fe46771b8) 
{{< /details >}}

{{< details title="Network Manager Qt" href="https://commits.kde.org/networkmanager-qt" >}}
+ Correctly read manually-specified ipv6 addresses from Networkmanager. [Commit.](http://commits.kde.org/networkmanager-qt/7e04bde94e629652987fab8e51750b3b8e0b1f14) Fixes bug [#476008](https://bugs.kde.org/476008). Fixes bug [#453453](https://bugs.kde.org/453453)
+ Simplify loops and avoid creating iterator on temporary. [Commit.](http://commits.kde.org/networkmanager-qt/fc82dda7ad50ef341c9bb6c66007c760483d779a) 
+ Use static regex for reusable objects. [Commit.](http://commits.kde.org/networkmanager-qt/5241f7f88e19d71193737c7337463230a511c42e) 
{{< /details >}}

{{< details title="Purpose" href="https://commits.kde.org/purpose" >}}
+ [imgur] Restrict to actually supported MIME types. [Commit.](http://commits.kde.org/purpose/adf379d1e3774e62477cf925bd5b955193314900) 
+ Add extraJsonTranslationPaths.txt file for purpose specific translations. [Commit.](http://commits.kde.org/purpose/e4d51e55e25ff8fd890013586cba24f998ca18b2) 
+ Ensure KPlugin object contains no unstandardized keys. [Commit.](http://commits.kde.org/purpose/a0cb50e0deeb53a969cf58a5219d1b8f1bbefa26) 
{{< /details >}}

{{< details title="QQC2 Desktop Style" href="https://commits.kde.org/qqc2-desktop-style" >}}
+ Kquickstyleitem: Don't crash if colors changed and style option is null. [Commit.](http://commits.kde.org/qqc2-desktop-style/eee248c15a1dab73cf1ec9f141ec85a3012c8519) 
+ Kirigamiintegration: Track changes to PlatformTheme where needed. [Commit.](http://commits.kde.org/qqc2-desktop-style/d26d84928f1da2a276497cfb06241de08a2d89e6) 
+ TextFieldContextMenu: Open menu by keyPressed at TextField.cursorRectangle position. [Commit.](http://commits.kde.org/qqc2-desktop-style/f8e615fd49ba8c3c4dad8e4bd4f0aefce98a812e) 
{{< /details >}}

{{< details title="Solid" href="https://commits.kde.org/solid" >}}
+ Fstab: add missing signal override. [Commit.](http://commits.kde.org/solid/dde42d2336ba41eb6740518524b3e5a330833131) 
+ Fstab: Emit accessibilityChanged only when actually changed. [Commit.](http://commits.kde.org/solid/493e5e3b919d7e421e5355c43fd5dbdfcdbabaa8) 
+ Get rid of implicit QString and QChar conversions. [Commit.](http://commits.kde.org/solid/461d2a97de2ec381e03871a9ae0980116fd7ad5c) 
+ Get rid of implicit QByteArray to const char* conversions. [Commit.](http://commits.kde.org/solid/aec24d1a63cfccf9fa8643af44c82b591dc492f6) 
+ [Fstab] Minor cleanups (new style connect, extraneous include). [Commit.](http://commits.kde.org/solid/d09b2c7d00505090151580831e2d7a95ec664d65) 
+ [Fstab] Remove mntent wrapper macros. [Commit.](http://commits.kde.org/solid/82bb2094a5279e383d3c54ab01b53cff337daa4e) 
+ [Fstab] Remove remnants of Solaris support. [Commit.](http://commits.kde.org/solid/5ebd9dbe87f9e72783a7b7524e0e4d158e4766d2) 
{{< /details >}}

{{< details title="Sonnet" href="https://commits.kde.org/sonnet" >}}
+ Fail if none of the plugins can be build. [Commit.](http://commits.kde.org/sonnet/98b594f019a16ee658b7341163527e3b1ac83805) 
{{< /details >}}

{{< details title="Syndication" href="https://commits.kde.org/syndication" >}}
+ Search for private link dependencies in static builds. [Commit.](http://commits.kde.org/syndication/0dde27f98da4ada4957154d4510e2a555b245f79) 
{{< /details >}}

{{< details title="Syntax Highlighting" href="https://commits.kde.org/syntax-highlighting" >}}
+ Upload the uncompressed files. [Commit.](http://commits.kde.org/syntax-highlighting/a4ea5539216947f8cd75b11b504a54300328c1da) 
+ Odin: add missing items, fix attribute, add directive. [Commit.](http://commits.kde.org/syntax-highlighting/dbc8f2446462bf2ab00b8b0c51d07e6f759b8799) 
+ Swift: fix detection of end of protocol method declaration. [Commit.](http://commits.kde.org/syntax-highlighting/4e76ba20fcaf3c0e6c079853fd4eec1540bbb81a) Fixes bug [#493459](https://bugs.kde.org/493459)
+ Indexer: treats 1-character StringDetect as a DetectChar for unreachable rules and the merge suggestion. [Commit.](http://commits.kde.org/syntax-highlighting/514b783f7f41ede52a278da6dbfa6c4c253437a6) 
+ Indexer: check that WordDetect does not contain spaces at the beginning and end of text. [Commit.](http://commits.kde.org/syntax-highlighting/cad71562eed74d78ee5f9e8650b4ca1ac33c84f5) 
+ Simplify installed xml syntax files to speed up reading. [Commit.](http://commits.kde.org/syntax-highlighting/21e863bc752b8d20021351495f798401411ec578) 
+ Indexer: replace some QString with QStringView and QLatin1Char with char16_t literal. [Commit.](http://commits.kde.org/syntax-highlighting/a6bffd29b35b234c1cfe08423d30bda9d4ca7ec6) 
+ Indexer: fix default value for char with LineContinuation. [Commit.](http://commits.kde.org/syntax-highlighting/d8acfcaf68bcb07ff835a5db6dd89d4f04204e6d) 
+ Orgmode.xml: Fix orgmode syntax highlighting not ending properly. [Commit.](http://commits.kde.org/syntax-highlighting/1e0a81aef22592b822d896ecc777b5d36382a427) 
+ Jira, Markdown, Org Mode: use rhtml syntax with erb language. [Commit.](http://commits.kde.org/syntax-highlighting/beb532a2472e73d1ffa8be538fa40bc435759e7f) 
+ Haml: complete the syntax and fix the highlighting of Ruby line following the change in ruby.xml. [Commit.](http://commits.kde.org/syntax-highlighting/bf4da01867767d4dece8bbc58123979b9ee97d4c) 
+ Ruby: fix %W, dot member, some parenthesis ; add ?c, escape char, etc. [Commit.](http://commits.kde.org/syntax-highlighting/994c8cc92598a9dac20aa99a89a75b50e21ae3f0) Fixes bug [#488014](https://bugs.kde.org/488014)
+ Gleam: Minor modifications to syntax and example file. [Commit.](http://commits.kde.org/syntax-highlighting/2f2b3dadba0f6d975d73ec94e26ed9ece6d58e2c) 
+ Remove truncase from Common Lisp. [Commit.](http://commits.kde.org/syntax-highlighting/8355a2b9178004939547f9c1bbac7bb6d131f126) 
{{< /details >}}

