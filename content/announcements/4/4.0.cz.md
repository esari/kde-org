---
aliases:
- ../4.0
- ../4.0.cz
date: '2008-01-11'
layout: single
title: Vydáno KDE 4.0
---

<h3 align="center">
   Projekt KDE vydává čtvrtou hlavní verzi moderního svobodného pracovního prostředí
</h3>
<p align="justify">
  <strong>
    Uvedením této verze vstupujeme do éry KDE 4.
  </strong>
</p>

<p>
Komunita KDE nadšeně oznamuje okamžitou dostupnost
<a href="/announcements/4.0/">KDE 4.0.0</a>. Toto významné
vydání představuje jak konec dlouhého a intenzivního vývojového cyklu
započatého pro KDE 4.0, tak začátek éry KDE 4.
</p>

<div class="text-center">
<a href="/announcements/4/4.0/desktop.png">
<img src="/announcements/4/4.0/desktop_thumb.png" class="img-fluid">
</a> <br/>
<em>Pracovní prostředí KDE 4.0</em>
</div>
<br/>

<p>
<strong>Knihovny</strong> KDE 4 zaznamenaly významná vylepšení téměř ve všech oblastech.
Multimediální framework Phonon poskytuje všem aplikacím KDE podporu multimédií nezávislou 
na platformě, platforma Solid pro integraci hardwaru usnadňuje interakci
s&nbsp;(výměnnými) zařízeními a poskytuje nástroje pro lepší správu napájení.
<p />
<strong>Pracovní prostředí</strong> KDE 4 získalo nové možnosti. Plasma
nabízí nové rozhraní plochy včetně panelu, nabídky, widgetů na ploše
a funkce dashboardu. KWin, správce oken KDE, nyní podporuje pokročilé
grafické efekty, které zjednoduší práci s&nbsp;okny.
<p />
Vylepšily se také mnohé <strong>aplikace</strong>. Ať už jde o vyladění vzhledu
díky vektorovým grafickým prvkům, změny v základních knihovnách, zdokonalení
uživatelského rozhraní, nové vlastnosti či dokonce i nové aplikace - cokoliv vás napadne,
KDE 4.0 to obsahuje. Nový prohlížeč dokumentů Okular a nový správce souborů Dolphin jsou jedny
z&nbsp;mnoha aplikací využívajících nové technologie KDE 4.0.
<p />
<img src="/announcements/4/4.0/images/oxybann.png" align="right" hspace="10"/>
Tým pro <strong>vzhled</strong> provzdušnil pracovní prostředí.
Novou tvář získaly téměř všechny jeho viditelné součásti i aplikace.
Krása a soudržnost jsou dva základní rysy Oxygenu.
</p>

<h3>Pracovní prostředí</h3>
<ul>
	<li>Plasma tvoří nové rozhraní pracovního prostředí. Zajišťuje interakci mezi panelem a nabídkou
	  a poskytuje též další intuitivní prostředky pro práci s&nbsp;plochou a aplikacemi.
	</li>
	<li>KWin, osvědčený správce oken KDE, nyní podporuje pokročilou kompozici.
	  Hardwarově akcelerované zobrazování zajišťuje plynulejší a intuitivnější práci
	  s&nbsp;okny.
	</li>
	<li>Oxygen přináší do KDE 4 nové grafické prvky. Poskytne vám krásné a soudržné
	    grafické pojetí, které je příjemné na pohled.
	</li>
</ul>
Další informace o novém rozhraní pracovního prostředí KDE jsou dostupné v&nbsp;<a href="./guide">průvodci KDE 4.0</a>.

<h3>Aplikace</h3>
<ul>
	<li>Konqueror je osvědčený webový prohlížeč v KDE. Konqueror je nenáročný, dobře začleněný a
	    podporuje nejnovější standardy jako CSS 3.</li>
	<li>Dolphin je nový správce souborů v KDE, při jehož vývoji byl kladen důraz na snadnost použití.
		Jedná se o uživatelsky přívětivý a výkonný nástroj.
	</li>
	<li>Nastavení systému obsahuje nové ovládací rozhraní. Monitor systému
	    KSysGuard zjednodušuje sledování a kontrolu zdrojů a aktivity systému.
	</li>
	<li>Okular, prohlížeč dokumentů v KDE 4, podporuje mnoho formátů.
	    Okular je jednou z mnoha aplikací KDE 4 vylepšených ve spolupráci s&nbsp;projektem
	    <a href="http://openusability.org">OpenUsability</a>.
	</li>
	<li>Vzdělávací aplikace byly mezi prvními, které se dočkaly portace na
	    technologii KDE 4. Kalzium, grafická periodická soustava prvků,
	    a Marble Desktop Globe jsou dva z mnoha klenotů mezi vzdělávacími aplikacemi.
	    O vzdělávacích aplikacích se můžete dočíst více v&nbsp;<a href="./education">průvodci</a>
	</li>
	<li>V KDE bylo též aktualizováno mnoho her. Novou tvář získaly hry jako je KMines, hledání min,
	    a KPat, hra trpělivosti. Díky novým vektorovým technologiím a grafickým 
	    možnostem jsou hry méně závislé na rozlišení.
	</li>
</ul>
Některé aplikace jsou podrobně představeny v <a href="./applications">průvodci KDE 4.0</a>.

<div class="text-center">
<a href="/announcements/4/4.0/dolphin-systemsettings-kickoff.png">
<img src="/announcements/4/4.0/dolphin-systemsettings-kickoff_thumb.png" class="img-fluid">
</a> <br/>
<em>Správce souborů, Nastavení systému a Menu v akci</em>
</div>
<br/>

<h3>Knihovny</h3>
<p>
<ul>
	<li>Phonon nabízí aplikacím multimediální možnosti jako přehrávání audia a videa.
	    Phonon využívá různé backendy, které lze za běhu přepínat. 
		Standardním backendem pro KDE 4.0 bude Xine, dodávající vynikající podporu
		pro mnoho různých formátů. Phonon také uživateli umožňuje vybrat výstupní zařízení
		automaticky podle typu přehrávaného multimédia.
	</li>
	<li>Solid, rozhraní pro hardware, integruje pevná a výměnná zařízení 
	    do aplikací KDE. Solid také spolupracuje s možnostmi systému pro správu
	    napájení, stará se o správu síťových připojení a zařízení Bluetooth. Solid spojuje
	    sílu HALu, programu NetworkManager a Bluetooth implementace Bluez, avšak umožňuje tyto komponenty 
        nahradit bez nežádoucího narušení aplikací, vše v&nbsp;zájmu maximální možné přenositelnosti.
	</li>
	<li>KHTML je jádro pro zobrazení webových stránek používané v Konqueroru, webovém prohlížeči KDE. KHTML
	    je nenáročné a podporuje moderní standardy jako CSS 3. KHTML bylo také první jádro,
	    které prošlo slavným testem Acid 2. 
	</li>
	<li>Knihovna ThreadWeaver z&nbsp;kdelibs poskytuje rozhraní
	    pro lepší využití současných vícejádrových systémů, což přispívá k&nbsp;plynulosti aplikací KDE
	    a jejich lepšímu využívání zdrojů dostupných v&nbsp;systému.
	</li>
    <li>Díky přechodu na knihovnu Qt verze 4 se nové KDE pyšní menší paměťovou náročností a pokročilými grafickými schopnostmi.
        Knihovna kdelibs poskytuje užitečná rozšíření knihovny Qt, vylepšuje funkčnost a usnadňuje práci vývojářům.
	</li>
</ul>
</p>
<p>Stránky <a href="http://techbase.kde.org">TechBase</a> obsahují více informací o knihovnách KDE.</p>

<h4>Nechte se provést…</h4>
<p>
<a href="./guide">Průvodce KDE 4.0</a> obsahuje zběžný přehled různých nových
a vylepšených technologií KDE 4.0. Mnoho snímků obrazovky vás provede různými částmi
KDE 4.0 a ukáže některé zajímavé technologie a vylepšení pro uživatele. Seznamte se s
novými vlastnostmi <a href="./desktop">pracovního prostředí</a> a <a href="./applications">aplikacemi</a> jako je Nastavení systému, prohlížeč dokumentů Okular a správce souborů Dolphin. Představeny jsou také <a href="./education">vzdělávací aplikace</a> a <a href="./games">hry</a>.
</p>

<h4>Vyzkoušejte si…</h4>
<p>
Několik distribucí nám oznámilo, že svým uživatelům poskytnou balíčky krátce po vydání KDE 4.
Úplný a aktuální seznam lze najít
na <a href="/info/4/4.0">informační stránce KDE 4.0</a>, kde také naleznete odkazy na zdrojový kód, informace o kompilaci, bezpečnosti a dalších záležitostech.
</p>
<p>
Následující distribuce nás upozornily na dostupnost balíčků pro KDE 4.0 nebo spustitelných Live CD:

<ul>
       <li>
       An alpha version of KDE4-based <strong>Arklinux 2008.1</strong> is expected
       shortly after this release, with an expected final release within 3 or 4 weeks.
    </li>
       <li>
       <strong>Debian</strong> KDE 4.0 packages are available in the experimental branch.
       The KDE Development Platform will even make it into <em>Lenny</em>. Watch for
       announcements by the <a href="http://pkg-kde.alioth.debian.org/">Debian KDE Team</a>.
       Rumours are that a Live CD is planned as well.
       </li>
       <li>
       <strong>Fedora</strong> will feature KDE 4.0 in Fedora 9, to be <a
       href="http://fedoraproject.org/wiki/Releases/9">released</a>
       in April, with Alpha releases being available from
       24th of January.  KDE 4.0 packages are in the pre-alpha <a
       href="http://fedoraproject.org/wiki/Releases/Rawhide">Rawhide</a> repository.
       </li>
       <li>
       <strong>Gentoo Linux</strong> provides KDE 4.0 builds on
       <a href="http://kde.gentoo.org">http://kde.gentoo.org</a>.
       </li>
       <li>
		<strong>Kubuntu</strong> packages are included in the upcoming "Hardy Heron"
		(8.04) and also made available as updates for the stable "Gutsy Gibbon" (7.10).
		A Live CD is available for trying out KDE 4.0.
		More details can be found in  the <a href="http://kubuntu.org/announcements/kde-4.0">
		announcement on Kubuntu.org</a>.
        </li>
        <li>
			<strong>Mandriva</strong> will provide packages for 
			<a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.0/Mandriva/">2008.0</a> and aims
			at producing a Live CD with the latest snapshot of 2008.1.
        </li>
        <li>
			<strong>openSUSE</strong> packages <a href="http://en.opensuse.org/KDE/KDE4">are available</a> 
			for openSUSE 10.3 
			(<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_10.3/KDE4-BASIS.ymp">one-click 
			install</a>), openSUSE Factory 
			(<a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Desktop/openSUSE_Factory/KDE4-BASIS.ymp">one-click 
			install</a>) and openSUSE 10.2. A <a href="http://home.kde.org/~binner/kde-four-live/">KDE 
			Four Live CD</a> with these packages is also available. KDE 4.0 will be part of the upcoming 
			openSUSE 11.0 release.
        </li>
</ul>

</p>

<h2>O KDE 4</h2>
<p>
KDE 4.0 je novátorské pracovní prostředí obsahující mnoho aplikací
pro každodenní použití i konkrétní účely vydávané pod svobodnou licencí. Plasma je nové rozhraní pracovního prostředí vyvinuté pro
KDE 4. Poskytuje intuitivní rozhraní pro práci s desktopem a aplikacemi. Webový
prohlížeč Konqueror integruje web do pracovního prostředí. Výbavu doplňuje správce
souborů Dolphin, prohlížeč dokumentů Okular a ovládací centrum Nastavení systému.
<br />
KDE je založeno na knihovnách KDE, které poskytují jednoduchý přístup ke zdrojům na síti
prostřednictvím platformy KIO a pokročilé možnosti vzhledu díky použití knihovny Qt4 firmy Trolltech. Phonon a Solid, které jsou také součástí
knihoven KDE, přidávají multimediální rozhraní a lepší integraci hardwaru do všech aplikací KDE.
</p>
