---
title: KDE Gear 24.05.0
layout: gear-24-05
noContainer: true
date: 2024-05-23
scssFiles:
- /scss/gear/24.05.scss
hero_image: hero.png
images:
  - /announcements/gear/23.08.0/Hero.webp

intro: |
  It seems like yesterday when we released the Megarelease with apps included
  in Gear 24.02. But, if you check your (Merkuro) calendars, you'll notice that
  KDE developers have already put in three more months of intense work to bring
  you a whole new set of improved versions of your favorite KDE applications.

  Read on to discover what's new in the Dolphin file manager,
  Kdenlive video editor, Itinerary travel assistant, Elisa music player,
  Kate text editor, and many more programs and utilities.

new_apps:
  title: New Arrivals
  description: |
    Gear is growing! *Audex*, *Accessibility Inspector*, *Francis*, *Kalm* and
    *Skladnik* are all joining the Gear releases, ensuring they will get a
    sustained stream of updates from now on.
  apps:
    - name: Audex
      summary: |
        [Audex](https://apps.kde.org/audex/) is a CD ripper application
        that lets you copy your music CDs to your hard disk.
      img: audex.png
      logo: https://apps.kde.org/app-icons/org.kde.audex.svg
      appid: audex
    - name: Accessibility Inspector
      summary: |
        [Accessibility Inspector](https://apps.kde.org/accessibilityinspector/)
        helps you check how accessible your application is.
      img: accessibilityinspector.png
      logo: https://apps.kde.org/app-icons/org.kde.accessibilityinspector.svg
      appid: accessibilityinspector
    - name: Francis
      summary: |
        [Francis](https://apps.kde.org/francis/) helps your productivity
        by reminding you to take short breaks between focus phases using the
        pomodoro technique.
      img: https://cdn.kde.org/screenshots/francis/main.png
      logo: https://apps.kde.org/app-icons/org.kde.francis.svg
      appid: francis
    - name: Kalm
      summary: |
        Boost your wellness with [Kalm](https://apps.kde.org/kalm/) as it
        guides you through various breathing techniques.
      img: kalm.png
      logo: https://apps.kde.org/app-icons/org.kde.kalm.svg
      appid: kalm
    - name: Skladnik
      summary: |
        [Skladnik](https://apps.kde.org/skladnik/) is a twist on the
        Japanese warehouse keeper game “Sokoban”.
      img: https://cdn.kde.org/screenshots/skladnik/skladnik.png
      logo: https://apps.kde.org/app-icons/org.kde.skladnik.svg
      appid: skladnik
---

{{< container class="intro" >}}

{{< announcements/donorbox >}}

{{< /container >}}

{{< container >}}

## [Dolphin](https://apps.kde.org/dolphin/)

Dolphin lets you navigate your folders and files, move and copy things from
one place to another, connect to file servers and manage everything in your
local and remote storage.

It is important to the Dolphin team that you can see what is happening at all
times, and we have implemented animations to help you follow every action. For
example, dragging a file or folder over another folder triggers a subtle
animation if the option to open the folder is enabled. Dolphin's bars also
animate when they appear and disappear.

{{< video src-webm="/announcements/gear/24.05.0/dolphin.webm" loop="true" muted="true" autoplay="true" controls="false" >}}

Dolphin also provides more tailored and informative insights into specific
folders by default, so when browsing through recently used files and folders,
users will find modification times listed by default and have streamlined
access to the most recent items. Similarly, the Trash folder now offers detailed
information on the time and origin of each deleted file.

During searches, Dolphin has refined its result views to offer more pertinent
details. For images, we display dimensions and creation times, while audio
files reveal track information such as author, album, and duration. For general
searches, results are conveniently accompanied by their respective paths and
modification times, so you have all the context you need at your fingertips.

Seamless navigation through interfaces is crucial for users around the world,
and our latest update delivers just that. Now, when using right-to-left
languages such as Arabic or Hebrew, Dolphin's arrow navigation works
flawlessly.

## [Itinerary](https://apps.kde.org/itinerary/)

Itinerary now shows more information about your train and coach facilities
(where this information is available). This includes general comfort features
such as air conditioning or WiFi, as well as things specifically relevant if
you are traveling with young children, a bicycle, or a wheelchair. These can
also be qualified by availability (e.g. if they require a special reservation)
and marked as disrupted. This information is displayed when viewing a train's
car layout and when searching for a connection.

The Itinerary team, in collaboration with other open source projects, has
started work on a community-run, vendor-neutral international public transport
routing service called [Transitous](https://transitous.org/). Transitous aims
to focus on users' interests rather than on those of public transport
operators). It is free to use, respects users' privacy, and does not stop at
borders. We are now at a point where public transport information is available
for a large part of Europe, and the data for services outside Europe is growing.
The amount of information is now large enough that we have decided to enable
support for Transitous by default in Itinerary and KTrip.

![Current Transitous coverage for long-distance travel in Europe.](https://www.volkerkrause.eu/assets/posts/176/transitous-european-coverage.jpg)

As with most updates, we've improved the coverage of travel document extractors,
as well as adding support for a number of companies including AMSBus, ANA,
Deutsche Bahn, Eckerö Line, Elron, European Sleeper, Eurostar, Eventim, Finnair,
Flibco, Leo Express, LTG Link, Moongate, National Express, Pasažieru vilciens,
Salzbergwerk, SNCF, Thalys, ti.to, Trenitalia and UK National Railways.

{{< /container >}}

{{< section class="blue gear-section py-5 tokodon" >}}

{{< colorful-heading content="NeoChat" class="display-3 mt-0" id="neochat" logo="https://apps.kde.org/app-icons/org.kde.neochat.svg" >}}

[NeoChat](https://apps.kde.org/neochat/) is a chat app that lets you take full
advantage of the Matrix network.

In its newest version, we moved the search to a popup allowing you to search
for a conversation independently of the space you are in.

![](neochat-search.png)

NeoChat will also now scan PDFs and other files sent to the chat for travel
documents, and displaying all the relevant information directly in your
conversation. All the processing is done on your device and no private
information is sent to any third parties servers. Similarly your text documents
will be directly displayed in the timeline.

{{< video src-webm="/announcements/gear/24.05.0/neochat-timeline.mp4" loop="true" muted="true" autoplay="true" controls="false" >}}

{{< colorful-heading content="Tokodon" class="display-3 mt-0" id="tokodon" logo="https://apps.kde.org/app-icons/org.kde.tokodon.svg" >}}

[Tokodon](https://apps.kde.org/tokodon/) brings the Mastodon federated social
media platform to your fingertips. With Tokodon you can read, post, and message
easily. Now when writing a new post, it is possible to do that in a separate
window, allowing you to continue using Tokodon while writing your post.

![](tokodon.png)

In this release, we also added a badge counter for follow requests in the
sidebar.

{{< /section >}}

{{< container >}}

## [Kdenlive](https://kdenlive.org/)

Kdenlive is KDE's full-featured video editor that gives you everything you need
to build advertisements, documentaries, TV shows, and full-fledged movies.

Version 24.05 adds Group Effects, effects that can be added to clips grouped
together all at the same time. You can also reach wider audiences by using an
offline AI that can translate your subtitles with the Automatic Subtitle
Translations feature.

The feature that allows you to capture audio from your desktop or microphone
from directly within Kdenlive is back, and the performance of moving
clips with the spacer tool has been hugely improved. The multiple resource bins
management feature (bins being the areas where you keep your clips, images,
titles and animations) has also been reworked and improved.

{{< video src="/announcements/gear/24.05.0/kdenlive_24.05.mp4" loop="true" muted="true" autoplay="true" controls="false" >}}

{{< /container >}}

{{< section class="blue gear-section py-5 text-center elisa" >}}

{{< colorful-heading content="Elisa" class="display-3 mt-0" >}}

[Elisa](https://apps.kde.org/elisa/) is KDE's elegant and feature-rich music
player. Yet another improvement to its already sleek design is that
this new version lets you switch between list and grid views.

{{< video src-webm="/announcements/gear/24.05.0/elisa.mp4" loop="true" muted="true" autoplay="true" controls="false" >}}

{{< /section >}}

{{< container >}}

## And all this too...

[Ark](https://apps.kde.org/ark/) helps you manage compressed files and archives.
Ark can now open and un-archive self-extracting `.exe` archive files

![](https://invent.kde.org/utilities/ark/uploads/a2478ce699cbbceb89530420065fac4f/Screenshot_20240221_014525.png)

The date and time picker in [Merkuro](https://apps.kde.org/merkuro.calendar/)
has been updated and is now significantly faster.

![](https://kontact.kde.org/blog/2024/2024-05-01-kde-pim-march-april-2024/images/merkuro.png)

The reading experience on [Akregator](https://apps.kde.org/akregator/), KDE's
RSS news reader, is more pleasant in this version thanks to a new layout and
the support of dark themes.

![Akgregator](akregator.png)

{{< announcements/gear_major_outro >}}

{{< /container >}}
