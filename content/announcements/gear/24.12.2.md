---
date: 2025-02-06
appCount: 180
layout: gear
---
+ kalk: Fixes for the History view ([Commit](http://commits.kde.org/kalk/952c54d0872e14ccaf6a6eecaad0128afd1f4634/))
+ dolphin: Fix pixelated preview images ([Commit](http://commits.kde.org/dolphin/c7e2cb3a5a4a72b1b560153cea8454280f8155a8/), fixes bug [#497576](https://bugs.kde.org/497576))
+ kdevelop: Fix locations of Uses in macro expansions when using clang 19 or later ([Commit](http://commits.kde.org/kdevelop/ee828bc1149d2d48f966f8d21ab44f121168cb62), fixes bug [#496985](https://bugs.kde.org/496985))
