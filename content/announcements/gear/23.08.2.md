---
date: 2023-10-12
appCount: 120
image: true
layout: gear
---
+ kdeconnect: Avoid adding duplicate devices to Dolphin's side panel ([Commit](http://commits.kde.org/kdeconnect-kde/de45a8017cff7df63ec0c967d0bfd8774afbddf8), fixes bug [#461872](https://bugs.kde.org/461872))
+ merkuro: Fix shifting of date by one day/month ([Commit](http://commits.kde.org/merkuro/60586e4b7c238e3871c690550a09f4ffa7356192), fixes bug [#473866](https://bugs.kde.org/473866))
+ kdenlive:  Fix multiple audio streams broken by MLT's new astream property ([Commit](http://commits.kde.org/kdenlive/e4b545e87a48fa35c56b471e82dfe5124ecbe875), fixes bug [#474895](https://bugs.kde.org/474895))
