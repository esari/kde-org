---
aliases:
- ../announce-3.5.8
- ../announce-3.5.8.sl
custom_about: true
date: '2007-10-16'
title: Najava izdaje KDE 3.5.8
---

<h3 align="center">
   Projekt KDE je izdal osmo servisno različico vodilnega prostega namizja,
   ki prinaša tudi posodobljene prevode
</h3>

<p align="justify">
  KDE 3.5.8 je preveden v 65 jezikov (vključno s slovenščino) in prinaša
  izboljšave paketa programov za upravljanje z osebnimi informacijami in
  ostalih programov.
</p>

<p align="justify">
  Pri <a href="/">Projektu
  KDE</a> so danes najavili takojšnjo dostopnost KDE 3.5.8, vzdrževalne
  izdaje najnovejše generacije najnaprednejšega in najzmogljivejšega
  <em>prostega</em> namizja za GNU/Linux in ostale UNIX-e. KDE sedaj
  podpira <a href="http://l10n.kde.org/stats/gui/stable/">65 jezikov</a>,
  s čimer je v materinem jeziku na voljo več ljudem kot večina komercialnih
  programov. Skupine, ki želijo prispevati k odprtokodnemu projektu lahko na
  preprost način dodajo podporo za ostale jezike. Če se želite pridružiti
  slovenski ekipi prevajalcev obiščite spletno stran
  <a href="http://wiki.lugos.si/slovenjenje:kde:kde">Skupine za slovenjenje KDE</a>.
</p>

<p align="justify">
    Medtem ko so razvijalci v glavnem osredotočeni na dokončevanje različice KDE 4.0,
    serija 3.5 še vedno ostaja prva izbira. Je preizkušena, stabilna in dobro podprta.
    Izdaja 3.5.8, z dobesedno stotinami popravkov, ponovno izboljšuje uporabniške
    izkušnje. Glavne izboljšave v KDE 3.5.8 so:
    <ul>
        <li>
        Izboljšave v programu Konqueror in v pogonu za izris spletnih strani KHTML.
        Odpravljene so težave pri povezavah HTTP, izboljšana je podpora nekaterim
        elementom CSS in skladnost s spletnimi standardi.
        </li>
        <li>
        V tej izdaji sta bila deležna precej popravkov programa KPDF (ogledovalnik
        datotek PDF) in Kolourpaint (program za risanje).
        </li>
        <li>
        Kot ponavadi je paket programov za upravljanje z osebnimi informacijami doživel
        številne izboljšave stabilnosti. Sem spadata odjemalec elektronske pošte KMail
        in osebni organizator KOrganizer.
        </li>
    </ul>
</p>

<p align="justify">
  Za podrobnejši seznam izboljšav glede na
  <a href="/announcements/announce-3.5.7">izdajo KDE 3.5.7</a>,
  dne 22. maja 2007, si oglejte
  <a href="/announcements/changelogs/changelog3_5_7to3_5_8">seznam sprememb v KDE 3.5.8</a>.
</p>

<p align="justify">
  KDE 3.5.8 vsebuje osnovno namizje in petnajst ostalih paketov (upravljanje
  z osebnimi podatki, administracija, omrežje, izobraževanje, pripomočki, večpredstavnost,
  igre, spletni razvoj, programiranje...). KDE-jeva orodja in programi so prejeli več
  nagrad in so na voljo v <strong>65 jezikih</strong>. Med njimi je tudi slovenščina.
</p>

<h4>
  Distribucije, ki vsebujejo KDE
</h4>
<p align="justify">
  Večina distribucij operacijskih sistemov Linux in UNIX ne vključi novih izdaj
  KDE-ja takoj. Pakete, ki sestavljajo KDE 3.5.8, bodo vključili v prihodnje izdaje
  svojih distribucij. Oglejte si <a href="/distributions">seznam
  distribucij, ki vsebujejo KDE</a>.
</p>

<h4>
  Nameščanje paketov za KDE 3.5.8
</h4>
<p align="justify">
  <em>Ustvarjalci paketov</em>.
  Nekateri ponudniki operacijskih sistemov so sami pripravili pakete namizja
  KDE 3.5.8 za nekatere različice svojih distribucij. V določenih primerih pa so
  to storili prostovoljci iz skupnosti uporabnikov.
  Nekateri od teh paketov so za prost prenos na voljo na KDE-jevem strežniku
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.8/">download.kde.org</a>.
  Dodatni ali posodobljeni paketi so lahko na voljo v prihajajočih tednih.
</p>

<p align="justify">
  <a id="package_locations"><em>Lokacije paketov</em></a>
  Za trenuten seznam razpoložljivih paketov, o katerih je bil obveščen
  Projekt KDE, obiščite angleško stran
  <a href="/info/1-2-3/3.5.8">KDE 3.5.8 Info Page</a>.
</p>

<h4>
  Prevajanje izvorne kode KDE 3.5.8
</h4>
<p align="justify">
  <a id="source_code"></a><em>Izvorna koda</em>
  Celotna izvorna koda za KDE 3.5.8 je na voljo za
  <a href="http://download.kde.org/stable/3.5.8/src/">prost
  prenos</a>. Navodila za prevajanje in nameščanje KDE 3.5.8
  so na voljo na angleški strani <a href="/info/1-2-3/3.5.8">KDE
  3.5.8 Info Page</a>.
</p>

<h4>
  Kako podpreti KDE
</h4>
<p align="justify">
KDE je projekt <a href="http://www.gnu.org/philosophy/free-sw.html">prostega programja</a>,
ki obstaja in raste le zaradi pomoči mnogih prostovoljcev, ki vanj vložijo svoj čas in trud.
KDE vedno išče nove prostovoljce in prispevkarje. Pomagate lahko s programiranjem,
odpravljanjem napak ali testiranjem in poročanjem o napakah. Pišete lahko dokumentacijo,
prevajate, promovirate KDE ali pa nam pomagate denarno. Vsi prispevki so izredno
cenjeni in nestrpno pričakovani. Za dodatne informacije si oglejte angleško stran
<a href="/community/donations/">Supporting KDE</a>.</p>

<p align="justify">
Veseli bomo, če boste stopili v stik z nami!
</p>

<h4>
  O projektu KDE
</h4>
<p align="justify">
  KDE, ki je prejel <a href="/community/awards/">več nagrad</a>, je neodvisen projekt <a href="/people/">več sto</a>
  programerjev, prevajalcev, umetnikov in ostalih profesionalcev z vsega sveta, ki sodelujejo prek interneta.
  Njihov cilj je ustvariti in prosto razširjati sofisticirano, prilagodljivo in stabilno
  namizje in pisarniško okolje. Pri tem uporabljajo fleksibilno arhitekturo, ki temelji na komponentah
  in deluje tudi preko omrežja. Vse to omogoča izvrstno platformo za razvoj.</p>

<p align="justify">
  KDE ponuja stabilno namizje, ki vključuje moderen spletni brskalnik
  (<a href="http://konqueror.kde.org/">Konqueror</a>), orodja za upravljanje
  z osebnimi podatki (<a href="http://kontact.org/">Kontact</a>), celoten
  pisarniški paket (<a href="http://www.koffice.org/">KOffice</a>), obsežen
  nabor omrežnih programov in orodij ter učinkovito in intuitivno okolje za razvoj
  in programiranje <a href="http://www.kdevelop.org/">KDevelop</a>.</p>

<p align="justify">
  KDE je delujoč dokaz, da odprtokoden razvoj programja v slogu bazarja
  lahko privede do najboljših tehnologij, ki so enakovredne najbolj
  zapletenim komercialnim programom, oziroma jih celo prekašajo.
</p>

<hr />

<p align="justify">
  <font size="2">
  <em>Opombe o zaščitenih znamkah</em>
  Logotipa KDE<sup>&#174;</sup> in K Desktop Environment<sup>&#174;</sup> sta
  zaščiteni znamki organizacije KDE e.V.

Linux je zaščitena znamka, ki si jo lasti Linus Torvalds.

UNIX je v ZDA in v drugih državah zaščitena znamka konzorcija The Open Group.

Vse ostale zaščitene znamke in avtorske pravice, ki so omenjene v tej najavi,
pripadajo posameznim lastnikom.
</font>

</p>

<hr />
