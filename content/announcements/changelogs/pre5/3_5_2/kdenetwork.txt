2006-01-20 10:56 +0000 [r500475]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/yahooaccount.cpp:
	  fix html-escape regexp. BUG:120469

2006-01-21 15:49 +0000 [r500923]  dmacvicar

	* branches/KDE/3.5/kdenetwork/kopete/protocols/gadu/gaduprotocol.cpp:
	  backport

2006-01-22 14:31 +0000 [r501246]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libyahoo2/libyahoo2.c:
	  fix compiler warning

2006-01-22 14:37 +0000 [r501251-501248]  mueller

	* branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/readcfg++.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/repairs.c,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/announce.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/table.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/machines/answmach.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/proto.h,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/print.c,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/machines/forwmach.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/find_user.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/process.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/talkd.cpp,
	  branches/KDE/3.5/kdenetwork/ktalkd/ktalkd/machines/talkconn.cpp:
	  rename debug() -> ktalk_debug

	* branches/KDE/3.5/kdenetwork/krfb/libvncserver/main.c: avoid
	  format string warning

	* branches/KDE/3.5/kdenetwork/kppp/pppstats.cpp: avoid compiler
	  warning

2006-01-22 18:29 +0000 [r501369]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcspreferences.cpp:
	  Some preparations for Kopete::ManagedConnectionAccount

2006-01-23 00:34 +0000 [r501462]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/iconnector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsiface.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetworkstatus.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetworkstatus.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kinternetiface.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.h:
	  * using dcopidl2cpp stub generated from kinternetiface.h (from
	  kinternet package), no more own implementation * experimental
	  implementation of the the KDED-NetworkStatus (not active, yet) *
	  significantly speeded up automatic detection of a SMPPPD *
	  BUGFIX: reloading the plugin in a already running Kopete will no
	  more result in an inactive plugin

2006-01-24 01:03 +0000 [r501833]  ratz

	* branches/KDE/3.5/kdenetwork/kget/main.cpp: Sometimes valid
	  filenames are not recognized by KURL::isLocalFile, leading to
	  problems when saving some links. Check for KURL::isValid for
	  those cases. BUG:120228

2006-01-25 14:13 +0000 [r502259]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationwidget.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest/Makefile.am
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsconfig.kcfgc
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcspreferences.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefsimpl.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs_config.desktop
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcspreferences.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefsimpl.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdsearcher.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcs.kcfg
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefs.ui
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdsearcher.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationui.ui
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest/clienttest.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdclient.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationwidget.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest/clienttest.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest/main.cpp
	  (added): using KConfigXT for configuration

2006-01-25 19:31 +0000 [r502359]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Backport: in Kopete::Message: HTML header wasn't stipped in
	  setBody when using RichText.

2006-01-25 23:22 +0000 [r502433]  ctennis

	* branches/KDE/3.5/kdenetwork/krfb/krfb_httpd/krfb_httpd: Change
	  head argument so it's compliant with latest coreutils

2006-01-26 16:25 +0000 [r502587]  amantia

	* branches/KDE/3.5/kdenetwork/kppp/logview/loginfo.h,
	  branches/KDE/3.5/kdenetwork/kppp/logview/monthly.cpp,
	  branches/KDE/3.5/kdenetwork/kppp/logview/loginfo.cpp: Support
	  monthly transfers greater than 2GB/month in the log.

2006-01-27 10:44 +0000 [r502772]  hschaefer

	* branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationwidget.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs_config.desktop
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdsearcher.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefsimpl.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcspreferences.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefs.ui
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcspreferences.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kopete_smpppdcs.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetworkstatus.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/smpppdclient.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/kinternetiface.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationwidget.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/iconnector.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationwidget.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsconfig.kcfgc
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcspreferences.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsprefsimpl.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectordcop.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcspreferences.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefsimpl.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcs.kcfg
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/kopete_smpppdcs_config.desktop,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/libsmpppdclient/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetstat.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdsearcher.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdlocationui.ui
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdsearcher.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/smpppdcsiface.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectornetworkstatus.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdcsprefs.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/config/smpppdlocationui.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/Changelog.smpppdcs,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/unittest
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/plugins/smpppdcs/detectorsmpppd.h:
	  Reverted changes mistakingly done in the stable branch. All
	  changes are now in the dev-0.12-branch.

2006-01-30 23:11 +0000 [r503992]  ratz

	* branches/KDE/3.5/kdenetwork/kget/kmainwidget.cpp: Use
	  kio/renamedlg when downloading multiple files and file(s) already
	  exist BUG:120097

2006-01-31 02:19 +0000 [r504009]  ratz

	* branches/KDE/3.5/kdenetwork/kget/kmainwidget.cpp: Add size and
	  modification time of destination to rename dialog, see commit
	  503992

2006-02-03 19:22 +0000 [r505387]  jriddell

	* branches/KDE/3.5/kdenetwork/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdenetwork
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdenetwork

2006-02-04 21:39 +0000 [r505782]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnnotifysocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnprotocol.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnprotocol.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.cpp:
	  Backport to KDE 3.5 branch: Do not reports that the user has a
	  webcam when it is not configured or available. CCBUG: 120790

2006-02-05 14:30 +0000 [r505978]  mkoller

	* branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/LuxembourgOnline_FreeInternet.rst
	  (added),
	  branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/Local.rst
	  (removed),
	  branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/CMD_InternetGratuit.rst
	  (added),
	  branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/PetT_KioskSurf.rst
	  (added),
	  branches/KDE/3.5/kdenetwork/kppp/Rules/Luxembourg/PetT_ClassicSurf.rst
	  (added): BUG: 70714 Removed obsolete Local.rst Added 4 new .rst
	  files from Gilles Schintgen

2006-02-10 21:44 +0000 [r508147]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteawayaction.cpp:
	  Backport fix for Bug 119862: if you press escape or cancel in the
	  new message status-window there is a checkbox next to it and you
	  can't click on it anymore CCBUG: 119862

2006-02-15 19:16 +0000 [r509880]  annma

	* branches/KDE/3.5/kdenetwork/doc/ksirc/index.docbook,
	  branches/KDE/3.5/kdenetwork/doc/kppp/index.docbook: fix empty
	  <para> tags either by removing them or filling them

2006-02-18 09:23 +0000 [r510822]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/kwifimanager.cpp: make the tray
	  icon transparent also if numbers are not shown BUG:105637

2006-02-18 09:30 +0000 [r510825]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/kwifimanager.cpp: send an update
	  event to the tray icon when TX power changes this should fix
	  BUG:118114 but my own NIC doesn't support tpower setting, so I
	  can't test it properly :-(

2006-02-18 09:54 +0000 [r510830]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/ifconfigpagebase.ui:
	  make entering the script after connect work CCBUG:120844 for the
	  other part of the bug (i18n() issue): I'm still investigating

2006-02-19 15:36 +0000 [r511336]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/kopete/kopeteiface.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteaway.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteaway.h:
	  backport patches for bugs 120989, 92949, and 117513 Should be in
	  KDE 3.5.2 and the upcoming 0.12 release. CCBUG: 120989, 92949,
	  117513

2006-02-19 16:21 +0000 [r511356]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  don't show the same SSID multiple times in the results window,
	  this confuses users BUG:119836

2006-02-19 16:37 +0000 [r511359]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/networkscanning.cpp,
	  branches/KDE/3.5/kdenetwork/wifi/networkscanning.h: only enable
	  the "Switch to network" button when all settings are plausible
	  BUG:118880,118881

2006-02-21 18:52 +0000 [r512076]  deller

	* branches/KDE/3.5/kdenetwork/krdc/maindialogbase.ui: micro docu
	  fix: fix URLs, "vnc:/" -> "vnc://" and "rdp:/" -> "rdp://"
	  CCMAIL: kde-i18n-doc@kde.org

2006-02-21 19:16 +0000 [r512082]  deller

	* branches/KDE/3.5/kdenetwork/krdc/maindialogbase.ui: revert my
	  last commit. somehow this didn't worked.

2006-02-22 13:25 +0000 [r512416]  thiago

	* branches/KDE/3.5/kdenetwork/kdnssd/kdedmodule/Makefile.am: Cannot
	  add -no-undefined, but link to the rest of the dependency
	  libraries

2006-02-22 13:28 +0000 [r512418]  thiago

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/Makefile.am:
	  Adding -no-undefined: Link to your dependency libraries

2006-02-22 15:40 +0000 [r512460]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/private/kopeteprefs.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessage.cpp:
	  Fix typo.

2006-02-24 14:44 +0000 [r513116]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig_videoconfig.ui,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicemodelpool.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicemodelpool.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videoinput.cpp:
	  Syncing with 0.12

2006-02-24 16:43 +0000 [r513227]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/Makefile.am:
	  Fix build

2006-03-02 09:35 +0000 [r514989]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop: Surely I'm
	  the only one using 3.5 branch and kdevelop ;)

2006-03-03 09:57 +0000 [r515266]  lueck

	* branches/KDE/3.5/kdenetwork/doc/ksirc/index.docbook: fixed error
	  with generation of translated docbook

2006-03-03 14:05 +0000 [r515353]  mueller

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  try better finding the iwlist binary

2006-03-05 01:28 +0000 [r515831]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/closeconnectiontask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/closeconnectiontask.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Backport fix for bug 122827: crash in ICQ on disconnect due to
	  connect elsewhere. We were emitting signal from already deleted
	  object. Signal was removed because it wasn't used anyway. CCBUG:
	  122827

2006-03-05 20:51 +0000 [r516060]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/incomingtransfer.cpp:
	  Backport to KDE 3.5.2 of: Workaround fix about zero-sized MSN
	  file transfer bug. Patch by Bartosz Fabianowski, thank you dude.
	  CCBUG: 113525

2006-03-07 13:20 +0000 [r516513]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/incomingtransfer.cpp:
	  Backport to KDE 3.5.2: CCBUG: 113525 better fix for msn
	  filetransfers. much faster receiving from 7.5 users, but might
	  have issues with trillian n'stuff. Thanks to Bartosz Fabianowski
	  for this great patch :)

2006-03-10 10:44 +0000 [r517200]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.h:
	  backport workaround for Bug 113425: Webcam stops unexpected
	  ($contact has left the chat) CCBUG: 113425 The fix will be in KDE
	  3.5.2

2006-03-10 12:53 +0000 [r517231]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabbercontact.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabbercontact.cpp:
	  Backport to KDE 3.5.2 the fix about delayed sync

2006-03-10 18:12 +0000 [r517353]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jabberaccount.cpp:
	  backport fix for bug 121507 CCBUG: 121507

2006-03-10 21:37 +0000 [r517395]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/behavior/behaviorconfig.cpp:
	  Backport fix for Bug 123259

2006-03-10 22:58 +0000 [r517420]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnnotifysocket.cpp:
	  Backport fix for Bug 122362

2006-03-11 10:43 +0000 [r517519]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnchatsession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnchatsession.h:
	  Backport fix for bug 119390 (identify from which contact the
	  nudge is comming)

2006-03-11 12:06 +0000 [r517546]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnsecureloginhandler.cpp:
	  Backport fix for bug 118760 (problem to login with commas in
	  password) BUG: 118760

2006-03-16 11:02 +0000 [r519136]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/gwerror.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontactinstancetask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/createcontacttask.cpp:
	  Backport contact addition fixes

2006-03-17 02:32 +0000 [r519405]  aseigo

	* branches/KDE/3.5/kdenetwork/kdict/applet/kdictapplet.cpp: this
	  works at slightly smaller sizes, too

2006-03-17 21:34 +0000 [r519787]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: tagging 3.5.2

