Dir: kdegraphics
----------------------------
new version numbers



Dir: kdegraphics/kdvi
----------------------------
fixes bug #91954
----------------------------
robustification: no redraw when the same zoom value is set twice
----------------------------
fixes bug 89013
----------------------------
fixes name clash with kdeprint subsystem; bugfix by Luciano Montanaro



Dir: kdegraphics/kdvi/Attic
----------------------------
fixes bug #91954



Dir: kdegraphics/kfax
----------------------------
backport KFax bugfixes from HEAD
BUG:91659
BUG:89934
BUG:87788
BUG:75034
BUG:51058
BUG:47080
BUG:90604
BUG:89972
BUG:69704
BUG:20028
BUG:19765
BUG:84147
FEATURE:50286



Dir: kdegraphics/kfaxview
----------------------------
remove private unmaintained copy of libtiff



Dir: kdegraphics/kfile-plugins/png
----------------------------
Don't crash if the file grows while reading the metainfo
BUG: 86360



Dir: kdegraphics/kghostview
----------------------------
Add missing paper sizes.

Patch by Jens Krueger



Dir: kdegraphics/kolourpaint
----------------------------
   * Fix crash due to text box when scaling image behind it
----------------------------
   * Empty text clipboard fixes:
     - Don't get stuck on a wait cursor after attempting to paste empty
       text into a text box
     - Prevent pasting text from creating a new text box if text is empty
     - Prevent copying of empty text box
----------------------------
up ver to 1.2.2; all docs in this branch are up-to-date now
----------------------------
   * Smaller selection and text box resize handles (visually not
     actually) - covers up fewer selected pixels, doesn't cover up text
----------------------------
   * Restore mouse cursor after deselecting selection/text tools



Dir: kdegraphics/kolourpaint/pics/custom
--------------------------------

8-bit is a special case because of the Palette (aka Color Table).

- "Image/Invert Colors" inverts the colour values in the Color Table,
   instead of the indexes into the Color Table (now gives visually
   similar results to what happens in higher bit depths, faster)
- ditto for "Image/Convert to Grayscale"
- Tool Options Widgets now actually display their options on 8bpp displays
  (using QPixmap::createHeuristicMask() on a pixmap that already has
   a mask results in undefined behaviour)
- can now open monochrome images

4. Other Enhancements
----------------------------

A truckload of New Features:

1. KolourPaint the Icon Editor - Transparent Image Support
----------------------------------------------------------

- kpView now displays transparent images on a checkerboard
  (instead of on random garbage)
- new "Transparent Color" entry in the Colour Palette
  (TODO: finish the new colour displayer)
- all tools are "transparent-aware"
! selections probably won't work with transparent images
  but selections need to be redone anyway

As a bonus (and to show that transparent image support really works),
I changed all of KolourPaint's icons' ugly white backgrounds to
transparent and made 16x16 versions of the Tool icons so that they don't
look fuzzy when scaled for the toolbox.

2. KolourPaint the Paint Program - Renderer Improvements
--------------------------------------------------------

Rewritten renderer:
- now fully double-buffered: much flicker is gone
! performance improvements are desparately needed

- can scale between responsiveness and performance
  (kpViewManager::setFastUpdates()) for specific operations
- can queue updates to reduce flicker
  (kpViewManager::setQueueUpdates()).  This differs from
  QWidget::update() & QWidget::setUpdatesEnabled() in that it only
  updates the regions that have changed and only after the
  transaction is complete (rather than when Qt reenters the
  event loop).

- origin preparations for the thumbnail comeback

3. Improved Support for <= 8-bit



Dir: kdegraphics/kolourpaint/tests
--------------------------------

8-bit is a special case because of the Palette (aka Color Table).

- "Image/Invert Colors" inverts the colour values in the Color Table,
   instead of the indexes into the Color Table (now gives visually
   similar results to what happens in higher bit depths, faster)
- ditto for "Image/Convert to Grayscale"
- Tool Options Widgets now actually display their options on 8bpp displays
  (using QPixmap::createHeuristicMask() on a pixmap that already has
   a mask results in undefined behaviour)
- can now open monochrome images

4. Other Enhancements
----------------------------

A truckload of New Features:

1. KolourPaint the Icon Editor - Transparent Image Support
----------------------------------------------------------

- kpView now displays transparent images on a checkerboard
  (instead of on random garbage)
- new "Transparent Color" entry in the Colour Palette
  (TODO: finish the new colour displayer)
- all tools are "transparent-aware"
! selections probably won't work with transparent images
  but selections need to be redone anyway

As a bonus (and to show that transparent image support really works),
I changed all of KolourPaint's icons' ugly white backgrounds to
transparent and made 16x16 versions of the Tool icons so that they don't
look fuzzy when scaled for the toolbox.

2. KolourPaint the Paint Program - Renderer Improvements
--------------------------------------------------------

Rewritten renderer:
- now fully double-buffered: much flicker is gone
! performance improvements are desparately needed

- can scale between responsiveness and performance
  (kpViewManager::setFastUpdates()) for specific operations
- can queue updates to reduce flicker
  (kpViewManager::setQueueUpdates()).  This differs from
  QWidget::update() & QWidget::setUpdatesEnabled() in that it only
  updates the regions that have changed and only after the
  transaction is complete (rather than when Qt reenters the
  event loop).

- origin preparations for the thumbnail comeback

3. Improved Support for <= 8-bit



Dir: kdegraphics/kolourpaint/tools
----------------------------
   * Restore mouse cursor after deselecting selection/text tools



Dir: kdegraphics/kolourpaint/widgets
----------------------------
backport mueller's "fix compile (gcc 4.0)"



Dir: kdegraphics/kpdf/kpdf
----------------------------
Print PS instead of Images
BUG: 92204
----------------------------
Version bump so i don't forget for kde 3.3.2 :-D



Dir: kdegraphics/kpdf/xpdf
----------------------------
Print PS instead of Images
BUG: 92204
----------------------------
make the comparison unsigned



Dir: kdegraphics/kpovmodeler
----------------------------
missing i18n()
----------------------------
Backporting gcc 3.4.2 compilation fix



Dir: kdegraphics/kuickshow/src
----------------------------
wrong branch



Dir: kdegraphics/kviewshell
----------------------------
Make mousewheel-zooming consistent with the rest of KDE.
Patch by Christian Mueller.
----------------------------
fixes bug #88380
