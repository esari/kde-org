---
aliases:
- ../changelog4_8_0to4_8_1
hidden: true
title: KDE 4.8.1 Changelog
---

<h2>Changes in KDE 4.8.1</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_8_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix KConfigDialogManager fails to handle subclasses of QComboBox with custom property. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293702">293702</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/c27b939ac971f23e8cf8950e76c3f2745c634d72">c27b939</a>. </li>
        <li class="bugfix normal">Write to the correct xmlFile in KToolBar::Private::slotContextShowText(). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292574">292574</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/0f9004209464f2bdfbef9031836ed72c63f52887">0f90042</a>. </li>
        <li class="bugfix normal">Re-establishes connection with networkstatus module when kded restarts. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=262732">262732</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/e95f3be9062f98263e35329ec2ed87b27933d2e9">e95f3be</a>. </li>
        <li class="bugfix normal">Make KGlobalSettings reread locale settings before calling settingsChanged(). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=289094">289094</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/87756493f51716594c5ad40f29a5c92ee66e0425">8775649</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_8_1/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://dolphin.kde.org" name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Do not make items invisible when switching previews on or off in some situations. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291841">291841</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/cc8f5b4939062df44ce0bd314ac4ae04973b9830">cc8f5b4</a>. </li>
        <li class="bugfix normal">Make sure that Control+click toggles the selection state. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292250">292250</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/84a9cc4bf6e9decc4c102102c4b04162369eb0fe">84a9cc4</a>. </li>
        <li class="bugfix normal">Check if the shell process in the Terminal Panel is still running before trying to kill it. Prevents unwanted killing of other processes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=286367">286367</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/7dfe3632f52d30f79f1c21c9acbaf68c05d63006">7dfe363</a>. </li>
        <li class="bugfix normal">Handle folder names containing special character, like Space, correctly in the Folders Panel. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291781">291781</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/80d9bfec580bf01b0ee584fc4bb46e3d59a0ba7c">80d9bfe</a>. </li>
        <li class="bugfix normal">Fix crash when opening a new tab if "Split view" is enabled by default. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292470">292470</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/5d22b71437b8e978a051d8245b6140f09253639b">5d22b71</a>. </li>
        <li class="bugfix normal">Re-enable dropping items on tabs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292505">292505</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/e3fc04a1e4e9b46537f9645dc46f0a971c2457dc">e3fc04a</a>. </li>
        <li class="bugfix normal">Mark items as unhovered after copying them via drag and drop. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292501">292501</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/bc80a99ffa5a658ba1d7be00c681f5394e365790">bc80a99</a>. </li>
        <li class="bugfix normal">Respect the user's "Natural sorting" setting. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292270">292270</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/803f50b4e78ca4de6b9fd6a6b4a6667c6d749a90">803f50b</a>. </li>
        <li class="bugfix normal">Fix possible crash in Compact View if the view height is smaller than the item height. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292816">292816</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/f5ecaee9c40c940fe8e1bf4a7d50fe0b18e1932b">f5ecaee</a>. </li>
        <li class="bugfix normal">Fix drag and drop issues with non-local URLs. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=292821">292821</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=292355">292355</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/fdc20584aef44e148b0186e8034ddf667bfe9a07">fdc2058</a>. </li>
        <li class="bugfix normal">Synchronize view-mode settings before the settings dialog gets opened. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292698">292698</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/02e3235275ca2d58daa4fb5ca16b2ffc63016fd7">02e3235</a>. </li>
        <li class="bugfix normal">Respect a change of the user's "Home directory" setting also in existing tabs. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291043">291043</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/d03d06a92f3af9fe88ead0d50ced7de55f408c83">d03d06a</a>. </li>
        <li class="bugfix normal">Update the zoom-slider in the statusbar after changing the zoom-level in the settings. See Git commit <a href="http://commits.kde.org/kde-baseapps/47b4e1dfdc7f9480dd6daf2b0867376ef859b707">47b4e1d</a>. </li>
        <li class="bugfix normal">Implement "sort by path" and sorting by other sort roles that were still missing. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292941">292941</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/86a49188f5a917386e0be7c7c5a6183663073b11">86a4918</a>. </li>
        <li class="bugfix normal">Fix keyboard navigation issue that occurred when Home or End and then an arrow key were pressed. See Git commit <a href="http://commits.kde.org/kde-baseapps/999234a94ae41cfb4f2167d2779b562ebf8127ce">999234a</a>. </li>
        <li class="bugfix normal">Do not crash when opening a new tab while a tool tip is shown. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=278302">278302</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/d80f35e634aba07281f4339125e8d0a51cd07eda">d80f35e</a>. </li>
        <li class="bugfix normal">Fix an integer overflow, which could cause problems when "Sort by size" is enabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293086">293086</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/e196f065efeb911db931f549004daa8d9bbaa914">e196f06</a>. </li>
        <li class="bugfix normal">Show expansion toggles in the Folders Panel also for directories on ISO images. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292642">292642</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/6f65a1745caccc1fe27522a001592a3315325084">6f65a17</a>. </li>
        <li class="bugfix normal">When hovering an item with the mouse, show its size in the status bar. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292687">292687</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/73e4324c25813486568c122a8ba9068350262774">73e4324</a>. </li>
        <li class="bugfix normal">Show the value "Unknown" for the item-count only after it has been verified. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291823">291823</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/d9bc83e626dc2c789ad71c61f367495eaa2830d7">d9bc83e</a>. </li>
        <li class="bugfix normal">When entering a directory which is not shown in the Folders Panel yet, show it by expanding its parents, but do not expand the directory itself. See Git commit <a href="http://commits.kde.org/kde-baseapps/89082ca391807abdc26d8985efe6b4c27183a9b1">89082ca</a>. </li>
        <li class="bugfix normal">Use the whole available width of the icon area for previews of landscape images. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293268">293268</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/0ade2b6ec9363cd9f987d4871bd36af076889d91">0ade2b6</a>. </li>
        <li class="bugfix normal">Fix the issue that decreasing the window size may result in an endless loop. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293318">293318</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/81034de5074071d45bd4da212c80fb3dd5ea4933">81034de</a>. </li>
        <li class="bugfix normal">Animation fixes: Don't start animations that result in overlapped items. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=289238">289238</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/84bb46aa4b913fa138ee10841ecb64ab323a9009">84bb46a</a>. </li>
        <li class="bugfix normal">Compact view: Don't hide items at the bottom when grouping is turned on. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291640">291640</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/44b8e12e54f53a687ece168baa97801b37ec09bb">44b8e12</a>. </li>
        <li class="bugfix normal">Drag and drop: Adjust the destination if the item is no directory or desktop-file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293511">293511</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/13d975a6d7ea6552202a13d0d324754a4247d730">13d975a</a>. </li>
        <li class="bugfix normal">Details view: Draw parent-branches. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=290276">290276</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/e505c3ae5a8df306992777b7ba121036a7d8933d">e505c3a</a>. </li>
        <li class="bugfix normal">Folders Panel: Use the whole width as selection region. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=294111">294111</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/98d98f7c2457dc24223d77da77398c816dddf8b8">98d98f7</a>. </li>
        <li class="bugfix normal">Prevent showing tooltips when dragging items into another Dolphin window. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=294533">294533</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/72fe0317cac590dec78283bc71d2bb98fcd4ea32">72fe031</a>. </li>
        <li class="bugfix normal">Don't crash when switching to the details-view when specific roles are visible. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=294531">294531</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/abd23c29110560dd893e0dc2b542fd04e1845a0f">abd23c2</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_8_1/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kopete">kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix "kopete does not save passwords correctly if it contains symbols". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=165308">165308</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1281264&amp;view=rev">1281264</a>. </li>
        <li class="bugfix normal">Prevent warning messages "KXMLGUIClient::~KXMLGUIClient: 0x1605120 deleted without having been removed from the factory first. This will leak standalone popupmenus and could lead to crashes." See SVN commit <a href="http://websvn.kde.org/?rev=1283135&amp;view=rev">1283135</a>. </li>
        <li class="bugfix normal">Do not add unnamed groups to contact list. See SVN commit <a href="http://websvn.kde.org/?rev=1282202&amp;view=rev">1282202</a>. </li>
        <li class="bugfix normal">NowListening plugin: do not show song info when player is in pause state. See SVN commit <a href="http://websvn.kde.org/?rev=1282203&amp;view=rev">1282203</a>. </li>
        <li class="bugfix normal">Modify Yahoo login URL to correctly encode username and password. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=165308">165308</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1281264&amp;view=rev">1281264</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_8_1/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="ksmserver">ksmserver</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix shutdown/reboot/logout do not work when sound system is broken. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=228005">228005</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/82d5f2a118a9a40e18b86628a3dc3109adf4f7bc">82d5f2a</a>. </li>
      </ul>
      </div>
      <h4><a name="solid">solid</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Cache data in ModemManager backend to prevent desktop freezes. See Git commit <a href="http://commits.kde.org/kde-workspace/21ff56d2058f0e5ed022f464d2cfd8dbd71fbd11">21ff56d</a>. </li>
        <li class="bugfix normal">Always install networkmanager.png icons. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293711">293711</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/5a2669f151e65dca5adededade2b30cc40dc7c0f">5a2669f</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kderuntime"><a name="kderuntime">kderuntime</a><span class="allsvnchanges"> [ <a href="4_8_1/kderuntime.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="networkstatus">networkstatus</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Delay statusChanged(Solid::Networking::Connected) signal a bit. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=273342">273342</a>.  See Git commit <a href="http://commits.kde.org/kde-runtime/d0ce3b783382016a0b6049a78066a5fdb089043b">d0ce3b7</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdesdk"><a name="kdesdk">kdesdk</a><span class="allsvnchanges"> [ <a href="4_8_1/kdesdk.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kompare">Kompare</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix tab expansion for widths &lt; 4. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=251025">251025</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1278833&amp;view=rev">1278833</a>. </li>
      </ul>
      </div>
      <h4><a name="umbrello">umbrello</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Features:</em>
      <ul>
        <li class="feature">Code import for <a href="http://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29">C#</a> See SVN commits <a href="http://websvn.kde.org/?rev=1274012&amp;view=rev">1274012</a>, <a href="http://websvn.kde.org/?rev=1276286&amp;view=rev">1276286</a> and <a href="http://websvn.kde.org/?rev=1276910&amp;view=rev">1276910</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Don't reset box widget size on copy/paste (see comment #8). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=70925">70925</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1273986&amp;view=rev">1273986</a>. </li>
        <li class="bugfix normal">PHP5 code generation: Don't create illegal tag without any parameter. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155382">155382</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1279522&amp;view=rev">1279522</a>. </li>
        <li class="bugfix normal">Don't move classes in sub-packages to top level packages on edit. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=174184">174184</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1279523&amp;view=rev">1279523</a>. </li>
        <li class="bugfix crash">Don't crash when clicking on / trying to move datatype in diagram. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202410">202410</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1280135&amp;view=rev">1280135</a>. </li>
        <li class="bugfix crash">Fix crash importing Java code with variable length parameters. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=230770">230770</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1279959&amp;view=rev">1279959</a>. </li>
        <li class="bugfix normal">Allow unchecking of "Show Public Only". Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=242545">242545</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1280148&amp;view=rev">1280148</a>. </li>
        <li class="bugfix crash">Don't crash on drawing Containment in wrong direction. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=271243">271243</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1280794&amp;view=rev">1280794</a>. </li>
        <li class="bugfix normal">Reduce gratuitous warnings on loading project file. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=283744">283744</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1279111&amp;view=rev">1279111</a>, <a href="http://websvn.kde.org/?rev=1279514&amp;view=rev">1279514</a>, <a href="http://websvn.kde.org/?rev=1279515&amp;view=rev">1279515</a> and <a href="http://websvn.kde.org/?rev=1280354&amp;view=rev">1280354</a>. </li>
        <li class="bugfix crash">Don't crash on copy/paste. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=287179">287179</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=280770">280770</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=262842">262842</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=286449">286449</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=274265">274265</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=275372">275372</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=241915">241915</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=257410">257410</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=257584">257584</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1273902&amp;view=rev">1273902</a>. </li>
        <li class="bugfix normal">Fix copy/paste of elements in activity and state diagrams. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291557">291557</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1273895&amp;view=rev">1273895</a>. </li>
        <li class="bugfix normal">Don't create many 'new_package_xx' packages on file loading. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291159">291159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1282856&amp;view=rev">1282856</a>. </li>
        <li class="bugfix crash">Fix crash on importing templated java source code. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293770">293770</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1279716&amp;view=rev">1279716</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_8_1/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix adding keys to groups in group editor. See Git commit <a href="http://commits.kde.org/kgpg/622ecfca7ed0ab15acc7cf43f7076062e0c6ffaf">622ecfc</a>. </li>
        <li class="bugfix crash">Fix crashes deep in Qt model code, often happening when more than one key group was defined. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=254090">254090</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=294097">294097</a>.  See Git commits <a href="http://commits.kde.org/kgpg/c12a848ed88f99978fdca1bfdd2e4d7c01887edd">c12a848</a> and <a href="http://commits.kde.org/kgpg/751c40508de14dfc4f756ec6e5388d849d63004f">751c405</a>. </li>
        <li class="bugfix normal">Do not overwrite keyserver search string when importing keys. See Git commit <a href="http://commits.kde.org/kgpg/e20a07f0a470ad0de5d7ce8a6d4a202d81daa874">e20a07f</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_8_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix accelerator of the search entry not working. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=292209">292209</a>.  See Git commit <a href="http://commits.kde.org/okular/69526f2ee5fde235b18f16430f987a14c196109d">69526f2</a>. </li>
        <li class="bugfix crash">Fix crash when extracting the text on some documents. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=294633">294633</a>.  See Git commit <a href="http://commits.kde.org/okular/04716f9fd185a2f4253bfdac027595f792621558">04716f9</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_8_1/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kgeography">KGeography</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix compilation in Solaris. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=291912">291912</a>.  See Git commit <a href="http://commits.kde.org/kgeography/e9b303a1a6581b81f81c0af2613eb76464ff4967">e9b303a</a>. </li>
        <li class="bugfix normal">Fix some maps not being correctly installed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293538">293538</a>.  See Git commit <a href="http://commits.kde.org/kgeography/266cb4d42f2743d2ed069d58bb0fc51559f586e6">266cb4d</a>. </li>
        <li class="bugfix normal">Fix issue in Azerbaijan map. See Git commit <a href="http://commits.kde.org/kgeography/b4aa039303da6d11d06d203e9346768b7f6714c2">b4aa039</a>. </li>
        <li class="bugfix normal">Fix issue in Dominican Republic map. See Git commit <a href="http://commits.kde.org/kgeography/011484376bdf9074a5bb977db0bfcbb985034a18">0114843</a>. </li>
        <li class="bugfix normal">Fix issue in Great Britain map. See Git commit <a href="http://commits.kde.org/kgeography/596faffe4e8b5c33b2b834b0bbc01764ed72f510">596faff</a>. </li>
        <li class="bugfix normal">Fix issue in Greece map. See Git commit <a href="http://commits.kde.org/kgeography/72fe6df2064d4c02ab6b603ebd48084349e72c4f">72fe6df</a>. </li>
        <li class="bugfix normal">Fix issue in Haryana map. See Git commit <a href="http://commits.kde.org/kgeography/ff19dd920992289b24d8c52f92ab631420693bdc">ff19dd9</a>. </li>
        <li class="bugfix normal">Fix issue in New Zealand map. See Git commit <a href="http://commits.kde.org/kgeography/e5be5a90e29ed6bb37b18a376d147ca7c2387da7">e5be5a9</a>. </li>
        <li class="bugfix normal">Fix issue in Peru map. See Git commit <a href="http://commits.kde.org/kgeography/e85a01f00c56c60c581ed5ddf0897b3bd33146b1">e85a01f</a>. </li>
        <li class="bugfix normal">Fix issue in Spain map. See Git commit <a href="http://commits.kde.org/kgeography/751b99fce597f66f5291cdf61f01f73fb595adc0">751b99f</a>. </li>
        <li class="bugfix normal">Fix issue in Philippines map. See Git commit <a href="http://commits.kde.org/kgeography/3abf2d8a0e198ebbca5889e2a0758aa95664e8d0">3abf2d8</a>. </li>
        <li class="bugfix normal">Fix issue in Sudan map. See Git commit <a href="http://commits.kde.org/kgeography/cca78adb222c428744a84ba5a0fe429f33398bd1">cca78ad</a>. </li>
        <li class="bugfix normal">Fix issue in Iran map. See Git commit <a href="http://commits.kde.org/kgeography/ee3c890640e9fe9fab88c8be357ddb9f612ba831">ee3c890</a>. </li>
        <li class="bugfix normal">Fix issue in Manipur map. See Git commit <a href="http://commits.kde.org/kgeography/a15c09560db02d138363964d762b208740fa8e5a">a15c095</a>. </li>
        <li class="bugfix normal">Fix issue in Ecuador map. See Git commit <a href="http://commits.kde.org/kgeography/4d5b30d7a9dd4f80271c3b3136fa543c1da9ff6b">4d5b30d</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdepim"><a name="kdepim">kdepim</a><span class="allsvnchanges"> [ <a href="4_8_1/kdepim.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.astrojar.org.uk/kalarm" name="kalarm">KAlarm</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Fix crashes. Fixes bugs <a href="http://bugs.kde.org/show_bug.cgi?id=293193">293193</a>, <a href="http://bugs.kde.org/show_bug.cgi?id=293290">293290</a> and <a href="http://bugs.kde.org/show_bug.cgi?id=293360">293360</a>.  See Git commit <a href="http://commits.kde.org/kdepim/c55b943be61cded1cedd05e9e6b04612b2d6058c">c55b943</a>. </li>
        <li class="bugfix normal">Fix errors when creating default calendar resources. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293208">293208</a>.  See Git commit <a href="http://commits.kde.org/kdepim/8b8890cecd3d066f7454b17b113c748cbef53c96">8b8890c</a>. </li>
        <li class="bugfix normal">Automatically disable duplicated calendar resources. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=293193">293193</a>.  See Git commits <a href="http://commits.kde.org/kdepim/fa294799294fb4bb17375400f52c86b730724b22">fa29479</a> and <a href="http://commits.kde.org/kdepim/cd37be53796388dac990f2c5cd64731900bf5b50">cd37be5</a>. </li>
        <li class="bugfix normal">Prevent duplicate prompts to update the format of new calendar resources. See Git commit <a href="http://commits.kde.org/kdepim/5f2bb19193a8efcfaa0b2bb7a7308bb2a6534bfe">5f2bb19</a>. </li>
        <li class="bugfix normal">Only give a choice of fully writable calendars for saving new alarms in. See Git commit <a href="http://commits.kde.org/kdepim/c5735fa45baadc2b001814a440da9b9f3f97f438">c5735fa</a>. </li>
      </ul>
      </div>
    </div>