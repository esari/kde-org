2007-01-18 18:37 +0000 [r625024]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp:
	  Don't show an MP3 folder when lame isn't installed. Dragging mp3s
	  from it only leads to empty .mp3 files, without any error being
	  shown(!).

2007-02-01 04:35 +0000 [r628971]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/googlefetcher.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/main.cpp: Disable Google's
	  javascript when loading images for the cover search, and make
	  sure that any images we try to load have only one "http://"
	  protocol header. Based on a patch from Michal Bukovsky. I don't
	  get messages about exceptions right now but I also don't have
	  kdelibs built with debugging so I'm leaving the bug open for now.
	  I will forward port tomorrow. CCBUG:116181

2007-03-22 11:23 +0000 [r645336]  lunakl

	* branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop:
	  Remove NoDisplay=true introduced in r347538. I'm pretty sure it
	  doesn't show up in Konqueror's config when I want to use
	  audiocd:/ , so the log message is apparently wrong and I don't
	  see the point of hiding it anyway.

2007-03-23 13:17 +0000 [r645713]  binner

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp:
	  don't show configuration of not available encoders

2007-03-29 14:17 +0000 [r647782]  binner

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/eventsrc: fix
	  default sound file name

2007-04-02 14:29 +0000 [r649329]  aacid

	* branches/KDE/3.5/kdemultimedia/arts/gui/kde/kgraph.cpp,
	  branches/KDE/3.5/kdemultimedia/noatun/modules/artseffects/extrastereo_impl.cc,
	  branches/KDE/3.5/kdemultimedia/juk/artsplayer.cpp,
	  branches/KDE/3.5/kdemultimedia/mpeglib_artsplug/mpeglibartsplay.cpp:
	  build with GCC 4.3 snapshots
	  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=417286

2007-04-05 11:40 +0000 [r650759]  lueck

	* branches/KDE/3.5/kdemultimedia/doc/kmix/kmix-channel-switches.png
	  (added),
	  branches/KDE/3.5/kdemultimedia/doc/kaudiocreator/index.docbook,
	  branches/KDE/3.5/kdemultimedia/doc/kmix/index.docbook,
	  branches/KDE/3.5/kdemultimedia/doc/juk/index.docbook:
	  documentation backport from trunk

2007-04-24 14:25 +0000 [r657594]  lunakl

	* branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/libkcddb.desktop:
	  Revert r645336. I'm somewhat confused about all the cddb
	  configurations in KDE but oh well, better keep things the way
	  they are for 3.x.

2007-05-12 21:58 +0000 [r664000]  charles

	* branches/KDE/3.5/kdemultimedia/xine_artsplugin/xineAudioPlayObject.mcopclass:
	  musepack was supported all this time

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

