2007-01-15 22:16 +0000 [r623907]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/stations.dat,
	  branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: BUG:
	  89507 BUG: 113779 BUG: 109461 BUG: 139888 add/correct some
	  weather stations

2007-01-19 15:09 +0000 [r625227]  mlaurent

	* branches/KDE/3.5/kdetoys/kweather/kweatherreport.desktop:
	  Backport fix use good name

2007-02-23 20:51 +0000 [r636697]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop: BUG:
	  140517 Correct names of South African Airports

2007-02-23 21:18 +0000 [r636703]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/weather_stations.desktop:
	  Correct Portugese locations

2007-02-23 22:46 +0000 [r636720]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/dockwidget.h,
	  branches/KDE/3.5/kdetoys/kweather/kcmweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/kcmweather.h,
	  branches/KDE/3.5/kdetoys/kweather/kweather.h,
	  branches/KDE/3.5/kdetoys/kweather/prefdialogdata.ui: BUG: 80445
	  GUI: Add possibility to define the text color

2007-02-24 12:22 +0000 [r636839]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/reportview.cpp,
	  branches/KDE/3.5/kdetoys/kweather/weatherlib.cpp,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.h,
	  branches/KDE/3.5/kdetoys/kweather/dockwidget.cpp: BUG: 101955
	  Don't show a passive popup when the network is unreachable, as we
	  already show this situation by the "dunno" icon and now also
	  mention it in the tooltip

2007-02-24 12:43 +0000 [r636848]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/kweather.cpp,
	  branches/KDE/3.5/kdetoys/kweather/weatherservice.cpp,
	  branches/KDE/3.5/kdetoys/kweather/main.cpp: Make compile warning
	  free

2007-02-24 13:04 +0000 [r636851]  mkoller

	* branches/KDE/3.5/kdetoys/kweather/reportview.cpp: BUG: 113780
	  Don't display "Last updated on ..." but "Latest data from ..." to
	  make it clear that the weather data is from the given time, and
	  not only fetched at this time.

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

