commit dd69878b90c60f19104f166c7acf998e7201b8db
Author: Dirk Mueller <mueller@kde.org>
Date:   Fri Mar 4 10:37:53 2011 +0100

    make it possible to package this standalone

commit c324cc67e0430cfa1395fe9fb88f5e7dd6e6d446
Author: Sebastian Dörner <sebastian@sebastian-doerner.de>
Date:   Thu Mar 3 20:19:29 2011 +0100

    Adjust text color to new emblem for unstaged files
    
    The new emblem is green, not red, which should be reflected by the text
    color. Now, all files that will be part of the next commit
    (LocallyModifiedVersion and AddedVersion) are green and unstaged files
    darkGreen.

commit b87a1f5695e4b3b82c23e6ed02118b718bc4b732
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 2 14:43:15 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 6577a4db9e42f1a1a9b319894bd00da36634e45b
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Mon Feb 28 22:04:00 2011 +0100

    Fix issue that wrong directory is shown
    
    Commit 680009b522b2c9eda03201ac02dc18994b5a8bd0 resulted in a regression when switching from a view in the column-mode to a view in the icon- or details-mode: The wrong content has been shown. This patch reverts commit 680009b522b2c9eda03201ac02dc18994b5a8bd0 and solves the performance-issue in combination with previews by triggering the preview-generation in a queued way.

commit a60b1b6e1d3b983eccf893b9df874e368e7080c2
Author: Dirk Mueller <mueller@kde.org>
Date:   Mon Feb 28 17:33:53 2011 +0100

    readd missing copyright files

commit e64c498675518d287caae0301a1e3290644326ab
Author: Dirk Mueller <mueller@kde.org>
Date:   Mon Feb 28 13:46:12 2011 +0100

    konsole is in its own git module now

commit c2a963663b0c0b792ae3857a78f252276043ddd6
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Feb 27 14:14:16 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 2b0dcd8f2e1da9fb967032234e8dcf1201235da7
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Feb 26 14:13:59 2011 +0100

    SVN_SILENT made messages (.desktop file)
