------------------------------------------------------------------------
r879382 | coolo | 2008-11-03 07:20:44 +0000 (Mon, 03 Nov 2008) | 4 lines

backport:
 no idea why only a png is default
 BUG: 165463

------------------------------------------------------------------------
r881342 | pino | 2008-11-07 20:36:33 +0000 (Fri, 07 Nov 2008) | 4 lines

Backport SVN commit 881331 by piacentini:

Remove old courses site, took over by spammer (tks icwiener)

------------------------------------------------------------------------
r882241 | scripty | 2008-11-10 07:35:05 +0000 (Mon, 10 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r882664 | scripty | 2008-11-11 07:24:04 +0000 (Tue, 11 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r882693 | kleag | 2008-11-11 10:00:19 +0000 (Tue, 11 Nov 2008) | 1 line

Backport of two bugs corrections: word wrap in right dock and auto-defense
------------------------------------------------------------------------
r883553 | pino | 2008-11-13 09:05:14 +0000 (Thu, 13 Nov 2008) | 2 lines

remove just rc.cpp, not all the cpp files

------------------------------------------------------------------------
r884515 | scripty | 2008-11-15 08:49:24 +0000 (Sat, 15 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r886806 | fwang | 2008-11-20 05:51:15 +0000 (Thu, 20 Nov 2008) | 2 lines

build static libs (bug#175343)

------------------------------------------------------------------------
r890416 | kleag | 2008-11-29 10:15:47 +0000 (Sat, 29 Nov 2008) | 1 line

crash correction (thanks to Sérgio Luís for his patch)
------------------------------------------------------------------------
r890853 | aacid | 2008-11-30 14:14:28 +0000 (Sun, 30 Nov 2008) | 4 lines

Backport r890851
Fix crash when we get a such small size that font size at size 1 is too big.
This should not happen as there's a minimum size but maybe happens on startup

------------------------------------------------------------------------
r893662 | scripty | 2008-12-07 07:51:33 +0000 (Sun, 07 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r901889 | aacid | 2008-12-26 20:58:56 +0000 (Fri, 26 Dec 2008) | 5 lines

Backport r901864 by aacid:

use the untranslated deck name when reading/writing to disk, otherwise swithching langs causes decks not to be found
Also make frontTheme and backTheme check the theme that they are returning and if does not exist, return the default one

------------------------------------------------------------------------
r901890 | aacid | 2008-12-26 21:01:03 +0000 (Fri, 26 Dec 2008) | 4 lines

Backport r901859 by coolo:

no preview, no hit

------------------------------------------------------------------------
r904387 | scripty | 2009-01-02 07:48:40 +0000 (Fri, 02 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
