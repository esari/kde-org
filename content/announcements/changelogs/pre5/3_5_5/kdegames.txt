2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-13 00:06 +0000 [r572499]  s_dibbern

	* branches/KDE/3.5/kdegames/ktuberling/ktuberlingui.rc,
	  branches/KDE/3.5/kdegames/ktuberling/pics/layout.i18n,
	  branches/KDE/3.5/kdegames/ktuberling/pics/layout.xml: New
	  language Low Saxon added

2006-08-21 15:05 +0000 [r575429]  jriddell

	* branches/KDE/3.5/kdegames/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-09-07 20:58 +0000 [r581887]  ingwa

	* branches/KDE/3.5/kdegames/kpat/CHANGES,
	  branches/KDE/3.5/kdegames/kpat/mod3.cpp: Fix bug 131587: "cannot
	  win this game" message just after start of mod3 game - Check for
	  aces in the store and not only for empty places. BUGS: 131587

2006-09-09 14:26 +0000 [r582482]  aacid

	* branches/KDE/3.5/kdegames/kolf/courses/Hard.kolf: Make the
	  floater not have so big range so it can not put the ball out of
	  the walls. Patch by Paul Broadbent BUGS: 54097

2006-09-09 14:58 +0000 [r582490]  aacid

	* branches/KDE/3.5/kdegames/kolf/main.cpp: bug fixed so increase
	  version a bit

2006-09-11 08:46 +0000 [r582983]  coolo

	* branches/KDE/3.5/kdegames/ksirtet/ksirtet/Makefile.am,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/Makefile.am,
	  branches/KDE/3.5/kdegames/libksirtet/common/Makefile.am,
	  branches/KDE/3.5/kdegames/kfouleggs/Makefile.am: fix parallel
	  make now that Dirk's dashboard tests that

2006-10-02 11:08 +0000 [r591329]  coolo

	* branches/KDE/3.5/kdegames/kdegames.lsm: updates for 3.5.5

