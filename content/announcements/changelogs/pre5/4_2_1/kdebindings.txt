------------------------------------------------------------------------
r917124 | arnorehn | 2009-01-26 22:34:56 +0000 (Mon, 26 Jan 2009) | 3 lines

* Make PlasmaScripting.QGraphicsWidget inherit from IQGraphicsItem.
  backport from trunk

------------------------------------------------------------------------
r917275 | rdale | 2009-01-27 14:10:27 +0000 (Tue, 27 Jan 2009) | 2 lines

* Fix regression which was causing findChild() in qtruby to not work

------------------------------------------------------------------------
r917422 | sedwards | 2009-01-27 20:35:23 +0000 (Tue, 27 Jan 2009) | 5 lines

Added some CPython GIL control, otherwise the application crashes in
situations where one Python plugin indirectly calls the plugin again.
(e.g. a Python plasma applet loading a Python data engine).


------------------------------------------------------------------------
r917439 | sedwards | 2009-01-27 20:54:19 +0000 (Tue, 27 Jan 2009) | 4 lines

* Fixed pykdeuic to work when run in place (which is what most people do anyway).
* Fixed pykdeuic to generate the right code when it encounters a widget in from kio.


------------------------------------------------------------------------
r917971 | arnorehn | 2009-01-28 21:26:07 +0000 (Wed, 28 Jan 2009) | 2 lines

* backport from trunk

------------------------------------------------------------------------
r917978 | sedwards | 2009-01-28 21:39:27 +0000 (Wed, 28 Jan 2009) | 4 lines

* Added a bunch of /TransferThis/ annotations which should fix a heap of memory and object ownership related bugs.
* Added a couple of methods which were added to plasma at the 11th hour w.r.t. the 4.2.0 release cycle.


------------------------------------------------------------------------
r918430 | sedwards | 2009-01-29 22:36:40 +0000 (Thu, 29 Jan 2009) | 3 lines

Added manual code to bind the directories(), requiredDirectories(), files() and requiredFiles() methods.


------------------------------------------------------------------------
r918802 | arnorehn | 2009-01-30 19:06:05 +0000 (Fri, 30 Jan 2009) | 7 lines

* Mono 2.2 seems to have problems with generic parameters in method descriptions.
  Work around that by performing the method search ourselves. As a nice side-effect
  this adds type checking for generic parameters.

  backported from trunk.


------------------------------------------------------------------------
r926471 | sedwards | 2009-02-15 13:23:05 +0000 (Sun, 15 Feb 2009) | 4 lines

* Updated docs. Fixes some broken missing namespace docs in kdecore.
* Handle a couple more doxygen tags.


------------------------------------------------------------------------
r928052 | sedwards | 2009-02-18 20:26:29 +0000 (Wed, 18 Feb 2009) | 5 lines

Fixed method flash(const QString& text, int duration = 0, const QTextOption& option = QTextOption(Qt::AlignCenter)).

BUG: 184075


------------------------------------------------------------------------
r928402 | rdale | 2009-02-19 14:07:12 +0000 (Thu, 19 Feb 2009) | 2 lines

* Added new Qt 4.5 headers and classes

------------------------------------------------------------------------
r928405 | rdale | 2009-02-19 14:09:22 +0000 (Thu, 19 Feb 2009) | 2 lines

* Add tweaks to build smoke libs with Qt 4.5

------------------------------------------------------------------------
r928604 | arnorehn | 2009-02-19 16:32:16 +0000 (Thu, 19 Feb 2009) | 2 lines

backport from trunk

------------------------------------------------------------------------
r928612 | helio | 2009-02-19 16:51:29 +0000 (Thu, 19 Feb 2009) | 1 line

This directory not exists
------------------------------------------------------------------------
r931301 | sedwards | 2009-02-25 07:32:15 +0000 (Wed, 25 Feb 2009) | 5 lines

Fix for KSharedConfig.openConfig(). It stops the KSharedConfig object from
being destroyed too early and causing big trouble later when someone tries
to use it.


------------------------------------------------------------------------
r931553 | rdale | 2009-02-25 13:30:34 +0000 (Wed, 25 Feb 2009) | 4 lines

* Backport the port of QtRuby to Ruby 1.9.1 to the release branch

CCMAIL: kde-bindings@kde.org

------------------------------------------------------------------------
r931558 | rdale | 2009-02-25 13:35:20 +0000 (Wed, 25 Feb 2009) | 2 lines

* Raise the QtRuby version to 2.0.1 for the KDE 4.2.1 release

------------------------------------------------------------------------
r931576 | rdale | 2009-02-25 14:08:09 +0000 (Wed, 25 Feb 2009) | 2 lines

* Backport some fixes for Qyoto C# bindings code generation to the release branch

------------------------------------------------------------------------
r931578 | rdale | 2009-02-25 14:14:58 +0000 (Wed, 25 Feb 2009) | 2 lines

* Backport fixes to build the smoke libs with Qt 4.5 to the release branch

------------------------------------------------------------------------
r931605 | arnorehn | 2009-02-25 15:04:44 +0000 (Wed, 25 Feb 2009) | 2 lines

* backport the operator++/-- bugfix from trunk.

------------------------------------------------------------------------
r932363 | rdale | 2009-02-26 13:25:02 +0000 (Thu, 26 Feb 2009) | 2 lines

* Added code to set up the RUBY_VERSION

------------------------------------------------------------------------
r932383 | rdale | 2009-02-26 14:22:55 +0000 (Thu, 26 Feb 2009) | 2 lines

* Revert commit to remove unused code, and is actually was being used

------------------------------------------------------------------------
