------------------------------------------------------------------------
r915133 | scripty | 2009-01-22 13:52:33 +0000 (Thu, 22 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r915891 | alake | 2009-01-24 02:40:54 +0000 (Sat, 24 Jan 2009) | 2 lines

correct panel outline

------------------------------------------------------------------------
r915899 | alake | 2009-01-24 04:14:57 +0000 (Sat, 24 Jan 2009) | 2 lines

correct panel outline

------------------------------------------------------------------------
r916393 | coolo | 2009-01-25 10:54:53 +0000 (Sun, 25 Jan 2009) | 3 lines

update template and ran it - I guess the first time
in KDE4 :)

------------------------------------------------------------------------
r916402 | coolo | 2009-01-25 11:34:53 +0000 (Sun, 25 Jan 2009) | 2 lines

hardcode another place - looks like some use for for though

------------------------------------------------------------------------
r916412 | coolo | 2009-01-25 11:52:57 +0000 (Sun, 25 Jan 2009) | 2 lines

add categories

------------------------------------------------------------------------
r916413 | coolo | 2009-01-25 11:53:39 +0000 (Sun, 25 Jan 2009) | 2 lines

adding category

------------------------------------------------------------------------
r916534 | coolo | 2009-01-25 13:40:52 +0000 (Sun, 25 Jan 2009) | 4 lines

rework how xscreensaver is found. It seems you can't use
find_path on the same variable with different paths as it
caches that it found a result for one variable

------------------------------------------------------------------------
r916910 | scripty | 2009-01-26 13:04:59 +0000 (Mon, 26 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r918224 | scripty | 2009-01-29 17:19:20 +0000 (Thu, 29 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r918985 | scripty | 2009-01-31 08:53:22 +0000 (Sat, 31 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r919508 | scripty | 2009-02-01 08:44:11 +0000 (Sun, 01 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r920021 | scripty | 2009-02-02 08:37:37 +0000 (Mon, 02 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r920552 | scripty | 2009-02-03 08:34:37 +0000 (Tue, 03 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r921566 | scripty | 2009-02-05 08:40:13 +0000 (Thu, 05 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922448 | mpyne | 2009-02-07 03:30:08 +0000 (Sat, 07 Feb 2009) | 2 lines

Backport kdeasciiquarium major speedup patch for Qt 4.5 to KDE 4.2.1.

------------------------------------------------------------------------
r922559 | scripty | 2009-02-07 08:50:12 +0000 (Sat, 07 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r922927 | repinc | 2009-02-07 20:33:13 +0000 (Sat, 07 Feb 2009) | 2 lines

Added one more location to look for xscreensaver hacks in

------------------------------------------------------------------------
r923024 | alake | 2009-02-08 01:55:34 +0000 (Sun, 08 Feb 2009) | 2 lines

minor visual corrections.

------------------------------------------------------------------------
r923077 | scripty | 2009-02-08 07:41:45 +0000 (Sun, 08 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r923878 | woebbe | 2009-02-09 16:49:13 +0000 (Mon, 09 Feb 2009) | 1 line

link with gold
------------------------------------------------------------------------
r930773 | scripty | 2009-02-24 08:19:43 +0000 (Tue, 24 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r931995 | scripty | 2009-02-26 08:06:00 +0000 (Thu, 26 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
