2008-01-31 17:44 +0000 [r769153]  mueller

	* branches/KDE/4.0/kdegames/ksudoku/src/logic/sksolver.cpp: fix
	  logic

2008-02-01 20:36 +0000 [r769679]  lueck

	* branches/KDE/4.0/kdegames/doc/ksquares/index.docbook,
	  branches/KDE/4.0/kdegames/doc/lskat/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kshisen/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kfourinline/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kmahjongg/index.docbook,
	  branches/KDE/4.0/kdegames/doc/ksudoku/index.docbook,
	  branches/KDE/4.0/kdegames/doc/ktuberling/index.docbook,
	  branches/KDE/4.0/kdegames/doc/katomic/index.docbook,
	  branches/KDE/4.0/kdegames/doc/kbattleship/index.docbook:
	  documentation backport from trunk

2008-02-06 14:06 +0000 [r771605]  lueck

	* branches/KDE/4.0/kdegames/doc/kspaceduel/index.docbook,
	  branches/KDE/4.0/kdegames/doc/klines/index.docbook: backport from
	  trunk

2008-02-06 20:12 +0000 [r771725]  aacid

	* branches/KDE/4.0/kdegames/kspaceduel/topwidget.cpp,
	  branches/KDE/4.0/kdegames/kspaceduel/mainview.cpp: Backport
	  r771721 + r771722 ************************** correct code so that
	  the key appears translated change stretch so that the start
	  string fits

2008-02-06 22:11 +0000 [r771789]  aacid

	* branches/KDE/4.0/kdegames/kspaceduel/topwidget.cpp: backport
	  r771787

2008-02-08 21:46 +0000 [r772479]  dimsuz

	* branches/KDE/4.0/kdegames/klines/themes/CMakeLists.txt,
	  branches/KDE/4.0/kdegames/klines/scene.h,
	  branches/KDE/4.0/kdegames/klines/renderer.cpp,
	  branches/KDE/4.0/kdegames/klines/renderer.h,
	  branches/KDE/4.0/kdegames/klines/scene.cpp,
	  branches/KDE/4.0/kdegames/klines/README.themes,
	  branches/KDE/4.0/kdegames/klines/mwidget.cpp: Backport from
	  trunk: add support for border element in themes for klines

2008-02-09 16:14 +0000 [r772866]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/newgamedlg.cc,
	  branches/KDE/4.0/kdegames/konquest/gameview.cc: Backport the fix
	  for bug 157528 to KDE 4.0 BUG: 157528

2008-02-09 17:16 +0000 [r772891]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/mapscene.cc,
	  branches/KDE/4.0/kdegames/konquest/mapscene.h,
	  branches/KDE/4.0/kdegames/konquest/mapitems.cc,
	  branches/KDE/4.0/kdegames/konquest/gameview.cc: Clean up a bit
	  the drawing code, but it's impossible to fix bug 157531...

2008-02-09 20:09 +0000 [r772950]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/mapscene.cc,
	  branches/KDE/4.0/kdegames/konquest/mapscene.h,
	  branches/KDE/4.0/kdegames/konquest/mapitems.cc,
	  branches/KDE/4.0/kdegames/konquest/gameview.cc,
	  branches/KDE/4.0/kdegames/konquest/gamelogic.cc: Backport fixes
	  from trunk...

2008-02-18 21:04 +0000 [r776818]  lueck

	* branches/KDE/4.0/kdegames/ksquares/src/ksquares.desktop: make the
	  docs visible in khelpcenter

2008-02-20 19:23 +0000 [r777501]  coolo

	* branches/KDE/4.0/kdegames/klines/klines.cpp: backporting
	  highscore dialog

2008-02-20 21:47 +0000 [r777546]  bostrom

	* branches/KDE/4.0/kdegames/bovo/gui/scene.cc: Backport bug fix for
	  158111, game crash on new game when hint is shown

2008-02-26 12:16 +0000 [r779526]  coolo

	* branches/KDE/4.0/kdegames/kpat/main.cpp: don't try to be clever
	  about session management

2008-02-26 19:13 +0000 [r779663]  ducroquet

	* branches/KDE/4.0/kdegames/konquest/newgamedlg.cc: Backport fix
	  for bug 158465 to KDE 4.0

