------------------------------------------------------------------------
r1217033 | scripty | 2011-01-26 02:52:52 +1300 (Wed, 26 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1220674 | jriddell | 2011-02-15 04:51:42 +1300 (Tue, 15 Feb 2011) | 6 lines

Backport revision 1220673
Don't mark as changed when adding new job options, causes UI not to be filled
(will be marked as changed when editing the options instead)
BUG:254672


------------------------------------------------------------------------
r1222514 | scripty | 2011-02-25 03:30:26 +1300 (Fri, 25 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
