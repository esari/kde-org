2005-11-26 15:25 +0000 [r483453]  azehende

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmshell.cpp: fixed save
	  as in menu and render dialog (fix for last commit from binner)
	  BUG: 116895 BUG: 117075

2005-11-28 22:42 +0000 [r483971]  mueller

	* branches/KDE/3.5/kdegraphics/kooka/kookaimage.cpp: unbreak
	  compilation

2005-12-01 22:20 +0000 [r484782]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/presentationwidget.h: Remove
	  hack no longer needed at Lubos demand CCMAIL:l.lunak@suse.cz

2005-12-01 22:34 +0000 [r484787]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.h: Fix for
	  CAN-2005-3193 based on
	  ftp://ftp.foolabs.com/pub/xpdf/xpdf-3.01pl1.patch

2005-12-02 18:36 +0000 [r485055]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/pageView.cpp: Start in
	  continuous viewmode when continuous viewmode is selected. BUG:
	  117348

2005-12-04 13:07 +0000 [r485447]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp: commit patch to fix
	  117658

2005-12-04 18:52 +0000 [r485521]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc: Fix
	  typo produced when merging CAN-2005-3193 fix

2005-12-05 18:53 +0000 [r485802-485801]  kebekus

	* branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/djvurenderer.cpp:
	  fixes problems with saving DJVU files

	* branches/KDE/3.5/kdegraphics/kdvi/psgs.cpp,
	  branches/KDE/3.5/kdegraphics/kdvi/fontpool.cpp,
	  branches/KDE/3.5/kdegraphics/kdvi/special.cpp: fixes problems
	  with PSLATEX and references to non-existent PS files

2005-12-07 18:34 +0000 [r486398]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/kmultipage.cpp,
	  branches/KDE/3.5/kdegraphics/kdvi/kdvi_multipage.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/kviewshell.kcfg,
	  branches/KDE/3.5/kdegraphics/kviewshell/kmultipage.h,
	  branches/KDE/3.5/kdegraphics/kviewshell/tableOfContents.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/kviewpart.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/tableOfContents.h:
	  Backport commits 483452 and 485437 Save state of the sidebar
	  between sessions and on plugin switch.

2005-12-09 12:54 +0000 [r487066-487065]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: correct
	  misapplied hunk for CVE-2005-3193 fix

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/goo/gmem.c: fix integer
	  overflow in gmallocn and friends

2005-12-09 18:26 +0000 [r487190]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JPXStream.cc: never
	  ending story of xpdf integer overflows

2005-12-11 20:10 +0000 [r487750]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Page.cc: cropBox can
	  not be bigger than mediaBox Thanks to Peter Linnell (mrdocs) from
	  the Scribus Team for helping with the debugging BUGS: 118110

2005-12-13 11:14 +0000 [r488142]  cartman

	* branches/KDE/3.5/kdegraphics/kpdf/part.rc: Correct the name of
	  Tools menu else we end up with two menus with the same name when
	  kpdf is embedded inside konqi

2005-12-13 16:44 +0000 [r488232]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: protection
	  against the newest xpdf exploits of the day

2005-12-15 15:29 +0000 [r488715]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/JBIG2Stream.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Stream.cc: next round
	  of xpdf fixes..

2005-12-18 21:44 +0000 [r489480]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Page.cc: Better fix
	  for 118110, this one works with
	  http://infidigm.net/~jeff/page-size.pdf Thanks to Jeff Muizelaar
	  for warning my fix was halfwrong

2005-12-19 15:36 +0000 [r489722-489720]  mueller

	* branches/KDE/3.5/kdegraphics/kpovmodeler/pmpovrayparser.cpp,
	  branches/KDE/3.5/kdegraphics/kpovmodeler/pmobjectsettings.cpp:
	  fix badly placed ';''s

	* branches/KDE/3.5/kdegraphics/kghostview/kgvdocument.cpp: avoid
	  compiler warning

2005-12-19 19:47 +0000 [r489794]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kpdf/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kruler/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kghostview/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kcoloredit/index.docbook:
	  documentation updates (menus, checked guiitems, fixd bugs,
	  sanitized) CCMAIL:tsdgeos@yahoo.es CCMAIL:titus.laska@gmx.de
	  CCMAIL:lauri@kde.org

2005-12-20 23:59 +0000 [r490169]  mueller

	* branches/KDE/3.5/kdegraphics/kfile-plugins/raw/parse.c: fix
	  strict aliasing issue

2005-12-21 22:43 +0000 [r490452]  mhunter

	* branches/KDE/3.5/kdegraphics/kviewshell/kmultipage.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/djvumultipage.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/kprintDialogPage_DJVUconversionoptions_basewidget.ui:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-22 10:38 +0000 [r490540]  kebekus

	* branches/KDE/3.5/kdegraphics/kviewshell/kviewpart.cpp: fixes
	  crash when konqui is closed with kviewshell embedded

2005-12-22 12:49 +0000 [r490586]  goutte

	* branches/KDE/3.5/kdegraphics/kpovmodeler/hi48-app-kpovmodeler.png,
	  branches/KDE/3.5/kdegraphics/kghostview/hi64-app-kghostview.png,
	  branches/KDE/3.5/kdegraphics/kghostview/hi128-app-kghostview.png:
	  Remove the svn:executable property from a few standard
	  non-executable files (PNG, JPEG, DocBook, HTML, Makefile.am)
	  SVN_SILENT

2005-12-22 12:56 +0000 [r490589]  scripty

	* branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.h,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameraconfigdialog.cpp,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/pageRangeWidget.cpp,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameraconfigdialog.h,
	  branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/pageRangeWidget.h,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameradevice.cpp,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGTextElement.h,
	  branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.cpp,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGPolygonElement.h,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGLineElement.h,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kameradevice.h,
	  branches/KDE/3.5/kdegraphics/kamera/kioslave/kamera.h,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGPolylineElement.h,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGRectElement.h,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGEllipseElement.h,
	  branches/KDE/3.5/kdegraphics/ksvg/dom/SVGCircleElement.h,
	  branches/KDE/3.5/kdegraphics/kamera/kcontrol/kamera.cpp: Fix FSF
	  address (mainly old address) (goutte)

2005-12-23 10:28 +0000 [r490817]  tokoe

	* branches/KDE/3.5/kdegraphics/ksnapshot/regiongrabber.cpp: Fixed
	  region grab with dual head setup BUGS: 72118 106868 102648

2005-12-23 11:20 +0000 [r490834]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kpdf/configure.png (added):
	  added kpdf screenshot

2005-12-23 13:16 +0000 [r490866]  tokoe

	* branches/KDE/3.5/kdegraphics/ksnapshot/ksnapshot.cpp,
	  branches/KDE/3.5/kdegraphics/ksnapshot/ksnapshotwidget.ui.h:
	  Disable delay option when using region grabbing. BUGS:118332

2005-12-29 11:09 +0000 [r492242]  thurston

	* branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_soften_sharpen.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/tool_rectangles.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/eraser_shapes.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/line_width.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/brush_shapes.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/color_box.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_balance.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/kolourpaint-main.png
	  (added),
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_emboss.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_reduce_colors.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_flip.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_flatten.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_rotate.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/spraycan_patterns.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/view_thumbnails.png
	  (added),
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/selections_opaque_transparent.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/tool_selections.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/fcc_trans_text.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/fill_style.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/fcc_std_text.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_invert.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_resize_scale.png,
	  branches/KDE/3.5/kdegraphics/doc/kolourpaint/image_skew.png:
	  Improved screenshots of More Effects and Thumbnail. Ctrl and
	  Shift modifiers for zooming and aspect ratio respectively.

2005-12-29 21:09 +0000 [r492406]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kiconedit/kiconedit-configuration.png
	  (added), branches/KDE/3.5/kdegraphics/doc/kview/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kiconedit/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kview/kview-viewer-configuration.png
	  (added),
	  branches/KDE/3.5/kdegraphics/doc/kview/kview-application-configuration.png
	  (added): documentation update (menus, checked guiitems,
	  sanitized) BUG:96387

2005-12-29 22:22 +0000 [r492429]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kuickshow/index.docbook:
	  documentation update (menus, checked guiitems, sanitized)
	  BUG:96387

2006-01-01 15:57 +0000 [r493109]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/shell.cpp:
	  setautosavesettings takes care of that

2006-01-05 23:47 +0000 [r494666]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/DCTStream.cc: Fix DCT
	  decoding for broken files BUGS: 119569

2006-01-08 14:34 +0000 [r495613]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kurlwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/aboutwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/ChangeLog,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h: - fix
	  crash when deleting the last image without having a browser - fix
	  deleting images from image window and browser - support moving to
	  trash and make that the default - refactor delayed execution of
	  events/actions (for when the browser window needs to be loaded
	  lazily) - use F5 as default shortcut for "Reload image", keep
	  Enter as alternative - fixed crash (right-clicking on about
	  widget) - made the about widget not always-on-top, but a modal
	  widget, which prevents error messages from being hidden below the
	  about widget (i.e. when clicking the homepage link while being
	  offline) The new strings in kuickshow.cpp are reused from kdelibs
	  (kdiroperator.cpp) BUG: 111586 BUG: 110663 BUG: 106689 BUG:
	  103667 BUG: 99163 CCBUG: 101848

2006-01-08 14:38 +0000 [r495619]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp: Add
	  "don't show again" to "Can't load image" messagebox. BUG: 115490

2006-01-15 11:33 +0000 [r498292]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: 1.4.1_relight
	  for KDE 3.5.1: only real change is Thurston's docs update

2006-01-15 11:40 +0000 [r498294]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: 0.5.1

2006-01-17 20:02 +0000 [r499429-499428]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/CharCodeToUnicode.cc:
	  Fix for 120310 The check that was checking the length of some
	  strings was not having into account \n or \r that can be there
	  BUGS: 120310

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/XRef.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Parser.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Parser.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.cc,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/Lexer.h,
	  branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/XRef.cc: Fix for kpdf
	  is very slow on some kind of very broken docs like
	  http://www.serialata.org/docs/serialata10a.pdf

2006-01-18 21:57 +0000 [r499855]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Make non-exisant ps
	  file loading fail gracefully BUGS: 120343

