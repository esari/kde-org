---
title: Plasma 6.2.4 complete changelog
version: 6.2.4
hidden: true
plasma: true
type: fulllog
---

{{< details title="breeze-gtk" href="https://commits.kde.org/breeze-gtk" >}}
+ Checkbox: Apply indeterminate after checked. [Commit.](http://commits.kde.org/breeze-gtk/8f5600b18a2f062719342fc240cc9a4e56d9dbbf) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Hide irrelevant stuff for Flatpak runtimes. [Commit.](http://commits.kde.org/discover/f8681aa9128607887ae6e5ae2e404efe0d7e6815) Fixes bug [#496330](https://bugs.kde.org/496330). Fixes bug [#496329](https://bugs.kde.org/496329)
+ 🍒 LabelBackground: improve text readability. [Commit.](http://commits.kde.org/discover/c2a5e5e2d84960014ac462dda0ced8ea0a823592) Fixes bug [#492656](https://bugs.kde.org/492656)
+ Update: disable quitting on eventloop locker. [Commit.](http://commits.kde.org/discover/78cce3a269bbbe3f7b41755a20561426e68f8559) Fixes bug [#471548](https://bugs.kde.org/471548)
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ Update selected app when activating the running KCM. [Commit.](http://commits.kde.org/flatpak-kcm/4426bde7d17768391b8ffda90ddc5a0b5bdf4c11) 
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ Hide desktop file for krdpserver. [Commit.](http://commits.kde.org/krdp/44194efce775dbf4992ae4b437addf44f85ef938) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Plugins/cpu: Use simpler names when CPU core info is missing. [Commit.](http://commits.kde.org/ksystemstats/1516c31bad2225f1f45b0822f552188bfca6a7bf) 
+ Plugins/cpu: Test for the proper CPU property, skip nonexistent CPUs. [Commit.](http://commits.kde.org/ksystemstats/23f8291aec1c1123e240187280174c336dc9df57) Fixes bug [#495524](https://bugs.kde.org/495524)
{{< /details >}}

{{< details title="kwayland-integration" href="https://commits.kde.org/kwayland-integration" >}}
+ Initialize xdg activation. [Commit.](http://commits.kde.org/kwayland-integration/5a47aa1159d31c1998d3a2addde2266226a2cb23) See bug [#496365](https://bugs.kde.org/496365)
+ Fix obtaining wl_surface from WId. [Commit.](http://commits.kde.org/kwayland-integration/1432f93ed23dd73f7f9a9606da4bb5cf531a8ed9) Fixes bug [#496365](https://bugs.kde.org/496365)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Cancel interactive move resize only on Wayland. [Commit.](http://commits.kde.org/kwin/dccd3ccea85d36024db14af516251c3222dd6017) 
+ Backends/drm: reject cursor updates already in beginFrame. [Commit.](http://commits.kde.org/kwin/0539f18f7afcadbe95db991f85db6eb550d65041) See bug [#495843](https://bugs.kde.org/495843)
+ Compositor_wayland: don't commit cursor changes if the layer wasn't actually enabled. [Commit.](http://commits.kde.org/kwin/04b329a14a783f3c80c14b8413586e9a725531f3) Fixes bug [#495843](https://bugs.kde.org/495843)
+ Backends/drm: don't set the dpms mode to AboutToTurnOff if the screen is already off. [Commit.](http://commits.kde.org/kwin/13f4fd84e41388872cf125a3c78b50e53fb54389) 
+ Layers: add null check. [Commit.](http://commits.kde.org/kwin/686b20f4ba5b8c07421a825ba63e763ee1bf8982) 
+ Outputconfigurationstore: fix choosing the default mode. [Commit.](http://commits.kde.org/kwin/8d8ef0f9cafa64e9135af8e7134b16fdd6cb119e) 
+ Plugins/screencast, screenshot: switch color transforms to relative colorimetric. [Commit.](http://commits.kde.org/kwin/34cf899dedcd9fe8dbab16cf21c88a77f49d9949) Fixes bug [#496185](https://bugs.kde.org/496185)
+ Effects/overview: Properly map the windowHeap geometry. [Commit.](http://commits.kde.org/kwin/9996ff08bd1c577e7ba8c36a6e65e84a739df9af) 
+ Effects/overview: Don't make thumbnails fly off the screen. [Commit.](http://commits.kde.org/kwin/48b965b5c6c99360b65d12397127db862542dbfc) Fixes bug [#495444](https://bugs.kde.org/495444)
+ Core: Set object ownership for Output. [Commit.](http://commits.kde.org/kwin/faf476225b918d3c41fb7f7522230b518b0cab73) 
+ Backends/drm: re-allow HDR on NVidia with driver version 565.57.01+. [Commit.](http://commits.kde.org/kwin/c37a17f0bc2d544c151875071058e6f1fcdfb6ef) See bug [#488941](https://bugs.kde.org/488941)
+ Backends/drm: re-allow HDR on Intel by default. [Commit.](http://commits.kde.org/kwin/a1c094af29d3931a1a812ac667d0b4e1fc09361a) 
+ Revert "scene: Ignore xwayland window shape". [Commit.](http://commits.kde.org/kwin/89ef2bfb470321ea43b3d8a7383dc2378e3280ec) Fixes bug [#493934](https://bugs.kde.org/493934)
+ Tiling: fix some asserts from scripts. [Commit.](http://commits.kde.org/kwin/74083a9809f96b39ad5df0e39317cd65e8d29339) 
+ Utils: Use QList::removeLast() in DamageJournal::add(). [Commit.](http://commits.kde.org/kwin/1630dd49adbafee5f6ce86fc202185afbe24a214) 
+ Ensure active window isn't focused when screen is locked. [Commit.](http://commits.kde.org/kwin/f90ffedf14dff85c55030c2c465d166c30c7be14) Fixes bug [#495325](https://bugs.kde.org/495325)
+ Cancel interactive move resize when outputs change. [Commit.](http://commits.kde.org/kwin/7315ff49dc27d660e55481edf7b5a6fa544192c8) 
+ Backends/drm: fix DrmGpu::needsModeset check with leased outputs. [Commit.](http://commits.kde.org/kwin/2d543f4a790b93c465caf2eb3504cb2915477924) Fixes bug [#495400](https://bugs.kde.org/495400)
+ Effect/offscreenquickview: ensure the view that accepts touch down also gets touch up. [Commit.](http://commits.kde.org/kwin/e628a6a69e97ee8c7d7e6e1fd12a59c9ed7b3a29) 
+ Workspace: fix the dpms input event filter sometimes being wrongly deleted. [Commit.](http://commits.kde.org/kwin/a3a92c7b8cf52706cdc29fbaf9e36ddebcc2adcf) 
+ Close layer shell window if its preferred output has been removed. [Commit.](http://commits.kde.org/kwin/c4c0f6f14228f0812ac0a744d40b15beb7869749) 
+ Activation: don't activate windows that don't accept keyboard input. [Commit.](http://commits.kde.org/kwin/1518659f96b39056bd073efd75eaf81a14169a49) Fixes bug [#495537](https://bugs.kde.org/495537)
+ Tabbox: Do not add windows that have modal children. [Commit.](http://commits.kde.org/kwin/da63dc0941c21e4f0a016e0f2f7127a831d0d369) 
+ Core/colorspace: fix the max luminance of linear. [Commit.](http://commits.kde.org/kwin/14db57fc56d16b8d4deee5f33f4b2564db0ee630) Fixes bug [#494930](https://bugs.kde.org/494930)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Doctor: clarify the meaning of max. brightness zero. [Commit.](http://commits.kde.org/libkscreen/7660ca1b6ccd2beeaaf40f91f8fd64ec3e6bb704) Fixes bug [#495557](https://bugs.kde.org/495557)
+ Update Plasma Wayland Protocols to 1.14. [Commit.](http://commits.kde.org/libkscreen/d8d24eba101bc72dc4c96d4856530d6a2f6ea24e) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Faces: Ensure the temporary dir for a preset remains until installed. [Commit.](http://commits.kde.org/libksysguard/9ea3571c903db1ebe72d5091eefc052288af252f) Fixes bug [#485164](https://bugs.kde.org/485164)
+ Faces/piechart: Use GraphicalEffects.Glow for rendering outline of compact text. [Commit.](http://commits.kde.org/libksysguard/6e164d04743823fca6392ed9cce9e75acd537a0c) Fixes bug [#494495](https://bugs.kde.org/494495)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ FlatButtonBackground: Fix background disappearing when pressing checkable ToolButton. [Commit.](http://commits.kde.org/libplasma/7db3766687fba33fce6369291f7c37cfff3b7fed) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fill panel width/height in custom/fit content modes when flexible spacer is in panel. [Commit.](http://commits.kde.org/plasma-desktop/8789c9c2a847d2e1c83b8de7b915cf9888e68937) Fixes bug [#495378](https://bugs.kde.org/495378)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Popupnotifications: a bit of cleanup and bugfixes. [Commit.](http://commits.kde.org/plasma-mobile/f774ea289cfa4a8117ee6c50f1566ac75a4d5058) 
+ Lockscreen: handle backspace and enter key when switching from keypad. [Commit.](http://commits.kde.org/plasma-mobile/c0b7e1620c9c936807d59e03925d6fa41b8b0e8a) 
+ Homescreen/folio: fix icon drop position misalignment within folder and outside folder view check not accounting for size differences. [Commit.](http://commits.kde.org/plasma-mobile/f584e5c587df60acfd3a96a67a23f0c1976cf14a) 
+ Revert "envmanager: Replace convergentwindows script with kwin rules". [Commit.](http://commits.kde.org/plasma-mobile/7512eb8f07f5f723f0be22fcaf38154fb85e3ab3) 
+ Envmanager: Replace convergentwindows script with kwin rules. [Commit.](http://commits.kde.org/plasma-mobile/3aacd851f0267714e7272a5566624d0aa3bb7302) 
+ Kwin/convergentwindows: Ignore case with undefined desktop. [Commit.](http://commits.kde.org/plasma-mobile/06fa6be6d7578b0b08bd05cf3d8ffffc810816ad) 
+ Notifications: Implement popup notifications. [Commit.](http://commits.kde.org/plasma-mobile/c8179fc621032ef211767b550deaf4dd2b5d57f8) 
+ Suppress alpine CI on Plasma/6.2 since the KWin dep version changed. [Commit.](http://commits.kde.org/plasma-mobile/f4b369757ef5e2785ecdecfa13fbbd94620311e2) 
+ Fix accidental string freeze break. [Commit.](http://commits.kde.org/plasma-mobile/5dad710923e1c1fd588103da6f5d3412de775356) 
+ Widgets/krunner: Modernize design to match app drawer, and add section headers. [Commit.](http://commits.kde.org/plasma-mobile/d5fc825d12e7a344607d4aceb993f1ab3aebf8e4) 
+ Actiondrawer: hide action drawer while adjusting screen brightness. [Commit.](http://commits.kde.org/plasma-mobile/28c0db682d28dee8fc95738143d3dbc1772789cb) 
+ Envmanager: Ensure KWin is reloaded. [Commit.](http://commits.kde.org/plasma-mobile/5691491debb1d204a7ebc817ae3388b5024eb190) 
+ Navigationpanel: Add manual screen rotation button. [Commit.](http://commits.kde.org/plasma-mobile/807adbda33c5ff26356a20a43332ca5f56e7e568) 
+ Homescreen/folio: Fix folder apps still being Interactable when outside current page. [Commit.](http://commits.kde.org/plasma-mobile/156afe4f000b3dbed746ce903a708c83f167d205) 
+ Homescreens/folio: Homescreen folder page snapping position bugfix. [Commit.](http://commits.kde.org/plasma-mobile/792b9ab60746fa15b27bd6969f24069ddfa393be) 
+ Mobileshellstate: update isActionDrawerOpen to use intendedToBeVisible. [Commit.](http://commits.kde.org/plasma-mobile/b915516baca27465cb71036c9319806fa2fc0ee6) 
+ Quicksettings/audio: Close action drawer when opening volume osd. [Commit.](http://commits.kde.org/plasma-mobile/b31f85125d9428ab8f8ad1bdc0b9218c4528c3e7) 
+ Mobileshell: Cherry pick of inputregion change. [Commit.](http://commits.kde.org/plasma-mobile/367ec8610f2868bcf025802ce81191edb6899a4e) 
+ VolumeOSD: Increase animation duration and bugfix for audio applet page. [Commit.](http://commits.kde.org/plasma-mobile/c8098102f47ac42b5d1d4a5a81addfa1a1af7bb2) 
+ VolumeOSD: Prevent Stealing Focus from Applications. [Commit.](http://commits.kde.org/plasma-mobile/660fe89d0b510f90a35debe2025ee405356f62db) 
+ VolumeOSD: Improve design, and prevent touch events from being taken from outside the osd. [Commit.](http://commits.kde.org/plasma-mobile/75d363bf68bcedf19042ac810e88872758e735a7) 
+ Actiondrawer: use action drawer window as the open surface area. [Commit.](http://commits.kde.org/plasma-mobile/efede0ab88452345d96d61093cf482a1e3559bc7) 
+ Kwin/convergentwindows: keep current window maximized bugfix. [Commit.](http://commits.kde.org/plasma-mobile/bc10be29c2bbe91bbaf44830f850ec5a02e6a05b) 
+ Actioncenter: Allow expanded mode to be opened as soon as minimized offset crossed. [Commit.](http://commits.kde.org/plasma-mobile/37a84db711c82571405e5a62966bf2dee3e03a9f) 
+ Screenbrightnessplugin: Fix behaviour if dbus service is not initialized followup. [Commit.](http://commits.kde.org/plasma-mobile/97de07406800643025d9ac41dd67a3b128f92ee2) 
+ Screenbrightnessplugin: Fix behaviour if dbus service is not initialized. [Commit.](http://commits.kde.org/plasma-mobile/807d87bce9a07354220c0e4148df7e3cc8c24be6) 
+ Revert "homescreens: Use WheelHandler on app list". [Commit.](http://commits.kde.org/plasma-mobile/d250899268aa2af5e83e324b2bcd6d2a18d1f61e) 
+ Homescreen/folio: Fix favourites bar reordering when full. [Commit.](http://commits.kde.org/plasma-mobile/f1fc3ae309ac72c81a8c1dd2bd8f0230caca7da9) 
+ Homescreens/folio: Fix goToPage call not updated with new parameter. [Commit.](http://commits.kde.org/plasma-mobile/974b2acb6ce8d70781da0e6ca6fb187769ef189c) 
+ Quicksettings/screenrotation: Nullptr check. [Commit.](http://commits.kde.org/plasma-mobile/16a86ade9a11b95c276c300275ca6940c1d771cb) 
+ Homescreens/folio: Fix favourites bar ghost position when drag out. [Commit.](http://commits.kde.org/plasma-mobile/847c84ffe943dfa50865efc24e0f01fbabbd1c2d) 
+ Quicksettings/autorotate: Properly listen to external changes. [Commit.](http://commits.kde.org/plasma-mobile/f6d099627ff8ab3ebbf45ea2ea9da21bb15bd8c7) 
+ Actiondrawer: Use layershell to avoid taking focus from current window. [Commit.](http://commits.kde.org/plasma-mobile/1386d86efe42489c41dcff96fde7775c5aeb8b6e) 
+ Homescreens/folio: FavouritesBar: Icon position animation bugfix. [Commit.](http://commits.kde.org/plasma-mobile/6e2c5c7bbc5e3de728bb050e962f0d36b7ba55b2) 
+ Homescreens/folio: Homescreen page snapping position Bugfix. [Commit.](http://commits.kde.org/plasma-mobile/110462e711a1d62271d07c9943d135a0c6e69b2f) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ ConnectivityMonitor: Don't manually set m_notification to null. [Commit.](http://commits.kde.org/plasma-nm/3ace302d69ecaecc8e1285fc45859a4208010e20) 
+ Create a random-default password for hotspots if one is not set. [Commit.](http://commits.kde.org/plasma-nm/9a651635de4366a74ffe9dcde55a99a82256a467) 
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Listitemmenu: guard activePort against null. [Commit.](http://commits.kde.org/plasma-pa/49925e2a9598f27a526534dc2b4b1bc0bcee639b) See bug [#496067](https://bugs.kde.org/496067)
+ Kcm: Fix speaker test layout for Pro-Audio profile. [Commit.](http://commits.kde.org/plasma-pa/0b4a26ada8258f4d25fa5069f239f1041eefecb5) Fixes bug [#495752](https://bugs.kde.org/495752)
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ EditablePage: Set loadOverlay margins explicitly, set loader invisible during loading. [Commit.](http://commits.kde.org/plasma-systemmonitor/86bb7928461d10553ba09179895b15abf25f6aea) Fixes bug [#494902](https://bugs.kde.org/494902)
+ BaseCellDelegate: Use correct colorSet when selected. [Commit.](http://commits.kde.org/plasma-systemmonitor/13911268973ddde2304229a6565ce8429ff48ccd) Fixes bug [#494261](https://bugs.kde.org/494261)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ BatteryIcon: Add headphone icon. [Commit.](http://commits.kde.org/plasma-workspace/c9f787e96dc4c2b62ceb80403b706abd26539794) 
+ Plasmashell: begin earlier to watch for panel destruction. [Commit.](http://commits.kde.org/plasma-workspace/434e13d01e0e9c07548e2a46c0be45050720bba7) Fixes bug [#487684](https://bugs.kde.org/487684)
+ Plasmawindowed: Use shared contexts analogue to plasmashell. [Commit.](http://commits.kde.org/plasma-workspace/e8454b7b4458668a788158fed07ca559d4022878) See bug [#495758](https://bugs.kde.org/495758)
+ Applets/notifications: Be explicit about "when" binding on notificationAction. [Commit.](http://commits.kde.org/plasma-workspace/6abcbac464468c02d94aa284454646948e8b2354) 
+ Devicenotifier: An optical disc can also be storage access. [Commit.](http://commits.kde.org/plasma-workspace/d9c88f0313cfa8660db05a0b7b95c41c32b5db48) Fixes bug [#469706](https://bugs.kde.org/469706)
+ Ksmserver: Fix copying of auth data. [Commit.](http://commits.kde.org/plasma-workspace/8ee3defdf81e9b5ad3edef498d04fca0f4289e4a) Fixes bug [#491130](https://bugs.kde.org/491130)
+ Applets/systemtray: suppress useless tooltips. [Commit.](http://commits.kde.org/plasma-workspace/75bf470e650b5596f65af05508dcf780cfcf728e) Fixes bug [#494463](https://bugs.kde.org/494463)
+ X11/multiscreen: Always emit outputOrderChanged when a screen is disconnected. [Commit.](http://commits.kde.org/plasma-workspace/eb1896d2df36e7f6945df8d3c615a924a15cb536) See bug [#484838](https://bugs.kde.org/484838)
+ Coloreditor: Hide titlebar color buttons if header is set. [Commit.](http://commits.kde.org/plasma-workspace/47257619ae54a83397e2879b409cfeac45c4dba8) Fixes bug [#433059](https://bugs.kde.org/433059)
+ Devicenotifier: Fix ejecting busy optical media. [Commit.](http://commits.kde.org/plasma-workspace/00e0729545d61809f603fb06eb51159447558239) 
+ Panelview: Fix resized panel with NoBackground not receiving input in. [Commit.](http://commits.kde.org/plasma-workspace/3bda249d62527a033a2ebae3eef0551a87f24eba) 
+ Wallpapers/image: Fix wallpaper item resize for screen dimension change. [Commit.](http://commits.kde.org/plasma-workspace/46c6e3d64ad3d928f4c84f61f03a07b8e2a930d0) 
+ Kcms/users: refactor cropping. [Commit.](http://commits.kde.org/plasma-workspace/104b09248def5a3b02ee703e1e2ba01635bafff1) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Add dedicated headset and headphone icon for low battery notification. [Commit.](http://commits.kde.org/powerdevil/263f7899df3d17def92faf23e0da7b16a561a62c) 
+ Applets/brightness: toggle nightlight only, when enabled. [Commit.](http://commits.kde.org/powerdevil/82338a361f5eb8a9da4e6d9967d7a6901e651a90) Fixes bug [#496457](https://bugs.kde.org/496457)
+ Applets: allow to detect tlp without solid or upower. [Commit.](http://commits.kde.org/powerdevil/19c49043aba60687891b1edafe28e50c73df9783) Fixes bug [#495918](https://bugs.kde.org/495918)
+ Applets/brightness: Make brightness of multiple applets stay in sync. [Commit.](http://commits.kde.org/powerdevil/1197dc8a936b46578560f87764d98e37db0c6a4c) Fixes bug [#495661](https://bugs.kde.org/495661)
{{< /details >}}

{{< details title="print-manager" href="https://commits.kde.org/print-manager" >}}
+ DevicesModel: Register QList of QStringList to DBus. [Commit.](http://commits.kde.org/print-manager/2440d1b96b7af95259867b22ba8b4ba677ef0ade) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Fix displaying combo box content from abstract item models. [Commit.](http://commits.kde.org/qqc2-breeze-style/1e9bb7e2d10a4299023bba1b38ed88cb2e03088f) 
{{< /details >}}

{{< details title="Spacebar" href="https://commits.kde.org/spacebar" >}}
+ Fix infinite spinner showing on empty chats list. [Commit.](http://commits.kde.org/spacebar/aac545dd9e9967f920ab0e80537affcf68211f47) 
{{< /details >}}

