---
title: Plasma 6.1.4 complete changelog
version: 6.1.4
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Only chop off leading @ from tokenized license text if needed. [Commit.](http://commits.kde.org/discover/4edc093462d867084bca5ea1de708878bae2e4cf) Fixes bug [#491172](https://bugs.kde.org/491172)
+ Flatpak: Make sure we don't call versionCompare with a null resource. [Commit.](http://commits.kde.org/discover/354e0179b8e2071d2a04a76f67cef3d668b4eefb) 
+ Flatpak: Ensure we are caching the right icons. [Commit.](http://commits.kde.org/discover/3bbbf560665404e57b23fbf7d855128df7779fb6) Fixes bug [#490786](https://bugs.kde.org/490786)
+ Category: assert names aren't duplicated. [Commit.](http://commits.kde.org/discover/9188ab187ff428b473c82cd3e13dc85963b04363) See bug [#481303](https://bugs.kde.org/481303)
+ ApplicationPage: Do not use Item.visible to calculate the visibility of the parent. [Commit.](http://commits.kde.org/discover/15a277e189cdeedb8feb32428c1375d386f9a9dc) Fixes bug [#489745](https://bugs.kde.org/489745)
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Use frameworks version number from kcrash. [Commit.](http://commits.kde.org/drkonqi/2fd5d8b31c2e5a6b085e7d75f9574d42aff54606) 
+ Sentry: also send qt_version in the tags. [Commit.](http://commits.kde.org/drkonqi/ed1f25ab60ed51984ee7b8726992287920211918) 
+ Support consuming the qt version from kcrash. [Commit.](http://commits.kde.org/drkonqi/3203ec7f4e0c59854441493c99d301f08ba2d8c8) 
+ Coredump: synthesize crash metadata for kwin_x11. [Commit.](http://commits.kde.org/drkonqi/1ce48418d0b88fdaa616d714eac3445d9825f808) 
{{< /details >}}

{{< details title="kgamma" href="https://commits.kde.org/kgamma" >}}
+ Fix wrong types/obsolete keys for kcm_kgamma.json file. [Commit.](http://commits.kde.org/kgamma/43ed422ecf2ffaf59311bdddf698c3d4b742a9bd) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Sourcestream: make resilient against pipewire restarts. [Commit.](http://commits.kde.org/kpipewire/31552a8e7f01600588fe48d809859302b76e5415) 
+ Add encoder using libopenh264. [Commit.](http://commits.kde.org/kpipewire/bdb1152e85fdfb7456c6aa65cf41fe0f375fe30b) Fixes bug [#476187](https://bugs.kde.org/476187)
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ AbstractSession: Only start encodedStream if it's ready. [Commit.](http://commits.kde.org/krdp/6e5991192d5aa04d7b4b2cb131b3235a8e2ba4bc) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Emit OrientationSensor::availableChanged() when needed. [Commit.](http://commits.kde.org/kscreen/779c1f56d363f5f8afe7d71def5cd2d5699f8347) Fixes bug [#488764](https://bugs.kde.org/488764)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Greeter/pamauthenticators: also consider non-interactive info messages a "prompt". [Commit.](http://commits.kde.org/kscreenlocker/b8fa6b49c5c4fb3452c9008dca53aa2b55991096) Fixes bug [#490547](https://bugs.kde.org/490547)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Activation: fix X11 windows being stuck in should_get_focus more properly. [Commit.](http://commits.kde.org/kwin/949f32de96f3d79ee657f71201d3f8613c00bd14) 
+ Plugins/buttonrebinds: correctly handle level 1 keys. [Commit.](http://commits.kde.org/kwin/2dc6c88c5dba47a9966d2d645d8bbdcbb8d1538c) Fixes bug [#484367](https://bugs.kde.org/484367)
+ Backends/drm: don't block direct scanout if color profile source isn't set to ICC. [Commit.](http://commits.kde.org/kwin/2d1fd41501898e6d25ec3e3e542f625b5c894ac0) 
+ Fix checking whether GraphicsBufferView is null. [Commit.](http://commits.kde.org/kwin/6bca4b075e24067e9ad2e9829829316d2c5d4c9d) 
+ Backends/drm: don't apply the brightness factor without HDR. [Commit.](http://commits.kde.org/kwin/5e81c2199baf0c1cf53eac67d7f6355e93abb83f) 
+ Activation: don't add the active window to should_get_focus list. [Commit.](http://commits.kde.org/kwin/f509ff21c1c74e1e345ec56d6b41bb57d01ccd54) Fixes bug [#484155](https://bugs.kde.org/484155)
+ Core/renderloop: don't move the target presentation timestamp back when rescheduling. [Commit.](http://commits.kde.org/kwin/7b8f24698f797143531f0d5723cc6c263b9175e8) Fixes bug [#488843](https://bugs.kde.org/488843)
+ Core/renderloop: fix triple buffering hysteresis. [Commit.](http://commits.kde.org/kwin/aad82e9c772d52e853e7fa36a97620e41172df95) See bug [#488843](https://bugs.kde.org/488843)
+ Scene/itemrenderer_opengl: reset OpenGL state for YUV conversion back to RGB. [Commit.](http://commits.kde.org/kwin/6fef9821fb280af955e10d9b31be2e752cd24d16) 
+ Autotests: test placement to always put the titlebar on the screen. [Commit.](http://commits.kde.org/kwin/68c11e79c010236bb8c8aebe29e00f2008194b04) See bug [#489500](https://bugs.kde.org/489500)
+ Placement: keep the titlebar in the screen with centered placement. [Commit.](http://commits.kde.org/kwin/a962b30e9c43614a4140dd53019fc6288ae701f2) Fixes bug [#489500](https://bugs.kde.org/489500)
+ Placement: don't overwrite scheduled position change in cascadeIfCovering. [Commit.](http://commits.kde.org/kwin/8a5ac138ce2a93407a5ca64583d6fd89072b06f9) See bug [#489500](https://bugs.kde.org/489500)
+ Make Workspace::desktopResized() reassign outputs of uninitialized windows. [Commit.](http://commits.kde.org/kwin/65073ce6796fb6033929b5ce413612148eb7908e) See bug [#489632](https://bugs.kde.org/489632)
+ Wayland: Avoid klipper loop with existing but empty clipboards. [Commit.](http://commits.kde.org/kwin/c91040d3deee8a2028562451fa056c002f35ae42) Fixes bug [#469644](https://bugs.kde.org/469644)
+ Backends/drm: Fix a crash in DrmGpu::releaseBuffers(). [Commit.](http://commits.kde.org/kwin/921e76d87f40b502320aaa20c2085d58957e562b) 
+ Wayland: Ignore plasma shell reposition requests during interactive move resize. [Commit.](http://commits.kde.org/kwin/1066046283bbd8441da78c297b0bfd28f47f9e5c) Fixes bug [#481829](https://bugs.kde.org/481829)
+ Backends/drm: limit max_bpc to 8 by default with docks. [Commit.](http://commits.kde.org/kwin/56301045f63c545ab2c0863c817e1df427056fb4) 
+ Compositor_wayland: count rendering time for all steps of compositing. [Commit.](http://commits.kde.org/kwin/d7385d441417d8f43cfb09341b5c0ae449ccd219) See bug [#488782](https://bugs.kde.org/488782)
+ Xdgshellwindow: never request clients to resize to a negative size. [Commit.](http://commits.kde.org/kwin/52b8a0bb7c29732a44cf2710db4481ba926aebc2) Fixes bug [#489983](https://bugs.kde.org/489983)
+ Backends/libinput: Ignore redundant events for pointer buttons and keyboard keys when pressed/released on multiple devices. [Commit.](http://commits.kde.org/kwin/73c8b3a2ddbb1fc3d50c0215967db325844be1ba) Fixes bug [#486034](https://bugs.kde.org/486034)
+ Backends/x11: Fix crash that happens when toggling compositing. [Commit.](http://commits.kde.org/kwin/7c27d1d1826dd0e282519125f19d5d78a608e847) 
+ Plugins/stickykeys: Unlatch modifiers when locking. [Commit.](http://commits.kde.org/kwin/f9f39c03a02d92ea4b3ae07b684d4f2a93b9a6ed) 
+ Fix sticky keys for AltGr. [Commit.](http://commits.kde.org/kwin/0374acb1d82e827c254eeb1444cb0ce15aee635c) See bug [#444335](https://bugs.kde.org/444335)
+ Test locking sticky keys for all modifiers. [Commit.](http://commits.kde.org/kwin/95e39061ff1a9070a7632a2c5d607efb79bdf7b4) 
+ Release key in sticky key test. [Commit.](http://commits.kde.org/kwin/58db58aa7e86b0980a811ec98f36163ca86a3e8c) 
+ Utils: Fix gaining realtime scheduling with musl. [Commit.](http://commits.kde.org/kwin/2b7882fbbc734101b097b7b7b019197386769eb1) Fixes bug [#487996](https://bugs.kde.org/487996)
+ Core/renderloop: add some hysteresis to triple buffering. [Commit.](http://commits.kde.org/kwin/ce1ba4252552c7f314149e8be87d5d63fca0210c) See bug [#488843](https://bugs.kde.org/488843)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Make X-KDE-Init-Phase a proper in in JSON and remove obsolete X-KDE-Init-Symbol values. [Commit.](http://commits.kde.org/plasma-desktop/2bd50444207e9fd62bd98df11806670a62270017) 
+ Folder View: hide existing label while renaming. [Commit.](http://commits.kde.org/plasma-desktop/3a0cc5449fc36ad9530dd300e8c7d5d2463c4b9b) Fixes bug [#482802](https://bugs.kde.org/482802)
+ Folder View: re-transfer focus to grid view after rename. [Commit.](http://commits.kde.org/plasma-desktop/db0d9c7721061679957dcd60a4391d4a40cee03a) Fixes bug [#491088](https://bugs.kde.org/491088)
+ Use bindings for anchors. [Commit.](http://commits.kde.org/plasma-desktop/dd7531e287e1268ef4801634eda7d8207821d9d2) Fixes bug [#489492](https://bugs.kde.org/489492)
+ Restore the folderview title config. [Commit.](http://commits.kde.org/plasma-desktop/522ab0b91c5776c6cde72d2e0cb8df455496358a) 
+ Applets/taskmanager: fix size of textual list popup. [Commit.](http://commits.kde.org/plasma-desktop/63c2e83d15c8154ed36c7da0e20e3431f49d662a) 
+ Applets/taskmanager: Fix applet's layout size with multiple rows. [Commit.](http://commits.kde.org/plasma-desktop/e1c824a2c36ea3fa8e972341e0273a40171b83dc) Fixes bug [#490319](https://bugs.kde.org/490319)
+ [kcms/access] Set range for visual bell duration selector. [Commit.](http://commits.kde.org/plasma-desktop/46f671005130b69184912d432817f0ce2124c9c5) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Homescreens/folio: Fix settings saving for certain options. [Commit.](http://commits.kde.org/plasma-mobile/929534e87d66c9e40347b4c6518c05c69a054dfc) 
+ Homescreens/folio: Ensure applets list closes when settings is closed. [Commit.](http://commits.kde.org/plasma-mobile/9fe0c6d3622900b40bb05a94eed5d5b724843606) 
+ Homescreens/halcyon: Fix favorites delegate not activating with touch. [Commit.](http://commits.kde.org/plasma-mobile/e301e2dae35d964aba4b47e59f583290571754bf) Fixes bug [#486554](https://bugs.kde.org/486554)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Volumemonitor: don't crash when there is no pa_context. [Commit.](http://commits.kde.org/plasma-pa/ed1a2b4038fe1324049dfb05ee59dc6028be0d35) 
+ Kcm/SpeakerTest: Fix the minimum layout for Mono channel. [Commit.](http://commits.kde.org/plasma-pa/d2b7b6a0c524ba50529a319a9a9e97a0c1c7137c) Fixes bug [#476096](https://bugs.kde.org/476096)
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ TableViewHeader: remove from tab chain. [Commit.](http://commits.kde.org/plasma-systemmonitor/7125bff6edfcf973905e93584c11ec13686affec) See bug [#490929](https://bugs.kde.org/490929)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Watch for Look and Feel changed. [Commit.](http://commits.kde.org/plasma-workspace/c0bf16b7e066586ba57b05d3896bf2a12425d321) 
+ Applets/batterymonitor: Fix property names to show the right tool-tip text. [Commit.](http://commits.kde.org/plasma-workspace/31aa7a781eb575bba0968040a7e02425a9fcc41b) 
+ Applets/mediacontroller: Fix label width overflow in horizontal panel. [Commit.](http://commits.kde.org/plasma-workspace/926f9a9161d466cc37f901332624a241fd23ccd0) 
+ Track screen change. [Commit.](http://commits.kde.org/plasma-workspace/2092fe8f2fada49ce23a52be722866c8cd98911d) 
+ Respect centered images size. [Commit.](http://commits.kde.org/plasma-workspace/f480c5eb8a8dd7ea30cdfc6930452ef7c46d9212) Fixes bug [#490425](https://bugs.kde.org/490425). Fixes bug [#489250](https://bugs.kde.org/489250)
+ Xembed-sni-proxy: Check if descendant windows want button events. [Commit.](http://commits.kde.org/plasma-workspace/bf145579129e363cf14064bb369260c303b9ae23) See bug [#490666](https://bugs.kde.org/490666)
+ Freespacenotifier: Rework to not warn for read only partitions. [Commit.](http://commits.kde.org/plasma-workspace/63eae504d0dd132890909013e98a853e07f0e76a) 
+ Startplasma: Also set environment variable when it is not currently set. [Commit.](http://commits.kde.org/plasma-workspace/a413e7d985bcd0b54dea247556aa21328ffcd43b) Fixes bug [#490432](https://bugs.kde.org/490432)
+ Notifications: When doing a drag and drop, set the supported action to cpoy. [Commit.](http://commits.kde.org/plasma-workspace/cf41b532579cb010fac218e186cf3df8968f6bbf) Fixes bug [#469644](https://bugs.kde.org/469644)
+ Krunner: Fix icon property name for additionalActions list. [Commit.](http://commits.kde.org/plasma-workspace/bb95cef6bf93870951e3c21758072839363c7b0c) 
+ Kastatsfavoritesmodel: Fix a crash. [Commit.](http://commits.kde.org/plasma-workspace/fa57f929efb6307103e7d74bcb3c3737decb8a2c) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Daemon: Don't leave dangling Action pointers in idle-time containers. [Commit.](http://commits.kde.org/powerdevil/8c1686c9e97edb9a06e06e2f41cfe5351cef7986) Fixes bug [#490356](https://bugs.kde.org/490356). Fixes bug [#490421](https://bugs.kde.org/490421)
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix visual window artifact. [Commit.](http://commits.kde.org/sddm-kcm/919ab7326e4c61bb94519e0e0b81491d1f057dfc) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Actually start the KAuth::ExecuteJob. [Commit.](http://commits.kde.org/systemsettings/cb930d8f16dc969b7f57eaabf4a51c33cc740ac7) Fixes bug [#490507](https://bugs.kde.org/490507)
+ Runner: Don't match if just one query word matches. [Commit.](http://commits.kde.org/systemsettings/4f22c7378dae2d8c0545a8ef85cd20a6244dea53) Fixes bug [#488676](https://bugs.kde.org/488676)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Set up translation for FileDialog. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e8d03504c27a18e2b961a972083d60288d2af82d) 
+ Set timeout on DBus calls to KWin. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/806da604cb9e428140a27f84d254a09232d3e5af) 
+ Inhibit: Forward the correct inhibition flags to PolicyAgent. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/8fd86c5b0e6d6797bff850947b07e086544cd617) Fixes bug [#486506](https://bugs.kde.org/486506). See bug [#472541](https://bugs.kde.org/472541). See bug [#335729](https://bugs.kde.org/335729)
+ Remotedesktop: Only show the restore checkbox if the app wants to persist. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/2fa4cb52313b8a4d91d3525b398f7419d96ce9c6) See bug [#490666](https://bugs.kde.org/490666)
{{< /details >}}

