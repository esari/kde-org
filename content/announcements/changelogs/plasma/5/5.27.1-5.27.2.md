---
title: Plasma 5.27.2 complete changelog
version: 5.27.2
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Don't claim 3rd-party repos are part of the OS on Debian derivatives. [Commit.](http://commits.kde.org/discover/3fb783b4fdb95a326307e053c87d54c1f5489b84) 
+ Flatpak: correctly check for the appstream-qt version. [Commit.](http://commits.kde.org/discover/8253392341d590a6b090c798608322d0e1512846) 
+ ApplicationPage: Improve narrow layout with lots of buttons. [Commit.](http://commits.kde.org/discover/e7acc5058141c515be1451d6c08bd02413ce6f1a) 
+ Pk: Properly check for AppStream versions. [Commit.](http://commits.kde.org/discover/63eb667c8b0a768060ca315fc04a2084aca660f2) 
+ Fwupd: do fwupd_client_connect before setting user agent. [Commit.](http://commits.kde.org/discover/76e99d8fe8794b1f49f35f4c348dd2abc793c316) 
+ Fix rendering Missing Backends. [Commit.](http://commits.kde.org/discover/dfa99d53872ae35bdf21348c387a12aaeb9e8ce7) 
+ Pk: Fix searching by state. [Commit.](http://commits.kde.org/discover/5dadd14ff12b8a6afae2c557d5948c5c66e1898c) 
+ Screenshots: Only use AnimatedImage if we think there's a chance. [Commit.](http://commits.kde.org/discover/1ce185b8c82b4b915d813978761a519e3cbf29a1) 
+ Always show distro name for PackageKit apps. [Commit.](http://commits.kde.org/discover/e6c03350a4fcd393814154f10f82f67b9d63d018) Fixes bug [#465204](https://bugs.kde.org/465204)
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Add plasma-welcome to mappings file. [Commit.](http://commits.kde.org/drkonqi/e04f013931783c7e1ee7cce5d2a74a811b76c09a) 
+ Scroll bug description. [Commit.](http://commits.kde.org/drkonqi/74f538b6e7806f530aba3c335778839ad015046f) Fixes bug [#466180](https://bugs.kde.org/466180)
{{< /details >}}

{{< details title="Flatpak Permissions" href="https://commits.kde.org/flatpak-kcm" >}}
+ Adjust popup modals for the footer padding in KCM. [Commit.](http://commits.kde.org/flatpak-kcm/7c0bd6d7fab50aa76a20a2d4e1e94ee268fb9754) 
+ UI: Adapt "Apply / Discard" dialog to narrow screens: turn row layout into column. [Commit.](http://commits.kde.org/flatpak-kcm/7f743d0311505deda26e13111b15ff0d155ed011) 
+ Plasma/5.27: Revert partially last commit due to string freeze. [Commit.](http://commits.kde.org/flatpak-kcm/a46516bc5a6b746672fad4a8f49fc7cad34fe157) 
+ UI: Add changed app's name & icon to the "Apply / Discard" dialog. [Commit.](http://commits.kde.org/flatpak-kcm/d1728977515644c4b319e65a01d19715f6a7c6cd) 
+ UI: Reparent app switching dialog, so that it is centered to the whole view. [Commit.](http://commits.kde.org/flatpak-kcm/bb24b979ac4ff9330de77f16363c62261ed10a8f) 
+ Use standard QQC2 namespace for Dialog constants. [Commit.](http://commits.kde.org/flatpak-kcm/af1c291e6b92714de480c6be36c06f4c079d2c56) 
+ UI: Dynamically create "Apply / Discard" dialog when switching apps. [Commit.](http://commits.kde.org/flatpak-kcm/4539547d0abe4eba62e3395d02d792b73e31908c) 
+ Move app changing logic out of delegate, fetch ref directly from model. [Commit.](http://commits.kde.org/flatpak-kcm/03f195ea353f0ca522e49aacc76eb74612d2d653) 
+ UI: Don't try to reload app when clicking on the current one. [Commit.](http://commits.kde.org/flatpak-kcm/3a8760d2c5eab1a7d12066894473e7d7faea4d02) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Avoid creating gtkrc-2.0 if it does not exist. [Commit.](http://commits.kde.org/kde-gtk-config/85bfea563102e9a149c54ff420c78972387d73f7) Fixes bug [#415770](https://bugs.kde.org/415770). Fixes bug [#417534](https://bugs.kde.org/417534)
{{< /details >}}

{{< details title="KMenuEdit" href="https://commits.kde.org/kmenuedit" >}}
+ Fix crash when cutting an item that was dragged to the root. [Commit.](http://commits.kde.org/kmenuedit/8a3a31f43662e41717916d7a3c0ebe2c3629992b) Fixes bug [#466242](https://bugs.kde.org/466242)
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ PipewireSourceItem: Expose stream state. [Commit.](http://commits.kde.org/kpipewire/8647db4ab4258389cf2c3151bad0885146f23bc4) 
+ Finish the recording when the last frame has arrived. [Commit.](http://commits.kde.org/kpipewire/1390b6e9b3468a2fcc5eb132a58f9568fb723e33) 
+ Export logging categories where they belong. [Commit.](http://commits.kde.org/kpipewire/46a5a29a53448e2be707c6fd3ce8e0781e1ed670) 
+ Sourcestream: Allocate the buffer outside together with the pods. [Commit.](http://commits.kde.org/kpipewire/5fc04847ac1e114b059a47d50a202ea598c5b5f2) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Kcm: Guard against config being null in checkConfig(). [Commit.](http://commits.kde.org/kscreen/f219615dd17f061a104daa2a2902ae3b48b2b97b) Fixes bug [#464707](https://bugs.kde.org/464707)
+ Kcm: use onRejected to handle reject button click. [Commit.](http://commits.kde.org/kscreen/b65ae40b7d7fe4a5e56f3510f92fbd197ab369ab) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ X11: Fix client area lookup with pending move resize. [Commit.](http://commits.kde.org/kwin/6f7f7ed9dbb73f7e257fe7bc6e02bb0ef15308b7) 
+ Wayland: Fix xdg-toplevel and xdg-popup window initialization. [Commit.](http://commits.kde.org/kwin/c6b91e901af73f7cbc063fddef2719f175af178f) Fixes bug [#466530](https://bugs.kde.org/466530)
+ Screencasting: Choose the correct GL type to download into an image. [Commit.](http://commits.kde.org/kwin/548ccc80361b513a2748e55f30a43d9676d08695) Fixes bug [#466299](https://bugs.kde.org/466299)
+ Screencasting: Close streams when the output is disabled. [Commit.](http://commits.kde.org/kwin/d30b7e49abc4f5dec3b735cecd050d250aa50559) 
+ Wayland: Setup compositing for internal window when it's mapped. [Commit.](http://commits.kde.org/kwin/0d0474117ddf8024e7c5a2239154aadedf739597) Fixes bug [#466313](https://bugs.kde.org/466313)
+ Remove no longer relevant case in Workspace::replaceInStack(). [Commit.](http://commits.kde.org/kwin/98e01c4f4424511fd496a650a67315a0a010d043) 
+ Effects/zoom: Fix crash on X11. [Commit.](http://commits.kde.org/kwin/7bfd30279830737b245ae41fde6b1b9fe1861ae9) Fixes bug [#466376](https://bugs.kde.org/466376)
+ Fix edid serial parsing. [Commit.](http://commits.kde.org/kwin/fb59d975b5377f858b94dbfe333256ec9c4bfd6d) Fixes bug [#466136](https://bugs.kde.org/466136)
+ Effects/zoom: Fix rendering with mixed scale factors. [Commit.](http://commits.kde.org/kwin/554ad9ecda800bd850358c6a9a524d43fb77285d) 
+ Effects/magnifier: Reduce the number of heap allocations. [Commit.](http://commits.kde.org/kwin/dc465ec6ab78bc53de62efe472e32eb6f39f443e) 
+ Effects/magnifier: Fix rendering on multi screen setups. [Commit.](http://commits.kde.org/kwin/28fddf3bce23715c63c81beb86b7b462ba5f05b4) 
+ Backends/drm: Make sure attributes are always initialized. [Commit.](http://commits.kde.org/kwin/764f6d26cde7ad173696037fe258c0cf60453ec2) 
+ Backends/wayland: fall back to qpainter when there's no render node. [Commit.](http://commits.kde.org/kwin/b97e6f6c73317fef0570aa3fe504fb745e8a6a5a) Fixes bug [#466302](https://bugs.kde.org/466302)
+ Backends/x11: explicitly free the outputs. [Commit.](http://commits.kde.org/kwin/24a59dd2fe386df1c26626cd1d81c306b5ee5313) Fixes bug [#466183](https://bugs.kde.org/466183)
+ Backends/libinput: Fix crash upon receiving motion absolute events. [Commit.](http://commits.kde.org/kwin/6970199ccc6ff54badcbf2b33a22aada89c164cc) Fixes bug [#449317](https://bugs.kde.org/449317). Fixes bug [#465456](https://bugs.kde.org/465456)
+ Pointer input: always confine pointer to screens. [Commit.](http://commits.kde.org/kwin/5abb038a3c4e2a36a6d5ad3d8d792eaf7bdbcd91) Fixes bug [#461911](https://bugs.kde.org/461911)
+ Effects: Fix selecting hidden windows using keyboard. [Commit.](http://commits.kde.org/kwin/7ea6506d61cb741e18e944bbf74b485c439b9631) Fixes bug [#466120](https://bugs.kde.org/466120)
+ Feat: allow VT switching even with global shortcuts disabled. [Commit.](http://commits.kde.org/kwin/cb8c4d5b3d2e80604690c91aac6119d9b0d26499) 
+ Scene: Ensure there's current opengl context when DecorationItem is repainted for last time. [Commit.](http://commits.kde.org/kwin/be772c7fe3b91aaeb268ba2050977644d1d2435f) Fixes bug [#466186](https://bugs.kde.org/466186)
{{< /details >}}

{{< details title="layer-shell-qt" href="https://commits.kde.org/layer-shell-qt" >}}
+ Use the QScreen of the QWindow as default output. [Commit.](http://commits.kde.org/layer-shell-qt/ddc4aead870e3c29653acecfe3324ee94b6eea0c) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Revert "fix(randr): screen is dirty when switching display modes". [Commit.](http://commits.kde.org/libkscreen/b274575c23a7cccb1482da4e680826f2b9823e71) Fixes bug [#462725](https://bugs.kde.org/462725)
+ Dpms/wayland: properly manage org_kde_kwin_dpms instances. [Commit.](http://commits.kde.org/libkscreen/30e87ab054497034e663cb4f8455a5a7c147ffe1) 
+ Dpms/wayland: Explicitly set as unsupported if addRegistryListener didn't find the interface. [Commit.](http://commits.kde.org/libkscreen/1515b6533ffd23218d674ff4222218f846d415c4) Fixes bug [#466181](https://bugs.kde.org/466181)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ Do not run match automatically when query string changed. [Commit.](http://commits.kde.org/milou/2d5f08e955c1bca2ab53f41769d705f1c2e800e2) Fixes bug [#459859](https://bugs.kde.org/459859)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Kcms/keys: Don't show visual feedback on press for sole delegates. [Commit.](http://commits.kde.org/plasma-desktop/6b6ed3da9d1e3b978516d6682d81c444f118d0ab) 
+ Folder View: fix scrollbar overlapping list view items. [Commit.](http://commits.kde.org/plasma-desktop/4e6eff892000d159b282afcec53df7135e0602b7) Fixes bug [#465980](https://bugs.kde.org/465980)
+ Add an ui to remove desktop containments. [Commit.](http://commits.kde.org/plasma-desktop/755781e008823e253fb3dc29e5bfe3a818196ba7) 
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Fix exporting window id on Wayland. [Commit.](http://commits.kde.org/plasma-integration/073fac253f155b9e483af3b170dd5c1196c5b463) 
+ Codify runtime dependency on xdg-desktop-portal-kde. [Commit.](http://commits.kde.org/plasma-integration/bc1c5d66828429904ea9820154b72307d26a8529) Fixes bug [#466148](https://bugs.kde.org/466148)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Use themable icon for System Settings. [Commit.](http://commits.kde.org/plasma-welcome/3aa63aaee8371de8b87dd833daf621284ad57de3) Fixes bug [#466250](https://bugs.kde.org/466250)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Sddm: Focus something useful when switching between alternative login screens. [Commit.](http://commits.kde.org/plasma-workspace/fd67273f299f340c5e225f5f505c8559a0712066) 
+ Applets/systemtray: add test for xembed tray icon. [Commit.](http://commits.kde.org/plasma-workspace/bdf07c16a6b330b0b5af28dcc5fe63f841077be2) 
+ Libtaskmanager: consider current activity when computing first task index. [Commit.](http://commits.kde.org/plasma-workspace/47b66c7147bdc559e5ecdbfa16c56b7771272b83) See bug [#386055](https://bugs.kde.org/386055)
+ Libtaskmanager: test invalid preferred launchers are filtered out. [Commit.](http://commits.kde.org/plasma-workspace/664f8bbd31cbce6f3616a9cd7c0b6e693b026e6c) See bug [#436667](https://bugs.kde.org/436667)
+ Libtaskmanager: use icon name directly when icon value does not contain period. [Commit.](http://commits.kde.org/plasma-workspace/198f9c2ac13e45163d88120c9d4d05d0f91f1222) 
+ Kcms/users: Fallback to show username in title when real name isn't set. [Commit.](http://commits.kde.org/plasma-workspace/3cc5883cb383479374c222022bb3b801244f500a) 
+ Kcms/users: Limit connection scope to `this`. [Commit.](http://commits.kde.org/plasma-workspace/f0a945eb8b977a1a08d927f5c6913ce232f895f1) 
+ Add an action to remove the containments of a screen. [Commit.](http://commits.kde.org/plasma-workspace/3e776410d2e735da07f4cd789be817c5d5306fba) 
+ Libtaskmanager: filter out invalid preferred launcher tasks. [Commit.](http://commits.kde.org/plasma-workspace/df7bc69d9f38ccd58137775019eec90db6d26dc7) Fixes bug [#436667](https://bugs.kde.org/436667)
+ Shell: Have the DesktopView::title include which output it should be on. [Commit.](http://commits.kde.org/plasma-workspace/3ebce773b24a6a8d34d64fa494428a74cb1ee486) 
+ Kcms/color: Add highlight outline for color dots. [Commit.](http://commits.kde.org/plasma-workspace/a54df5f8be7bd9c2570a0dc53bec81c1ec20da53) Fixes bug [#465800](https://bugs.kde.org/465800)
+ Lock screen: Prevent Escape key from displaying UI if it's currently hidden. [Commit.](http://commits.kde.org/plasma-workspace/e1fa127278d3dc470141fe1cbefdf2ee0e393872) Fixes bug [#465920](https://bugs.kde.org/465920)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Cmake: Fix capitalisation in the find_package_handle_standard_args() call. [Commit.](http://commits.kde.org/powerdevil/602e175f81c805c81d58b4f2752b6cbd2d024c8b) 
+ Ddc: Fix cleaning up handles when we are redetecting screens. [Commit.](http://commits.kde.org/powerdevil/4a1bae9ecc65ccb476b811f3a0150636511492f1) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Sidebar: Replace OpacityAnimator with NumberAnimation for proper fade in. [Commit.](http://commits.kde.org/systemsettings/b0c635cc2584caf9902ff45e65352a0e3aa5028c) 
+ Sidebar: Prevent placeholder empty search results from jumping as it fades out. [Commit.](http://commits.kde.org/systemsettings/b75c2eb5886224035340d3ce33efc28615732bc7) 
{{< /details >}}

