---
title: Plasma 5.26.5 complete changelog
version: 5.26.5
hidden: true
plasma: true
type: fulllog
---
{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Libdiscover: fix permission text not being translated. [Commit.](http://commits.kde.org/discover/327f8ec98ae37697b1b0c4388c4a27244012e8c9) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ There is no translation for nativeText, use sequence instead. [Commit.](http://commits.kde.org/kdeplasma-addons/056806216ea74b0e31bcc2bf6315a8f86b59e704) Fixes bug [#463775](https://bugs.kde.org/463775)
+ Applets/notes: Close context menu on left click. [Commit.](http://commits.kde.org/kdeplasma-addons/fb5e0a87c8ee7addc334ecb3fa3c086afb6e763a) Fixes bug [#463530](https://bugs.kde.org/463530)
+ Wallpapers/potd: update picture at 0:01. [Commit.](http://commits.kde.org/kdeplasma-addons/02679ace137ad7a05d60c0f1239800b26c964379) Fixes bug [#463345](https://bugs.kde.org/463345)
+ Wallpapers/potd: use `i18ndc` to specify translation domain. [Commit.](http://commits.kde.org/kdeplasma-addons/006d751eac83750a81b69ccff50bcc658704d1c2) Fixes bug [#463103](https://bugs.kde.org/463103)
+ KateSessionsItemDelegate: Fix reference to signal parameter. [Commit.](http://commits.kde.org/kdeplasma-addons/3978569850d26527e7d177a11ac389348c70668b) Fixes bug [#462452](https://bugs.kde.org/462452)
+ Alternatecalendar: always use day name in full label. [Commit.](http://commits.kde.org/kdeplasma-addons/d3f0524126224dd9c49d345ff79cef7d00398834) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Fix KSignalHandler initialization order. [Commit.](http://commits.kde.org/kscreenlocker/a5afb4f6e16ed4684011098845ab2d259125f16f) 
+ Fix crash condition where kscreenlocker_greet doesn't start handle signals in time. [Commit.](http://commits.kde.org/kscreenlocker/a1d60aeb2072849dcf2e701949d0a897ecfdd35a) 
+ Fix. [Commit.](http://commits.kde.org/kscreenlocker/c2bebe02f83f00f27531a0ce58cea2caae268bd8) 
+ Fix wallpaper not loading (leaving the background black). [Commit.](http://commits.kde.org/kscreenlocker/50fc20dc940c87ffd62be0650b935324e164b59d) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Include missing cmath header for std::round. [Commit.](http://commits.kde.org/kwin/8a633ec6d22976277e3db18306cd77590b527c40) 
+ Backends/drm: extend connector lifetime to match their outputs. [Commit.](http://commits.kde.org/kwin/3a5eda5fe98f64d94f3d8e796e3cfba4f289c257) Fixes bug [#463334](https://bugs.kde.org/463334)
+ Ignore critical notifications in window heap effects. [Commit.](http://commits.kde.org/kwin/b52d2733c4f832011f7b3c0dd2b2b9802a231d5d) Fixes bug [#463437](https://bugs.kde.org/463437)
+ Workspace: don't make the placeholder output too big. [Commit.](http://commits.kde.org/kwin/dafb220e55b0ac1b13b19a6f4e9741a9947fd29b) See bug [#461848](https://bugs.kde.org/461848)
+ Kwinglutils: Restore the default FB when the last FBO is unbound. [Commit.](http://commits.kde.org/kwin/ad95798d2afb7f1eb523cf6dcb76265f9ccc74c9) 
+ Dmabuf: Create buffers without the SCANOUT flag. [Commit.](http://commits.kde.org/kwin/f5fd8a1edd1da12bc59ae412b972d7a14233d641) 
+ Screencast: Don't skip the last valid modifier. [Commit.](http://commits.kde.org/kwin/2cdb8e2628bb359467a97881fd4f4207aab36c2e) 
+ Wayland: Fix updating pixel data from translucent client buffers when using qpainter backend. [Commit.](http://commits.kde.org/kwin/4e6b7c26143f23c50133f6d9265c0fb13a196b69) 
+ Backends/drm: fix VRR detection. [Commit.](http://commits.kde.org/kwin/5cea93a4ef607866a1d9d3ea4790ec64d02e1d89) Fixes bug [#463172](https://bugs.kde.org/463172)
+ Backends/drm: fix error in finding the best plane. [Commit.](http://commits.kde.org/kwin/cd4dd11342c0c2376bce9d5e2ddb559cde0f808c) 
+ Wayland: Require to reboot computer after changing primary selection option. [Commit.](http://commits.kde.org/kwin/e72294d8bff83c9296546d1e1dee2979113fcd86) 
+ Revert "Disable PrimarySelection in seat setPrimarySelection". [Commit.](http://commits.kde.org/kwin/07195af2b02a044f1ef39bf0b3c26aab58e19dca) Fixes bug [#461498](https://bugs.kde.org/461498)
+ Scene: Fix a typo in WindowItem::updateShadowItem(). [Commit.](http://commits.kde.org/kwin/63b8e7c6f99cdaee0912f31383de2ef2a291e7a9) 
+ Workspace: handle duplicate output hashes correctly. [Commit.](http://commits.kde.org/kwin/e47d3d248fcf81685705f5066889700ce744dc08) 
+ Core/outputconfiguration: use std::weak_ptr for storing the mode. [Commit.](http://commits.kde.org/kwin/803c6c064d129ad99cad279c3d598211425c6c46) 
+ Workspace: match KScreen when computing the hash without an edid. [Commit.](http://commits.kde.org/kwin/86daf1e6edee895eab3dbb9950a9510eef913423) 
+ Backends/drm: don't break crtc<->plane connections. [Commit.](http://commits.kde.org/kwin/54a1858316b350b8ee3767d756f516f30b4a5b04) Fixes bug [#462214](https://bugs.kde.org/462214)
+ Effects/blendchanges: ignore fullscreen windows. [Commit.](http://commits.kde.org/kwin/a1e6b2d4f8f8b2dc54c448b94c882942e0433950) 
+ Wayland/outputmanagement: reject configurations if outputs change. [Commit.](http://commits.kde.org/kwin/fe84371cd05a4b2974122506e1711a9fb1e612ca) Fixes bug [#460953](https://bugs.kde.org/460953)
+ Backends/drm: don't leak gbm surface if creating egl surface fails. [Commit.](http://commits.kde.org/kwin/da425106e6d4154a1b1dd4b8ae74230f08ba0ad0) 
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Backends/kwayland: use the connector name. [Commit.](http://commits.kde.org/libkscreen/0cd03a95c90ac4fcac45c6fc46e56fe9c2d8306e) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Fix Kirigami longDuration reference. [Commit.](http://commits.kde.org/plasma-desktop/028d58c9c6010eb4d46245a30a26bc302bb6f5db) 
+ Applets/taskmanager: use empty array when artist does not exist. [Commit.](http://commits.kde.org/plasma-desktop/c52921873f6171f616320e73e760ab648d0f0a4a) 
+ Applets/taskmanager: `xesam:artist` is a stringlist. [Commit.](http://commits.kde.org/plasma-desktop/e4e77458077190c6c811a248f06fcd345127a1ae) 
+ Kcm/keys: don't allow collapsing single shortcut ation list items. [Commit.](http://commits.kde.org/plasma-desktop/753eff38aa2adb47bf1fbd848ef5004b7cbd37da) Fixes bug [#462141](https://bugs.kde.org/462141)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Lockscreen: Remove wallpaper workaround. [Commit.](http://commits.kde.org/plasma-mobile/b5ec1dab6d629f94e41a022beba04cf16ed51f41) 
+ Actiondrawer: Use simpler and more performant widescreen panel logic without clipping. [Commit.](http://commits.kde.org/plasma-mobile/d505d7c463d26009e996c93380319a459060ad35) 
+ Actiondrawer: Introduce fix for black text even when opacity is 0. [Commit.](http://commits.kde.org/plasma-mobile/1d2b1ff8e8c8d8530c74211b61228ade421f58a3) 
+ Actiondrawer: Use Expo animations for pane movement. [Commit.](http://commits.kde.org/plasma-mobile/9d71c323e253d28f361055b457f577b45e34a938) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Fix connecting to WPA3-personal networks. [Commit.](http://commits.kde.org/plasma-nm/7f29979bef7bb591822fdaef5fafa96663568a2c) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Fix time input bug caused by "or" in regex. [Commit.](http://commits.kde.org/plasma-workspace/66a8c134277791c53bc212578e4979f4db171f83) Fixes bug [#415028](https://bugs.kde.org/415028)
+ Wallpapers/image: fix finding the real symlink target. [Commit.](http://commits.kde.org/plasma-workspace/fcf12846368d088429ddeb254cb2e4654822f44b) Fixes bug [#461940](https://bugs.kde.org/461940)
+ Applets/systemtray: Fix copy-paste mistake (double assignment to width). [Commit.](http://commits.kde.org/plasma-workspace/f9fe329f21f83af81e6e7b1fbe34138120ca62f5) 
+ Fix building screenpooltest. [Commit.](http://commits.kde.org/plasma-workspace/299ec0bc70e7a226de636cc1e4c386e7d17d31b1) 
+ Panel: Add floating margins to avoid overflowing content when floating. [Commit.](http://commits.kde.org/plasma-workspace/12c258d531deb2fb6700e320d87463df1c948664) Fixes bug [#462130](https://bugs.kde.org/462130)
+ Find in system's path and sudo's path. [Commit.](http://commits.kde.org/plasma-workspace/a9d92efac4ee9061c0b040471ddfbd08640e7c1e) Fixes bug [#462127](https://bugs.kde.org/462127)
+ Kcms/region_language: set fix scrolling in language sheet. [Commit.](http://commits.kde.org/plasma-workspace/812fee152712a8b8775ffc5397460852c9b82dad) Fixes bug [#462057](https://bugs.kde.org/462057)
+ Revert "shell: Use the basic scene graph rendering loop on wayland". [Commit.](http://commits.kde.org/plasma-workspace/2e144eb73178ffa8a0db423410733e1322c93884) 
+ Revert "shell: Don't force basic render loop for Plasma Mobile". [Commit.](http://commits.kde.org/plasma-workspace/cb78ad83d677c12496bfbb5c3de65cede5d3e42c) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Do not hardcode ACPI battery names. [Commit.](http://commits.kde.org/powerdevil/2d3289ef452a674bd9e587d72842442673aae54c) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ ScrollView: Enable clip. [Commit.](http://commits.kde.org/qqc2-breeze-style/d3f5d1e31cd4641c471a48e83c22a7e8a4375760) 
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ A11y: make categoryitem interactable. [Commit.](http://commits.kde.org/systemsettings/680d517a2027790f947571c2fbbaea8267fcc4a9) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Notifications: Properly communicate the triggered action. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4f8df470f8b3b9df1844d606a05b3c83cce88772) Fixes bug [#462278](https://bugs.kde.org/462278)
+ Notification: Trigger org.freedesktop.Application.Activate* as spec'd. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/511bced812cc0615dfa8b827a1c108db4b3e919f) See bug [#462278](https://bugs.kde.org/462278)
{{< /details >}}

