---
aliases:
- ../../fulllog_releases-24.12.2
title: KDE Gear 24.12.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Collectionstatisticsdelegate.cpp - Use ActiveText role for unread count. [Commit](http://commits.kde.org/akonadi/d96f43d5f902c4377df8c1efb4d6492ec5f50d8b).
+ Fix build with Qt 6.10 (dev). [Commit](http://commits.kde.org/akonadi/2175ed890209ca7e2e167a840241d2d42feaf9eb).
+ Changerecorderjournal: Fix loading tags. [Commit](http://commits.kde.org/akonadi/3025e7ea967d562b223ac988158333961e1b2f73).
+ Optimize load tags. [Commit](http://commits.kde.org/akonadi/0bd5764dc5e89aba8ffbae713b92edebcd9494f6).
+ ItemFetchJob: split up the query to avoid SQL placeholder limit. [Commit](http://commits.kde.org/akonadi/b032b8c1ec483ffedd394c17a057075cd25c615a).
{{< /details >}}
{{< details id="akregator" title="akregator" link="https://commits.kde.org/akregator" >}}
+ Use AKREGATOR_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/akregator/d72bf23dc78e87732913133305cf1d67cf2d0909).
{{< /details >}}
{{< details id="alligator" title="alligator" link="https://commits.kde.org/alligator" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/alligator/e989fedc1b6af3ac39c9db50f24175786835d7d5).
+ Save settings when app suspends. [Commit](http://commits.kde.org/alligator/1792924bf355ab5074f2edaed8e88e9680b66fc8).
{{< /details >}}
{{< details id="analitza" title="analitza" link="https://commits.kde.org/analitza" >}}
+ Analyzer simplificator: Account for unary roots. [Commit](http://commits.kde.org/analitza/ed079065c9f1a66ef71901cf66aa91d6b8e63a2f).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Clear forward action popup menu before adding history actions. [Commit](http://commits.kde.org/dolphin/7ed90b58457f15041511e73a1a5c4d20df85a3c0).
+ Icons: when adding overlays pass in the output size. [Commit](http://commits.kde.org/dolphin/50d44ab177f93ca3acd372f492f4fc3b1015286e). Fixes bug [#498211](https://bugs.kde.org/498211).
+ Information panel: scale according dpr. [Commit](http://commits.kde.org/dolphin/c7e2cb3a5a4a72b1b560153cea8454280f8155a8). Fixes bug [#497576](https://bugs.kde.org/497576).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Use single quotes for string literal in SQL query (fixes FreeBSD CI). [Commit](http://commits.kde.org/elisa/7015597d27f0531488e3a7ca77d456b952a46c5d).
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Contextmenucontext: open terminal at local file path. [Commit](http://commits.kde.org/filelight/5ea963613565b8ed73f4d59bbeacbb4c0af5d397). Fixes bug [#498184](https://bugs.kde.org/498184).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Round temperatures before checking whether we need to show a range. [Commit](http://commits.kde.org/itinerary/2f2f71f940d6246fdc18c3ba33fff09a872c2f57).
+ Allow transfers to favorite location independent of the next reservation. [Commit](http://commits.kde.org/itinerary/605f1731763f1701c6250b827ecfbd21d8849deb).
+ Retain stop geo coordinates when merging realtime data. [Commit](http://commits.kde.org/itinerary/3d48b1c82aabd132203edabaa2b5063070ee36ae).
+ Fix QML syntax for partial alternative journey searches. [Commit](http://commits.kde.org/itinerary/359797e425751eb3a9d1317d6f6b5de1b59b4492).
+ Fix importing passes or other things without an attached trip group. [Commit](http://commits.kde.org/itinerary/6e44978a0cb22f4eb8d8704e9f212c3fdb723a3e).
+ Fix deleting generic pkpass passes. [Commit](http://commits.kde.org/itinerary/ec68edd0a0378f6417c21aa1cacd95357d73606d).
+ Add Appstream source code and donation links. [Commit](http://commits.kde.org/itinerary/93e0b6be5d96837837f8873cd44c4b2968da3381).
+ Add Appstream form factor information. [Commit](http://commits.kde.org/itinerary/46fa840538c69d086b42911a420a0f9e5114df15).
+ Fix map bounding box computation for path-less journeys. [Commit](http://commits.kde.org/itinerary/d52e18a60f142873cbd8833c5d361ed0edde3b02).
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/itinerary/e42a4b7236bdb174d065b78887019169f2ae748d).
+ Bundle icons needed by the latest Kirigami Addons about dialog. [Commit](http://commits.kde.org/itinerary/d2e257b30e2041d51ea403c9dbc8ce286d5cf8f7).
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ [kcm] Port away from SwipeListItem. [Commit](http://commits.kde.org/kaccounts-integration/dac3ece8996d6d034c12c313b1bbaea2c8e7d13f).
+ Wrap list item subtitles. [Commit](http://commits.kde.org/kaccounts-integration/029003b4cf90040aabebe3a787ebdfec06d036db).
{{< /details >}}
{{< details id="kaccounts-providers" title="kaccounts-providers" link="https://commits.kde.org/kaccounts-providers" >}}
+ Google: stop requesting gdrive permission. [Commit](http://commits.kde.org/kaccounts-providers/78a81aafedced524c1bc9f16788c4304acc3b807). Fixes bug [#480779](https://bugs.kde.org/480779).
{{< /details >}}
{{< details id="kaddressbook" title="kaddressbook" link="https://commits.kde.org/kaddressbook" >}}
+ Add missing include. [Commit](http://commits.kde.org/kaddressbook/88acf284ede908c814e1dbcc8aa139fcf718939e).
+ Use KADDRESSBOOK_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/kaddressbook/a3bdcffe4d12894bf5a2b88682849766118eb38b).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix CI fail on FreeBSD. [Commit](http://commits.kde.org/kalarm/00fed3b948e1274c54b0080e2d34b77d22e11d88).
+ Bug 498753: Fix system tray showing blank KAlarm icon under XFCE. [Commit](http://commits.kde.org/kalarm/ec60634309be8efdcd2b5629a79257e570e5f3c1).
{{< /details >}}
{{< details id="kalk" title="kalk" link="https://commits.kde.org/kalk" >}}
+ Fix History feature. [Commit](http://commits.kde.org/kalk/952c54d0872e14ccaf6a6eecaad0128afd1f4634).
{{< /details >}}
{{< details id="kalm" title="kalm" link="https://commits.kde.org/kalm" >}}
+ Appdata: add display size. [Commit](http://commits.kde.org/kalm/3a261ad27e789a7902e27acdc78d2c2506aa131b).
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/kalm/020966f8677bde2d4d8eec6e8db88514e07f48b3).
{{< /details >}}
{{< details id="kamera" title="kamera" link="https://commits.kde.org/kamera" >}}
+ Workaround https://github.com/gphoto/libgphoto2/issues/1077. [Commit](http://commits.kde.org/kamera/ead6ede6b44a5bc3762294cd154d428a984c0d83).
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Fix reuse. [Commit](http://commits.kde.org/kasts/86a55b06c77639579dc77d56c18583a8811f8376).
+ Appdata: add display size. [Commit](http://commits.kde.org/kasts/085640cd55440e70e1f1ed636494c81da1ac7ab0).
+ Fix android build by removing unused string from manifest. [Commit](http://commits.kde.org/kasts/8559695db5d3c7ee7702e71b20f0189823833e51).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Ensure we always stay in range. [Commit](http://commits.kde.org/kate/159de58609a49b6ed48a31728a063f11c04a9e76). Fixes bug [#498435](https://bugs.kde.org/498435).
+ Ensure the remove action works without selection, too. [Commit](http://commits.kde.org/kate/68ecab64dccac4e504f941eb49ddf24e3e62c79a). Fixes bug [#498268](https://bugs.kde.org/498268).
+ Allow ignoring QSqlQuery prepare failure. [Commit](http://commits.kde.org/kate/30bac820cb1bfa435f0419039a914caecdcdfe95). Fixes bug [#498431](https://bugs.kde.org/498431).
+ Fix Search MatchExport. [Commit](http://commits.kde.org/kate/3a645b4e949e9f18cba40e111a7b1591a4ebbb6b). Fixes bug [#499080](https://bugs.kde.org/499080).
+ Set path for eslint process. [Commit](http://commits.kde.org/kate/2970bae9613b653aba76c6e9163eebcb848f04bb).
{{< /details >}}
{{< details id="kblocks" title="kblocks" link="https://commits.kde.org/kblocks" >}}
+ Fix invalid memory accesses/crashes. [Commit](http://commits.kde.org/kblocks/3fe02f99f2e393fae7a4904cc7a8100d08b19fd8).
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Bind queued invocation to `this`. [Commit](http://commits.kde.org/kdenetwork-filesharing/f459aac65faf469c9ac7171193785014141e0f4c). Fixes bug [#497684](https://bugs.kde.org/497684).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Don't try to update monitor overlay if effect is disabled. [Commit](http://commits.kde.org/kdenlive/f2c81c43329a69f17ebeaa15c35bbd82d73811e6).
+ Fix crash setting empty name for folder. [Commit](http://commits.kde.org/kdenlive/230fac92d6573dbaddc7d65fe9d1bee5f112c72d). Fixes bug [#499070](https://bugs.kde.org/499070).
+ Better fix for expand library clips broken with proxies. [Commit](http://commits.kde.org/kdenlive/71da7364f51e0602708693de90da283b1c1630fd). Fixes bug [#499171](https://bugs.kde.org/499171).
+ Try to fix Whisper models folder on Flatpak. [Commit](http://commits.kde.org/kdenlive/1d468e892e8fd034d020f1e4c7c1144b573bbcf0). See bug [#499012](https://bugs.kde.org/499012).
+ Don't try to delete ui file elements on subtitlemanager close. [Commit](http://commits.kde.org/kdenlive/d31fe12d92a8e51ecdf25845bb698882a834904c).
+ Fix effect stack widget not properly resizing. [Commit](http://commits.kde.org/kdenlive/3bea584eb18b0c37344cc1d9b73a6b375706332e).
+ Ensure built-in effects reset button is enabled. [Commit](http://commits.kde.org/kdenlive/d983185c9df3687361efb9d8e14a0a08cbe01715).
+ Ensure vidstab external files are correctly listed and archived. [Commit](http://commits.kde.org/kdenlive/c8d935b110cd185a5690404a29151e8b7a6bb64c).
+ Added 2 decimals for the rotation parameter (addresses bug #498586). [Commit](http://commits.kde.org/kdenlive/4554dd507ee921d4d67e10aef610ba67a2b2198a).
+ Rescale 48-apps-kdenlive.png to 48x48. [Commit](http://commits.kde.org/kdenlive/26324cca1df76ab770cd45303f37fc23926d85ec).
+ Fix effects layout broken on resize. [Commit](http://commits.kde.org/kdenlive/3b4a0ce53305dfccdfda7aa31455d9ba942f8e27). Fixes bug [#498749](https://bugs.kde.org/498749).
+ Fix possible crash on exit. [Commit](http://commits.kde.org/kdenlive/6fd639692b5d396b958eddde24323ac6102462b4).
+ Reassemble proxy profile elements in the correct order after validation. [Commit](http://commits.kde.org/kdenlive/a48cf6ccf04b015b606b5656b87d2ee883f767c9). Fixes bug [#485356](https://bugs.kde.org/485356).
+ Fix the spinbox range for title position and size. [Commit](http://commits.kde.org/kdenlive/a62bfb91d2a9449df30e6eb959bc1ca52efdc0f8). Fixes bug [#487950](https://bugs.kde.org/487950).
+ Fix rendering progress not shown when rendering a zone. [Commit](http://commits.kde.org/kdenlive/1882f88c528afd30c0d3edb968ae76e319dca659).
+ Fix FFmpeg for STT not found on Mac. [Commit](http://commits.kde.org/kdenlive/37b97ddfc6e8caa33bb3c8ae0e946515c4e2666f). Fixes bug [#498949](https://bugs.kde.org/498949).
+ Backport fix for invalid file names in custom effects. [Commit](http://commits.kde.org/kdenlive/63f05b6a6b6110320a72e48288c6f77be2373b6c). Fixes bug [#498710](https://bugs.kde.org/498710).
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ Fix locations on Declarations in macro expansions. [Commit](http://commits.kde.org/kdevelop/ee828bc1149d2d48f966f8d21ab44f121168cb62). Fixes bug [#496985](https://bugs.kde.org/496985).
{{< /details >}}
{{< details id="kidentitymanagement" title="kidentitymanagement" link="https://commits.kde.org/kidentitymanagement" >}}
+ Make sure we do not create new entries when trying to delete some. [Commit](http://commits.kde.org/kidentitymanagement/07b3ce414389fe8dab153a9842c74545b93ba37b).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ [thumbnail] Fix assert message. [Commit](http://commits.kde.org/kio-extras/0ba7ab4e6941785d63ffd8d6c68a5853763454f5). See bug [#490827](https://bugs.kde.org/490827).
{{< /details >}}
{{< details id="kirigami-gallery" title="kirigami-gallery" link="https://commits.kde.org/kirigami-gallery" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/kirigami-gallery/99182a8b41b7ffeff589ed045f797bfbcaf8b9e8).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add extractor for CitizenM hotel bookings. [Commit](http://commits.kde.org/kitinerary/62e33fa2eeadaab3b5ac16f1cbceccd27aa0c83d).
+ Add Polregio extractor. [Commit](http://commits.kde.org/kitinerary/2f2c5c143c22bb2f5c0df4e5ce8b32ef0dd1868d).
+ Refactor and fix Koleo. [Commit](http://commits.kde.org/kitinerary/115fc00995ebf663abd33c99716bc91ab893b911).
+ Rewrite PKP-app to support both ticket layouts. [Commit](http://commits.kde.org/kitinerary/89eda693506855b3d9ccbd81a25ce9d64f387db0).
+ Simplify PKP Aztec barcode encoding. [Commit](http://commits.kde.org/kitinerary/20e36318f253ac5c1ee8b074a6ad7719d3b72b8a).
+ Adapt to Poppler 25.02 API changes. [Commit](http://commits.kde.org/kitinerary/3ca3a825ae7c4afe568450f4990e30fa1206f281).
+ Fix trainNumber and seatSection if route time doesn't have minutes. [Commit](http://commits.kde.org/kitinerary/1515aa79b3835c0efc94152dda04448ccc013854).
+ Add Universe extractor. [Commit](http://commits.kde.org/kitinerary/5541dfe8d036894563b30197e006bf8b0ae08611).
+ Support new/alternative Eurostar-branded Thalys PDF ticket layout. [Commit](http://commits.kde.org/kitinerary/84b2a6651f815286fac1103bc20686d46c5191ed).
+ Handle Flixbus tickets in Italian. [Commit](http://commits.kde.org/kitinerary/cc2afc5f754d35c2d30d729158d8a50c8ace7327).
+ Extend Koleo for more operators (PL). [Commit](http://commits.kde.org/kitinerary/7caa486e8fdf68a484cac25334bbaca42be45304).
+ Add Southwest reservations parser. [Commit](http://commits.kde.org/kitinerary/e24d11ed9e80f3def05c8c43d94e6c35ce47de42).
+ Add Brightline train ticket parsing. [Commit](http://commits.kde.org/kitinerary/ad12021e3139e2b104f3b1490b68752cf7ffa0b0).
+ Add American Airlines reservations parser. [Commit](http://commits.kde.org/kitinerary/8fdcee5d2ae2f70d11ae480c1e1e614fb3c601a0).
+ Remove leftover debug message from United parser. [Commit](http://commits.kde.org/kitinerary/da95ecf68472da3578b88b03e33e785e538006d9).
+ Handle the (new?) multi-column LH boarding pass layout. [Commit](http://commits.kde.org/kitinerary/2f53f4aeddafda496217e77c5f58634cbdf7b97a). Fixes bug [#498569](https://bugs.kde.org/498569).
+ Don't overwrite airport or terminals provided by preceding extractors. [Commit](http://commits.kde.org/kitinerary/2b8980edc60afaede7d6b812ee7dfa612012e960).
+ Strip empty field indicators from gate, terminal and boarding group fields. [Commit](http://commits.kde.org/kitinerary/d0e5316fff709ca069e4e305e90df8be7fc374f7).
+ Increase upper PDF page limit for extraction. [Commit](http://commits.kde.org/kitinerary/bff15dc89a4e759fe47f49472e66258c1d4733c9). Fixes bug [#498570](https://bugs.kde.org/498570).
+ Modernize booking.com fallback extractor and add Danish language support. [Commit](http://commits.kde.org/kitinerary/c57488e1caaa5ea0ae121e2c7e2a1eed04d812fc).
+ Relax SBB trigger pattern a bit. [Commit](http://commits.kde.org/kitinerary/7ad7a881384cc185c7ff7207a33aacae81f4aa2b).
+ For KD make aztecbin, not qrcode. [Commit](http://commits.kde.org/kitinerary/b270e1e4d05dae3cae97f7ca2ff617fd276333c2).
+ Add bilkom extractor. [Commit](http://commits.kde.org/kitinerary/8c0495c50fabef8a876c17f623f418c4b1c1b3d7).
+ Make the reservation-only DB event extractor really only trigger on those. [Commit](http://commits.kde.org/kitinerary/7f5197b79c3fb0734f3b1e11a7bac32c579082a1).
+ Add files pkp-app files. [Commit](http://commits.kde.org/kitinerary/1bd5a98d0d88732a6241949b8f5f8787af14fd0a).
+ Initial version of PKP IC app ticket extractor (EN/PL). [Commit](http://commits.kde.org/kitinerary/3588c7af6751e2d179c2855b405eb211f2deeb73).
{{< /details >}}
{{< details id="klettres" title="klettres" link="https://commits.kde.org/klettres" >}}
+ Fix knsrc name. [Commit](http://commits.kde.org/klettres/d9da914275b585b7b5cc48ead9d3d8f04616e3aa).
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Use KMAIL_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/kmail/19c1afe48397314e76a2a594f64e766d99fa6c2a).
+ Revert "Query keyserver when clicking on link for unknown certificate". [Commit](http://commits.kde.org/kmail/5204a8cd5b9bf63fb43ace48ad9060043635832c).
+ Query keyserver when clicking on link for unknown certificate. [Commit](http://commits.kde.org/kmail/9533b3272d9c3fbd4cb73cf9c9f447e0677e5b0b).
+ Do not create multiple entries when deleting an account. [Commit](http://commits.kde.org/kmail/70c325143ad4c1621ce3f573a65d9aea2afcc62f).
{{< /details >}}
{{< details id="koko" title="koko" link="https://commits.kde.org/koko" >}}
+ Fix leftover Qt6 port. [Commit](http://commits.kde.org/koko/bf7c6bfe4ef263bdef709ea1d3af5049bb38e9f3).
{{< /details >}}
{{< details id="kongress" title="kongress" link="https://commits.kde.org/kongress" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/kongress/f7aec4c5995bad52142d057fba664ba9badd3186).
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Work around a bug when setting focus to a WebEngineView. [Commit](http://commits.kde.org/konqueror/61e97948a76b797a4c41e3d54984a25572d3c8bc).
{{< /details >}}
{{< details id="kontact" title="kontact" link="https://commits.kde.org/kontact" >}}
+ Use KONTACT_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/kontact/e42e1a6f1463930d2bd570f276216e58a1ec9a6c).
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Call setNeedsSave(false). [Commit](http://commits.kde.org/korganizer/f262648e83da4e3372547adc4b8094379ac621e9).
+ Unbreak the build after 07c53045353fd0c6960bc34a0d4aab96d35ffc3f. [Commit](http://commits.kde.org/korganizer/32784cd99b2fc401349007be903e306da991888e).
+ Use KORGANIZER_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/korganizer/07c53045353fd0c6960bc34a0d4aab96d35ffc3f).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/kosmindoormap/321ecd227b76110b48447cf5fc50636fe2389a18).
{{< /details >}}
{{< details id="kpmcore" title="kpmcore" link="https://commits.kde.org/kpmcore" >}}
+ Add 1 to handleWidth in partresize widget. [Commit](http://commits.kde.org/kpmcore/681942bfaf3274f1449c291ca271d16de02b7491). Fixes bug [#499064](https://bugs.kde.org/499064).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Set API keys for HSL and Waltti backends as well. [Commit](http://commits.kde.org/kpublictransport/924f8d8e3c336cd153ddf33cceed63e60ec2bb6d).
+ Switch to Entur's v3 API. [Commit](http://commits.kde.org/kpublictransport/0bc416e214c0c6cbbb894984970e2ed6068620af).
+ Try harder to determine OTP line modes. [Commit](http://commits.kde.org/kpublictransport/a3e15e2e21162480f84b991f5447889a6553b086).
+ Switch Transitous to the new MOTIS v2 API. [Commit](http://commits.kde.org/kpublictransport/d87c424e6fc3981727eb5aeca4a36f747f19203f).
+ DB: Handle NS International tickets. [Commit](http://commits.kde.org/kpublictransport/21cd3823eb44b9963c52d1c32b4e4a26c720ee05).
+ Handle transfer sections in the new DB API. [Commit](http://commits.kde.org/kpublictransport/55e93cace4d58248250b886021c7bb3a6bd30f50).
+ Parse occupancy information also for the entire section. [Commit](http://commits.kde.org/kpublictransport/6953d3ddf8216cd7cc36166df498724b80dbde8b).
+ Add test for cancelled journeys in the new DB API. [Commit](http://commits.kde.org/kpublictransport/50260b731d865ba1fe5b95a1329b38bc82c58e94).
+ Update Rolph endpoint URL. [Commit](http://commits.kde.org/kpublictransport/96885033bac3de4a5f0697c753e96da33dbba502).
+ Remove Australian Navitia backend configuration. [Commit](http://commits.kde.org/kpublictransport/93af7f8fa0363e2b7d2a599871a44b7272e199e7).
+ Restore Digitransit support now that they require an API key. [Commit](http://commits.kde.org/kpublictransport/9fbf942cbaf28e21161146171de705f51b8097ae).
+ Reduce excessive debug output. [Commit](http://commits.kde.org/kpublictransport/51628a0e2ce4b23be1ea39651d21b8a9a93e2edc).
+ Switch to DB's new proxied Hafas API by default. [Commit](http://commits.kde.org/kpublictransport/751498ea70ad8fe88b12c0214fa505170880a436).
+ Switch the Hafas request unit tests to use ÖBB instead of DB. [Commit](http://commits.kde.org/kpublictransport/37caa9b50d5cc17724ce2611e5aa46820fa85eb1).
+ EFA: Improve arrival/departure time parsing for intermediate stops. [Commit](http://commits.kde.org/kpublictransport/5f814a6145550cd81ca03f2c4b9bd01e5452abe2).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Fix FreeBSD pipeline. [Commit](http://commits.kde.org/krdc/02356760763f5e08a54a4ef76b8dca67f07ff73d).
+ Fix crash on settings save when the systray icon is enabled. [Commit](http://commits.kde.org/krdc/cef5ab5d265059c0ebd21f572b6c8277f877f504).
{{< /details >}}
{{< details id="ktrip" title="ktrip" link="https://commits.kde.org/ktrip" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/ktrip/c17e44392b69534d613b40c451d0416c88ad0ada).
{{< /details >}}
{{< details id="kweather" title="kweather" link="https://commits.kde.org/kweather" >}}
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/kweather/c06073f0b62ee86b2b147b92997e0390a5607584).
{{< /details >}}
{{< details id="kwordquiz" title="kwordquiz" link="https://commits.kde.org/kwordquiz" >}}
+ Edit org.kde.kwordquiz.appdata.xml. [Commit](http://commits.kde.org/kwordquiz/d49a4d6e18a488f8e3c835812f5fa781b62b50ed).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Fix crash when trying to view Security settings in an invited room. [Commit](http://commits.kde.org/neochat/fcb32b1974f6a36444dc49022c8ea6c9f749cb58).
+ Fix: no icon under Windows. [Commit](http://commits.kde.org/neochat/3752c3b8722506d9f46cef6db0431cd99d74e8f2).
+ Make sure space drawer icons are available for android. [Commit](http://commits.kde.org/neochat/25f2693710441ebba1856a3b9202727ebb0d50df).
+ Add missing icons on Android. [Commit](http://commits.kde.org/neochat/2efcc1041bff346965a7e6840c8dfece845aac8c).
+ Always open the user details dialog in the focused window. [Commit](http://commits.kde.org/neochat/1f5823cec08791af9059d8ae076d931518f76940).
+ Don't spam pending invites every time NeoChat is started. [Commit](http://commits.kde.org/neochat/46b95662425498d959850d572984b2da6f59a370).
+ Disable TextHandlerTest on CI. [Commit](http://commits.kde.org/neochat/1344e4620141d9b6538a6040e6fdd83f93dea325).
+ Improve handling of DonwloadAction. [Commit](http://commits.kde.org/neochat/a41be9e19b61845fcbb4fd8ee168c08c2b8629e8).
+ Expose ProgressInfoRole also for other type of attachments. [Commit](http://commits.kde.org/neochat/aa5ece8bfb0d02e584beed918012f1c7902a7dd1).
+ Fix right clicking on NeoChatMaximizedComponent. [Commit](http://commits.kde.org/neochat/21f5ee74bafa96a0e3967400fbd2b30fc5912872).
+ Set explicitely parent in MaximizeComponent. [Commit](http://commits.kde.org/neochat/2e6cf03c15f0f6c0ce757ee03f0eb8faad56a407).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix apparent hang in files with lots of entries in a choice field. [Commit](http://commits.kde.org/okular/d67b3ff1227d85acea53f6990482e031062d8177). Fixes bug [#498729](https://bugs.kde.org/498729).
+ Only install relevant desktop files for dvi generator. [Commit](http://commits.kde.org/okular/f605c2a95e510ac165bc4d3a5a232ff68d491667).
+ Watch the QEvent::ApplicationPaletteChange in the proper object. [Commit](http://commits.kde.org/okular/6eaf5fccd2c4ee2939c04e678684602ceaae5b08). Fixes bug [#498441](https://bugs.kde.org/498441).
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/okular/589c8b8668b2e21a96f6d8b1b098c3f0f8cc4cb9).
{{< /details >}}
{{< details id="pim-data-exporter" title="pim-data-exporter" link="https://commits.kde.org/pim-data-exporter" >}}
+ Use PIMDATAEXPORTER_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/pim-data-exporter/b106274772c0156943cfceb6c16f8e5f8a819964).
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Use SIEVEEDITOR_RELEASE_VERSION_DATE. [Commit](http://commits.kde.org/pim-sieve-editor/6336d6c539a8c7abd9b2851daf6f63e558384e71).
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Fix option updating after reload. [Commit](http://commits.kde.org/skanpage/ad4475b0d617fe85337cbd46027d738e75bd414a). Fixes bug [#497880](https://bugs.kde.org/497880).
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ OptionsMenu: remove extra separator. [Commit](http://commits.kde.org/spectacle/53bdec7a9b9851044b813d33b2e16533f998aaa7).
+ OptionsMenu: only check capture on click when it's possible to use it. [Commit](http://commits.kde.org/spectacle/c1a3b021e14ba05b162764de7854601c2e08f26f).
+ OptionsMenu: Don't put separator before capture on click checkbox when the checkbox isn't visible. [Commit](http://commits.kde.org/spectacle/dac0b90db70b0555e156f814132cfed732c823ff).
+ OptionsMenu: Check the rectangle mode action in rectangle mode. [Commit](http://commits.kde.org/spectacle/b63b88080728130715a2be49e9071be5bdfdf8b8).
+ Force QR code inline message to always be HTML. [Commit](http://commits.kde.org/spectacle/6fdd77148f5356647f63cb0b6fce17b7a2649871). Fixes bug [#498618](https://bugs.kde.org/498618).
{{< /details >}}
{{< details id="telly-skout" title="telly-skout" link="https://commits.kde.org/telly-skout" >}}
+ Add 24.12.2 release description. [Commit](http://commits.kde.org/telly-skout/e84e8c350a3a6a21071963e598f11540e0e0a2f8).
+ Appdata: add display size. [Commit](http://commits.kde.org/telly-skout/9c14b987402ae2c8660e27a3b8dacff109104fc5).
+ "Favorites" page: show "Refetch" in top toolbar on mobile. [Commit](http://commits.kde.org/telly-skout/b1adff1b29fbbb1d7af211a73908d36f2a9c7d49).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Add bottom padding to the sidebar. [Commit](http://commits.kde.org/tokodon/6cf9fe8d7a3c64703e92b58bd7e189fa56bcecb9).
+ Better handle failed authentification. [Commit](http://commits.kde.org/tokodon/2f31bc2a614462be3fd07900af179d6f7ae01263).
+ Remove unused string from Android manifest. [Commit](http://commits.kde.org/tokodon/e1c7095ac88bbd128cb4654204594e0131af7b63).
+ Don't display a visual error when failing to fetch familiar followers. [Commit](http://commits.kde.org/tokodon/1a04d32c89d38bbf1ab18ea90908df83d37677d4).
+ Don't display a visual error when failing to fetch unread count. [Commit](http://commits.kde.org/tokodon/297d2c3a3d0c0e2db8ddbe5c5e65e485e747c7de). Fixes bug [#497562](https://bugs.kde.org/497562).
{{< /details >}}
