---
aliases:
- ../../fulllog_releases-23.08.5
title: KDE Gear 23.08.5 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="colord-kde" title="colord-kde" link="https://commits.kde.org/colord-kde" >}}
+ Extract i18n messages from *.qml. [Commit](http://commits.kde.org/colord-kde/1f4e897731f28be56b3d00b057f9905eac0756a5). Fixes bug [#432073](https://bugs.kde.org/432073).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix build with cmake >= 3.28. [Commit](http://commits.kde.org/dolphin/085c7991b7c5aa88e050e0216216c5645878a59c).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Make context drawer interactive on mobile. [Commit](http://commits.kde.org/elisa/2d73416c9382b5b6a3535612719ff2c94dc0246a). Fixes bug [#478121](https://bugs.kde.org/478121).
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ History: Store addresses with custom schemes. [Commit](http://commits.kde.org/falkon/2bce10f4c822b5d8921e34f65bc48f49ec03a94a). Fixes bug [#406375](https://bugs.kde.org/406375).
+ SpeedDial: Fix tooltip on Reload button. [Commit](http://commits.kde.org/falkon/9c5f2c1624022cf9b13d769645686b7739303522).
+ Display correct URL in urlBar after window restore. [Commit](http://commits.kde.org/falkon/866fa8a3c4dc408bf0bebba0e9bffe12782f7480). Fixes bug [#478823](https://bugs.kde.org/478823).
+ MainApplication: Enable localStorage for private. [Commit](http://commits.kde.org/falkon/7807414452760683e6b7e9118aeaa5badf472f27).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Build ZXing statically to avoid clashing with the one in the platform. [Commit](http://commits.kde.org/itinerary/dc8bebdf031c26219ac17264d077ad3ffc82b199). See bug [#479819](https://bugs.kde.org/479819).
+ Fix APK builds after KHealthCertificate became Qt6-only in master. [Commit](http://commits.kde.org/itinerary/f7a1984a773d284abe7d056d352f4ecf458ad94b).
{{< /details >}}
{{< details id="juk" title="juk" link="https://commits.kde.org/juk" >}}
+ Fix build with taglib 2. [Commit](http://commits.kde.org/juk/db3bab2c24c3d75a11ddbbe3d2d8bfc920f19299).
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix loading plugin KCMs. [Commit](http://commits.kde.org/k3b/2c0c3408ddc0a521407539eb604e21940ba4653d). Fixes bug [#253768](https://bugs.kde.org/253768).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 481132: Remove description of local directory calendars, which are no longer supported. [Commit](http://commits.kde.org/kalarm/268ecdf9300a900a6b9cd0908ae670ec342b59e9).
+ Fix version number. [Commit](http://commits.kde.org/kalarm/1d0cfe3b43ecf1ee0d5083b4df0a8dc35f6389ad).
+ Bug 481053: Fix --name command line option not using its parameter. [Commit](http://commits.kde.org/kalarm/a05422923b625f9cf5f1d6f167e6a6d0b3e60b7f).
+ Update version number. [Commit](http://commits.kde.org/kalarm/9414ba4bede64113ae60f5bd3d6dd4d87aac660e).
+ Remove 'spread windows' option for Wayland. [Commit](http://commits.kde.org/kalarm/7ea3eab6134d5ad4071b8b9c9ace7a8959d9c23a).
+ Fix build. [Commit](http://commits.kde.org/kalarm/8e3b24180dfdb1c039d52054f24eec867b852142).
+ Port away from KWindowSystem::setState. [Commit](http://commits.kde.org/kalarm/86d078db1572365aec8d15487535ddeed6882042).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Add further missing includes. [Commit](http://commits.kde.org/kate/bd8403ba1771d92586518868d0bfd42087b0db4d).
+ Fix build. [Commit](http://commits.kde.org/kate/83fcf7b4e7cc7bc54828192e9b89cfb286d864fd).
{{< /details >}}
{{< details id="kdeedu-data" title="kdeedu-data" link="https://commits.kde.org/kdeedu-data" >}}
+ Fix encoding of sv animals.kvtml file. [Commit](http://commits.kde.org/kdeedu-data/29266e86ca27b3f6fed0d871d24f19deaf6fb78a).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix undocked widgets don't have a title bar to allow moving / re-docking. [Commit](http://commits.kde.org/kdenlive/a97b6ff81793793b485b803aaed7e5d74433c065).
+ Multi guides export: replace slash and backslash in section names to fix rendering. [Commit](http://commits.kde.org/kdenlive/c6c53a8e642304931a2ad2678db7142abfc88d58). Fixes bug [#480845](https://bugs.kde.org/480845).
+ Fix sequence corruption on project load. [Commit](http://commits.kde.org/kdenlive/b2a028452aba7dabd969a0adb6379037f025626d). Fixes bug [#480776](https://bugs.kde.org/480776).
+ Fix multiple archiving issues. [Commit](http://commits.kde.org/kdenlive/617a8cd9fa1df0e0e15a0eb267644ae70b8fc552). Fixes bug [#456346](https://bugs.kde.org/456346).
+ Fix possible sequence corruption. [Commit](http://commits.kde.org/kdenlive/f8558885255594caa5420f1f23d984b61c273305). Fixes bug [#480398](https://bugs.kde.org/480398).
+ Fix sequences folder id not correctly restored on project opening. [Commit](http://commits.kde.org/kdenlive/e3ace26cc5bf376aee3d3865aae2b344f17b76f5).
+ Fix Luma issue. [Commit](http://commits.kde.org/kdenlive/138cea76db4b9064769ec6a2690832a693f9190c). See bug [#480343](https://bugs.kde.org/480343).
+ Fix subtitles not covering transparent zones. [Commit](http://commits.kde.org/kdenlive/d3469628132baf61efa63b45f2fa0de5ca68879e). Fixes bug [#480350](https://bugs.kde.org/480350).
+ Group resize: don't allow resizing a clip to length < 1. [Commit](http://commits.kde.org/kdenlive/641c89e8a8bfeabc99f255b2a0a22e683295c8e2). Fixes bug [#480348](https://bugs.kde.org/480348).
+ Fix crash cutting grouped overlapping subtitles. Don't allow the cut anymore, add test. [Commit](http://commits.kde.org/kdenlive/aa64e82c9e6d652f7581f37da800ee38df3a696f). Fixes bug [#480316](https://bugs.kde.org/480316).
+ Fix clip monitor not updating when clicking in a bin column like date or description. [Commit](http://commits.kde.org/kdenlive/b415f112ab79f1e15d1ca6d417ee8e3a4ef8e17e). Fixes bug [#480148](https://bugs.kde.org/480148).
+ Fix start playing at end of timeline. [Commit](http://commits.kde.org/kdenlive/89207f3d8df341fb343299ff1b8c47abce8ddaf1). Fixes bug [#479994](https://bugs.kde.org/479994).
+ Fix save clip zone from timeline adding an extra frame. [Commit](http://commits.kde.org/kdenlive/a9147421a960a4b09e86eccad7424a7da1732744). Fixes bug [#480005](https://bugs.kde.org/480005).
+ Fix clips with mix cannot be cut, add test. [Commit](http://commits.kde.org/kdenlive/0f0cdaca34d7a1bf3f4a4ba3dbf88d738b5947cc). See bug [#479875](https://bugs.kde.org/479875).
+ Fix project monitor loop clip. [Commit](http://commits.kde.org/kdenlive/24df69822b26c9718323503ce3ad1a42cde31351).
+ Fix monitor offset when zooming back to 1:1. [Commit](http://commits.kde.org/kdenlive/3d9c15366904084fad740c6f1fc4be2f85d97da4).
+ Fix sequence effects lost. [Commit](http://commits.kde.org/kdenlive/1b9dd950718959e0608682d3c8ff65abd2c8a717). Fixes bug [#479788](https://bugs.kde.org/479788).
+ Improved fix for center crop issue. [Commit](http://commits.kde.org/kdenlive/c056a1fcbe8615c1eb369fda3ad0bed8248bc5ce).
+ Fix center crop adjust not covering full image. [Commit](http://commits.kde.org/kdenlive/89350b5f3254a7a614a6d7e95d53d5683af9c3cc). Fixes bug [#464974](https://bugs.kde.org/464974).
+ Disable Movit until it's stable (should have done that a long time ago). [Commit](http://commits.kde.org/kdenlive/6142637f8e8a11479cd8c66e7940198dc8af93a4).
+ Fix cannot save list of project files. [Commit](http://commits.kde.org/kdenlive/9a5bf8b39d7d989968bdce47e51d83bfb9f85a97). Fixes bug [#479370](https://bugs.kde.org/479370).
+ Fix editing title clip with a mix can mess up the track. [Commit](http://commits.kde.org/kdenlive/22eb29f8546b183f61e4c61d3ce320f646060255). Fixes bug [#478686](https://bugs.kde.org/478686).
+ Fix audio mixer cannot enter precise values with keyboard. [Commit](http://commits.kde.org/kdenlive/0a3cb68a579a289f95e33f0d8a53ce7087df0521).
+ Prevent, detect and possibly fix corrupted project files, fix feedback not displayed in project notes. [Commit](http://commits.kde.org/kdenlive/dd0c809b64c8f8ae48edb11dc56415767183a5f7). See bug [#472849](https://bugs.kde.org/472849).
+ Test project's active timeline is not always the first sequence. [Commit](http://commits.kde.org/kdenlive/26e7c26d5842d7fc169626e372237b0aa06300e7).
+ Ensure secondary timelines are added to the project before being loaded. [Commit](http://commits.kde.org/kdenlive/2a105c73492e8c507cb74a7751a0bb25317ac8cc).
+ Ensure autosave is not triggered when project is still loading. [Commit](http://commits.kde.org/kdenlive/30b7b2b0df994edbabb7e44be6a9af637b2fa9d6).
+ Fix variable name shadowing. [Commit](http://commits.kde.org/kdenlive/fc8b2b152397bc6c3d177dda79276a6a6bc13b88).
+ When switching timeline tab without timeline selection, don't clear effect stack if it was showing a bin clip. [Commit](http://commits.kde.org/kdenlive/8b14d06bc1ffae66b1671e42cb12a946ec93bda6).
+ Fix crash pressing del in empty effect stack. [Commit](http://commits.kde.org/kdenlive/b491d94d2b263f126d71aa739510baf3c839ed9f).
+ Ensure check for HW accel is also performed if some non essential MLT module is missing. [Commit](http://commits.kde.org/kdenlive/8aca4961e07d7df4e57132d8945e1b0904c05e53).
+ Fix tests. [Commit](http://commits.kde.org/kdenlive/e0484391681d6e05dd6eb58ea0b0726f0cee8048).
+ Fix closed sequences losing properties, add more tests. [Commit](http://commits.kde.org/kdenlive/64468b079f04b80dadc0067e9f43a3fe8ecc4336).
+ Don't attempt to load timeline sequences more than once. [Commit](http://commits.kde.org/kdenlive/8caeb59641ca282f679ffbb50d38a43de6ef2e06).
+ Fix timeline groups lost after recent commit on project save. [Commit](http://commits.kde.org/kdenlive/eec349040894bf25fbe68bc9448ed38e12b94019).
+ Ensure we always use the correct timeline uuid on some clip operations. [Commit](http://commits.kde.org/kdenlive/e81b4be508c5d65c29a2b2aea901c0a5ae16ddfa).
+ Add animation: remember last used folder. [Commit](http://commits.kde.org/kdenlive/86e1f68ae4f46833b7ad346262a451d5f0fbfbf7). See bug [#478688](https://bugs.kde.org/478688).
+ Refresh effects list after downloading an effect. [Commit](http://commits.kde.org/kdenlive/865f6b7fa9727e21d544a994a32ada2d510a571e).
+ Fix audio or video only drag of subclips. [Commit](http://commits.kde.org/kdenlive/9d3702270d7baec463cf558848f0a23960ffa4cf). Fixes bug [#478660](https://bugs.kde.org/478660).
+ Fix editing title clip duration breaks title (recent regression). [Commit](http://commits.kde.org/kdenlive/008e7bde11e77bd67abf403b222e94fc750906c8).
+ Glaxnimate animations: use rawr format instead of Lottie by default. [Commit](http://commits.kde.org/kdenlive/6b9dbaeb1f983ccf4bf9ffeaef6d6a70e5fd5a49). Fixes bug [#478685](https://bugs.kde.org/478685).
+ Fix timeline focus lost when dropping an effect on a clip. [Commit](http://commits.kde.org/kdenlive/b93a61291090c4f3e0eb7c11e7797bd0ea4d1654).
+ Fix dropping lots of clips in Bin can cause freeze on abort. [Commit](http://commits.kde.org/kdenlive/bd0cb1cb3281998e533f1e2e5f21d7aa17ce84dd).
+ Right click on a mix now shows a mix menu (allowing deletion). [Commit](http://commits.kde.org/kdenlive/24bf262749b27c6861f822abdba3903a0edce234). Fixes bug [#442088](https://bugs.kde.org/442088).
+ Don't add mixes to disabled tracks. [Commit](http://commits.kde.org/kdenlive/5f50241808dd219fe4594b5fc318fc35db5df0d0). See bug [#442088](https://bugs.kde.org/442088).
+ Allow adding a mix without selection. [Commit](http://commits.kde.org/kdenlive/2a4c4a18273415946ada131a4c5df4beaf40e29c). See bug [#442088](https://bugs.kde.org/442088).
+ Remove line missing from merge commit. [Commit](http://commits.kde.org/kdenlive/84eb0a85ad87b34990c92f19be07854e0c76a028).
+ Fix proxied playlist clips (like stabilized clips) rendered as interlaced. [Commit](http://commits.kde.org/kdenlive/19838f9e1ec4bf93be10c179e59c2498f682dbc0). Fixes bug [#476716](https://bugs.kde.org/476716).
+ Always keep all timeline models opened. [Commit](http://commits.kde.org/kdenlive/e66afa13c9dd7a0e8f41946799e6249345772f85). See bug [#478745](https://bugs.kde.org/478745).
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix TRANSLATION_DOMAIN name. [Commit](http://commits.kde.org/kdepim-addons/af5fa974e74f3a9f3791da3238dcfb6c293450de).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Fix -Wenum-constexpr-conversion with latest clang. [Commit](http://commits.kde.org/kdepim-runtime/be465b7480555e7ae6a26c1c8acb71aafa991927).
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ Unbreak compile with Unix Makefile and newer CMake. [Commit](http://commits.kde.org/kdevelop/9f793a9e4054c2ebb8bd697ea4355d8bc746de3b).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Search for Threads::Threads. [Commit](http://commits.kde.org/kitinerary/ad7a7cdbecb61e3e56f7b517311cc418c3b9d242).
+ Don't crash on missing PDF link actions. [Commit](http://commits.kde.org/kitinerary/0daeb3b0956481f18492a791caa0b02e0e0d21d1).
+ Fix extracting B&B Hotel confirmations with breakfast. [Commit](http://commits.kde.org/kitinerary/f48c4873b119f0fa2b03d3cae8f4de9aebfafefb).
+ Handle one other boarding time variant of LH boarding passes. [Commit](http://commits.kde.org/kitinerary/4c4a7a40e824ea3151619eaaead3465741aa829c).
+ Extract order numbers from DB Next PDF tickets. [Commit](http://commits.kde.org/kitinerary/60d7d18aedbbc7ff7a933992d02cf9931767e5d2).
+ Make the Accor extractor more robust against HTML structure variations. [Commit](http://commits.kde.org/kitinerary/44ddd14d29c9511ce7b9573c4f95c12785c33024).
+ Add CCC pass identifier to the Pretix filter pattern. [Commit](http://commits.kde.org/kitinerary/280496198a1d14dc8166f8501c25811fb124e142).
+ Fix parsing RCT2 arrival times across the end-of-year boundary. [Commit](http://commits.kde.org/kitinerary/a55d382c8d877d084de839e4ac291f2e6bfadf45).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Fix compilation with Clang 16. [Commit](http://commits.kde.org/kleopatra/33f8d5247316beaada224fd7ba4ec93feedbfd8d).
{{< /details >}}
{{< details id="kmime" title="kmime" link="https://commits.kde.org/kmime" >}}
+ Use a locale for the tests that also works on FreeBSD. [Commit](http://commits.kde.org/kmime/0055c6ada5049624f309856a9f99f9304ff5b24c).
{{< /details >}}
{{< details id="knavalbattle" title="knavalbattle" link="https://commits.kde.org/knavalbattle" >}}
+ Fix test for placing a ship vertically. [Commit](http://commits.kde.org/knavalbattle/c3a96f90fbfd4e9d705c3bbc09c436491b559716).
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Fix bookmarks.desktop. [Commit](http://commits.kde.org/konqueror/3b89b40c91589dd0cd6d34e1cae531cbf89591c7).
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Show wallpaper on non-translucent top-levels. [Commit](http://commits.kde.org/konsole/80e6492114afe063d700309600d35aaa3ebceb9c). Fixes bug [#477800](https://bugs.kde.org/477800).
{{< /details >}}
{{< details id="ksudoku" title="ksudoku" link="https://commits.kde.org/ksudoku" >}}
+ Correct icon and text positions in game chooser for HiDPI mode. [Commit](http://commits.kde.org/ksudoku/18c1f7f8a28891219a0881c9600d29d9fb6a2a02).
+ Fix HiDPI rendering in game views. [Commit](http://commits.kde.org/ksudoku/cdaba58af8de33dc8649704019bd4d8135f5327f).
+ Remove duplicate 25x25 letter marker sprites from ksudoku_scrible.svg. [Commit](http://commits.kde.org/ksudoku/4c38a1b40b686892daa38532ea3965c930f272b0).
+ Fix 25x25 letter markers not showing up for ksudoku_scrible.svg. [Commit](http://commits.kde.org/ksudoku/7ea6780ea94b2415fda8eab5ebda31d558263888).
{{< /details >}}
{{< details id="kwordquiz" title="kwordquiz" link="https://commits.kde.org/kwordquiz" >}}
+ Fix mixed up Qt versions of the Kirigami Addons dependency. [Commit](http://commits.kde.org/kwordquiz/3799b984e6e02ee8c2a577f492d3bceff6e7411e).
{{< /details >}}
{{< details id="libkdepim" title="libkdepim" link="https://commits.kde.org/libkdepim" >}}
+ Load i18n catalog. [Commit](http://commits.kde.org/libkdepim/f33846afb7ef7a3f557bcc87201c20ca7b11dc0f).
{{< /details >}}
{{< details id="libkgapi" title="libkgapi" link="https://commits.kde.org/libkgapi" >}}
+ Introduce a BUILD_SASL_PLUGIN option for co-installability. [Commit](http://commits.kde.org/libkgapi/bcc705ce46a21104e640cf606b955ec8808028d2).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Don't break ts files with obsolete fields on saving them. [Commit](http://commits.kde.org/lokalize/a8e24987c2f3c5f922b905a7271ffd1e361f9e71). Fixes bug [#477779](https://bugs.kde.org/477779).
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Normalize polygon winding order for 3D buildings. [Commit](http://commits.kde.org/marble/9646d496832690c4ee575c415e1aae965c776964).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Fix saving images. [Commit](http://commits.kde.org/neochat/d26a6916478b9eb76c9d09a675c74ce6a04df3cd). Fixes bug [#479053](https://bugs.kde.org/479053).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix showing the side panel if not open when showing signatures panel. [Commit](http://commits.kde.org/okular/cac5da535917633895f697c4d01cd79fa017c2e2). Fixes bug [#478542](https://bugs.kde.org/478542).
{{< /details >}}
{{< details id="zanshin" title="zanshin" link="https://commits.kde.org/zanshin" >}}
+ Use -DTRANSLATION_DOMAIN=... otherwise it will not translate when embedded in kontact. [Commit](http://commits.kde.org/zanshin/ffd5b59329be7de7b252cf4ac47e4f55000e75cc).
{{< /details >}}
