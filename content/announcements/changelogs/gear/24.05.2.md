---
aliases:
- ../../fulllog_releases-24.05.2
title: KDE Gear 24.05.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Add missing change notification for initial calendar loading. [Commit](http://commits.kde.org/akonadi-calendar/1d1d14595e26be45874b2998a50edf776d0f4f27).
{{< /details >}}
{{< details id="angelfish" title="angelfish" link="https://commits.kde.org/angelfish" >}}
+ Fix icon caching. [Commit](http://commits.kde.org/angelfish/ece45082bfbd41a591b74a428160bca3fd72d0ad).
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Tests: Comment out test that fails to pass. [Commit](http://commits.kde.org/ark/7def2f05d0398aba38983f549e95507bcc50fb38).
{{< /details >}}
{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ ListHeader: Fix item width on mobile. [Commit](http://commits.kde.org/audiotube/ad83bc9d24bac727e65b59bbbfe3151ea648dd9a).
+ ShareMenu: Fix layout on mobile. [Commit](http://commits.kde.org/audiotube/c67df78fd3239dbab94ea05b1871c38debc00172).
{{< /details >}}
{{< details id="baloo-widgets" title="baloo-widgets" link="https://commits.kde.org/baloo-widgets" >}}
+ Fix FileMetadataItemCountTest::testItemCount. [Commit](http://commits.kde.org/baloo-widgets/c680d773931d5ff72748d081ecbd9905d483dc71).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Add missing GENERATE_MOC to kconfig_add_kcfg_files. [Commit](http://commits.kde.org/dolphin/a33f8744f598fd743f853e361b0debe1a130e605).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ ContextView: allow scrolling lyrics with the mouse wheel again. [Commit](http://commits.kde.org/elisa/be7083787e2c543f5c746adc2944b81b046928e8). Fixes bug [#481994](https://bugs.kde.org/481994).
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Fix warning "QObject::disconnect: Unexpected nullptr parameter" on startup. [Commit](http://commits.kde.org/eventviews/98c4a310272c85845567791268db934014d69d71).
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Craft: enable appx building. [Commit](http://commits.kde.org/filelight/d42d8e1997ee54157fa0ed625180e0f12cd67fe5).
+ Force hicolor to contain at least one icon. [Commit](http://commits.kde.org/filelight/c14663db2a15f4a503f65e14d5962238436383f0).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Don't register KContacts QML types with KF >= 6.3. [Commit](http://commits.kde.org/itinerary/b920463aae2daf152bd4a603418f2ba5c676e1b2).
+ Fix reading/writing of all-day events to/from the Android system calendar. [Commit](http://commits.kde.org/itinerary/fc09b2dc7f239974e442f1ff5188916de71b1c4b).
+ Fix corrupted release notes. [Commit](http://commits.kde.org/itinerary/c4447bb8cf9b0e289b2f2f4f95c740d6f4961966).
+ Use the same branch for PIM dependencies. [Commit](http://commits.kde.org/itinerary/0d18d2a325738df0cd30dfa82bfee8131f16eeeb).
+ Port away from deprecated KMime API. [Commit](http://commits.kde.org/itinerary/e314472c2a5ae32735e30b88ef29ec9766d770aa).
+ Build APKs against release branch dependencies. [Commit](http://commits.kde.org/itinerary/0f2d3798cf5ca9c6e28ff8b2d116173a18a07877).
+ Also request calendar read permissions when requesting write permissions. [Commit](http://commits.kde.org/itinerary/73a4c15fb00c6280a15ccd802ce09a76bb3397a9).
+ Hide barcode area in pkpasses entirely if there is none. [Commit](http://commits.kde.org/itinerary/796406bdf8d538896e3f2c8a9e2eeb442c5fd956).
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ Fix i18n domain. [Commit](http://commits.kde.org/kaccounts-integration/acde4c8d03f70d5b4455bc2c2b89d14616cf6328).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix version number. [Commit](http://commits.kde.org/kalarm/9354fe3a484e169451042da5c05e9833776c64f8).
+ Bug 488488: Fix crash after editing new alarm if spell checking is enabled. [Commit](http://commits.kde.org/kalarm/3de7a030c48f987d2605509a7c8122387c2adf69).
{{< /details >}}
{{< details id="kcalc" title="kcalc" link="https://commits.kde.org/kcalc" >}}
+ Add support for decimal numbers without integer part. [Commit](http://commits.kde.org/kcalc/90c2de1cd5046d740e6522b47bea24cf21d889fe). Fixes bug [#487659](https://bugs.kde.org/487659).
+ Chain result upon equal clicked. [Commit](http://commits.kde.org/kcalc/e11903526a36bb1a85529f9d137257b54e8163fa). Fixes bug [#487566](https://bugs.kde.org/487566).
+ Show result while editing input. [Commit](http://commits.kde.org/kcalc/b2acd92aad55aa11fad97fe4b979a7f91591a45c). Fixes bug [#480607](https://bugs.kde.org/480607).
+ Add locale to round functionality. [Commit](http://commits.kde.org/kcalc/75a6ac66df053d5ec30a32334056e0d60430ebc2).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix guides categories not correctly saved in document. [Commit](http://commits.kde.org/kdenlive/9e14a96dbe3720eac0683f894053d7a0cce48631). Fixes bug [#489079](https://bugs.kde.org/489079).
+ Fix adding record track adds a normal audio track. [Commit](http://commits.kde.org/kdenlive/ae67b4bed4dac64a86e454f291992c89379084a3). Fixes bug [#489080](https://bugs.kde.org/489080).
+ Fix rendering with aspect ratio change always renders with proxies. [Commit](http://commits.kde.org/kdenlive/752767c9f2566626a9c2ea55887a4dda9e663c30).
+ Fix compilation on Windows with KF 6.3.0. [Commit](http://commits.kde.org/kdenlive/de682e01e4706c1ef799fc938f74edd3591f9dfd).
+ Fix timeline duration not correctly updated, resulting in audio/video freeze in timeline after 5 min. [Commit](http://commits.kde.org/kdenlive/8be0e826471332bb739344ebe1859298c46e9e0f).
+ Fix Windows build without DBUS. [Commit](http://commits.kde.org/kdenlive/8e20374f0f803371ef8c22f522a25377398424db).
+ Fix crash on spacer tool with subtitles. [Commit](http://commits.kde.org/kdenlive/9cb547030b0b7be155c07e9b17d72161a66d6494).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Add default destructor to TransferJob. [Commit](http://commits.kde.org/kdepim-runtime/e6de26e6297b67c083d3098c85d27fba858ae6ea).
+ Do not try to subscribe to an empty list of folders. [Commit](http://commits.kde.org/kdepim-runtime/05864950c3d0956bb9cb3148acc1245825cdde77). See bug [#485799](https://bugs.kde.org/485799).
+ Fix a memory leak in TransferJob. [Commit](http://commits.kde.org/kdepim-runtime/6173a24fb1aa4a13978185184b74c96934c2aa50). See bug [#486861](https://bugs.kde.org/486861).
+ Google: explicitly request calendar.events scope in authentication. [Commit](http://commits.kde.org/kdepim-runtime/5c17caa8515de05bb21b0948c9d84939eb7adc6c).
{{< /details >}}
{{< details id="keysmith" title="keysmith" link="https://commits.kde.org/keysmith" >}}
+ 🍒 Fix missing TOTP health indicator. [Commit](http://commits.kde.org/keysmith/953c1165dd228cd10a9e75e2faa15c30a2050f4c).
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ Add developer tag to appdata. [Commit](http://commits.kde.org/kget/fb626008d73d337f98537269997d5990b62fc022).
{{< /details >}}
{{< details id="kio-gdrive" title="kio-gdrive" link="https://commits.kde.org/kio-gdrive" >}}
+ Fix regression in fileId lookup. [Commit](http://commits.kde.org/kio-gdrive/027c25b340e0405a0a3e0c53bf4d56454f99ca28). Fixes bug [#487021](https://bugs.kde.org/487021).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add extractor for Georgian Railway (საქართველოს რკინიგზა). [Commit](http://commits.kde.org/kitinerary/29850a2cb1105490274a43d750f636b9aa330c80).
+ If country name resolution failed, use the coordinate based lookup. [Commit](http://commits.kde.org/kitinerary/50cc1803af69e62c61a0dce1fd8a6f38dcad1bac).
+ Normalize whitespaces before doing anything with the country name. [Commit](http://commits.kde.org/kitinerary/55edf9d068786195fe6847a7755eb08e338600b9).
+ Add extractor for TCDD Taşımacılık (Turkish Railways). [Commit](http://commits.kde.org/kitinerary/b6bd20aefca09779e1dba67497fc0727970bcd64).
+ Ignore creation date for PDFsharp PDFs. [Commit](http://commits.kde.org/kitinerary/b5b13c1120471b1b8a0e1f97de546a3ee2852d37).
+ Add extractor script for Iberia reservation PDFs. [Commit](http://commits.kde.org/kitinerary/a5715575649b5376d548db4d62898172d02cdce2).
+ Create airports as places rather than simple objects in the flight template. [Commit](http://commits.kde.org/kitinerary/cfa7c254671424fc58ddb6bf1dbef17aac1b3c69).
+ Add RCT2 IRT detection pattern for Snälltâget. [Commit](http://commits.kde.org/kitinerary/740a80afc1904658f77817108cce87a44961dd1f).
+ Deal with Snälltâget having a typo in the U_TLAY type field. [Commit](http://commits.kde.org/kitinerary/c10a6bd7a81c9ee264530146e8ce7e3fd1642962).
+ Accommodate TCDD's barcode padding. [Commit](http://commits.kde.org/kitinerary/0a90028e28cafc79f28db5002085a0e5a90c7f23).
+ Make the 12go extractor more robust and handle more variants. [Commit](http://commits.kde.org/kitinerary/fc65cec60e768b5cfd249c80685405c58d9bf2f5).
+ Add basic hotels.com fallback extractors for emails without schema.org data. [Commit](http://commits.kde.org/kitinerary/5fd484f07a4e8f15ca05ef07fc52017a921e41c7).
+ Add a sanity check for company capital notes in the price extractor. [Commit](http://commits.kde.org/kitinerary/42f14ef01314dde8f2d039aa0f272ca29eea319d).
+ Extract RegioJet pkpass bus tickets. [Commit](http://commits.kde.org/kitinerary/1bc0af9267909533b7976b3d2e5d2128b3667cf7).
+ Add tobilet.pl event ticket extractor script. [Commit](http://commits.kde.org/kitinerary/9dd3a92492fdbedf1dd430c2c78fb6c489748ea4).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ We want to save the logs as plain text without special Unicode characters. [Commit](http://commits.kde.org/kleopatra/5eca19a818a88b975df4147db549313d7e546ff7).
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Add obviously missing '!' in if() statement. [Commit](http://commits.kde.org/korganizer/2036e65df397065fdcddf26fb7e12a5886d9405b).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Check for errors when mmap'ing tile files. [Commit](http://commits.kde.org/kosmindoormap/76cf61c598976e34c733a14d0b50c95e3e65bddd). See bug [#488664](https://bugs.kde.org/488664).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ DateTimeState: Use locale for firstDayOfWeek. [Commit](http://commits.kde.org/merkuro/b80e60c10db59252c9ac971b7eb9ce8180ac6c99).
+ MonthView: Update Basic grid when changing months. [Commit](http://commits.kde.org/merkuro/eeac99d9a58a669986fe5dbbeac31d2adf7f45a2). Fixes bug [#477161](https://bugs.kde.org/477161).
+ Fix cliping in Schedule view. [Commit](http://commits.kde.org/merkuro/2c09c96b2311c26ec15810475108460232bac640).
+ HourlyView: Use Calendar.DateTimeState. [Commit](http://commits.kde.org/merkuro/ebffb2c93d3157731c23c4636006215a667fa0c9).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ GlobalMenu: remove shortcut for QuickSwitcher. [Commit](http://commits.kde.org/neochat/4991c5f7719ece2a7f66446f4078aa4db409466f). Fixes bug [#488212](https://bugs.kde.org/488212).
+ Fix global menu. [Commit](http://commits.kde.org/neochat/95cb745536b32d696b777bfd0c040bb8f94a0669).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix crash on certain pdf actions. [Commit](http://commits.kde.org/okular/26497332293c5041bfd1c353ad71ed10810c7c51).
{{< /details >}}
{{< details id="partitionmanager" title="partitionmanager" link="https://commits.kde.org/partitionmanager" >}}
+ Fixed crash caused by clicking remove mount point button. [Commit](http://commits.kde.org/partitionmanager/ee14890037bd77cf87e6c0da2649bd9cab473d55). Fixes bug [#432103](https://bugs.kde.org/432103).
{{< /details >}}
{{< details id="plasmatube" title="plasmatube" link="https://commits.kde.org/plasmatube" >}}
+ Add developer name to appdata. [Commit](http://commits.kde.org/plasmatube/32d4cb61965e28fc4e2d3b27ab12c2ede915f8d1).
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ History: fix strict-aliasing warnings by using std::ranges. [Commit](http://commits.kde.org/spectacle/4517ecdc8284dc7e5fc4a815b8cbb665325555bf).
+ Rename 24.02.0-change_placeholder_format script ID to 24.05.2-change_placeholder_format. [Commit](http://commits.kde.org/spectacle/e0f6ba578539603f1de526b2b5dc78aba7662666). Fixes bug [#484211](https://bugs.kde.org/484211).
+ Prevent TextTool TextEdit shortcuts from being used by the window while TextTool is focused. [Commit](http://commits.kde.org/spectacle/f5aff2d331f6dcc635a2e6292e073420febd47f4). Fixes bug [#487740](https://bugs.kde.org/487740).
+ AnnotationDocument: Preserve image metadata. [Commit](http://commits.kde.org/spectacle/912aa0cb936853bb4640944eef1db2ea18f4b5e5).
+ ExportManager: Even more handling for empty <title>. [Commit](http://commits.kde.org/spectacle/8d9f290a26dd74b600fd098460c91d7cdfa3cc4b).
+ ExportManager: remove duplicate dir separators. [Commit](http://commits.kde.org/spectacle/f7264ec91f18289a2db66dac5b0efb1469ff0e35).
+ ExportManager: keep dir separators near <title>. [Commit](http://commits.kde.org/spectacle/9c6f7e4effc8c6b7a48433196f0117110ad98c58). Fixes bug [#445084](https://bugs.kde.org/445084).
+ FilenameTest: use a map and for loops for testing titles instead of copy/pasting strings. [Commit](http://commits.kde.org/spectacle/158c14100acfb25ddd6f5a229a4c39b453aa4b04).
+ Use new placeholder format for the keep_old_filename_templates script. [Commit](http://commits.kde.org/spectacle/787a3c8f08bc40db3dcc398cdf609c04d5546f6b).
+ Lock temp dirs while still in use. [Commit](http://commits.kde.org/spectacle/963c46cbe0b893c41b55274152b79b31874e8ab6).
+ ConfigUtils: rename continueUpdate to isFileOlderThanDateTime. [Commit](http://commits.kde.org/spectacle/30bc3655702fe5ae01e9e516c95450e4178ec522).
+ Fixed warning. [Commit](http://commits.kde.org/spectacle/9cfc43ee2bcbf5a84567a05f9503e32e6e72ca05).
+ Removed unused members. [Commit](http://commits.kde.org/spectacle/be924beede7985ea5b52a0ebd7e200be67cfbadc).
+ Traits: make getImage a const reference. [Commit](http://commits.kde.org/spectacle/41af99bc7fdae601647db54e4233b7902b815727).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Fix the private post notice messing with the post width in odd ways. [Commit](http://commits.kde.org/tokodon/423ef7e3fc2db9f56c439b8d0250aa90ddfd61a5).
+ Disable hidden post menu entries. [Commit](http://commits.kde.org/tokodon/63705f4cdef79b9084c2c06ee08f7d38a51a0eeb).
+ Fix the loading indicator not showing up initially on the timeline. [Commit](http://commits.kde.org/tokodon/4273c629eb42b76fdfae5eef224f696aeba96152). See bug [#488412](https://bugs.kde.org/488412).
+ Let users be able to clear content warning. [Commit](http://commits.kde.org/tokodon/c939d15790ef12a58efa2996ecc9b006f231a876). Fixes bug [#488631](https://bugs.kde.org/488631).
+ Actually propagate the spoiler text to the composer when editing a post. [Commit](http://commits.kde.org/tokodon/0296d5dac9f0c534b648471a7f983e6c122b2cc1). Fixes bug [#488631](https://bugs.kde.org/488631).
+ Prevent notes field on profiles from stopping keyboard navigation. [Commit](http://commits.kde.org/tokodon/54e190082d8a82faa9be751864904952dcf92c1f). Fixes bug [#488236](https://bugs.kde.org/488236).
{{< /details >}}
