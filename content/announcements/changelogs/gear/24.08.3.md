---
aliases:
- ../../fulllog_releases-24.08.3
title: KDE Gear 24.08.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="analitza" title="analitza" link="https://commits.kde.org/analitza" >}}
+ Fix QML issue in ExpressionInput of Kalgebra. [Commit](http://commits.kde.org/analitza/05311760dc3dddeea0ce40596cdceb0a1f96e113).
{{< /details >}}
{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ LibraryPage: Fix text colour. [Commit](http://commits.kde.org/audiotube/b16154f5089ec0937d0ebb6e1537d54ac79d0223).
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Fix build with julia 1.11. [Commit](http://commits.kde.org/cantor/54905b64898228d1f36036ac65c70d46dbc4ab0d).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix missing icon on certain platform. [Commit](http://commits.kde.org/elisa/875b3706cb939acbe91cbceae9cf5a61bb92e626).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Enable app-specific language selection in Android 33. [Commit](http://commits.kde.org/itinerary/19c01572a44a606fd431d335cd07b5a94a6f27e1).
+ Skip Appium tests on Qt 6.8.0 on the CI that need pending Qt fixes. [Commit](http://commits.kde.org/itinerary/11d6faa4ed6c19adc445ab9b490ce90c99bcf180).
+ Fix Android build with Qt 6.8. [Commit](http://commits.kde.org/itinerary/38ea62e9e6ada8e1b4477e88bc4dc509a8a66865).
+ Fix checks for journey disruption effects. [Commit](http://commits.kde.org/itinerary/c6813b487f63f2b2ff8c22a4891c0fbdd0153457).
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix conditional ripped file pattern parsing. [Commit](http://commits.kde.org/k3b/911905376fc90911343105bc54e902f1d43536e7). Fixes bug [#494653](https://bugs.kde.org/494653).
+ Remove musicbrainz code. [Commit](http://commits.kde.org/k3b/b48ad5640624b56b4766fc63360e2273d92580a8). Fixes bug [#494670](https://bugs.kde.org/494670).
{{< /details >}}
{{< details id="kaccounts-integration" title="kaccounts-integration" link="https://commits.kde.org/kaccounts-integration" >}}
+ Categorize logging. [Commit](http://commits.kde.org/kaccounts-integration/d8aca0305df3a05f8402e3a8b1d6ce877b5a169c).
+ Abort gracefully when file to remove doesn't exist. [Commit](http://commits.kde.org/kaccounts-integration/e270a4a1282b09817854bddaa5f0dc27220fd3d7). Fixes bug [#495344](https://bugs.kde.org/495344).
+ Avoid dangling reference in removeNetAttach. [Commit](http://commits.kde.org/kaccounts-integration/0db671fa8e0503e7477a662aa9e85ef173575aef).
{{< /details >}}
{{< details id="kalzium" title="kalzium" link="https://commits.kde.org/kalzium" >}}
+ Fix build with Qt 6.8. [Commit](http://commits.kde.org/kalzium/5c6c068cf126b0232af1d8ba8ec70450a407d15f).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix sql copy/export is randomly ordered. [Commit](http://commits.kde.org/kate/5adfcc095fa54f0269ac2c2aacd676569858e3db). Fixes bug [#461419](https://bugs.kde.org/461419).
+ Formatting: Listen to all process signals properly. [Commit](http://commits.kde.org/kate/6316fc10f42e30d735d1a13f56adb25e8326259f).
+ KateViewSpace: Store session group name on save. [Commit](http://commits.kde.org/kate/e4c098c102d58496999b0e27566dbd6adc313a0f).
+ Fix QString.arg calls. [Commit](http://commits.kde.org/kate/e985f4d0f28bcba7001f51988702d3d7ff0469ac).
+ Add libffi8 needed by opensuse. [Commit](http://commits.kde.org/kate/6d9930139160e784e53ca1e4f76e1ec30db96904).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix crash caused by incorrect codec passed on opening subtitle. [Commit](http://commits.kde.org/kdenlive/6326febc90eda509ec18a158d6f0bf2d6c079cfb). Fixes bug [#495410](https://bugs.kde.org/495410).
+ Fix shadowed variable causing incorrect clip removal on project opening, fix crash opening project with timeline clip missing in bin. [Commit](http://commits.kde.org/kdenlive/56dcc2ff74ebbfd66795eaae59ee29e6665382bf). See bug [#493486](https://bugs.kde.org/493486).
+ Fix qml crash building timeline with Qt 6.8 - ensure context property exists before setting source. [Commit](http://commits.kde.org/kdenlive/81453e228724c43a9f786eb6811f8b95d7caa710). See bug [#495335](https://bugs.kde.org/495335).
+ Fix generate proxy when frame size is above a value not using the current project setting. [Commit](http://commits.kde.org/kdenlive/43b72af499f01a51410215c4a22f17315c14bafc).
+ Fix shadow variable causing clip removal on project opening. [Commit](http://commits.kde.org/kdenlive/9f2918f43ecf8317f51f5603e665858e09f1502e).
+ Fix monitor seek to prev/next keyframe not working in rotoscoping. [Commit](http://commits.kde.org/kdenlive/98046572d483036c3b2640a36c874d0f74118b45).
+ Fix missing build-in LUT files not correctly fixed on project open. [Commit](http://commits.kde.org/kdenlive/b18d5b71822f870454243df17d6edec146d84ee1). See bug [#494726](https://bugs.kde.org/494726).
+ Fix clip jobs like stabilize creating invalid folders. [Commit](http://commits.kde.org/kdenlive/71c1dd31812a243738f6cc0d7e1b93b5e3ec7b93).
+ Fix freeze loading project with invalid folder id. [Commit](http://commits.kde.org/kdenlive/fb1b324b4bcf8ed55b525a8329b3efebab5f30c4).
+ Don't invalidate timeline preview when replacing an audio clip in bin. [Commit](http://commits.kde.org/kdenlive/612e4b102a6dc4215712db8e95be0119b86630f8).
+ Ensure monitor is cleared and ruler hidden when no clip or a folder is selected in bin. [Commit](http://commits.kde.org/kdenlive/c552539a92f5370fb33ebd5944d57dca7c3fa1ac).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ EWS: Check that mEventReq is not null in EwsSubscriptionManager. [Commit](http://commits.kde.org/kdepim-runtime/21d14f3e349a01c574c8d1c288d6070a7e30dc6a). Fixes bug [#495155](https://bugs.kde.org/495155).
+ It generates warning. [Commit](http://commits.kde.org/kdepim-runtime/ed82c945e6a3bd440bdd769ced74530f350f8e01).
{{< /details >}}
{{< details id="kdevelop" title="kdevelop" link="https://commits.kde.org/kdevelop" >}}
+ MesonManager: remove test suites when a project is closing. [Commit](http://commits.kde.org/kdevelop/d9a2e3f2d355e1f8299c849032610fc78c82d132). Fixes bug [#427157](https://bugs.kde.org/427157).
+ VcsEventLogModel: disconnect from log jobs when destroyed. [Commit](http://commits.kde.org/kdevelop/47b436caf2382998c31d713276867b2de3289599).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Thumbnail: image plugin add webp support. [Commit](http://commits.kde.org/kio-extras/d04e1d070ee92a6c3cac796dbfdef80c70ffff85).
+ Audiothumbnail and imagethumbnail: Mention supported mimetypes explicitly. [Commit](http://commits.kde.org/kio-extras/d5ad888f6980e7e6647744c927f9d94fef6966af).
{{< /details >}}
{{< details id="kio-gdrive" title="kio-gdrive" link="https://commits.kde.org/kio-gdrive" >}}
+ CI: Build libkgapi from the same branch. [Commit](http://commits.kde.org/kio-gdrive/0942be8668df2f72b1b2969a0312c4a029d2b75a).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add extractor for Thai state railway tickets. [Commit](http://commits.kde.org/kitinerary/dd18c194eb8f3b383e24899eb5982b00d5923a0b).
+ Handle date format of NS RCT2 ticket barcodes. [Commit](http://commits.kde.org/kitinerary/fc54579e12ac49c08ea6fb34f3de7ad455c5e5ed).
+ Add planway.com extractor script. [Commit](http://commits.kde.org/kitinerary/97a7ab0f5ea86af5eabf261205672ab76e2e0ce4).
+ Add VietJet Air extractor script. [Commit](http://commits.kde.org/kitinerary/572402392aeb077efda32b9cf0e5015217072514).
+ Add Spanish language plain text extraction patterns for booking.com. [Commit](http://commits.kde.org/kitinerary/6e6b1a7f01cf14bd9db6b564c7f75e4fe2cdefed).
+ Sync OSM data types from KOSMIndoorMap. [Commit](http://commits.kde.org/kitinerary/af6a020c02add34e45675fc517555003f7550b59).
+ Make Renfe barcode trigger pattern more strict. [Commit](http://commits.kde.org/kitinerary/b39345698f7b091528548c3e1c8e4712d9e379c5). Fixes bug [#494613](https://bugs.kde.org/494613).
+ Extract passenger names from Renfe tickets when present. [Commit](http://commits.kde.org/kitinerary/62a53fd6f627f2b96b07d5817c33dadc5177b62c). See bug [#494613](https://bugs.kde.org/494613).
+ Make Renfe extractor script more robust. [Commit](http://commits.kde.org/kitinerary/bd5e22bee797b2102841e17939d78f951eb0dd63). Fixes bug [#494617](https://bugs.kde.org/494617).
+ Update Agoda extractor to handle confirmation mails in Spanish. [Commit](http://commits.kde.org/kitinerary/d74b78d9111a1865dc695bff0b37292685ff2182).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Fix crash on non-kwin wayland compositors. [Commit](http://commits.kde.org/kleopatra/96eccd7c47069fa629a09fe5838b30bf11fceb45). Fixes bug [#488090](https://bugs.kde.org/488090).
+ Do not create RevokersWidget when gpgme is too old. [Commit](http://commits.kde.org/kleopatra/7502abdb0ff5c7190a41de3be4decf90be614fc1). Fixes bug [#492829](https://bugs.kde.org/492829).
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix OSC 4 past colorTable and apply the check to OSC 104. [Commit](http://commits.kde.org/konsole/f57933fd7bbdcb01e457f070a3182594bb3a6b3f).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Add missing org.kde.contacts dependency. [Commit](http://commits.kde.org/kosmindoormap/565e58753ac338d9cf7dab71ce3f30c4c4d5b29c).
+ Remove too aggressive operator< overload. [Commit](http://commits.kde.org/kosmindoormap/d7cba90cb7f06170d5e265c219aeb6dbacad14c7).
+ Add namespace for Android as required by newer gradle versions. [Commit](http://commits.kde.org/kosmindoormap/1b7ed63bc3f0f673a1906ae38229aaccf133ab2c).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Fix arrival delay parsing in NS onboard API. [Commit](http://commits.kde.org/kpublictransport/c3d3f4cfe514fa04774d28941345e727eded9313).
+ Decode JSON numbers provided as strings as well. [Commit](http://commits.kde.org/kpublictransport/3eff4f2c3f4b6de387266d4e7a1cb2024c800df5).
+ Don't encode timezone information separately in UTC. [Commit](http://commits.kde.org/kpublictransport/d009a0b823460b79a2e3f55726031c1ce68e6be4).
+ Add onboard backend for nstrein. [Commit](http://commits.kde.org/kpublictransport/d081918ee4dfc1cc0a7e290dea2e0272832cecd4).
+ Remove DE and ES coverage from Navitia. [Commit](http://commits.kde.org/kpublictransport/587e40f28abc4e5c144e46862f81ffc2e7f53aaa). Fixes bug [#494608](https://bugs.kde.org/494608).
+ Add namespace for Android as required by newer gradle versions. [Commit](http://commits.kde.org/kpublictransport/45e1188aeace1b524c9f913fc25ab3a49bcb42bb).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Avoid use of out of scope temporary QByteArray. [Commit](http://commits.kde.org/krdc/0e7ef596d7262ece88d7b2d1162fb9a73492877c).
+ Fix folder share; BUG: 482137. [Commit](http://commits.kde.org/krdc/977ba5799f467dcd3817e064e09a01ec5fcbf108).
{{< /details >}}
{{< details id="ktorrent" title="ktorrent" link="https://commits.kde.org/ktorrent" >}}
+ Accept HTTPS webseeds too. [Commit](http://commits.kde.org/ktorrent/cdb33edc17acea7663660292ef4d857c29573a41). Fixes bug [#492399](https://bugs.kde.org/492399).
+ Fix tooltips having white on white text. [Commit](http://commits.kde.org/ktorrent/2538633291220dddf30d0136a0bfc10cc4cf964f).
{{< /details >}}
{{< details id="libkleo" title="libkleo" link="https://commits.kde.org/libkleo" >}}
+ Watch the keyboxd database file for changes. [Commit](http://commits.kde.org/libkleo/bfbfc23e81c2069c5799fae69c27662e50de1a49).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ PO Summit wraps within <...>. [Commit](http://commits.kde.org/lokalize/06f74d9c1717e79c66d6a907e382c19df24ad248).
+ Add craft windows CD. [Commit](http://commits.kde.org/lokalize/cf31280173861f3ccc60d8b61684b614b3a0292a).
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Add the capability to query the centerLongitude/Latitude inside the MarbleQuickItem. [Commit](http://commits.kde.org/marble/d65cf1d68dd0153430e81e7e025b819938af9d51).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Fix adding new incidence in day grid view. [Commit](http://commits.kde.org/merkuro/7c3d41fd117b8cf74fe9dab57623cf37ba040434).
+ Fix use of DayMouseArea (now DayTapHandler) in hourly view-based views. [Commit](http://commits.kde.org/merkuro/4f2398e3e8b3453575214c381919350343fd8a55).
+ Center alignment of text on incidence-less month list day entries. [Commit](http://commits.kde.org/merkuro/719864c9371e22179757db9c73a5b6fde3e360e2).
+ Match top and bottom margins for month list incidence items. [Commit](http://commits.kde.org/merkuro/1bf24a6e4fa0609e9729ef0f7b32b32fa82ce28f).
+ Rename DayMouseArea to DayTapHandler. [Commit](http://commits.kde.org/merkuro/f64209dd1666555f43d9eaf6c539338e84506d61).
+ Fix double opening of incidence add window when double-clicking day grid view square. [Commit](http://commits.kde.org/merkuro/aa7efe50bda900cdc923f2820f7fdad202a1f8cc).
+ Replace DayMouseArea as key delegate in BasicMonthListView with background rectangle. [Commit](http://commits.kde.org/merkuro/de35b95dd63752c48f890a4342f091234d2987da).
+ Fix uses of DayMouseArea in BasicMonthListView. [Commit](http://commits.kde.org/merkuro/6817678694b006135f6ef112f36f436f2eaa42b6).
+ Convert DayMouseArea into a TapHandler. [Commit](http://commits.kde.org/merkuro/e9524ab898621b64f1632f2a431c5777b573668d).
+ Fix tag and name filtering incidences in calendar views. [Commit](http://commits.kde.org/merkuro/a11007df00dceb0545ccacbc680c76d11b7a2f40).
+ Fix references to non-existent openNewSubTodoEditorDialog. [Commit](http://commits.kde.org/merkuro/2921e14f5419950448c224d173f4ad404e470571).
+ Once collection combo box model starts receiving collections, set a valid default collection by default. [Commit](http://commits.kde.org/merkuro/cab7ecaad650f7149a42c0a255202d4cbb2d2e56).
+ Re-enable geolocation components in the incidence editor page. [Commit](http://commits.kde.org/merkuro/5139392d111721df01aecb3d30b6782137475ec0).
+ Reenable use of map in incidence info contents. [Commit](http://commits.kde.org/merkuro/934927ca6f0cf8a059a11f619046a504bfdbd5ca).
+ Fix import of Calendar in LocationMap. [Commit](http://commits.kde.org/merkuro/899ab90486436ae05e329664bf8ba7783e0295fb).
+ Adapt LocationMap to Qt6. [Commit](http://commits.kde.org/merkuro/032290cb59954fd9a57c54ef8e24f4bb965e4961).
+ Fix broken Calendar references in task completion slider onMoved handler. [Commit](http://commits.kde.org/merkuro/43764e439d2e94195353116784b77f43a74daf07).
+ Fix incidence info popup positioning. [Commit](http://commits.kde.org/merkuro/dc6952415b9e83f0447c55e2feb3330e8fb33cb7).
+ Add missing window parameter to onCreateNew... handlers in calendar main. [Commit](http://commits.kde.org/merkuro/6dd121b520dc871a382bed03c8c223a423413d03).
+ Fix error in incidenceinfodrawer in main caused by null currentItem in pageStack. [Commit](http://commits.kde.org/merkuro/9e9b169f3cd97922d6216bb383767ff32e25950b).
+ Fix workaround for incidence info drawer getting cut off when menu bar loaded. [Commit](http://commits.kde.org/merkuro/636131129591d49cde168a2d3f1bab4ac8dcb036).
+ Fix alignment of incidence info drawer header text. [Commit](http://commits.kde.org/merkuro/194b018daf7f35fd20e450e7b499b6a1e2ab09dd).
+ Fix references to removed CalendarUiUtils.setUpAdd function. [Commit](http://commits.kde.org/merkuro/02cbba73d098dac4e4c1ec27f47813fcc76fb9a0).
+ Rename InfiniteCalendarViewModel to InfiniteMerkuroCalendarViewModel. [Commit](http://commits.kde.org/merkuro/de4f9911d995403e4f850a7bdb3dc9029824fe82).
+ Fix showing/hiding of hamburger menu in Calendar main drawer. [Commit](http://commits.kde.org/merkuro/0c4968ef56df13ede11231b2a82b178bbac1038d).
+ Fix contact config. [Commit](http://commits.kde.org/merkuro/34e76e278cf3ef41bce87b9354b5d4c45aab859a).
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix(messagelist): refresh 'Today' and 'Yesterday' reference point every day. [Commit](http://commits.kde.org/messagelib/16616bedb2c6c02eeb17d831f9abac0e5c8323c4).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Cherry pick compatability changes for libquotient 0.9. [Commit](http://commits.kde.org/neochat/eb802ff91f98204784d87db61a698b4038b7ef84).
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Fixed FolderDialog property to properly save user default. [Commit](http://commits.kde.org/skanpage/8b707915efead72f1d5401945c3df12175569841).
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Work around bugged qmlcachegen. [Commit](http://commits.kde.org/spectacle/4912338e39d0c835e7a4f2d129c20b9ca37c0f5f). Fixes bug [#494281](https://bugs.kde.org/494281).
+ Fix negative animation duration without capture on click support. [Commit](http://commits.kde.org/spectacle/c9c97ed835216b991c664655230886cb92561a37). Fixes bug [#495216](https://bugs.kde.org/495216).
+ AnnotationViewport: fix stretching odd sized images. [Commit](http://commits.kde.org/spectacle/a941ac4ff41b940d7251dbd90d8532d3db696cb0).
+ Revert "Make screenshots with scales multiplied or divided by integers look sharper in AnnotationViewport". [Commit](http://commits.kde.org/spectacle/55b25661c90d04f5d8e235bf1e508f2e835ee413).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Fix the "Add Account" button under Settings not working. [Commit](http://commits.kde.org/tokodon/b469baeabb67cf7e05dd0add6b77df527f00d9ff). Fixes bug [#493485](https://bugs.kde.org/493485).
+ Fix the notification page breaking when someone posts a status. [Commit](http://commits.kde.org/tokodon/71a37574c2e5350020007a4afc09a4b375652551). Fixes bug [#490556](https://bugs.kde.org/490556).
+ Fix the "add list" and "edit list" pages not working. [Commit](http://commits.kde.org/tokodon/034c8e77e8513ea0373dd50449bc821e9cf34915).
+ Handle deletion inside of actionRedraft itself, fix lots of signals. [Commit](http://commits.kde.org/tokodon/5e361cad5bca2ba7b92e86c4ec3b34ee4fbeb18d).
+ Fix undefined error in TimelinePage.title. [Commit](http://commits.kde.org/tokodon/7acc34e3e8b8a3e2821366039aa0cf73c1dc805a).
+ Fix AttachmentMenu runtime warnings. [Commit](http://commits.kde.org/tokodon/cf32f240485f9b5ac44eaf7c97c6b690307abd57).
+ Stop pins from duplicating themselves. [Commit](http://commits.kde.org/tokodon/fb20d5cca5e183bb0c0b387d84f0c3d43132b945). Fixes bug [#494871](https://bugs.kde.org/494871).
+ Add test for SocialGraphModel, fix a bug. [Commit](http://commits.kde.org/tokodon/02750cb5f80629f164fc5e861390ed3b0f31dd84).
{{< /details >}}
{{< details id="zanshin" title="zanshin" link="https://commits.kde.org/zanshin" >}}
+ Fix concurrency queries, similar to previous commit. [Commit](http://commits.kde.org/zanshin/da2f2d800c6166639d0aeb0f80913bed07d1b4c5).
+ Fix doubled projects after unchecking and checking again a data source. [Commit](http://commits.kde.org/zanshin/50a3e34f36423f52ba018c1b21e6295a17b819ee).
+ Akonadilivequeryhelperstest: factorize duplicated code. [Commit](http://commits.kde.org/zanshin/fae6832328c96458095b442532b2d3230e2c0c13).
{{< /details >}}
