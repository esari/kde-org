---
aliases:
- ../../fulllog_releases-23.08.2
title: KDE Gear 23.08.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-search" title="akonadi-search" link="https://commits.kde.org/akonadi-search" >}}
+ Move convertion from html to text out-of-process. [Commit](http://commits.kde.org/akonadi-search/8421ace60b1befcc751b977fb06cbe179a93bc95). Fixes bug [#474747](https://bugs.kde.org/474747).
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Drop unused ItemModels dependency. [Commit](http://commits.kde.org/ark/149271d3e341eea531405191ff1002fda86fdf84).
+ Add missing KWindowSystem dependency. [Commit](http://commits.kde.org/ark/d5d38e688c455fc3a5277ea225ccb18533f28bbe).
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ [maxima] clear the variables created in TestMaxima::testVariableModel() so we don't pollute other tests executed after it. [Commit](http://commits.kde.org/cantor/9bc32f49f9872415f6fe9004dbced7899ddaa8a3).
{{< /details >}}
{{< details id="ffmpegthumbs" title="ffmpegthumbs" link="https://commits.kde.org/ffmpegthumbs" >}}
+ Drop unused KI18n header/dependency. [Commit](http://commits.kde.org/ffmpegthumbs/741f5285ee7dedd86d5e6142d4f6a411c65667ad).
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Revert "Fix ignore path support. Fix crash when we load root path". [Commit](http://commits.kde.org/filelight/ca940302e142dc552c25c43b2494f7803e9877c7).
+ Tests: cripple freebsd testing. [Commit](http://commits.kde.org/filelight/a51c23f4e9dcd4fb076b946f2d886eaf20186d6e).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add more sanity checks for transfers. [Commit](http://commits.kde.org/itinerary/c1da7b696c487b73a92c63249f0b3799051203b0).
+ Fix several word wrap issues for health certificate details. [Commit](http://commits.kde.org/itinerary/99aab23b8653f1f6528544f83d3f6f9ace14c6ce).
+ Ensure DB ticket reference is upper-cased. [Commit](http://commits.kde.org/itinerary/ad3f7cede9456065cce60e99a9604ed432778cab).
+ Disable Poppler Qt bindings. [Commit](http://commits.kde.org/itinerary/1bca8f2d0cef8c3e58adc64f8267e749e416b710).
+ Use stable branch for Qt5 PIM libraries. [Commit](http://commits.kde.org/itinerary/1797b5cb103d92481933deeb4708cb70c14525b0).
+ Require POST_NOTIFICATION permission for Android SDK 33. [Commit](http://commits.kde.org/itinerary/843d76319f17dd6c315aa2f3efbb428c23f16908). See bug [#474643](https://bugs.kde.org/474643).
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Fix ejecting when there's a disk on the drive. [Commit](http://commits.kde.org/k3b/c8c74d5c307ef41a6a4f9ed882e7702fdc8ea140). Fixes bug [#473889](https://bugs.kde.org/473889).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix Edit Alarm dialog More/Less Options button being wrongly labelled "Defaults". [Commit](http://commits.kde.org/kalarm/fcc6d69f1cb7511fae5b5bb32c300dde1e95d5d4).
+ Fix 'show today' button not showing in date picker when using Breeze. [Commit](http://commits.kde.org/kalarm/323623343be964392757ecadfd4c2917bd15ee17).
+ Replace deprecated methods. [Commit](http://commits.kde.org/kalarm/d00c9f9b576254e5276558c7fc94ffe29bec1698).
+ When alarm is deferred, ensure it's deleted from archive calendar. [Commit](http://commits.kde.org/kalarm/96dd0a97b66854e955c22de308feab8769a917d3).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Ensure we don't crash if no item is selected. [Commit](http://commits.kde.org/kate/56302841c969170e102742849c69bdef7d831ce4). Fixes bug [#475379](https://bugs.kde.org/475379).
+ Fix prettier.resolveConfig.sync is not a function. [Commit](http://commits.kde.org/kate/1ac4542290a2205e93069495f0a7b6e4ae2ed453). Fixes bug [#474722](https://bugs.kde.org/474722).
+ Explicitly call QCoreApplication::exit(). [Commit](http://commits.kde.org/kate/a03c5190fb1b2202d684674b7ed32f6a05388042). See bug [#469690](https://bugs.kde.org/469690).
{{< /details >}}
{{< details id="kblackbox" title="kblackbox" link="https://commits.kde.org/kblackbox" >}}
+ About data: pass homepage url by homepage argument, not copyright one. [Commit](http://commits.kde.org/kblackbox/b844019e21e22a75219ffa9ffe03b3658e729ff4).
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Iterate KFilePlacesModel instead of using closestItem to remove SFTP entries. [Commit](http://commits.kde.org/kdeconnect-kde/de45a8017cff7df63ec0c967d0bfd8774afbddf8). Fixes bug [#461872](https://bugs.kde.org/461872).
+ Remove "Open on connected device via KDE Connect". [Commit](http://commits.kde.org/kdeconnect-kde/8b78ba6a583fd83d44b8b6912283c826b0314daa). Fixes bug [#472697](https://bugs.kde.org/472697).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix Fit Zoom. [Commit](http://commits.kde.org/kdenlive/65f7bf7f9a106c43c5792e4153b281138ca87fd6). Fixes bug [#472754](https://bugs.kde.org/472754).
+ Fix cannot move clip to 0 in some cases. [Commit](http://commits.kde.org/kdenlive/28efa5c1dcec0f1499f304b3ffa33f4be2ee6537).
+ Fix erratic behavior when requesting to add same track transitions (mixes) to several clips. [Commit](http://commits.kde.org/kdenlive/4ea6b3bfe1f49e69b172d6fbf813d4fec5532244).
+ Redirect Settings > Configure Kdenlive > Help to the online documentation. [Commit](http://commits.kde.org/kdenlive/e96bf9e01b45c15350d74f64e0005f9a852e95c0).
+ Fix multiple audio streams broken by MLT's new astream property. [Commit](http://commits.kde.org/kdenlive/e4b545e87a48fa35c56b471e82dfe5124ecbe875). Fixes bug [#474895](https://bugs.kde.org/474895).
+ Fix dialogs not correctly deleted, e.g. add track dialog, causing crash on exit. [Commit](http://commits.kde.org/kdenlive/edf3195e825d27422de10b92fc5315e43dddbe1f).
+ Ensure clips with audio (for exemple playlists) don't block audio when inserted on video track. [Commit](http://commits.kde.org/kdenlive/38fe02f47b38d701d6acd9e0dd8a25268a13aa35).
+ Ensure translations cannot mess with file extensions. [Commit](http://commits.kde.org/kdenlive/a23a7fb1b419c08aad7e72d2561439a420587c98).
+ Fix another case blocking separate track move. [Commit](http://commits.kde.org/kdenlive/281f2071d285f470bb88cc63d8813faf0c1373b9).
+ Fix grabbed clips cannot be moved on upper track in some cases. [Commit](http://commits.kde.org/kdenlive/c13810cf42549c405f72d116bfe0ab2bb3d7b66b).
+ Fix move clip part of a group on another track not always working. [Commit](http://commits.kde.org/kdenlive/0085018cb95703b9286fdecc48fc9a78bd56d3f7).
+ Fix playlist count not correctly updated, allowing to delete last sequence. [Commit](http://commits.kde.org/kdenlive/c2d738f9e7cc85f049febbe65351edc3ea8832ad). Fixes bug [#474988](https://bugs.kde.org/474988).
+ Fix motion-tracker Nano file name and links to the documentation. [Commit](http://commits.kde.org/kdenlive/ad5d2706c55810721685473154f9d664dbba26fd).
+ Stop installing kdenliveui.rc also as separate file, next to Qt resource. [Commit](http://commits.kde.org/kdenlive/7c20d1a2e5c0dc5cfeca4c4274a0d2e2afb33bcb).
+ Fix tests and possible corruption in recent mix fix. [Commit](http://commits.kde.org/kdenlive/92fcb745fcaa39696c9385c74f66553b1a3a4261).
+ Correctly highlight newly dropped files in library. [Commit](http://commits.kde.org/kdenlive/ba9ceb859a1cf82d86b737cd001048b6b9c7b6c1).
+ Fix threading issue crashing in resource widget. [Commit](http://commits.kde.org/kdenlive/68a3e0aaad7124554cd27f32af96abeaee94cf54).
+ Fix freeze on adding mix. [Commit](http://commits.kde.org/kdenlive/96cd6edc81f631743e9527ca853776ab174af683).
+ Make Lift work as expected by most users. [Commit](http://commits.kde.org/kdenlive/b096aed4e93ca44cdd2d8c3f1072baf881780048). Fixes bug [#447948](https://bugs.kde.org/447948). Fixes bug [#436762](https://bugs.kde.org/436762).
+ Fix load task discarding kdenlive settings (caused timeline clips to miss the "proxy" icon. [Commit](http://commits.kde.org/kdenlive/b152e4d93f65beda8abf8c05cba38eae656a52cf).
+ Fix multiple issues with Lift/Gamma/Gain undo. [Commit](http://commits.kde.org/kdenlive/79dae6dcd0c81d94956aa58b17fde752cc121eb3). Fixes bug [#472865](https://bugs.kde.org/472865). Fixes bug [#462406](https://bugs.kde.org/462406).
+ Fix freeze / crash on project opening. [Commit](http://commits.kde.org/kdenlive/2c5d4212a9a5323a630ededa05a2daeb84b365b8).
+ Optimize RAM usage by not storing producers on which we did a get_frame operation. [Commit](http://commits.kde.org/kdenlive/e29b16d73869ff1ce03ff5153c94f386bb2f282c).
+ Fix guide multi-export adding an extra dot to the filename. [Commit](http://commits.kde.org/kdenlive/4732230d57c08001bedd51238ef3b4430e3e11aa).
+ COrrectly update effect stack when switching timeline tab. [Commit](http://commits.kde.org/kdenlive/d4154e08a3e0eb4b0392ef2198e54890eee34ddf).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Fix reading DAV provider properties. [Commit](http://commits.kde.org/kdepim-runtime/ff5bb3af626d3de993c3f89c8ffbdff3c9c8c72c).
+ Fix kpimtextedit dependency. [Commit](http://commits.kde.org/kdepim-runtime/cfa3cd6e6b1be4b04b498c4b4d588ebdcfbaa457).
+ Actually start job to read secret key. [Commit](http://commits.kde.org/kdepim-runtime/c99afc75a503fff6ca83df2f47a5d5d951579524). Fixes bug [#470820](https://bugs.kde.org/470820).
+ Revert "Revert "Fix race condition when building"". [Commit](http://commits.kde.org/kdepim-runtime/2fd3604ad6a9594b9dd8b0f1529f4f4589fe831f).
+ Revert "Revert "Use config plugin instead of out of process config dialog"". [Commit](http://commits.kde.org/kdepim-runtime/fb0d78fb3bcfce5bd4ff7f51e4cd1405693e0275).
+ Fix signal/slot. [Commit](http://commits.kde.org/kdepim-runtime/c56e58c8734256db3d7f1754e574af7ba14c1111).
{{< /details >}}
{{< details id="kdf" title="kdf" link="https://commits.kde.org/kdf" >}}
+ Add missing JobWidgets dependency. [Commit](http://commits.kde.org/kdf/d4379757723826f5bfcd9ab60918b089303a1281).
{{< /details >}}
{{< details id="kfourinline" title="kfourinline" link="https://commits.kde.org/kfourinline" >}}
+ Fix memory leakage of button signal handler objects. [Commit](http://commits.kde.org/kfourinline/5bb75fa1ab674d31594020ff1707b9c8171207b3).
+ Fix memory leakage of intro animation data. [Commit](http://commits.kde.org/kfourinline/18609d37478acca3571bb2acf07d36bd0f63c6d4).
{{< /details >}}
{{< details id="kgoldrunner" title="kgoldrunner" link="https://commits.kde.org/kgoldrunner" >}}
+ Fix accidentally localized homepage URL. [Commit](http://commits.kde.org/kgoldrunner/812397249515a4b3a34221bbe0a5e8f74c045a76).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Thumbnail Jpegcreator: skip exif thumbnail when too small. [Commit](http://commits.kde.org/kio-extras/8a0c0d9702b718defb89652762a373dfd8c94979). Fixes bug [#466658](https://bugs.kde.org/466658). See bug [#465336](https://bugs.kde.org/465336).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fix several copy/paste errors in SortUtil::hasStart/EndTime(). [Commit](http://commits.kde.org/kitinerary/56613b0df80985533cf9d67756c0e8f73a3cb424).
+ Add bookingkit PDF extractor script. [Commit](http://commits.kde.org/kitinerary/a1c35b6b50c6cd4d658d7ed61fbcfa3805721340).
+ Handle departure/arrival/duration triples in generic extraction as well. [Commit](http://commits.kde.org/kitinerary/24f2b7f752bdaf3dd185fb3540a9df20eb9b3d12).
+ Make the Amadeus PDF timestamp workaround a bit less specific. [Commit](http://commits.kde.org/kitinerary/96bbd5b5b39bfa6b996870d2cf92339d04c7c75f).
+ Correctly compare times with and without timezones when merging. [Commit](http://commits.kde.org/kitinerary/ce624627f0e6507a8624ced81d2b0d7c75807de1).
+ Fix parsing newer UK railway PDF tickets. [Commit](http://commits.kde.org/kitinerary/8c10d8ce1c77899ae497569ce12bbe6ca5d351aa).
+ Deal with an alternative way to mark non-reservation RSP-6 tickets. [Commit](http://commits.kde.org/kitinerary/5e1341c9bb8c6429a89266488eb796c2cf187e93).
+ Fix parsing GWR iCal attachments for multi-leg train trips. [Commit](http://commits.kde.org/kitinerary/ebf1d4db00beced7b5c84d6b6ea78c143562e9aa).
+ Manually merge international Renfe results. [Commit](http://commits.kde.org/kitinerary/fec3de087f446aa81c059834ab9daef7f46add19).
+ Unify reservation/ticket number extraction for Renfe/Ouigo ES barcodes. [Commit](http://commits.kde.org/kitinerary/0d08f83537bdd0e31ae23f4a45e52e0141e9444a).
+ Move Renfe barcode documentation to the wiki. [Commit](http://commits.kde.org/kitinerary/b2f2f33d1584bd4cddea1c396777c825b1609235).
+ Add a workaround for fixing broken UIC 918.3 payloads in Renfe tickets. [Commit](http://commits.kde.org/kitinerary/134962e5f57f1363c07ad9933ff013d33eb3439b).
+ Sanity-check the RCT2 traveler name. [Commit](http://commits.kde.org/kitinerary/3b83992e1aa38df973489f01baf12803a942bc18).
+ Handle one more data format in RCT2 tickets. [Commit](http://commits.kde.org/kitinerary/43e4f7a85ae70b084d71c3111f9916be511dfedf).
+ Try barcode decoding both on transformed and untranformed source images. [Commit](http://commits.kde.org/kitinerary/2f6a55837e4cb309c20e3a79fdc974eb25521ed8).
+ Remove some excessive debug output. [Commit](http://commits.kde.org/kitinerary/b1b351cb4e820f72bcb5333492724df466920622).
+ Extract un-styled Pretix PDF tickets. [Commit](http://commits.kde.org/kitinerary/65434797a7cf20fa519aea5bc59fc3cdc1701f08).
+ Extract multi-leg Renfe tickets correctly. [Commit](http://commits.kde.org/kitinerary/646513759477d5e604e1b7627ec52103d2c4b645).
+ Make train number matching slightly less strict. [Commit](http://commits.kde.org/kitinerary/7d520a450ed89c4621add30a15531749be5a2949).
+ Merge the two ÖBB UIC 918.3 extractor scripts. [Commit](http://commits.kde.org/kitinerary/528d941f33c8a2c55b222e38f1bf326814139f99).
+ Extract multi-leg ÖBB PDF tickets. [Commit](http://commits.kde.org/kitinerary/96f77331aabc16318df4c121df0cf4a852436bcf).
+ Fix online import of unidirectional SNCF bookings. [Commit](http://commits.kde.org/kitinerary/3dd8d2174e22aa6ae12459b146dabd96b79a4415). Fixes bug [#474197](https://bugs.kde.org/474197).
{{< /details >}}
{{< details id="kjumpingcube" title="kjumpingcube" link="https://commits.kde.org/kjumpingcube" >}}
+ Add missing JobWidgets dependency. [Commit](http://commits.kde.org/kjumpingcube/dbd431226b47f364843ccf04ac9d87827c281933).
{{< /details >}}
{{< details id="kmime" title="kmime" link="https://commits.kde.org/kmime" >}}
+ Allow to parse specific Date. So we reduce the number of. [Commit](http://commits.kde.org/kmime/c888cad75b38d37f8ef1df9f3a66928d5ff6cf0a).
{{< /details >}}
{{< details id="kolf" title="kolf" link="https://commits.kde.org/kolf" >}}
+ About data: pass homepage url by homepage argument, not othertext one. [Commit](http://commits.kde.org/kolf/965d7ab98c3d378539f2cfbd8e9e1c9726605ada).
+ Hide StrokeCircle when AdvancedPutting is disabled. [Commit](http://commits.kde.org/kolf/5ca57769742a6bb4e894688e257da4cd84eccbda).
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Fix compiling with strict iterators. [Commit](http://commits.kde.org/konqueror/9419be516c0864f01363c3ce685c933803a52e98).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Render waterfalls, public book cases and co-working spaces. [Commit](http://commits.kde.org/kosmindoormap/6772e8e0c8e5ed9ad614c3865f4d1cd2d3e6d1ec).
+ Match not found tag keys against the KeyNotSet operator. [Commit](http://commits.kde.org/kosmindoormap/2ae1ede2531e600610b7aea4e5bd84cff025ea80).
+ Improve rendering of some tourist attractions. [Commit](http://commits.kde.org/kosmindoormap/ef4b4e28422853df2c9a1ead692032e795769744).
+ Handle another Wikepedia link tagging variant. [Commit](http://commits.kde.org/kosmindoormap/4d95d882c94acf6216d0c3161148e038739db699).
+ Render showers as well. [Commit](http://commits.kde.org/kosmindoormap/cb4a81d8f1bf8ce29583bf48add2dc3b673f9c93).
{{< /details >}}
{{< details id="kpat" title="kpat" link="https://commits.kde.org/kpat" >}}
+ Add missing QtXml dependency. [Commit](http://commits.kde.org/kpat/c2a4b5a0e39f40f153eb2f2d9e82da5567d550e5).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Test: querytest: Fixup au_navitia locations. [Commit](http://commits.kde.org/kpublictransport/7bcc1dcb6377f8b25eac70aea0278dc281260066).
+ Networks: au_navitia: Use stop_point instead of stop_area. [Commit](http://commits.kde.org/kpublictransport/d3b8533d947368b7aaad3d41277182c67764b558).
+ Networks: au_navitia: Expand to all of Australia. [Commit](http://commits.kde.org/kpublictransport/e28d589d0ba4a6449344f665a6f4b95980740254).
+ Networks: au_navitia: Fixup latitude and longitude. [Commit](http://commits.kde.org/kpublictransport/b7a1b4a4241488ebf204d2911ae1db7e6f8a7653).
+ Fix typo in onboard API header installation. [Commit](http://commits.kde.org/kpublictransport/95f67074f4810e377c388820ad8481898c9b1bc4).
{{< /details >}}
{{< details id="kreversi" title="kreversi" link="https://commits.kde.org/kreversi" >}}
+ Fix missing "icon" image provider for QML popup messages. [Commit](http://commits.kde.org/kreversi/bd31bb93bf38867fbd370aaaa962531ba6aff407).
+ Add missing JobWidgets dependency. [Commit](http://commits.kde.org/kreversi/22ce17c0db5e788f6e095a7231d2ab63a88d8e58).
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Fix configure module name for Qt5 as well. [Commit](http://commits.kde.org/kwalletmanager/5a07e6c38fcbd76f38398606bfdfda59301bf364).
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ Fix memory leakage in destructors of KGameIOPrivate subclasses. [Commit](http://commits.kde.org/libkdegames/1d3ebb9cc30cd91e5a1c90d313a7264c78bb804c).
+ Fix memory leakage of networking messages. [Commit](http://commits.kde.org/libkdegames/f47ad7999b97b2beda36f5cfd9de4549d28f1efb).
{{< /details >}}
{{< details id="libkmahjongg" title="libkmahjongg" link="https://commits.kde.org/libkmahjongg" >}}
+ Drop hardcoded non-zero margin & spacing from selector UIs. [Commit](http://commits.kde.org/libkmahjongg/f2e576b99e53497e0fb714812becb8e0c8388e91).
+ Do not leak tileset objects from tileset selector widget destructor. [Commit](http://commits.kde.org/libkmahjongg/5ef1ce3c0725ab03701bef0f644d144c88af1b0b).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Fix more issue with wrong date due to timezone. [Commit](http://commits.kde.org/merkuro/920675154a8476bed32d02cf86e20dac4ca413ca).
+ Fix shifting of date by one day/month. [Commit](http://commits.kde.org/merkuro/60586e4b7c238e3871c690550a09f4ffa7356192). Fixes bug [#473866](https://bugs.kde.org/473866).
+ Fix displaying of Basic HourlyView. [Commit](http://commits.kde.org/merkuro/a6ff17a58823a563a7294e56302fbf8b1cb46ab7).
+ DateUtils: Trim userProvidedYear to remove whitespace before parsing. [Commit](http://commits.kde.org/merkuro/cf95cbe180c4a4f0e6c3608d51560ed0fd5776bc).
+ Fix invalid parent collection error. [Commit](http://commits.kde.org/merkuro/5e4e2593490423798a9509516dd5ff2772e2fce4).
+ Fix triggering mailto: and tel: actions twice. [Commit](http://commits.kde.org/merkuro/2bcd50c796f09dd44d4c8c55b2b720adbd2e80f9).
+ Fixes bug in photoEditor.qml. [Commit](http://commits.kde.org/merkuro/e91ce3a8709bd819a8e73ae3f104a93b8a64cf9b).
+ Fix crash when running merkuro with other locales. [Commit](http://commits.kde.org/merkuro/734d83d57cd6f4f75506cca716922979aa7ff7a9). Fixes bug [#474191](https://bugs.kde.org/474191).
+ Fix adding new contact. [Commit](http://commits.kde.org/merkuro/9e3a58a3303caf6caffb658c0ab7fedca08f96c5). Fixes bug [#474380](https://bugs.kde.org/474380).
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix crash when switching signed/encrypted emails very fast. [Commit](http://commits.kde.org/messagelib/938fab622b7eb258f89f9b1492a3a264c260be58). Fixes bug [#463083](https://bugs.kde.org/463083).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Markdown: Fix rendering with discount3. [Commit](http://commits.kde.org/okular/043c3521229704558a6a06081a53bd46baac060a). Fixes bug [#475198](https://bugs.kde.org/475198).
{{< /details >}}
{{< details id="palapeli" title="palapeli" link="https://commits.kde.org/palapeli" >}}
+ Fix a crash with hardened_malloc. [Commit](http://commits.kde.org/palapeli/dd5ec1fc965c153abe76415f79ac68c9f6034fa9).
{{< /details >}}
{{< details id="qmlkonsole" title="qmlkonsole" link="https://commits.kde.org/qmlkonsole" >}}
+ Use RELEASE_SERVICE_VERSION instead of hardcoded PROJECT_VERSION. [Commit](http://commits.kde.org/qmlkonsole/cb73057c1642e33a33bf90b30132d5467a00c52f).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Protect against notifications that don't have a post associated. [Commit](http://commits.kde.org/tokodon/7d2df9e3a4c6ae88772694efff3129ea4cba8929). Fixes bug [#475057](https://bugs.kde.org/475057).
+ Require kirigami-addons 0.11. [Commit](http://commits.kde.org/tokodon/9d95716a709f6a872b33edde133466780d5a5eb6).
+ Fix the follows page in notifications not working at all. [Commit](http://commits.kde.org/tokodon/8105035aa1e329df72a4ae26b5a3018ea78f7e35).
+ Use the TextArea's implicitHeight when calculating composer height. [Commit](http://commits.kde.org/tokodon/dc2974f149d8329fa5093a2bccf0cf6c80de4a91). Fixes bug [#471519](https://bugs.kde.org/471519).
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Fix 'ERD lost foreign keys when loaded again'. [Commit](http://commits.kde.org/umbrello/17fed409debff57cf5f25864a141ac74f87d6a0e). Fixes bug [#468728](https://bugs.kde.org/468728).
+ Fix umbrello not starting from KDE applications menu. [Commit](http://commits.kde.org/umbrello/e1c4f985179ffcf398d6bfd0ab1f86525c6ebb3f). Fixes bug [#474587](https://bugs.kde.org/474587).
+ Testumlroledialog: Fix crash. [Commit](http://commits.kde.org/umbrello/69078c8d2ed5400df66d923395091c3e6b4b4b9f). Fixes bug [#474694](https://bugs.kde.org/474694).
+ Fixed display errors in the dialogs for searching objects and exporting images. [Commit](http://commits.kde.org/umbrello/9d63ccbff8991209f00e87c67fca44b408f9b2be). Fixes bug [#474539](https://bugs.kde.org/474539).
{{< /details >}}
