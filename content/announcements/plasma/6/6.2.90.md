---
date: 2025-01-09
changelog: 6.2.5-6.2.90
layout: plasma
video: false
asBugfix: false
draft: false
---

Here are the new modules available in the Plasma 6.3 beta:

### Spectacle
- We have moved our screenshot and screen recording tool to Plasma to better align with the tech it uses
