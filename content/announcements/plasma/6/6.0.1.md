---
date: 2024-03-05
changelog: 6.0.0-6.0.1
layout: plasma
video: false
asBugfix: true
draft: false

aliases:
 - ../6.0.1 # We previously had the announcement at that location
---

+ KWin: Fix confined pointer being able to escape the surface. [Commit.](http://commits.kde.org/kwin/7da6ecb3df5edc940eede219dec7efed764e4c22) Fixes bug [#482448](https://bugs.kde.org/482448). See bug [#477124](https://bugs.kde.org/477124)
+ Fix kicker close on click away. [Commit.](http://commits.kde.org/plasma-desktop/15f704c84a2280a7312680ac21a975e7f77b1012) Fixes bug [#482324](https://bugs.kde.org/482324)
+ Show panels on entering edit mode and "add widgets". [Commit.](http://commits.kde.org/plasma-workspace/8fc5105fec06e80cd2e6d1a3541bf0d8211d9c9f) Fixes bug [#448393](https://bugs.kde.org/448393)
