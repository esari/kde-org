---
date: 2024-12-31
changelog: 6.2.4-6.2.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KScreenLocker: X11locker lower m_background when hiding. [Commit.](http://commits.kde.org/kscreenlocker/2d258e6c72d4c08d5ff01c92ff6e954b34b3ebe6) See bug [#483163](https://bugs.kde.org/483163)
+ Powerdevil Daemon: Don't crash in PowerDevil::Core::unloadAllActiveActions(). [Commit.](http://commits.kde.org/powerdevil/29f9b949c3ad75f11499825c79ba2a39344a26b0) Fixes bug [#492349](https://bugs.kde.org/492349)
+ Discover: UpdatesPage, Fix update description box overlapping with its text. [Commit.](http://commits.kde.org/discover/453cb49cc9509aa9a00c40de04c32fae1f50cdf3) Fixes bug [#491821](https://bugs.kde.org/491821)
