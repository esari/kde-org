---
date: 2025-01-06
changelog: 5.27.11-5.27.12
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Lookandfeel: Explicitly set theme to Breeze in defaults. [Commit.](http://commits.kde.org/plasma-workspace/1e9651d6bea39ce7391fe6fc34a93366b41c6856)
+ Applets/battery: Check actual battery for charge state workaround. [Commit.](http://commits.kde.org/plasma-workspace/f8041a8fe0115a873942d1fa709cc9f0870d0339)
+ Weather/dwd: don't crash on empty json objects. [Commit.](http://commits.kde.org/plasma-workspace/ce1ed1ce55470a68935b192d10ead9f3fd37c360) Fixes bug [#481596](https://bugs.kde.org/481596)

