---
aliases:
- ../../plasma-5.4.3
changelog: 5.4.2-5.4.3
date: 2015-11-10
layout: plasma
figure:
  src: /announcements/plasma/5/5.4.0/plasma-screen-desktop-2-shadow.png
---

{{< i18n_date >}}

{{< i18n "annc-plasma-bugfix-intro" "5" "5.4.3" >}}

{{% i18n "annc-plasma-bugfix-minor-release-8" "5.4" "/announcements/plasma/5/5.4.0" "2015" %}}

{{< i18n "annc-plasma-bugfix-worth-5" >}}

{{< i18n "annc-plasma-bugfix-last" >}}

- Update the KSplash background to the 5.4 wallpaper. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=467d997d8ad534d42b779719cec03a8cbfb66162">Commit.</a>
- Muon fixes PackageKit details display. <a href="http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=f110bb31d0599fda5478d035bdaf5ce325419ca6">Commit.</a>
- Fix crash when exiting kscreen kcm in systemsettings. <a href="http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=4653c287f844f2cb19379ff001ca76d7d9e3a2a1">Commit.</a> Fixes bug <a href="https://bugs.kde.org/344651">#344651</a>. Code review <a href="https://git.reviewboard.kde.org/r/125734">#125734</a>
- <a href="http://blog.martin-graesslin.com/blog/2015/10/looking-at-some-crashers-fixed-this-week/">Several crashes fixed in KWin</a>
