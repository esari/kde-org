---
aliases:
- ../../plasma-5.16.3
changelog: 5.16.2-5.16.3
date: 2019-07-09
layout: plasma
youtube: T-29hJUxoFQ
figure:
  src: /announcements/plasma/5/5.16.0/plasma-5.16.png
  class: text-center mt-4
asBugfix: true
---

- DrKonqi will now automatically log into bugs.kde.org when possible. <a href="https://commits.kde.org/drkonqi/add2ad75e3625c5b0d772a555ecff5aa8bde5c22">Commit.</a> Fixes bug <a href="https://bugs.kde.org/202495">#202495</a>. Phabricator Code review <a href="https://phabricator.kde.org/D22190">D22190</a>
- Fix compilation without libinput. <a href="https://commits.kde.org/plasma-desktop/a812b9b7ea9918633f891dd83998b9a1f47e413c">Commit.</a>
- Keep Klipper notifications out of notification history. <a href="https://commits.kde.org/plasma-workspace/120aed57ced1530d85e4d522cfb3697fbce605fc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408989">#408989</a>. Phabricator Code review <a href="https://phabricator.kde.org/D21963">D21963</a>
