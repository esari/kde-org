---
date: 2022-05-03
changelog: 5.24.4-5.24.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover Flatpak backend: Improve stability of different sources integration. [Commit.](http://commits.kde.org/discover/a8ac52085d9adfbac6885b24e9b5dc83aabf6231)
+ Plasma Audio Volume Control: SpeakerTest: Fix subwoofer test. [Commit.](http://commits.kde.org/plasma-pa/183a26d9a668e1613605e708d613bceb2ffa3396) Fixes bug [#445523](https://bugs.kde.org/445523)
+ xdg-desktop-portal-kde: Fix saving file dialog view options. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e6d796d1461311d587ea08ebba0d2f0a2458b526)
