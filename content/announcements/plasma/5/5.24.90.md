---
#scssFiles:
#  - /scss/plasma-5-25.scss
authors:
  - SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-05-19
changelog: 5.24.5-5.24.90
layout: plasma
title: Plasma 5.25 Beta
draft: false
---

Today we are bringing you the preview version of KDE's Plasma 5.25 desktop release. Plasma 5.25 Beta is aimed at testers, developers, and bug-hunters.

To help KDE developers iron out bugs and solve issues, install Plasma 5.25 Beta and test run the features listed below. Please report bugs to our [bug tracker](https://bugs.kde.org).

We will be holding a Plasma 5.25 beta review day on May 26 (details will be published on our [social media](https://community.kde.org/Promo/People/social_media)) and you can join us for a day of bug-hunting, triaging and solving alongside the Plasma devs!

The final version of Plasma 5.25 will become available for the general public on the 14th of June.

**DISCLAIMER:** This release contains untested and unstable software. It is highly recommended you **do not use this version in a production environment** and do not use it as your daily work environment. You risk crashes and loss of data.

See below the most noteworthy changes that need testing:

## Breeze

* [Support for accent colored titlebars in Breeze Classic](https://invent.kde.org/plasma/breeze/-/merge_requests/182)

## Plasma Workspace

* [Rejection animation on login and lock screens in response to a wrong password](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1708)
* [Dialog to manage containments when in Edit Mode](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1136)
* [Option to apply accent color from wallpaper](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1325)
* [Option to use accent color for window titlebars and/or full header areas](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1304)
* [Option to tint whole color scheme with accent color](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1620)
* [Global Themes page in System Settings lets you pick and choose which parts of a Global Theme to apply](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1043)


{{< img src="AccentTitlebars.png" alt="Accents now change title bars too." img_class="max-width-800" >}}

* [Smoothly cross-fade between old and new states when changing color schemes](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1618)
* [Enable keyboard navigation for Panels and the System Tray](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1143)

## Plasma Desktop

* [Option to control when Touch Mode is enabled](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/933)
* [Optional floating panel for Plasma Themes](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/714)
* [Save positions of folder view icons on a per-resolution basis](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/608)
* [Increase Task Manager icon spacing in Touch Mode](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/880)

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/spacing.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/spacing.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/spacing.png" fig_class="mx-auto max-width-800" >}}

* [Allow non-file "Recent Documents" in Task Manager task context menus](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/551)

## KWin

* [Default touchpad gesture that opens Overview by default](https://invent.kde.org/plasma/kwin/-/merge_requests/2390)
* [Realtime screen edges gestures for scripted effects](https://invent.kde.org/plasma/kwin/-/merge_requests/2260)

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/ScreenEdges.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/ScreenEdges.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/5.25/Beta/ScreenEdges.png" fig_class="mx-auto max-width-800" >}}

* [Shader support in scripted effects](https://invent.kde.org/plasma/kwin/-/merge_requests/2227)
* [Port KWin Scripts KCM to QML](https://invent.kde.org/plasma/kwin/-/merge_requests/2212)
* [Add new blend effect](https://invent.kde.org/plasma/kwin/-/merge_requests/2088)
* [Slide effect improvements](https://invent.kde.org/plasma/kwin/-/merge_requests/1980)
* [More slide effect improvements](https://invent.kde.org/plasma/kwin/-/merge_requests/2162)
* [Support touchpad realtime activation for overview](https://invent.kde.org/plasma/kwin/-/merge_requests/2196)
* [Improvements to Gesture System](https://invent.kde.org/plasma/kwin/-/merge_requests/1973)
* [Support realtime activation for screen edges gestures](https://invent.kde.org/plasma/kwin/-/merge_requests/2101)

## Discover

* [Display permissions for Flatpak applications](https://invent.kde.org/plasma/discover/-/merge_requests/282)
* [Drawer shows all subcategories from the Applications category](https://invent.kde.org/plasma/discover/-/merge_requests/234)
* [Overhaul app page](https://invent.kde.org/plasma/discover/-/merge_requests/246)

{{< img src="DiscoverOverhaul_Mobile_shadow.png" alt="Discover's app pages have been overhauled." >}}

## Plasma Addons

* [Show wallpaper information in the config dialog](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/106)

## Info Center

* [Add some more data to "About This System" page](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/88)
* [Add new "Firmware Security" page](https://invent.kde.org/plasma/kinfocenter/-/merge_requests/86)

## Other

Other things to look out for are glitches both in the X11 and Wayland versions of Plasma, incomplete or incorrect translations, and anything that behaves incorrectly but used to work in prior versions of Plasma. 

Remember to check and see if [your bug has already been reported](https://bugs.kde.org) before reporting a new one.
