---
aliases:
- ../../plasma-5.19.2
changelog: 5.19.1-5.19.2
date: 2020-06-23
layout: plasma
video: true
asBugfix: true
---

+ Fix KRunner positioning on X11 with High DPI and Qt scaling on Plasma. <a href="https://commits.kde.org/plasma-workspace/0f8c6c3a660d9778b04722f6a7319224205e3aa0">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422578">#422578</a>
+ Fix case of monitored service in startplasma's shutdown. <a href="https://commits.kde.org/plasma-workspace/aaed0138ca8feebb9d45b9c4a2dfd5df651ad972">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422870">#422870</a>
+ KSysGuard: Expose better size hints. <a href="https://commits.kde.org/libksysguard/3a133b7067c36e0ec36ea400908c7425ecacfdac">Commit.</a> Fixes bug <a href="https://bugs.kde.org/422669">#422669</a>. Fixes bug <a href="https://bugs.kde.org/422888">#422888</a>
