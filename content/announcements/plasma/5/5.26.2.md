---
date: 2022-10-25
changelog: 5.26.1-5.26.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KScreen: Make error messages translatable. [Commit.](http://commits.kde.org/libkscreen/b5163394764ea8ff4df0abd979c91d47e1363ddf) 
+ Plasma Remotecontrollers: Fix desktop file. [Commit.](http://commits.kde.org/plasma-remotecontrollers/3b1c1b52d81ccc6d49b6b7bd912e191520384059) Fixes bug [#460924](https://bugs.kde.org/460924)
+ Wallpapers/image: disable animated wallpaper on X11 due to memory leak. [Commit.](http://commits.kde.org/plasma-workspace/304633713bee125a5b4b9caec79a6d66e68d1b9a) 
