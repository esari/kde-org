---
aliases:
- ../../plasma-5.13.5
changelog: 5.13.4-5.13.5
date: 2018-09-04
layout: plasma
youtube: C2kR1_n_d-g
figure:
  src: /announcements/plasma/5/5.13.0/plasma-5.13.png
  class: text-center mt-4
asBugfix: true
---

- Picture of the Day: Actually update the image every day. <a href="https://commits.kde.org/kdeplasma-addons/81e89f1ea830f278fdb6f086baa4741c9606892f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397914">#397914</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15124">D15124</a>
- Prevent paste in screen locker. <a href="https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/388049">#388049</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14924">D14924</a>
- Fix QFileDialog not remembering the last visited directory. <a href="https://commits.kde.org/plasma-integration/b269980db1d7201a0619f3f5c6606c96b8e59d7d">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14437">D14437</a>
